#include "../HEADERS/baseInclude.h"
#include "../HEADERS/StructDefinitions.h"

DOUB inlet_velocity(int k, DOUB &vel_inlet, int &pos_inlet, int &width_inlet, DOUB &tf, DOUB &T)
 {
  return -vel_inlet*(-(DOUB(k-pos_inlet)*(k-pos_inlet))/(DOUB(width_inlet*width_inlet))+1.0)*(tanh(0.2*(tf-10.0+T))+1.0)*0.5;
 }

void Toll_set(DOUB &toll, DOUB &Npr, DOUB & Nsor, vector<DOUB> divergence)
 {
  DOUB D(0.0);
  for(int k=0; k<divergence.size();k++)
   {
    D += divergence[k];
   }
  D /= (DOUB(divergence.size()));
  if(Npr>200000){Npr -=20.0; }
  if(Npr>100000){Npr -=10.0; }
  if(abs(D)>=1e-6){if(Npr<8000){Npr +=1.0;} }
  else{if(Npr>20){Npr -=1.0;} }
  //cout << "<Div U> = " << D << "\n";
 }

void In_flow_outflow(parsf &parsFDF, FDFS &FDF, DOUB &tf, DOUB &T, ofstream &fout6, ofstream &fout7, ofstream &fout8, ofstream &fout9)
 {
  DOUB Inflow(0.0), Outflow(0.0), InflowF(0.0), Inflow_bound(0.0), Divergence(0.0), Inflow_tracer(0.0);
  for(int k=parsFDF.pos_inlet-parsFDF.width_inlet;k<=parsFDF.pos_inlet+parsFDF.width_inlet;k++)
   {
    Inflow += parsFDF.dx*FDF.UY[k*(parsFDF.Nfy+1)+parsFDF.Nfy];
    InflowF += parsFDF.dx*inlet_velocity(k, parsFDF.vel_inlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T);
    Inflow_bound += parsFDF.dx*parsFDF.VU[k];
    Inflow_tracer += FDF.intensity[k*(parsFDF.Nfy+2)+parsFDF.Nfy+1];
#ifdef inflow
    fout6 << tf << " " << FDF.gridX[k] << " " << FDF.gridY[FDF.gridY.size()-1] << " " << FDF.UY[k*(parsFDF.Nfy+1)+parsFDF.Nfy] << " "                                                                         << 0.25*(FDF.UX[(k-1)*(parsFDF.Nfy+2)+parsFDF.Nfy]+FDF.UX[(k-1)*(parsFDF.Nfy+2)+parsFDF.Nfy+1]                                                                                                 +FDF.UX[k*(parsFDF.Nfy+2)+parsFDF.Nfy]+FDF.UX[k*(parsFDF.Nfy+2)+parsFDF.Nfy+1])                                                                                                << " " << parsFDF.VU[k] << " " << 0.5*(parsFDF.UU[k-1]+parsFDF.UU[k]) << " "                                                                                                           << inlet_velocity(k, parsFDF.vel_inlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T) << " " << 0.0 << " "                                                                            << FDF.intensity[k*(parsFDF.Nfy+2)+parsFDF.Nfy+1] << "\n";
#endif
   }
  for(int k=0;k<FDF.divergence_coupler.size();k++)
   {
    Divergence += FDF.divergence_coupler[k];
   }
  //Divergence /= (DOUB(FDF.divergence.size()));
  Divergence *= parsFDF.dx*parsFDF.dy;
#ifdef inflow
  fout6 << "\n";
#endif
  for(int k=1; k<2*parsFDF.width_outlet+2;k++)//outlet flow at 80% of outlet length
   {
    for(int l=10; l<20;l++)
     {
      Outflow += parsFDF.dx*FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*parsFDF.length_outlet+l]/10.0;
     }
   }
#ifdef outflow
  fout7 << tf << " " << Inflow << " " << InflowF << " " << Inflow_bound << " " << Inflow_tracer << " " << Outflow << " " << Divergence << " " << parsFDF.toll << "\n";
#endif
#ifdef averageUV_velocity
  DOUB Vave(0.0), Uave(0.0);
  //calculation of averaged y velocity
  for(int k=0;k<parsFDF.Nfx;k++)
   {
    Vave=0.0;
    for(int l=0;l<parsFDF.Nfy;l++)
     {
      Vave += FDF.UY_coupler[k*parsFDF.Nfy+l];
     }
    fout8 << tf << " " << FDF.gridX[k] << " " << Vave/(DOUB(parsFDF.Nfy)) << "\n";
   }
  fout8 << "\n";
  //calculation of averaged x velocity
  for(int l=0;l<parsFDF.Nfy;l++)
   {
    Uave=0.0;
    for(int k=0;k<parsFDF.Nfx;k++)
     {
      Uave += FDF.UX_coupler[k*parsFDF.Nfy+l];
     }
    fout9 << tf << " " << FDF.gridY[l] << " " << Uave/(DOUB(parsFDF.Nfx)) << "\n";
   }
  fout9 << "\n";
#endif
 }

void CFL(vector<DOUB> &UX, vector<DOUB> &UY, DOUB &dtf, DOUB &dx, DOUB &dy, DOUB &D)
 {
  //### FIND MAXIMUM VELOCITY ###//
  DOUB UXm(-1e6), UYm(-1e6), dt1(0.0), dt2(0.0);
  for(int k=0; k<UX.size();k++)
   {
    if(UX[k]>=UXm){UXm=UX[k];}
    if(UY[k]>=UYm){UYm=UY[k];}
   }
  dt1=0.1*0.5*dx*dx/D;
  dt2=0.1*2.0*D/(UXm*UXm+UYm*UYm);
  if(dt1>dt2){dtf=dt2;}
  else{dtf=dt1;}
 }

void NS_boundary_set(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &Pr, parsf &parsFDF, DOUB &tf, DOUB &T)
 {
  //### U velocities ###//
  for(int k=0; k<parsFDF.Nfx+1;k++)
   {
    //### upper boundary ###//
    parsFDF.UU[k] = 0.0;
#ifdef cavity_upper_wall
    parsFDF.UU[k] = parsFDF.vel_inlet;//moving wall
#endif
    //### lower boundary ###//
    parsFDF.UL[k]= 0.0;//no slip condition
#ifdef pressure_outlet
    if(abs(k-parsFDF.pos_outlet)<=parsFDF.width_outlet){parsFDF.UL[k]= (UX[k*(parsFDF.Nfy+2)] + UX[k*(parsFDF.Nfy+2)+1])/2.0;}
    if(k-parsFDF.pos_outlet==parsFDF.width_outlet){parsFDF.UL[k]=0.0;};//using field values to change nothing in the lower layer of the flow (if outlet is present)
#endif
   }
  //### V velocities ###//
  for(int k=0; k<parsFDF.Nfx+2;k++)
   {
    //### upper boundary ###//
    parsFDF.VU[k]= 0.0;
#ifdef pressure_outlet
    if(abs(k-parsFDF.pos_inlet)<=parsFDF.width_inlet)
     {
      parsFDF.VU[k] = inlet_velocity(k, parsFDF.vel_inlet, parsFDF.pos_inlet,parsFDF.width_inlet, tf, T);
      //cout << "inlet flow " << k << "= " << parsFDF.VU[k] << "\n";
     }//Poiseulle flow with time rampoff from t=0
#endif
    //### lower boundary ###//
    parsFDF.VL[k]= 0.0;//no slip condition
#ifdef pressure_outlet
    if(abs(k-parsFDF.pos_outlet)<=parsFDF.width_outlet){parsFDF.VL[k]= UY[k*(parsFDF.Nfy+1)];}//using field values to change nothing in the lower layer of the flow (if outlet is present)
#endif
   }
  //### U velocities ### // 
  for(int l=0; l<parsFDF.Nfy+2;l++)
   {
    //### left boundary ###//
    parsFDF.ULE[l] = 0.0;
    //### right boundary ###//
    parsFDF.UR[l] = 0.0;
   }
  //### V velocities ###//
  for(int l=0; l<parsFDF.Nfy+1;l++)
   {
#ifdef pressure_outlet
    //### left boundary ###//
    parsFDF.VLE[l] = 0.0;
    //### right boundary ###//
    parsFDF.VR[l] = 0.0;
#else
#ifdef cavity_upper_wall
    //### left boundary ###//
    parsFDF.VLE[l] = 0.0;
    //### right boundary ###//
    parsFDF.VR[l] = 0.0;
#else
    //### left boundary ###//
    parsFDF.VLE[l] = parsFDF.vel_inlet;
    //### right boundary ###//
    parsFDF.VR[l] = parsFDF.vel_inlet;
#endif
#endif
   }
 }

void NS_STEP0(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &UL, vector<DOUB> &UU, vector<DOUB> &ULE, vector<DOUB> &UR, vector<DOUB> &VL, vector<DOUB> &VU, vector<DOUB> &VLE, vector<DOUB> &VR, int &Nfx, int &Nfy, int &Nfxo, int &Nfyo, int &Xo)//initial velocity step
 {
  //### Boundary conditions ###//
  //### U velocities ###//
  for(int k=0; k<Nfx+1;k++)
   {
    //lower boundary:
    UX[k*(Nfy+2)] = 2.0*UL[k] - UX[k*(Nfy+2)+1];
    //upper boundary:
    UX[k*(Nfy+2)+Nfy+1] = 2.0*UU[k] - UX[k*(Nfy+2)+Nfy];
   }

  //### V velocities ###//
  for(int k=0; k<Nfx+2;k++)
   {
    //lower boundary:
    UY[k*(Nfy+1)] = VL[k];
    //upper boundary:
    UY[k*(Nfy+1)+Nfy] = VU[k];
   }
  
  //### U velocities ###//
  for(int l=0; l<Nfy+2;l++)
   {
    //left boundary:
    UX[l] = ULE[l];
    //right boundary:
    UX[Nfx*(Nfy+2)+l] = UR[l];
   }
  
  //### V velocities ###//
  for(int l=0; l<Nfy+1;l++)
   {
    //left boundary:
    UY[l] = 2.0*VLE[l] - UY[(Nfy+1)+l];
    //right boundary:
    UY[(Nfx+1)*(Nfy+1)+l] = 2.0*VR[l] - UY[Nfx*(Nfy+1)+l];
   }

#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //left edge
  for(int l=0;l<Nfyo;l++)
   {
    UX[(Nfx+1)*(Nfy+2)+l] = 0.0;
    UY[(Nfx+2)*(Nfy+1)+l] = -UY[(Nfx+2)*(Nfy+1)+Nfyo+l];
   }
  //left edge u velocity in lower boundary at outlet
  UX[(Xo-1-(Nfxo-1)/2)*(Nfy+2)] = 0.0;
  //right edge u velocity in lower boundary at outlet
  UX[(Xo+(Nfxo-1)/2)*(Nfy+2)] = 0.0;
  //right edge
  for(int l=0;l<Nfyo;l++)
   {
    UX[(Nfx+1)*(Nfy+2)+Nfxo*Nfyo+l] = 0.0;
    UY[(Nfx+2)*(Nfy+1)+(Nfxo+1)*Nfyo+l] = -UY[(Nfx+2)*(Nfy+1)+Nfxo*Nfyo+l];
   }
  //bottom edge (outlet)
  for(int k=0;k<Nfxo+1;k++)
   {
    // velocities
    UX[(Nfx+1)*(Nfy+2)+k*Nfyo] = UX[(Nfx+1)*(Nfy+2)+k*Nfyo+1];
    UY[(Nfx+2)*(Nfy+1)+k*Nfyo] = UY[(Nfx+2)*(Nfy+1)+k*Nfyo+1];
   }
#endif
 }

void NS_STEP1(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> &UXN, vector<DOUB> &UYN, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB dtf, DOUB &rho, DOUB &mu, int &Nfxo, int &Nfyo, int &Xo)//initial velocity step
 {
  //+   +   +   +
  //  x   x4  x
  //+   +9  +8  +
  //  x3  x1  x2
  //+   +6  +7  +
  //  x   x5  x
  //+   +   +   +
  //
  //212113134141156721398

  //### Approximation of velocity ###//
  //UX
  //cout << " UX in main domain \n";
  for(int k=1;k<Nfx;k++)//x
  {
   for(int l=1;l<Nfy+1;l++)//y
    {
    UXN[k*(Nfy+2)+l] = UX[k*(Nfy+2)+l] + dtf*(-((UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])*(UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])                                                                                                          -(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])*(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l]))*0.25/dx                                                                                                  -((UX[k*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l])*(UY[(k+1)*(Nfy+1)+l]+UY[k*(Nfy+1)+l])                                                                                                            -(UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])*(UY[(k+1)*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                + mu/rho*((UX[(k+1)*(Nfy+2)+l]-2.0*UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                        + (UX[k*(Nfy+2)+l+1]-2.0*UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])/(dy*dy)));
    }
  }
  //UY
  //cout << " UY in main domain \n";
  for(int k=1;k<Nfx+1;k++)//x
  {
   for(int l=1;l<Nfy;l++)//y
    {
    UYN[k*(Nfy+1)+l] = UY[k*(Nfy+1)+l] + dtf*(-((UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l+1])*(UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l])                                                                                                            -(UX[(k-1)*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l])*(UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l]))*0.25/dx                                                                           		    -((UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])*(UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])                                                                                                              -(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])*(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                      + mu/rho*((UY[(k+1)*(Nfy+1)+l]-2.0*UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l])/(dx*dx)                                                                                                        + (UY[k*(Nfy+1)+l+1]-2.0*UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])/(dy*dy)));
    }
  }

#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //UX
  //cout << "UX in outlet domain \n";
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+l] = UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]                                                                                                                                           + dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])                                               -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]))*0.25/dx                                              -((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                            + mu/rho*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)));
     }
   }
 //coupling to main domain
 //UX in outlet
 //cout << " UX in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo;k++)//x at l=Nfyo-1
  {
   UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] = UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                            + dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])                                  -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                        + mu/rho*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
 
  }
 //UX in main domain
 //cout << " UX in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
  {
   UXN[k*(Nfy+2)] = UX[k*(Nfy+2)] + dtf*(-((UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])*(UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])                                                                                                           -(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])*(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)]))*0.25/dx                                                                                                          -((UX[k*(Nfy+2)+1]+UX[k*(Nfy+2)])*(UY[(k+1)*(Nfy+1)]+UY[k*(Nfy+1)])                                                                                                                    -(UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]                                                             +UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                                                                                                        + mu/rho*((UX[(k+1)*(Nfy+2)]-2.0*UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                              + (UX[k*(Nfy+2)+1]-2.0*UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
 //UY
 //cout << "UY in outlet domain \n";
 for(int k=1;k<Nfxo+1;k++)//x
  {
   for(int l=1;l<Nfyo-1;l++)//y
    {
     UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+l] = UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]                                                                                                                                           + dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l])                                                 -(UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l]))*0.25/dx                                        -((UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                          -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                                  + mu/rho*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/(dy*dy)));
    }
  }
 //coupling to main domain
 //UY in outlet
 //cout << " UY in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
  {
   UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] = UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]                                                                                                                                  + dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1])                                 -(UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])*(UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                              -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                              + mu/rho*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
  }
 //UY in main domain
 //cout << " UY in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
  {
   UYN[k*(Nfy+1)] = UY[k*(Nfy+1)] + dtf*(-((UX[k*(Nfy+2)]+UX[k*(Nfy+2)+1])*(UY[k*(Nfy+1)]+UY[(k+1)*(Nfy+1)])                                                                                                                    -(UX[(k-1)*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)])*(UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)]))*0.25/dx                                                                                                    -((UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])*(UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])                                                                                                                      -(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                      + mu/rho*((UY[(k+1)*(Nfy+1)]-2.0*UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)])/(dx*dx)                                                                                                              + (UY[k*(Nfy+1)+1]-2.0*UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
#endif
 }

void NS_STEP1_HEUN(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> &UXN, vector<DOUB> &UYN, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB dtf, DOUB &rho, DOUB &mu, int &Nfxo, int &Nfyo, int &Xo)//initial velocity step
 {
  //+   +   +   +
  //  x   x4  x
  //+   +9  +8  +
  //  x3  x1  x2
  //+   +6  +7  +
  //  x   x5  x
  //+   +   +   +
  //
  //212113134141156721398

  //### Approximation of velocity ###//
  //UX
  //cout << " UX in main domain \n";
  for(int k=1;k<Nfx;k++)//x
  {
   for(int l=1;l<Nfy+1;l++)//y
    {
    UXN[k*(Nfy+2)+l] = dtf*(-((UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])*(UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])                                                                                                          -(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])*(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l]))*0.25/dx                                                                                                  -((UX[k*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l])*(UY[(k+1)*(Nfy+1)+l]+UY[k*(Nfy+1)+l])                                                                                                            -(UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])*(UY[(k+1)*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                + mu/rho*((UX[(k+1)*(Nfy+2)+l]-2.0*UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                        + (UX[k*(Nfy+2)+l+1]-2.0*UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])/(dy*dy)));
    }
  }
  //UY
  //cout << " UY in main domain \n";
  for(int k=1;k<Nfx+1;k++)//x
  {
   for(int l=1;l<Nfy;l++)//y
    {
    UYN[k*(Nfy+1)+l] = dtf*(-((UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l+1])*(UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l])                                                                                                            -(UX[(k-1)*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l])*(UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l]))*0.25/dx                                                                           		    -((UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])*(UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])                                                                                                              -(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])*(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                      + mu/rho*((UY[(k+1)*(Nfy+1)+l]-2.0*UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l])/(dx*dx)                                                                                                        + (UY[k*(Nfy+1)+l+1]-2.0*UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])/(dy*dy)));
    }
  }
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //UX
  //cout << "UX in outlet domain \n";
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+l] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])                                               -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]))*0.25/dx                                              -((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                            + mu/rho*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)));
     }
   }
 //coupling to main domain
 //UX in outlet
 //cout << " UX in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo;k++)//x at l=Nfyo-1
  {
   UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])                                  -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                        + mu/rho*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
 
  }
 //UX in main domain
 //cout << " UX in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
  {
   UXN[k*(Nfy+2)] = dtf*(-((UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])*(UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])                                                                                                           -(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])*(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)]))*0.25/dx                                                                                                          -((UX[k*(Nfy+2)+1]+UX[k*(Nfy+2)])*(UY[(k+1)*(Nfy+1)]+UY[k*(Nfy+1)])                                                                                                                    -(UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]                                                             +UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                                                                                                        + mu/rho*((UX[(k+1)*(Nfy+2)]-2.0*UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                              + (UX[k*(Nfy+2)+1]-2.0*UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
 //UY
 //cout << "UY in outlet domain \n";
 for(int k=1;k<Nfxo+1;k++)//x
  {
   for(int l=1;l<Nfyo-1;l++)//y
    {
     UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+l] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l])                                                 -(UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l]))*0.25/dx                                        -((UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                          -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                                  + mu/rho*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/(dy*dy)));
    }
  }
 //coupling to main domain
 //UY in outlet
 //cout << " UY in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
  {
   UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1])                                 -(UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])*(UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                              -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                              + mu/rho*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
  }
 //UY in main domain
 //cout << " UY in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
  {
   UYN[k*(Nfy+1)] = dtf*(-((UX[k*(Nfy+2)]+UX[k*(Nfy+2)+1])*(UY[k*(Nfy+1)]+UY[(k+1)*(Nfy+1)])                                                                                                                    -(UX[(k-1)*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)])*(UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)]))*0.25/dx                                                                                                    -((UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])*(UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])                                                                                                                      -(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                      + mu/rho*((UY[(k+1)*(Nfy+1)]-2.0*UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)])/(dx*dx)                                                                                                              + (UY[k*(Nfy+1)+1]-2.0*UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
#endif
 }

void NS_STEP2(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &Pr, int &Nfx, int &Nfy, DOUB &beta, DOUB &toll, DOUB &dx, DOUB &dy, DOUB &dtf, DOUB &rho, int &Nfxo, int &Nfyo, int &Xo, DOUB &Npr, DOUB &Nsor)//solve for pressur
 {
  DOUB dPrmax(1.0), Prold(0.0), A(1.0-beta), sornum(0.0); // B(parsFDF.beta/(2.0/(parsFDF.dx*parsFDF.dx)+2.0/(parsFDF.dx*parsFDF.dx))),
  vector<DOUB> B((Nfx+2)*(Nfy+2)+(Nfxo+2)*Nfyo);
  
  //fill array
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      B[k*(Nfy+2)+l] = beta/(2.0/(dx*dx)+2.0/(dy*dy));
     }
   }
  //k=1,l=1 lower left corner
  B[(Nfy+2)+1] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //k=1,l=Ny upper left corner
  B[(Nfy+2)+Nfy] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //k=Nx,l=1 lower right corner
  B[Nfx*(Nfy+2)+1] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //k=Nx,l=Ny upper right corner
  B[Nfx*(Nfy+2)+Nfy] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //left edge k=1
  for(int l=2; l<Nfy;l++)
   {
    B[(Nfy+2)+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  //right edge k=Nx
  for(int l=2; l<Nfy;l++)
   {
    B[Nfx*(Nfy+2)+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  //lower edge l=1
  for(int k=2; k<Nfx;k++)
   {
#ifdef pressure_outlet
    if(abs(k-Xo)<=(Nfxo-1)/2){B[k*(Nfy+2)+1] = beta/(2.0/(dx*dx)+2.0/(dy*dy));}//takes into account pressur gradient to the outlet
    else{B[k*(Nfy+2)+1] = beta/(2.0/(dx*dx)+1.0/(dy*dy));}//no gradient of pressur
#else
    B[k*(Nfy+2)+1] = beta/(2.0/(dx*dx)+1.0/(dy*dy));
#endif
   }
  //upper edge l=Ny
  for(int k=2; k<Nfx;k++)
   {
    B[k*(Nfy+2)+Nfy] = beta/(2.0/(dx*dx)+1.0/(dy*dy));
   }

#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //lower edge l=0 (towards outlet in main domain)
  for(int k=1; k<Nfx+1;k++)
   {
    if(abs(k-Xo)<=(Nfxo-1)/2){B[k*(Nfy+2)] = beta/(2.0/(dx*dx)+2.0/(dy*dy));}//takes into account pressur gradient to the outlet
   }
  //left outlet wall at l=0, k= Xo
  B[(Xo-(Nfxo-1)/2)*(Nfy+2)] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
  //right outlet wall
  B[(Xo+(Nfxo-1)/2)*(Nfy+2)] = beta/(1.0/(dx*dx)+2.0/(dy*dy));

  // outlet main domain (including cells touching the main domain)
  for(int k=1;k<Nfxo+1;k++)
   {
    for(int l=1;l<Nfyo;l++)
     {
      B[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = beta/(2.0/(dx*dx)+2.0/(dy*dy));
     }
   }
  // left edge k=1
  for(int l=2;l<Nfyo;l++)
   {
    B[(Nfx+2)*(Nfy+2)+Nfyo+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  // right edge k=Nfxo
  for(int l=2;l<Nfyo;l++)
   {
  B[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  // bottom edge
  for(int k=2;k<Nfxo;k++)
   {
    B[(Nfx+2)*(Nfy+2)+k*Nfyo+1] = beta/(2.0/(dx*dx)+2.0/(dx*dx));
   }
  // left bottom outlet corner
  B[(Nfx+2)*(Nfy+2)+Nfyo+1] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
  // right bottom outlet corner
  B[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+1] = beta/(1.0/(dx*dx)+2.0/(dx*dx));
#endif

  //### Solve for pressure ###//
  while(dPrmax==1.0)
  //while(sornum <= 4000.0)
   {
    //cout << " SOR step= " << sornum << "\n";
    sornum += 1.0;
    dPrmax = 0.0;
    //### main domain ###//
    //cout << "Pr in main domain \n";
    for(int k=1;k<Nfx+1;k++)
     {
      for(int l=1;l<Nfy+1;l++)
       {
	Prold=Pr[k*(Nfy+2)+l];
	Pr[k*(Nfy+2)+l] = A*Pr[k*(Nfy+2)+l] + B[k*(Nfy+2)+l]*((Pr[(k+1)*(Nfy+2)+l] + Pr[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                               + (Pr[k*(Nfy+2)+l+1] + Pr[k*(Nfy+2)+l-1])/(dy*dy)                                                                                                                                      - rho/dtf*(UX[k*(Nfy+2)+l] - UX[(k-1)*(Nfy+2)+l])/dx                                                                                                                                   - rho/dtf*(UY[k*(Nfy+1)+l] - UY[k*(Nfy+1)+l-1])/dy);
	if(abs(Prold-Pr[k*(Nfy+2)+l])>toll)//stopping criterion
	 {
          dPrmax=1.0;
	 }
       }
     }
#ifdef pressure_outlet
    //### Outlet boundary cells ###//
    //cout << "Pr in outlet main domain \n";
    for(int k=1;k<Nfxo+1;k++)
     {
      for(int l=1;l<Nfyo-1;l++)
       {
        Prold=Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l];
        Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = A*Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + B[(Nfx+2)*(Nfy+2)+k*Nfyo+l]*((Pr[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] + Pr[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                           + (Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] + Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)                                                                                                            - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/dx                                                                                                         - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/dy);
        if(abs(Prold-Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l])>toll)//stopping criterion
         {
          dPrmax=1.0;
          //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
         }
       }
     }//*/
    //coupling to main domain
    //Pr in outlet at l=Nfyo-1
    //cout << "Pr in outlet at l=Nfyo-1 \n";
    for(int k=1;k<Nfxo+1;k++)
     {
      Prold=Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1];
      Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = A*Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                                                  + B[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]*((Pr[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] + Pr[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                            + (Pr[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] + Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1-1])/(dy*dy)                                                                                                      - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/dx                                                                                               - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])/dy);
      if(abs(Prold-Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1])>toll)//stopping criterion
       {
        dPrmax=1.0;
        //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
       }
     }
    // Pr in main domain at l=0
    //cout << "Pr in main domain at l=0 \n";
    for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)
     {
      Prold=Pr[k*(Nfy+2)];
      Pr[k*(Nfy+2)] = A*Pr[k*(Nfy+2)] + B[k*(Nfy+2)]*((Pr[(k+1)*(Nfy+2)] + Pr[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                      + (Pr[k*(Nfy+2)+1] + Pr[(Nfx+2)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)                                                                                                      - rho/dtf*(UX[k*(Nfy+2)] - UX[(k-1)*(Nfy+2)])/dx                                                                                                                                       - rho/dtf*(UY[k*(Nfy+1)] - UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/dy);
      if(abs(Prold-Pr[k*(Nfy+2)])>toll)//stopping criterion
       {
        dPrmax=1.0;
        //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
       }
     }//*/
#endif
    if(sornum > Npr){dPrmax=0.0;} //blocks to high iterations 
   }//off while
  Nsor = sornum; //keeps track of how expensive sor is for adjustment
  //cout << "steps needed= " << sornum << "\n";
 }

void NS_STEP3(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &Pr, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB &dtf, DOUB &rho, int &Nfxo, int &Nfyo, int &Xo)//pressur correction
 {
  for(int k=1;k<Nfx;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      UX[k*(Nfy+2)+l] += -dtf/rho*(Pr[(k+1)*(Nfy+2)+l] - Pr[k*(Nfy+2)+l])/dx; // x velocity
     }
   }
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy;l++)//y
     {
      UY[k*(Nfy+1)+l] += -dtf/rho*(Pr[k*(Nfy+2)+l+1] - Pr[k*(Nfy+2)+l])/dy;   // y velocity
     }
   }
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo;l++)//y (includes touching layer at l=Nfyo-1) but only for UX!
     {
      UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] += -dtf/rho*(Pr[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] - Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l])/dx; // x velocity
     }
   }
  for(int k=1;k<Nfxo+1;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] += -dtf/rho*(Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] - Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l])/dy;   // y velocity
     }
   }
  //coupling to main domain
  //UX in main domain
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
   {
    UX[k*(Nfy+2)] += -dtf/rho*(Pr[(k+1)*(Nfy+2)] - Pr[k*(Nfy+2)])/dx; // x velocity
   }
  //UY in outlet
  for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
   {
    UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] += -dtf/rho*(Pr[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] - Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1])/dy;   // y velocity
   }
  //UY in main domain
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
   {
    UY[k*(Nfy+1)] += -dtf/rho*(Pr[k*(Nfy+2)+1] - Pr[k*(Nfy+2)])/dy;   // y velocity
   }
#endif
 }

void NS_STEP1_UPDATE(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &UX_old, vector<DOUB> &UY_old, vector<DOUB> &FX, vector<DOUB> &FY, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB &dtf, DOUB &rho, int &Nfxo, int &Nfyo, int &Xo)//pressur correction
 {
  for(int k=1;k<Nfx;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      UX[k*(Nfy+2)+l] = UX_old[k*(Nfy+2)+l] + FX[k*(Nfy+2)+l]; // x velocity
     }
   }
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy;l++)//y
     {
      UY[k*(Nfy+1)+l] = UY_old[k*(Nfy+1)+l] + FY[k*(Nfy+1)+l]; // y velocity
     }
   }
#ifdef pressure_outlet
 //### Update in outlet domain ###//
 //UX
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] = UX_old[(Nfx+1)*(Nfy+2)+k*Nfyo+l] + FX[(Nfx+1)*(Nfy+2)+k*Nfyo+l];//x component
     }
   }
 //coupling to main domain
 //UX in outlet
 //cout << " UX in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo;k++)//x at l=Nfyo-1
  {
   UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] = UX_old[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] + FX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1];//x component
 
  }
 //UX in main domain
 //cout << " UX in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
  {
   UX[k*(Nfy+2)] = UX_old[k*(Nfy+2)] + FX[k*(Nfy+2)];//x component
  }
 //UY
 //cout << "UY in outlet domain \n";
 for(int k=1;k<Nfxo+1;k++)//x
  {
   for(int l=1;l<Nfyo-1;l++)//y
    {
     UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] = UY_old[(Nfx+2)*(Nfy+1)+k*Nfyo+l] + FY[(Nfx+2)*(Nfy+1)+k*Nfyo+l];//x component
    }
  }
 //coupling to main domain
 //UY in outlet
 //cout << " UY in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
  {
   UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] = UY_old[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] + FY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1];//x component
  }
 //UY in main domain
 //cout << " UY in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
  {
   UY[k*(Nfy+1)] = UY_old[k*(Nfy+1)] + FY[k*(Nfy+1)];//x component
  }
#endif
 }

void NS_STEP4_TRACER(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &F, vector<DOUB> &Fn, int &Nfx, int &Nfy, int &Nfxo, int &Nfyo, int &Xo, DOUB &dx, DOUB &dy, int &Xi, int &wi, DOUB &dtf, DOUB &DTr1, DOUB &tf, DOUB &T2) 
 {
  //### Boundary ###//
  //k=0
  for(int l=1;l<Nfy+1;l++)
   {
    F[l] = F[Nfy+l];
   }
  //k=parsFDF.Nfx+1
  for(int l=1;l<Nfy+1;l++)
   {
    F[(Nfx+1)*(Nfy+2)+l] = F[Nfx*(Nfy+2)+l];
   }
  //l=0
  for(int k=1;k<Nfx+1;k++)
   {
    if(abs(Xo-k)>(Nfxo-1)/2)//exclude outlet cells from beeing updated
     {
      F[k*(Nfy+2)] = F[k*(Nfy+2)+1];
     }
   }
  //l=parsFDF.Nfy+1
  for(int k=1;k<Nfx+1;k++)
   {
    F[k*(Nfy+2)+Nfy+1] = F[k*(Nfy+2)+Nfy];
   }
#ifdef pressure_outlet
#ifndef tracer_distribution
  //### tracer inflow ###//
  for(int k=Xi-wi;k<Xi+wi;k++)
   {
    F[k*(Nfy+2)+Nfy+1] = (tanh(0.25*(tf-0.3*T2))+1.0)*0.5*(tanh(-0.5*(tf-0.6*T2))+1.0)*0.25;//highest concentration possible flows into the domain
   }
#else
  for(int k=Xi-wi;k<Xi+wi;k++)
   {
    F[k*(Nfy+2)+Nfy+1] = 0.0;//no tracer flows in
   }
#endif
  //### outlet boundary cells ###//
  for(int l=0;l<Nfyo;l++)
   {
    //k=0
    F[(Nfx+2)*(Nfy+2) + l] = F[(Nfx+2)*(Nfy+2) + Nfyo+l];
    //k=Nfxo
    F[(Nfx+2)*(Nfy+2) + (Nfxo+1)*Nfyo+l] = F[(Nfx+2)*(Nfy+2) + Nfxo*Nfyo+l];
   }
  //NOTE: l=0 (lower boundary of outlet) is supposed to have zero concentration, no action needed
#endif
  //### main latice ###//
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      Fn[k*(Nfy+2)+l] = F[k*(Nfy+2)+l] + dtf*((UX[(k-1)*(Nfy+2)+l]*F[(k-1)*(Nfy+2)+l] - UX[k*(Nfy+2)+l]*F[(k+1)*(Nfy+2)+l])/(2.0*dx)                                                                                                +(UY[k*(Nfy+1)+l-1]*F[k*(Nfy+2)+l-1] - UY[k*(Nfy+1)+l]*F[k*(Nfy+2)+l+1])/(2.0*dy)                                                                                                      +DTr1*((F[(k+1)*(Nfy+2)+l] - 2*F[k*(Nfy+2)+l] + F[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                                  +(F[k*(Nfy+2)+l+1] - 2*F[k*(Nfy+2)+l] + F[k*(Nfy+2)+l-1])/(dy*dy)));
      //cout << "F[" << k*(Nfy+2)+l << "] = " << F[k*(Nfy+2)+l] << " Fn = " << Fn[k*(Nfy+2)+l] << "\n";
     }
   }
#ifdef pressure_outlet
  //### outlet main domain ###//
  for(int k=1;k<Nfxo+1;k++)
   {
    for(int l=1;l<Nfyo-1;l++)
     {
      Fn[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = F[(Nfx+2)*(Nfy+2)+k*Nfyo+l]                                                                                                                                                          + dtf*((UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]*F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l]                                                                                                                     - UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]*F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l])/(2.0*dx)                                                                                                               +(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1]                                                                                                                         - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1])/(2.0*dy)                                                                                                           +DTr1*((F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                                                           +(F[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + F[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)));
     }
   }
  //cells in main domain at l=0
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)
   {
    Fn[k*(Nfy+2)] = F[k*(Nfy+2)] + dtf*((UX[(k-1)*(Nfy+2)]*(F[(k-1)*(Nfy+2)]+F[k*(Nfy+2)]) - UX[k*(Nfy+2)]*(F[(k+1)*(Nfy+2)]+F[k*(Nfy+2)]))/(2.0*dx)                                                                          +(UY[(Nfx+2)*(Nfy+1)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]+F[k*(Nfy+2)])                                                                - UY[k*(Nfy+1)]*(F[k*(Nfy+2)+1]+F[k*(Nfy+2)]))/(2.0*dy)                                                                                                                          +DTr1*((F[(k+1)*(Nfy+2)] - 2*F[k*(Nfy+2)] + F[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                                  +(F[k*(Nfy+2)+1] - 2*F[k*(Nfy+2)] + F[(Nfx+2)*(Nfy+2)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1])/(dy*dy)));
   }
  //cells in outlet domain at l=Nfyo-1
  for(int k=1;k<Nfxo+1;k++)
   {
    Fn[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                                                     + dtf*((UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])                                                                                                         - UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]))/(2.0*dx)                                                                                                   +(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-2]*(F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2])                                                                                                                 - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]*(F[(Xo-(Nfxo-1)/2-1+k)*(Nfy+2)]))/(2.0*dy)                                                                                                   +DTr1*((F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] + F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                            +(F[(Xo-(Nfxo-1)/2-1+k)*(Nfy+2)] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] + F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2])/(dy*dy)));
   }
#endif
 }

void Integrator_FDFS_EULER(FDFS &FDF, DOUB &tf, parsf &parsFDF, DOUB &T, DOUB &T2)
 {
  //### higher viscosity at the start ###//
  //if (tf<=0.1*T){parsFDF.mu -= 4.0/(0.1*T);}
  
  int Nfxo(2*parsFDF.width_outlet+1), Nfyo(parsFDF.length_outlet);//outlet boundary dimensions

  //integrates fluid fields
  //cout << "NS: STEP1 \n";
  vector<DOUB> UX_old(FDF.UX.size());
  vector<DOUB> UY_old(FDF.UY.size());
  vector<DOUB> UXi(FDF.UX.size());
  vector<DOUB> UYi(FDF.UY.size());
  UX_old = FDF.UX;
  UY_old = FDF.UY;
  
  //### Intermediate velocities ###//
  //first step (Euler) with dt
  NS_STEP1(UX_old,UY_old,FDF.UX,FDF.UY,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### Pressure solver ###//
  //cout << "NS: STEP2 \n";
  NS_STEP2(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr,parsFDF.Nsor);
  //### Pressure correction ###//
  //cout << "NS: STEP3 \n";
  NS_STEP3(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### Tracer advection ###//
  NS_STEP4_TRACER(FDF.UX,FDF.UY,FDF.intensity,FDF.intensity, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr1, tf, T2);// parsFDF);
  //### boundary conditions for next time step for the main domain ###//
  //cout << "set new boundary condition tf= " << tf << "\n";
  tf += parsFDF.dtf;
  NS_boundary_set(FDF.UX, FDF.UY, FDF.Pr, parsFDF, tf, T);
  NS_STEP0(FDF.UX, FDF.UY, parsFDF.UL, parsFDF.UU, parsFDF.ULE, parsFDF.UR, parsFDF.VL, parsFDF.VU, parsFDF.VLE, parsFDF.VR, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet);
  //cout << "tf = " << tf << "\n";
 }

void Integrator_FDFS_HEUN(FDFS &FDF, DOUB &tf, parsf &parsFDF, DOUB &T, DOUB &T2)
 {
  int Nfxo(2*parsFDF.width_outlet+1), Nfyo(parsFDF.length_outlet);//outlet boundary dimensions

  //integrates fluid fields
  //cout << "NS: STEP1 \n";
  vector<DOUB> UX_old(FDF.UX.size());
  vector<DOUB> UY_old(FDF.UY.size());
  vector<DOUB> UXi(FDF.UX.size());
  vector<DOUB> UYi(FDF.UY.size());
  vector<DOUB> FX1(FDF.UX.size());
  vector<DOUB> FY1(FDF.UY.size());
  vector<DOUB> FX2(FDF.UX.size());
  vector<DOUB> FY2(FDF.UY.size());
  vector<DOUB> intensity_old(FDF.intensity.size());
  vector<DOUB> intensityi(FDF.intensity.size());
  UX_old = FDF.UX;
  UY_old = FDF.UY;
  intensity_old=FDF.intensity;
  
  //### FIRST HEUN STEP ###//
  //cout << "fist Heun step \n";
  //### Intermediate velocities ###//
  //first step (Euler) with dt
  NS_STEP1_HEUN(UX_old,UY_old,FX1,FY1,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet);
  NS_STEP1_UPDATE(UXi,UYi,UX_old,UY_old,FX1,FY1,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### Pressure solver ###//
  //cout << "NS: STEP2 \n";
  NS_STEP2(UXi,UYi,FDF.Pri,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr,parsFDF.Nsor);
  //### Pressure correction ###//
  //cout << "NS: STEP3 \n";
  NS_STEP3(UXi,UYi,FDF.Pri,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### Tracer advection ###//
  NS_STEP4_TRACER(UX_old,UY_old,intensity_old,intensityi, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr1, tf, T2);//parsFDF);
  //### boundary conditions for next time step for the main domain ###//
  //cout << "set new boundary condition tf= " << tf << "\n";
  tf += parsFDF.dtf;
  NS_boundary_set(UXi, UYi, FDF.Pr, parsFDF, tf, T);
  NS_STEP0(UXi, UYi, parsFDF.UL, parsFDF.UU, parsFDF.ULE, parsFDF.UR, parsFDF.VL, parsFDF.VU, parsFDF.VLE, parsFDF.VR, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet);
  
  //### SECOND HEUN STEP ###//
  //cout << "second Heun step \n";
  //### Intermediate velocities ###//
  NS_STEP1_HEUN(UXi,UYi,FX2,FY2,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### Tracer advection ###//
  NS_STEP4_TRACER(UXi,UYi,intensityi,FDF.intensity, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr1, tf, T2);//parsFDF);

  //### Heuns method ###//
//  cout << "final step of Heuns method \n";
  for(int k=0; k<UX_old.size(); k++)
   {
    FDF.UX[k] = UX_old[k] + 0.5*(FX1[k]+FX2[k]); //0.5*(UXi2[k] + UX_old[k]);//both derivative terms are calculated in terms of the obtained approximations
   }  
  for(int k=0; k<UY_old.size(); k++)
   {
    FDF.UY[k] = UY_old[k] + 0.5*(FY1[k]+FY2[k]); //0.5*(UYi2[k] + UY_old[k]);
   }  
  for(int k=0; k<FDF.intensity.size(); k++)
   {
    FDF.intensity[k] = 0.5*(FDF.intensity[k] + intensity_old[k]);
   }  

  //### Pressure solver of final velocity ###//
//  cout << "NS: STEP2 \n";
  NS_STEP2(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr, parsFDF.Nsor);
  //### Pressure correction of final velocity ###//
//  cout << "NS: STEP3 \n";
  NS_STEP3(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### boundary conditions for next time step for the main domain ###//
  //cout << "set new boundary condition tf= " << tf << "\n";
  NS_boundary_set(FDF.UX, FDF.UY, FDF.Pr, parsFDF, tf, T);
  NS_STEP0(FDF.UX, FDF.UY, parsFDF.UL, parsFDF.UU, parsFDF.ULE, parsFDF.UR, parsFDF.VL, parsFDF.VU, parsFDF.VLE, parsFDF.VR, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet);
//  cout << "tf = " << tf << "\n";
 }

void soft_boundary(DOUB &wallx, DOUB &wally, DOUB &X, DOUB &Y,params &pars)
 {//TODO: implement force integral for wall, now: jsut one dimensional repulsion
  DOUB wrad(0.125);
  if(abs(X-pars.xi)<wrad)
   {
    wallx = -1.0 + wrad*wrad/((pars.xi-X)*(pars.xi-X));//if in xi range
    //cout << "close to xi: del= " << X-pars.xi << " xi= " << pars.xi << " Fxi= " << -1.0 - wrad/(pars.xi-X) << "\n";
   }
  if(abs(X-pars.xf)<wrad)
   {
    wallx = 1.0 - wrad*wrad/((pars.xf-X)*(pars.xf-X));//if in xf range
    //cout << "close to xf: del= " << X-pars.xi << " xf= " << pars.xi << " Fxf= " << 1.0 - wrad/(pars.xf-X) << "\n";
   }
  if(abs(Y-pars.yi)<wrad)
   {
    wally = -1.0 + wrad*wrad/((pars.yi-Y)*(pars.yi-Y));//if in xf range
    //cout << "close to yi: del= " << X-pars.xi << " yi= " << pars.xi << " Fyi= " << -1.0 - wrad/(pars.yi-Y) << "\n";
   }
  if(abs(Y-pars.yf)<wrad)
   {
    wally = 1.0 - wrad*wrad/((pars.yf-Y)*(pars.yf-Y));//if in xf range
    //cout << "close to yf: del= " << X-pars.xi << " yf= " << pars.xi << " Fyf= " << 1.0 - wrad/(pars.yf-Y) << "\n";
   }
 }

DOUB Kernel_fit(DOUB &xg, DOUB &yg, DOUB xi, DOUB yi, DOUB kappa)
 {
  return exp(-((xg-xi)*(xg-xi)+(yg-yi)*(yg-yi))*kappa);
 }

void Interpolate_Image_to_Grid(string img, FDFS &FDF, parsf &parsFDF)
 {
  //interpolate image data onto the simulation grod as specified by parameters //
  cout << "interpolating data from file " << img << "\n";
  string line;
  ifstream imgdat (img);
  vector<DOUB> imgx;//xaxis
  vector<DOUB> imgy;//yaxis
  vector<DOUB> imgF;//field value
  int cx(0);        //counts blocks of changing y axis
  //### EXTRACT FILE CONTENT OF FORM data <SPACE> time \n to struct members ###//
  if(imgdat.is_open())
   {
    DOUB a(0.0);
    while(getline(imgdat,line))
     {
      istringstream is(line);
      vector<DOUB> vecline;
      int jj(0);
      while(is >> a)
       {
        if(jj==0)//x axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==1)//y axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==2)//field data
         {
          vecline.push_back(DOUB(a));
         }
        jj +=1;
       }
      if(jj==0){cx +=1;}//counts the number of blocks in which y axis is changed
      cout << "data extracted x,y,F= "; for(int k=0; k<vecline.size();k++){cout << vecline[k] << ", ";} cout << "length of elementsin line is " << jj << "\n";
      if(jj>0){imgF.push_back(vecline[2]);}//index 2 for image intensity!!!
     }
    imgdat.close();
   }
  int cy(imgF.size()/cx);
  cout << "extracted image with " << cx << " points in x direction and " << cy << " points in y direction \n";
  //### interpolate image onto simulational grid by means of gaussian smoothing ###//
  DOUB dximg((parsFDF.fxf-parsFDF.fxi)/cx), dyimg((parsFDF.fyf-parsFDF.fyi)/cy);//set axis steps of image as given by parameters of CFD grid

  DOUB knorm(0.0); //normalization of kernel
  for(int k=0; k<FDF.gridX.size();k++)//iterate CFD x axis
   {
    for(int l=0; l<FDF.gridY.size();l++)//iterate CFD y axis
     {
      knorm=0.0;
      for(int m=0; m<cx; m++)//iterate image x axis
       {
        for(int n=0; n<cy; n++)//iterate image y axis
	 {
          FDF.intensity_coupler[k*parsFDF.Nfy+l] += imgF[m*cy+n]*Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);
	  knorm += Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);
	 }
       }
      if(knorm<1e-6){knorm=1.0;}//avoid division by zero
      //cout << " before: FDF.intensity[" << k*parsFDF.Nfy+l << "]= " << FDF.intensity[k*parsFDF.Nfy+l] << "\n";
      FDF.intensity_coupler[k*parsFDF.Nfy+l] /=knorm;
      cout << " after: norm kernel= " << knorm << " FDF.intensity[" << k*parsFDF.Nfy+l << "]= " << FDF.intensity_coupler[k*parsFDF.Nfy+l] << "\n";
     }
   }
 }

void set_Interpolation_indices(vector<int> &FDF_indexXK, vector<int> &FDF_indexYK, int &nx, int &ny, DOUB &x, DOUB &y, DOUB &FDFx, DOUB &FDFy)
 {//  closest point seen from grid  ... from particle
  if((x<FDFx)&(y<FDFy))//lower left ... upper right
   {
    FDF_indexXK[0]=-1+nx;
    FDF_indexXK[1]=-1+nx;
    FDF_indexXK[2]=nx;
    FDF_indexXK[3]=nx;
    FDF_indexYK[0]=-1+ny;
    FDF_indexYK[1]=ny;
    FDF_indexYK[2]=ny;
    FDF_indexYK[3]=-1+ny;
   }
  if((x<FDFx)&(y>FDFy))//upper left ... lower right
   {
    FDF_indexXK[0]=-1+nx;
    FDF_indexXK[1]=-1+nx;
    FDF_indexXK[2]=nx;
    FDF_indexXK[3]=nx;
    FDF_indexYK[0]=ny;
    FDF_indexYK[1]=1+ny;
    FDF_indexYK[2]=1+ny;
    FDF_indexYK[3]=ny;
   }
  if((x>FDFx)&(y>FDFy))//upper right ... lower left
   {
    FDF_indexXK[0]=nx;
    FDF_indexXK[1]=nx;
    FDF_indexXK[2]=1+nx;
    FDF_indexXK[3]=1+nx;
    FDF_indexYK[0]=ny;
    FDF_indexYK[1]=1+ny;
    FDF_indexYK[2]=1+ny;
    FDF_indexYK[3]=ny;
   }
  if((x>FDFx)&(y<FDFy))//lower right ... upper left
   {
    FDF_indexXK[0]=nx;
    FDF_indexXK[1]=nx;
    FDF_indexXK[2]=1+nx;
    FDF_indexXK[3]=1+nx;
    FDF_indexYK[0]=-1+ny;
    FDF_indexYK[1]=ny;
    FDF_indexYK[2]=ny;
    FDF_indexYK[3]=-1+ny;
   }
 }

void Update_FDF_index(vector<DOUB> &y, int &N, vector<DOUB> &gridX, vector<DOUB> &gridY, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY)
 {
  // finds the vector index corresponding to closest grid point of each agents
  DOUB distold(0.0),dist(0.0);
  int nx(0), ny(0), fx(0), fy(0); //how many steps in x direction, how many steps in y direction
  for(int k=0;k<N; k++)
   {
    nx=FDF_indexX[k][0];//current x index of agents in CFD grid
    ny=FDF_indexY[k][0];//y index
    distold=sqrt((gridX[nx]-y[2*k])*(gridX[nx]-y[2*k]) + (gridY[ny]-y[2*k+1])*(gridY[ny]-y[2*k+1]));
    int goOn(1); //how many layers arround current grid index position -> at least one
    fx=FDF_indexX[k][0];
    fy=FDF_indexY[k][0];
    while(goOn==1)
     {
      goOn=0;
      for(int l=nx-1;l<=nx+1;l++)
       {
        for(int m=ny-1;m<=ny+1;m++)
 	 {
          dist=sqrt((gridX[l]-y[2*k])*(gridX[l]-y[2*k]) + (gridY[m]-y[2*k+1])*(gridY[m]-y[2*k+1]));
          //cout << "k,l,m= " << k << ", " << l << ", " << m << " dist= " << dist << "\n";
          if((!((l==nx)&(m==ny)))&(dist<distold))
	   {
	    distold=dist;
	    fx=l;
	    fy=m;
	    //FDF_indexX[k]=l;
	    //FDF_indexY[k]=m;
	    goOn=1;
	   }
	 }
       }
      nx=fx;
      ny=fy;
     }//off while
    set_Interpolation_indices(FDF_indexX[k], FDF_indexY[k], nx, ny, y[2*k], y[2*k+1], gridX[nx], gridY[ny]);
   }
 }

void FDF_index(vector<DOUB> &y, int &N, vector<DOUB> &gridX, vector<DOUB> &gridY, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY)
 {
  // finds the vector index corresponding to closest grid point of each agents
  DOUB distold(0.0),dist(0.0);
  int nx(0), ny(0);
  for(int k=0;k<N; k++)
   {
    distold=10000000.0;//large initial number
    nx=0;
    ny=0;
    for(int l=0; l<gridX.size();l++)
     {
      for(int m=0; m<gridY.size();m++)
       {
	dist= sqrt((gridX[l]-y[2*k])*(gridX[l]-y[2*k]) + (gridY[m]-y[2*k+1])*(gridY[m]-y[2*k+1]));
        if(distold>dist)
	 {
          distold=dist;
          nx = l;//index in x
          ny = m;//index in y
	 }
       }
     }
    //set surrounding integers of nodes -> interpolation of fields
    set_Interpolation_indices(FDF_indexX[k], FDF_indexY[k], nx, ny, y[2*k], y[2*k+1], gridX[nx], gridY[ny]);
   }//off for k
 }

void Interpolator(vector<DOUB> &y, vector<DOUB> &F_inter, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY, vector<DOUB> &F, vector<DOUB> &gridX, vector<DOUB> &gridY, parsf &parsFDF)
 {
  //1(1,2)   2(2,2)
  //#--------# 
  //|   P    |
  //|   +    |
  //|        |
  //#--------#
  //0(1,1)   3(2,1)
  //update field values (F) at position of agents (y) stored in F_inter
  for(int k=0; k<F_inter.size(); k++)
   {
    F_inter[k] = (F[FDF_indexX[k][0]*parsFDF.Nfy+FDF_indexY[k][0]]*(gridX[FDF_indexX[k][2]]-y[2*k])*(gridY[FDF_indexY[k][2]]-y[2*k+1]))                                                               + (F[FDF_indexX[k][3]*parsFDF.Nfy+FDF_indexY[k][3]]*(-gridX[FDF_indexX[k][0]]+y[2*k])*(gridY[FDF_indexY[k][2]]-y[2*k+1]))                                                              + (F[FDF_indexX[k][1]*parsFDF.Nfy+FDF_indexY[k][1]]*(gridX[FDF_indexX[k][2]]-y[2*k])*(-gridY[FDF_indexY[k][0]]+y[2*k+1]))                                                              + (F[FDF_indexX[k][2]*parsFDF.Nfy+FDF_indexY[k][2]]*(-gridX[FDF_indexX[k][0]]+y[2*k])*(-gridY[FDF_indexY[k][0]]+y[2*k+1]));
    F_inter[k] /= ((gridX[FDF_indexX[k][2]]-gridX[FDF_indexX[k][0]])*(gridY[FDF_indexY[k][2]]-gridY[FDF_indexY[k][0]]));
   }
 }

DOUB detector(DOUB &fieldval, DOUB &thresh)
 {
  DOUB detect(0.0);
  if(fieldval>=thresh){detect=1.0;}
  return detect;
 }

DOUB Coupling_internal_state_Agent_attraction(vector<DOUB> y, int k, int l, int N, int n, params &pars)
 {
  return 1.0 + pars.J*cos(y[(n-1)*N/n+l]-y[(n-1)*N/n+k]);
 }

DOUB Coupling_direction(vector<DOUB> y, int k, int l, int N, int n, params &pars)
 {
  DOUB del(atan2((y[2*l+1] - y[2*k+1]),(y[2*l] - y[2*k])));//relative angle between the two agents
#ifdef Strogatz
  return 1.0;
#else
  return 1.0 - pars.agent_direction + pars.agent_direction*((1.0-cos(2.0*(del - y[(n-2)*N/n+k])))*pars.J2*0.5 + (1.0-pars.J2))*(1.0+cos(y[(n-2)*N/n+l]-del))*0.5;//detection*transmission of positional information
#endif
 }

DOUB Int_Phase_coupling(vector<DOUB> &y, int k, int l, int N, int n, params &pars)
 {
  return pars.eps*sin(y[(n-1)*N/n+l]-y[(n-1)*N/n+k])/sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));//1/r
 }

DOUB Int_Direction_coupling(vector<DOUB> &y, int k, int l, int N, int n, params &pars)
 {
  DOUB del(atan2((y[2*l+1] - y[2*k+1]),(y[2*l] - y[2*k])));//relative angle in between agents
  return -pars.eps2*sin(y[(n-2)*N/n+l] - y[(n-2)*N/n+k] + pars.dir_lag*sin(del))/sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));//1/r
 }

DOUB Int_Direction_coupling_velocity(vector<DOUB> &y, int k, int N, int n, params &pars)
 {
  DOUB angU(atan2(pars.UYA[k],pars.UXA[k]));//angle of flow velocity measured from positive x axis
  return pars.eps3*sin(y[(n-2)*N/n+k]-angU);//agent 
 }

DOUB Int_Direction_coupling_vorticity(vector<DOUB> &y, int k, int N, int n, params &pars)
 {
  DOUB angGV(atan2(pars.Vorticity_gradYA[k],pars.Vorticity_gradXA[k]));//angle of vorticity measured from positive x axis
  return pars.eps4*sin(y[(n-2)*N/n+k]-angGV);//agent
 }

DOUB Act_Direction_switch(vector<DOUB> &y, int k, int N, int n, params &pars)
 {
  return pars.tr_dir1*(1.0-(y[(n-3)*N/n+k]/(pars.tr_dir_steepnes + y[(n-3)*N/n+k])));
 }
 
DOUB Tracer_Direction_switch(vector<DOUB> &y, int k, int N, int n, params &pars)
 {
  return pars.tr_dir2*(1.0-(pars.intensity[k]/(pars.tr_steepnes + pars.intensity[k])));
 }
 
DOUB Activity_Direction_stepp(vector<DOUB> &y, int k, int N, int n, params &pars)
 {
  return pars.tr_dir3*(1.0-(y[(n-3)*N/n+k]/(pars.act_steepnes_2 + y[(n-3)*N/n+k])));
 }

DOUB Int_Direction_coupling_tracer(vector<DOUB> &y, int k, int N, int n, params &pars)
 {
  DOUB angGV(atan2(pars.TrgradYA[k],pars.TrgradXA[k]));//angle of vorticity measured from positive x axis
  return -pars.eps5*sin(y[(n-2)*N/n+k]-angGV)*pars.tr_dir3*(y[(n-3)*N/n+k]/(pars.tr_dir_steepnes + y[(n-3)*N/n+k]));//agent 
 }

DOUB Agent_attractionX(vector<DOUB> &y, int k, int l, int N, int n, params &pars)
 {
  return pars.agent_atr*(y[2*l]-y[2*k])/sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));//1/r
 }

DOUB Agent_repulsionX(vector<DOUB> &y, int k, int l, int N, int n, params &pars)
 {
  return -(y[2*l]-y[2*k])*(1.0/((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]))*pars.agent_rep - pars.cut);//r^-2 + cutting potential term
 }

DOUB Agent_attractionY(vector<DOUB> &y, int k, int l, int N, int n, params &pars)
 {
  return pars.agent_atr*(y[2*l+1]-y[2*k+1])/sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));//1/r
 }

DOUB Agent_repulsionY(vector<DOUB> &y, int k, int l, int N, int n, params &pars)
 {
  return -(y[2*l+1]-y[2*k+1])*(1.0/((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]))*pars.agent_rep - pars.cut);//r^-2
 }

DOUB Velocity(vector<DOUB> &y, int k, int N, int n, params &pars)
 {
  DOUB U2(sqrt(pars.UXA[k]*pars.UXA[k]+pars.UYA[k]*pars.UYA[k]) + pars.counter_current_act*y[(n-3)*N/n+k]);//magnitude of local velocity of agents - measure of kinetic energy of fluid motion
  return (pars.vel0 + pars.counter_curent*pars.vel_c*U2/(pars.Vs+U2))*exp(pars.J3*cos(y[(n-1)*N/n+k]))/exp(pars.J3);//self propulsion as function of internal state (phase) and velocity of background flow
 }

DOUB freq_activity(DOUB &tr, params &pars)
 {
  return 1.0 + pars.osc_activity*pars.freq_c*tr/(pars.freq_c + tr);
 }

DOUB dir_diff(DOUB &tr, params &pars)
 {
  return 1.0 + pars.dif_activity*pars.freq_c*tr/(pars.freq_c + tr);
 }

DOUB Season(DOUB &t, DOUB &P, DOUB &switcher)
 {
#ifdef seasons
  return (1.0-switcher) + switcher*0.5*(1.0+tanh(0.25*(t-P)))*0.5*(1.0+tanh(10.0*sin(2.0*pi*t/P)));//ramps up seasons at P and lets oscillate at frequency 1/P
#else
  return 1.0;
#endif
 }

void dgl2(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars)
 {
  {
  //### update interactions ###//
  #pragma omp parallel for shared(y)
  for(int k=0; k<N/n;k++)
   {
    DOUB repulsivex(0.0), repulsivey(0.0), dist(0.0), nosc_radius(0.0), wallx(0.0), wally(0.0);
    repulsivex=0.0;
    repulsivey=0.0;
    dist=0.0;
    nosc_radius=0.0;
    soft_boundary(wallx,wally,y[2*k],y[2*k+1],pars);//soft wall repulsion TODO: implement version with force integral along the wall if wall is inside interaction radius
    //### update position ###//
    dydx[2*k] = pars.UXA2[k] + wallx;   //xpositions
    dydx[2*k+1] = pars.UYA2[k] + wally; //ypositions
   }
  #pragma omp barrier
  }//off pragma
 }

void dgl(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars)
 {
  //cout << "in dgl \n";
  {
  //### update interactions ###//
  #pragma omp parallel for shared(y)
  for(int k=0; k<N/n;k++)
   {
    DOUB repulsivex(0.0), repulsivey(0.0), interaction_direction(0.0), dist(0.0), nosc_radius(0.0), attractivex(0.0), attractivey(0.0), internalstate_interaction(0.0), wallx(0.0), wally(0.0), delTr(0.0), ri(0.0), offset(0.0), Like(0.0), cond(0.0);
    repulsivex=0.0;
    repulsivey=0.0;
    interaction_direction=0.0;
    dist=0.0;
    nosc_radius=0.0;
    attractivex=0.0;
    attractivey=0.0;
    internalstate_interaction=0.0;
    for(int l=0; l<N/n; l++)
     {
      if(l!=k)
       {
	dist = sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));
	Like= Coupling_internal_state_Agent_attraction(y, k, l, N, n, pars)*Season(t,pars.thresh,pars.seasons_switc);//calculate like function
        
        //### CALCULATE INTERACTION RADIUS AND CUTOFF ###//
        if(pars.offset == 0.0){ri=pars.irad;}//exactly if irad=pars.agent_rep
        else if(pars.offset>0.0){cout << "error: ri choosen smaller than pars.agent_rep! \n" ;}
        else //offset<0.0 force needs to be lowered, OK 
         {
          if(Like>0.0)//Like==0 is isolated from acceptable Like values by Like=L_crit given by the solvability of the radius equation (sqrt condition) 
           {
            cond = 1.0 + 4.0*pars.offset*pars.agent_rep/(Like*Like);//sqrt condition
            if(cond>=0.0)
             {
              ri = -0.5*Like/pars.offset*(1.0+sqrt(cond));//calculate the actual interaction radius
             }
            else
             {
              ri = 10000.0;//radius becomes effectively infinite if Like=0 as then, there exists just repulsion
             }
           }
          else
           {
            ri = 10000.0;//if Like =0 then, no attraction but long range repulsion
           }
         }// TODO: find proper way of dealing with the interaction term and parallel computations currently an error occurs that lets the ensemble explode when omp is activated  */ 
        pars.cut = pars.agent_rep/(ri*ri) - Like/ri;//calculate cutoff constant
        
        if((dist<ri))
         {
	  //interaction_direction += y[(n-2)*N/n+l];//interaction of direction angle -> Viscek model
	  interaction_direction += Int_Direction_coupling(y, k, l, N, n, pars)*Coupling_direction(y, k, l, N, n, pars); //interaction of direction angle -> Kuramoto like
	  internalstate_interaction += Int_Phase_coupling(y, k, l, N, n, pars)*Coupling_direction(y, k, l, N, n, pars); //interaction of internal states
	  attractivex += Agent_attractionX(y, k, l, N, n, pars)*Coupling_internal_state_Agent_attraction(y, k, l, N, n, pars)*Coupling_direction(y, k, l, N, n, pars)*Season(t,pars.thresh,pars.seasons_switc);
          attractivey += Agent_attractionY(y, k, l, N, n, pars)*Coupling_internal_state_Agent_attraction(y, k, l, N, n, pars)*Coupling_direction(y, k, l, N, n, pars)*Season(t,pars.thresh,pars.seasons_switc);
          repulsivex += Agent_repulsionX(y, k, l, N, n, pars)*Coupling_direction(y, k, l, N, n, pars);
          repulsivey += Agent_repulsionY(y, k, l, N, n, pars)*Coupling_direction(y, k, l, N, n, pars);
  	  nosc_radius +=1.0;
         }      
       }
     }
    if(nosc_radius==0.0){nosc_radius=1.0;}
    soft_boundary(wallx,wally,y[2*k],y[2*k+1],pars);//soft wall repulsion TODO: implement version with force integral along the wall if wall is inside interaction radius
    delTr = atan2(pars.TrgradYA[k],pars.TrgradXA[k]); //angle of tracer gradient
    //### update position ###//
    dydx[2*k] = cos(y[(n-2)*N/n+k])*Velocity(y, k, N, n, pars) + pars.UXA[k] + repulsivex/nosc_radius + attractivex/nosc_radius + wallx;   //xpositions
    dydx[2*k+1] = sin(y[(n-2)*N/n+k])*Velocity(y, k, N, n, pars) + pars.UYA[k] + repulsivey/nosc_radius + attractivey/nosc_radius + wally; //ypositions
    //### Ornstein-Uhlenbeck noise ###//
    dydx[(n-4)*N/n+k] = pars.dirdamp*y[(n-4)*N/n+k];
    //### update memory of agents ###//
    dydx[(n-3)*N/n+k] = sqrt(pars.TrgradXA[k]*pars.TrgradXA[k] + pars.TrgradYA[k]*pars.TrgradYA[k])*0.5*(abs(cos(y[(n-2)*N/n+k]-delTr)) +cos(y[(n-2)*N/n+k]-delTr)) + pars.mem_damp*y[(n-3)*N/n+k];
    //### update direction angle ###//
    dydx[(n-2)*N/n+k] = y[(n-4)*N/n+k] + interaction_direction/nosc_radius + Int_Direction_coupling_velocity(y, k, N, n,pars)*(Tracer_Direction_switch(y, k, N, n, pars) + 1.0-pars.tr_dir2)                           + Int_Direction_coupling_vorticity(y, k, N, n,pars)*(Act_Direction_switch(y, k, N, n, pars) + 1.0-pars.tr_dir1)                                                 + Int_Direction_coupling_tracer(y, k, N, n, pars)*(Activity_Direction_stepp(y, k, N, n, pars) + 1.0 - pars.tr_dir3);
    //### update internal state ###//
    dydx[(n-1)*N/n+k] = pars.freq[k]*freq_activity(y[(n-3)*N/n+k],pars) + internalstate_interaction/nosc_radius;
    //cout << "memory " << k << "= " << freq_tracer(y[(n-3)*N/n+k],pars) << "\n";
   }
  #pragma omp barrier
  }//off pragma
 }

void rk1(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB &ddt, params &pars)
 {
  vector<DOUB> dydx1(N);
  cout << "start rk1 \n";
  for(int j=0; j<Nsim; j++)
   {
    (*derives)(y, dydx1,N,n,t, pars);
    //cout << "after function \n";
    for(int i=0; i<N; i++)
     {
      y[i] = y[i] + ddt*dydx1[i];
     }
    t = t + ddt;
   }
  }

void rk4(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB dt, params &pars)
 {
  vector<DOUB> dydx1(N);
  vector<DOUB> dydx2(N);
  vector<DOUB> dydx3(N);
  vector<DOUB> dydx4(N);
  vector<DOUB> y1(N);
  vector<DOUB> y2(N);
  vector<DOUB> y3(N);
  DOUB xold(0.0), yold(0.0);
  for(int j=0; j<Nsim; j++)
   {
    (*derives)(y, dydx1,N,n,t,pars);//k1
    for(int i=0; i<N; i++)
     {
      y1[i] = y[i] + 0.5*dt*dydx1[i];//for k2
     }
    t = t + 0.5*dt;
    (*derives)(y1,dydx2,N,n,t,pars);//k2
    for(int i=0; i<N; i++)
     {
      y2[i] = y[i] + 0.5*dt*dydx2[i] ;//for k3
     }
    (*derives)(y2,dydx3,N,n,t,pars);
    for(int i=0; i<N; i++)
     {
      y3[i] = y[i] + dt*dydx3[i];
     }
    t = t + 0.5*dt;
    (*derives)(y3,dydx4,N,n,t,pars);
    for(int i=0; i<N/n; i++)//spatial components
     {
      xold=y[2*i];
      yold=y[2*i+1];
      y[2*i] = y[2*i] + ((dt/6.0)*dydx1[2*i] + (dt/3.0)*dydx2[2*i] + (dt/3.0)*dydx3[2*i] + (dt/6.0)*dydx4[2*i]);//posx
      y[2*i+1] = y[2*i+1] + ((dt/6.0)*dydx1[2*i+1] + (dt/3.0)*dydx2[2*i+1] + (dt/3.0)*dydx3[2*i+1] + (dt/6.0)*dydx4[2*i+1]);//posy
      y[(n-2)*N/n+i] = y[(n-2)*N/n+i] + ((dt/6.0)*dydx1[(n-2)*N/n+i] + (dt/3.0)*dydx2[(n-2)*N/n+i] + (dt/3.0)*dydx3[(n-2)*N/n+i] + (dt/6.0)*dydx4[(n-2)*N/n+i]);//direction
      y[(n-1)*N/n+i] = y[(n-1)*N/n+i] + ((dt/6.0)*dydx1[(n-1)*N/n+i] + (dt/3.0)*dydx2[(n-1)*N/n+i] + (dt/3.0)*dydx3[(n-1)*N/n+i] + (dt/6.0)*dydx4[(n-1)*N/n+i]);//internal state
      //reflecting_boundary(y[2*i],y[2*i+1], xold, yold, y[(n-2)*N/n+i], pars);
     }
   }
  //cout << "rk4 finished \n";
 }

void rk2_stoch(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB dt, params &pars, vector<DOUB> &noise, vector<DOUB> &vel)
 {
  vector<DOUB> dydx1(N);
  vector<DOUB> dydx2(N);
  vector<DOUB> y1(N);
  DOUB xold(0.0), yold(0.0);
  for(int j=0; j<Nsim; j++)
   {
    (*derives)(y, dydx1,N,n,t,pars);//k1
    for(int i=0; i<N; i++)
     {
      y1[i] = y[i] + dt*dydx1[i] + sqrt(2.0*pars.D[i]*dt)*noise[i];//for k2
     }
    t = t + dt;
    (*derives)(y1,dydx2,N,n,t,pars);//k2
    for(int i=0; i<N/n; i++)//spatial components
     {
      xold=y[2*i];
      yold=y[2*i+1];
      y[2*i] = y[2*i] + (dt/2.0)*(dydx1[2*i]+dydx2[2*i]) + sqrt(2.0*pars.D[2*i]*dt)*noise[2*i];//x
      y[2*i+1] = y[2*i+1] + (dt/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]) + sqrt(2.0*pars.D[2*i+1]*dt)*noise[2*i+1];//y
      y[(n-4)*N/n+i] = y[(n-4)*N/n+i] + (dt/2.0)*(dydx1[(n-4)*N/n+i]+dydx2[(n-4)*N/n+i]) + sqrt(2.0*pars.D[(n-4)*N/n+i]*dir_diff(y[(n-3)*N/n+i],pars)*dt)*noise[(n-3)*N/n+i];//n-4: O-U noise
      y[(n-3)*N/n+i] = y[(n-3)*N/n+i] + (dt/2.0)*(dydx1[(n-3)*N/n+i]+dydx2[(n-3)*N/n+i]) + sqrt(2.0*pars.D[(n-3)*N/n+i]*dt)*noise[(n-3)*N/n+i];//n-3: memory has no noise
      y[(n-2)*N/n+i] = y[(n-2)*N/n+i] + (dt/2.0)*(dydx1[(n-2)*N/n+i]+dydx2[(n-2)*N/n+i]) + sqrt(2.0*pars.D[(n-2)*N/n+i]*dt)*noise[(n-2)*N/n+i];//direction
      y[(n-1)*N/n+i] = y[(n-1)*N/n+i] + (dt/2.0)*(dydx1[(n-1)*N/n+i]+dydx2[(n-1)*N/n+i]) + sqrt(2.0*pars.D[(n-1)*N/n+i]*dt)*noise[(n-1)*N/n+i];//internal state
      //### keep velocities ###//
      vel[2*i] = (1.0/2.0)*(dydx1[2*i]+dydx2[2*i]) + sqrt(2.0*pars.D[2*i]/dt)*noise[2*i];
      vel[2*i+1] = (1.0/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]) + sqrt(2.0*pars.D[2*i+1]/dt)*noise[2*i+1];
     }
   }
 }

void rk2(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB dt, params &pars, vector<DOUB> &vel)
 {
  vector<DOUB> dydx1(N);
  vector<DOUB> dydx2(N);
  vector<DOUB> y1(N);
  DOUB xold(0.0), yold(0.0);
  for(int j=0; j<Nsim; j++)
   {
    (*derives)(y, dydx1,N,n,t,pars);//k1
    for(int i=0; i<N; i++)
     {
      y1[i] = y[i] + dt*dydx1[i];//for k2
     }
    t = t + dt;
    (*derives)(y1,dydx2,N,n,t,pars);//k2
    for(int i=0; i<N/n; i++)//spatial components
     {
      y[2*i] = y[2*i] + (dt/2.0)*(dydx1[2*i]+dydx2[2*i]);//x
      y[2*i+1] = y[2*i+1] + (dt/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]);//y
      //### keep velocities ###//
      vel[2*i] = (1.0/2.0)*(dydx1[2*i]+dydx2[2*i]);
      vel[2*i+1] = (1.0/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]);
     }
   }
 }

void ERROR_OUT(Vectordata &Data, int &N, params &pars)
 {
  for(int k=0;k<N;k++)
   {
    for(int l=0; l<N; l++)
     {
      if((l!=k) && (sqrt((Data.pos[2*k]-Data.pos[2*l])*(Data.pos[2*k]-Data.pos[2*l])+(Data.pos[2*k+1]-Data.pos[2*l+1])*(Data.pos[2*k+1]-Data.pos[2*l+1]))<0.01))
       {
        //cout << "ERR1: close encounter " << k << ", " << l << " distance= " << sqrt((Data.pos[2*k]-Data.pos[2*l])*(Data.pos[2*k]-Data.pos[2*l])+(Data.pos[2*k+1]-Data.pos[2*l+1])*(Data.pos[2*k+1]-Data.pos[2*l+1])) << "\n";
       }
     }
    if(Data.pos[2*k]<pars.xi){cout << " ERR2.1: agent outside domain x<xi for unit " << k << "\n";}
    if(Data.pos[2*k]>pars.xf){cout << " ERR2.2: agent outside domain x>xf for unit " << k << "\n";}
    if(Data.pos[2*k+1]<pars.yi){cout << " ERR2.3: agent outside domain x<yi for unit " << k << "\n";}
    if(Data.pos[2*k+1]>pars.yf){cout << " ERR2.4: agent outside domain x<yf for unit " << k << "\n";}
   }
 }

void Data_Print(DOUB &t, Vectordata &Data, int N, int n, params &pars, ofstream &fout, ofstream &fout3)
 {
  for(int k=0;k<N;k++)
   {
    fout << t << " " << Data.pos[2*k] << " " << Data.pos[2*k+1];
    for(int l=3; l<=n-1;l++){fout << " " <<  Data.pos[l*N+k];} //iterate different domains of vector of data start after x,y and Ornstein-Uhlenbeck noise
    fout << " " << pars.intensity[k] << " " << pars.VorticityA[k] << " " << Data.vel[2*k] << " " << Data.vel[2*k+1] << " " << pars.UXA[k] << " " << pars.UYA[k] << " " << pars.TrgradXA[k] << " " << pars.TrgradYA[k] << " " << pars.Vorticity_gradXA[k] << " " << pars.Vorticity_gradYA[k] << " " << Data.pos2[2*k] << " " << Data.pos2[2*k+1] << " " << Data.vel2[2*k] << " " << Data.vel2[2*k+1] << " " << pars.UXA2[k] << " " << pars.UYA2[k] << " " << Data.pos[2*N+k] << " " << pars.intensity2[k] << " " << pars.VorticityA2[k] << "\n";
   }
  fout << "\n";
 }

void Data_print_FDF(DOUB &t, FDFS &FDF, int Nfx, int Nfy, ofstream &fout4, int Nfxo, int Nfyo, int Xo, ofstream &fout5)
 {
  for(int k=0; k<Nfx; k++)
   {
    for(int l=0; l<Nfy; l++)
     {
      fout4 << t << " " << FDF.gridX[k] << " " << FDF.gridY[l] << " " << k << " " << l << " " << FDF.intensity_coupler[k*Nfy+l] << " " << FDF.UX_coupler[k*Nfy+l] << " " << FDF.UY_coupler[k*Nfy+l] << " " << FDF.Pr_coupler[k*Nfy+l] << " " << FDF.vorticity_coupler[k*Nfy+l] << " " << FDF.divergence_coupler[k*Nfy+l] << " " << FDF.TrgradX_coupler[k*Nfy+l] << " " << FDF.TrgradY_coupler[k*Nfy+l] << " " << FDF.vorticitygradX_coupler[k*Nfy+l] << " " << FDF.vorticitygradY_coupler[k*Nfy+l] << "\n";
     }
    fout4 << "\n";
   }
  //### outlet fields ###//
  for(int k=1; k<Nfxo+1; k++)
   {
    for(int l=1; l<Nfyo; l++)
     {
      fout5 << t << " " << (FDF.gridX[1]-FDF.gridX[0])*(DOUB(Xo-1-(Nfxo-1)/2+k-Nfx/2-1)) << " " << (FDF.gridY[1]-FDF.gridY[0])*(DOUB(l-Nfyo-Nfy/2-2)) << " " << k << " " << l << " " << FDF.intensity[(Nfx+2)*(Nfy+2)+k*Nfyo+l] << " " << 0.5*(FDF.UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+FDF.UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]) << " " << 0.5*(FDF.UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+FDF.UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]) << " " << FDF.Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] << " " << FDF.vorticity[(Nfx+2)*(Nfy+2)+k*Nfyo+l] << " " << FDF.divergence[(Nfx+2)*(Nfy+2)+k*Nfyo+l] << "\n";
     }
    fout5 << t << " " << (FDF.gridX[1]-FDF.gridX[0])*(DOUB(Xo-1-(Nfxo-1)/2+k-Nfx/2-1)) << " " << (FDF.gridY[1]-FDF.gridY[0])*(DOUB(-Nfy/2-1)) << " " << k << " " << 5 << " " << FDF.intensity[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << 0.5*(FDF.UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+FDF.UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]) << " " << 0.5*(FDF.UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+FDF.UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]) << " " << FDF.Pr[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << FDF.vorticity[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << FDF.divergence[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << "\n";//highest layer of outlet in main domain
    fout5 << "\n";
   }
 }

void CFD_grid_to_agent_grid(FDFS &FDF, parsf &parsFDF)
 {
  for(int k=0; k<parsFDF.Nfx; k++)
   {
    for(int l=0; l<parsFDF.Nfy; l++)
     {
      FDF.intensity_coupler[k*parsFDF.Nfy+l] = FDF.intensity[(k+1)*(parsFDF.Nfy+2)+(l+1)];                         //copy intenisty
      FDF.vorticity_coupler[k*parsFDF.Nfy+l] = FDF.vorticity[(k+1)*(parsFDF.Nfy+2)+(l+1)];                         //copy vorticity
      FDF.divergence_coupler[k*parsFDF.Nfy+l] = FDF.divergence[(k+1)*(parsFDF.Nfy+2)+(l+1)];                       //copy divergence
      FDF.UX_coupler[k*parsFDF.Nfy+l] = 0.5*(FDF.UX[k*(parsFDF.Nfy+2)+(l+1)]+FDF.UX[(k+1)*(parsFDF.Nfy+2)+(l+1)]); //copy x velocity
      FDF.UY_coupler[k*parsFDF.Nfy+l] = 0.5*(FDF.UY[(k+1)*(parsFDF.Nfy+1)+l]+FDF.UY[(k+1)*(parsFDF.Nfy+1)+(l+1)]); //copy y velocity
      FDF.Pr_coupler[k*parsFDF.Nfy+l] = FDF.Pr[(k+1)*(parsFDF.Nfy+2)+(l+1)];                                       //copy pressur
      FDF.TrgradX_coupler[k*parsFDF.Nfy+l] = FDF.TrgradX[(k+1)*(parsFDF.Nfy+2)+(l+1)];                             //copy X gradient of tracer
      FDF.TrgradY_coupler[k*parsFDF.Nfy+l] = FDF.TrgradY[(k+1)*(parsFDF.Nfy+2)+(l+1)];                             //copy Y gradient of tracer
      FDF.vorticitygradX_coupler[k*parsFDF.Nfy+l] = FDF.vorticitygradX[(k+1)*(parsFDF.Nfy+2)+(l+1)];               //copy X gradient of vorticity
      FDF.vorticitygradY_coupler[k*parsFDF.Nfy+l] = FDF.vorticitygradY[(k+1)*(parsFDF.Nfy+2)+(l+1)];               //copy Y gradient of vorticity
     }
   }
 }

void Divergence_calc(FDFS &FDF, parsf &parsFDF)
 {
  //main domain //
  for(int k=1; k<parsFDF.Nfx+1; k++)
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      FDF.divergence[k*(parsFDF.Nfy+2)+l] = (FDF.UX[k*(parsFDF.Nfy+2)+l]-FDF.UX[(k-1)*(parsFDF.Nfy+2)+l])/parsFDF.dx                                                                                                             + (FDF.UY[k*(parsFDF.Nfy+1)+l]-FDF.UY[k*(parsFDF.Nfy+1)+l-1])/parsFDF.dy;
     }
   }
  //### Outlet domain ###//
  for(int k=1; k<2*parsFDF.width_outlet+2; k++)
   {
    for(int l=3; l<parsFDF.length_outlet-1; l++)//avoid counting layers very close to the outlet itself
     {
      FDF.divergence[(parsFDF.Nfy+2)*(parsFDF.Nfx+2)+k*parsFDF.length_outlet+l] = (FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l]                                                                                                                   -FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l])/parsFDF.dx                                                                                                 + (FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l]                                                                                                                   -FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l-1])/parsFDF.dy;
     }
   }
 }

void Vorticity_calc(FDFS &FDF, parsf &parsFDF)
 {
  //main domain //
  for(int k=1; k<parsFDF.Nfx+1; k++)
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      FDF.vorticity[k*(parsFDF.Nfy+2)+l] = 0.25/(parsFDF.dx)*(FDF.UY[(k+1)*(parsFDF.Nfy+1)+l] + FDF.UY[k*(parsFDF.Nfy+1)+l]                                                                                                                        + FDF.UY[(k+1)*(parsFDF.Nfy+1)+l-1] + FDF.UY[k*(parsFDF.Nfy+1)+l-1]                                                                                                                    - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l] - FDF.UY[k*(parsFDF.Nfy+1)+l]                                                                                                                        - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l-1] - FDF.UY[k*(parsFDF.Nfy+1)+l-1])                                                                                                - 0.25/(parsFDF.dy)*(FDF.UX[(k-1)*(parsFDF.Nfy+2)+l+1] + FDF.UX[(k-1)*(parsFDF.Nfy+2)+l]                                                                                                                  + FDF.UX[k*(parsFDF.Nfy+2)+l+1] + FDF.UX[k*(parsFDF.Nfy+2)+l]                                                                                                                          - FDF.UX[(k-1)*(parsFDF.Nfy+2)+l-1] - FDF.UX[(k-1)*(parsFDF.Nfy+2)+l]                                                                                                                  - FDF.UX[k*(parsFDF.Nfy+2)+l-1] - FDF.UX[k*(parsFDF.Nfy+2)+l]);
     }
   }
  //### Outlet domain ###//
  for(int k=1; k<2*parsFDF.width_outlet+2; k++)
   {
    for(int l=1; l<parsFDF.length_outlet-1; l++)
     {
      FDF.vorticity[(parsFDF.Nfy+2)*(parsFDF.Nfx+2)+k*parsFDF.length_outlet+l] = 0.25/(parsFDF.dx)*(FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k+1)*(parsFDF.length_outlet)+l]                                                                                                              + FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l]                                                                                                                  + FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k+1)*(parsFDF.length_outlet)+l-1]                                                                                                            + FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l-1]                                                                                                                - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k-1)*(parsFDF.length_outlet)+l]                                                                                                              - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l]                                                                                                                  - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k-1)*(parsFDF.length_outlet)+l-1]                                                                                                            - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l-1])                                                                                            - 0.25/(parsFDF.dy)*(FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l+1]                                                                                                            + FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l]                                                                                                              + FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l+1]                                                                                                                + FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l]                                                                                                                  - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l-1]                                                                                                            - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l]                                                                                                              - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l-1]                                                                                                                - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l]);
     }
   }
 }

void Grads(FDFS &FDF, parsf &parsFDF)
 {
  DOUB Gx(0.0), Gy(0.0);
  for(int k=1; k<parsFDF.Nfx+1; k++)
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      Gx = (FDF.intensity[(k+1)*(parsFDF.Nfy+2)+l] - FDF.intensity[(k-1)*(parsFDF.Nfy+2)+l])*0.5/parsFDF.dx;
      Gy = (FDF.intensity[k*(parsFDF.Nfy+2)+l+1] - FDF.intensity[k*(parsFDF.Nfy+2)+l-1])*0.5/parsFDF.dy;
      FDF.TrgradX[k*(parsFDF.Nfy+2)+l] = Gx;                                                                           // X gradient of tracer
      FDF.TrgradY[k*(parsFDF.Nfy+2)+l] = Gy;                                                                           // Y gradient of tracer
      Gx = (abs(FDF.vorticity[(k+1)*(parsFDF.Nfy+2)+l]) - abs(FDF.vorticity[(k-1)*(parsFDF.Nfy+2)+l]))*0.5/parsFDF.dx; // gradient of vorticity absolute value x
      Gy = (abs(FDF.vorticity[k*(parsFDF.Nfy+2)+l+1]) - abs(FDF.vorticity[k*(parsFDF.Nfy+2)+l-1]))*0.5/parsFDF.dy;     // gradient of vorticity absolute value y 
      FDF.vorticitygradX[k*(parsFDF.Nfy+2)+l] = Gx;                                                                    // X gradient of abs(vorticity)
      FDF.vorticitygradY[k*(parsFDF.Nfy+2)+l] = Gy;                                                                    // Y gradient of abs(vorticity)
     }
   }
 }

int main(int argc, char* argv[])
{
 //### measuring time ###//
 clock_t starttime, pt, t_agent, t_coupling, t_cfd ,endtime;
 //DOUB starttime, endtime;
 starttime=clock()/CLOCKS_PER_SEC;
 //starttime=omp_get_wtime();
 cout << "t_s= " << starttime << "\n";
 
 //### initialize data arays ###//
 FDFS FDF;        //fluid dynamics field structures
 parsf parsFDF;   //fluid dynamics parameters
 params pars;     //parameters of agents
 Vectordata Data; //vector data of agents
 
 cout << "read parameters \n";
 // argv[1];                                           // distinguish file parameter , argv[0] own name
 int N=int(strtod(argv[2],NULL));                      // number of units
 pars.freq1=DOUB(strtod(argv[3],NULL));                // mean frequency
 pars.freq2=DOUB(strtod(argv[4],NULL));                // standart deviation of freq distribution
 pars.vel0 =DOUB(strtod(argv[5],NULL));                // offset velocity
 pars.eps  =DOUB(strtod(argv[6],NULL));                // intercation strength of internal states
 pars.irad =DOUB(strtod(argv[7],NULL));                // interaction radius
 DOUB T=DOUB(strtod(argv[8],NULL));                    // transient time
 DOUB dt=DOUB(strtod(argv[9],NULL));                   // dt 
 pars.xi=DOUB(strtod(argv[10],NULL));                  // left x bound
 pars.xf=DOUB(strtod(argv[11],NULL));                  // right x bound
 pars.yi=DOUB(strtod(argv[12],NULL));                  // lower y bound
 pars.yf=DOUB(strtod(argv[13],NULL));                  // upper y bound
 pars.thresh=DOUB(strtod(argv[14],NULL));              // detection threshold
 pars.sposx=DOUB(strtod(argv[15],NULL));               // center x position of rectangle
 pars.sposy=DOUB(strtod(argv[16],NULL));               // center x position of rectangle
 pars.sposedge=DOUB(strtod(argv[17],NULL));            // edge of starting positions rectangle
 pars.dirdamp=DOUB(strtod(argv[18],NULL));             // directional damping
 pars.J=DOUB(strtod(argv[19],NULL));                   // coupling of internal state and attraction of agents
 int modu=int(strtod(argv[20],NULL));                  // which time step to write out
 parsFDF.Nfx=int(strtod(argv[21],NULL));               // number of grid points in CFD simulation
 parsFDF.Nfy=int(strtod(argv[22],NULL));               // number of grid points in CFD simulation
 parsFDF.fxi=DOUB(strtod(argv[23],NULL));              // fluid fields left x bound
 parsFDF.fxf=DOUB(strtod(argv[24],NULL));              // right x bound
 parsFDF.fyi=DOUB(strtod(argv[25],NULL));              // left y bound
 parsFDF.fyf=DOUB(strtod(argv[26],NULL));              // right y bound
 DOUB Dx=DOUB(strtod(argv[27],NULL));                  // Diffusion constant x
 DOUB Dy=DOUB(strtod(argv[28],NULL));                  // y
 DOUB Dangle=DOUB(strtod(argv[29],NULL));              // angle of direction 
 DOUB Dphi=DOUB(strtod(argv[30],NULL));                // internal state
 parsFDF.image_path = argv[31];                        // file if read
 parsFDF.kappa=DOUB(strtod(argv[32],NULL));            // gaussian smoothing parameter to interpolate pixels onto internal grid
 pars.dir_lag=DOUB(strtod(argv[33],NULL));             // directional phase lag parameter
 pars.J2=DOUB(strtod(argv[34],NULL));                  // directional sensitivity of coupling
 pars.J3=DOUB(strtod(argv[35],NULL));                  // velocity oscillation parameters
 pars.eps2=DOUB(strtod(argv[36],NULL));                // intercation strength of direction 

 parsFDF.rho=DOUB(strtod(argv[37],NULL));              // density of fluid 
 parsFDF.mu=DOUB(strtod(argv[38],NULL));               // viscosity of fluid
 parsFDF.beta=DOUB(strtod(argv[39],NULL));             // relaxation prameter of pressur solver
 parsFDF.toll=DOUB(strtod(argv[40],NULL));             // tolerance of pressur solver 
 parsFDF.dtf=DOUB(strtod(argv[41],NULL));              // dt of fluid solver (initial value)
 parsFDF.DTr1=DOUB(strtod(argv[42],NULL));             // Diffusion coefficients of first tracer
 
 pars.vel_c=DOUB(strtod(argv[43],NULL));               // adaption velocity to counter the mean flow 
 pars.eps3=DOUB(strtod(argv[44],NULL));                // interaction strength of direction and velocity (counter current swimming)
 parsFDF.pos_inlet=int(strtod(argv[45],NULL));         // position of inlet
 parsFDF.pos_outlet=int(strtod(argv[46],NULL));        // position of outlet
 parsFDF.width_inlet=int(strtod(argv[47],NULL));       // width of outlet
 parsFDF.width_outlet=int(strtod(argv[48],NULL));      // width of outlet
 parsFDF.vel_inlet=DOUB(strtod(argv[49],NULL));        // velocity maximum after rampoff at the center of the inlet
 parsFDF.length_outlet=int(strtod(argv[50],NULL));     // how long is the outlet channel
 DOUB T2=DOUB(strtod(argv[51],NULL));                  // sim time
 pars.freq_c=DOUB(strtod(argv[52],NULL));              // frequency_activity adaption
 pars.mem_damp=DOUB(strtod(argv[53],NULL));            // memory damping value
 pars.eps4=DOUB(strtod(argv[54],NULL));                // interaction strength of direction and velocity (counter current swimming)
 pars.counter_curent=DOUB(strtod(argv[55],NULL));      // switcher for counter-curent swimming
 pars.osc_activity=DOUB(strtod(argv[56],NULL));        // switcher for coupling of frequency and activity
 pars.dif_activity=DOUB(strtod(argv[57],NULL));        // switcher for coupling of diffusivity and activity
 pars.agent_atr=DOUB(strtod(argv[58],NULL));           // switcher for attraction of agents
 pars.seasons_switc=DOUB(strtod(argv[59],NULL));       // switcher for seasons
 pars.agent_rep=DOUB(strtod(argv[60],NULL));           // switcher for repulsion of agents
 pars.x_tracer=DOUB(strtod(argv[61],NULL));            // x position of tracer distribution
 pars.y_tracer=DOUB(strtod(argv[62],NULL));            // y position of tracer distribution
 pars.amp_tracer=DOUB(strtod(argv[63],NULL));          // amplitude of tracer density
 pars.agent_direction=DOUB(strtod(argv[64],NULL));     // switcher for directionality of agents (Q function that determines sensitivity of sensing)
 pars.Vs=DOUB(strtod(argv[65],NULL));                  // turning point paramter of counter-current swimming 
 pars.tr_dir3=DOUB(strtod(argv[66],NULL));             // switcher for tracer orientation 1 means on, 0 means off 
 pars.tr_dir_steepnes=DOUB(strtod(argv[67],NULL));     // 1/tr_dir_steepnes determines how steep the switching from 0 to 1 takes place when activity increases
 pars.eps5=DOUB(strtod(argv[68],NULL));                // interaction strength of direction and tracer
 pars.counter_current_act=DOUB(strtod(argv[69],NULL)); // switcher counter curent activity enhancement 1 means on, 0 means off 
 pars.tr_steepnes=DOUB(strtod(argv[70],NULL));         // turning point paramter of counter-current swimming direction adaption (switches off cc swimming and swimming towards prey)
 pars.act_steepnes_2=DOUB(strtod(argv[71],NULL));      // turning point parameter for switch off of gradient swimming
 pars.tr_dir2=DOUB(strtod(argv[72],NULL));             // switcher for counter-current swimming reduction when tracer is present 1 means loss of orientation, 
						       // 0 means no loss of orientation 
 pars.tr_dir1=DOUB(strtod(argv[73],NULL));             // switcher for vorticity avoidance 1 means avoidance changes with activity, 0 means avoidance is not affected by vorticity 
 
 
 //### initialize vectors ###//
 FDF.intensity.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);      //intensity (CFD grid with halo cells)
 FDF.intensity_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                         //intensity at positions of agents
 FDF.UX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                                //x-velocity at position of agents
 FDF.UY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                                //y-velocity at positions of agents
 FDF.Pr_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                                //pressur at position of agents
 FDF.vorticity_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                         //vorticity at position of agents
 FDF.vorticitygradX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                    //vorticity gradient X at position of agents
 FDF.vorticitygradY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                    //vorticity gradient Y at position of agents
 FDF.divergence_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                        //divergence at position of agents
 FDF.TrgradX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                           //tracer gradient at position of agents
 FDF.TrgradY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);                                                           //tracer gradient at position of agents
 FDF.gridX.resize(parsFDF.Nfx);                                                                                 //x positions in CFD grid (grid excluding boundary cells)
 FDF.gridY.resize(parsFDF.Nfy);                                                                                 //y positions in CFD grid (grid excluding boundary cells)
 FDF.PrgridX.resize(parsFDF.Nfx+2);                                                                             //x positions of Pressur in CFD grid (includes boundary cells 
													        //which are outside of the computational domain)
 FDF.PrgridY.resize(parsFDF.Nfy+2);                                                                             //y positions of Pressur in CFD grid
 FDF.UX.resize((parsFDF.Nfx+1)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+2)*parsFDF.length_outlet);             //x velocity of fluid in CFD grid with boundary cells
 FDF.UY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+1) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);             //x velocity of fluid TODO: 3D simulation
 FDF.Pr.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);             //pressur of fluid
 FDF.Pri.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);            //intermediate pressur of fluid
 FDF.Pri2.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);           //intermediate pressur no 2 of fluid
 parsFDF.UU.resize(parsFDF.Nfx+1);                                                                              //boundary X velocity of upper boundary
 parsFDF.UL.resize(parsFDF.Nfx+1);                                                                              //boundary X velocity of lower boundary
 parsFDF.ULE.resize(parsFDF.Nfy+2);                                                                             //boundary X velocity of left boundary
 parsFDF.UR.resize(parsFDF.Nfy+2);                                                                              //boundary X velocity of right boundary
 parsFDF.VU.resize(parsFDF.Nfx+2);                                                                              //boundary V velocity of upper boundary
 parsFDF.VL.resize(parsFDF.Nfx+2);                                                                              //boundary V velocity of lower boundary
 parsFDF.VLE.resize(parsFDF.Nfy+1);                                                                             //boundary V velocity of left boundary
 parsFDF.VR.resize(parsFDF.Nfy+1);                                                                              //boundary V velocity of right boundary
 FDF.vorticity.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);      //vorticity (CFD grid with halo cells)
 FDF.divergence.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);     //divergence (CFD grid with halo cells)
 FDF.TrgradX.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);        //tracer gradient X (CFD grid with halo cells)
 FDF.TrgradY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);        //tracer gradient Y (CFD grid with halo cells)
 FDF.vorticitygradX.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //gradient of vorticity X (CFD grid with halo cells)
 FDF.vorticitygradY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //gradient of vorticity Y (CFD grid with halo cells)
 
 pars.freq.resize(N);                           //internal state frequency
 vector<int> I4(4);                             //holds four indexes of X or Y position
 for(int k=0;k<N;k++)
  {
   pars.FDF_indexX.push_back(I4);               //index of closest grid point X direction
   pars.FDF_indexY.push_back(I4);               //index of closest grid point Y direction
   pars.FDF_indexX2.push_back(I4);              //index of closest grid point X direction passive agents
   pars.FDF_indexY2.push_back(I4);              //index of closest grid point Y direction passive agents
  }
 pars.intensity.resize(N);                      //interpolated intensity field for detection at positions of agents
 pars.intensity2.resize(N);                     //interpolated intensity field for detection at positions of passive agents
 pars.UXA.resize(N);                            //interpolated x velocity at position of agents
 pars.UYA.resize(N);                            //interpolated y velocity at position of agents 
 pars.UXA2.resize(N);                           //interpolated x velocity at position of passive agents
 pars.UYA2.resize(N);                           //interpolated y velocity at position of passive agents 
 pars.PrA.resize(N);                            //interpolated pressur at positions of agents
 pars.VorticityA.resize(N);                     //interpolated vorticity at positions of agents
 pars.VorticityA2.resize(N);                    //interpolated vorticity at positions of passive agents
 pars.Vorticity_gradXA.resize(N);               //interpolated vorticity at positions of agents
 pars.Vorticity_gradYA.resize(N);               //interpolated vorticity at positions of agents
 pars.TrgradXA.resize(N);                       //interpolated tracer X gradient at positions of agents
 pars.TrgradYA.resize(N);                       //interpolated tracer Y gradient at positions of agents
 int n(6);                                      //how many sets of variables x,y, Ornstein-Uhlenbeck noise, memory, direction angle, internal state (phi) =6*N
 Data.pos.resize(n*N);                          //positions x,y, O-U noise, memory, direction angle, internal state
 Data.pos2.resize(2*N);                         //positions x,y of passive tracers
 Data.vel.resize(2*N);                          //velocity of agents
 Data.vel2.resize(2*N);                         //velocity of passive agents
 pars.D.resize(n*N);                            //diffusion coefficients of noise
 Data.noise.resize(n*N);                        //noise components of dynamics

 //### initialize constants ###//
 parsFDF.dx=(parsFDF.fxf-parsFDF.fxi)/(parsFDF.Nfx);//x step
 parsFDF.dy=(parsFDF.fyf-parsFDF.fyi)/(parsFDF.Nfy);//y step
 cout << " set diffusion parameters \n";
 for(int k=0; k<N;k++)//diffusion coefficients
  {
   pars.D[2*k] = Dx;
   pars.D[2*k+1] = Dy;
   pars.D[(n-4)*N+k] = Dangle;//for Ornstein-Uhlenbeck noise in direction
   pars.D[(n-1)*N+k] = Dphi;
  }

 //### DEFINE FILE FOR PARAMETERS ###//
 cout << " output all model parameters \n"; 
 std::stringstream fouts2; fouts2 << "parameters" << argv[1];
 std::string name2 = fouts2.str(); ofstream fout2(name2,ios::out);
 fout2.setf(ios::scientific); fout2.precision(6);
 fout2 << pars.xi << "\n";
 fout2 << pars.xf << "\n";
 fout2 << pars.yi << "\n";
 fout2 << pars.yf << "\n";
 fout2 << "number of agents " << N << "\n";
 fout2 << "offset velocity " << pars.vel0 << "\n";
 fout2 << "interaction strength " << pars.eps << "\n";
 fout2 << "interaction radius " << pars.irad << "\n";
 fout2 << "mean turning frequency " << pars.freq1 << "\n";
 fout2 << "stdev turning frequency " << pars.freq2 << "\n";
 fout2 << "### singel frequencies of units \n";
 
 //cout << "seed frequencies \n";
 //### set frequency distribution ###//
 random_device generator;
 //default_random_engine generator;
 normal_distribution<DOUB> distribution(pars.freq1,pars.freq2);
 for(int k=0;k<pars.freq.size();k++)
  {
   pars.freq[k] = distribution(generator);
   fout2 << "unit " << k << " freq= " << pars.freq[k] << "\n";
  }
 fout2 << "thresh " << pars.thresh << "\n";
 fout2 << "minx init " << pars.sposx-0.5*pars.sposedge << "\n";
 fout2 << "maxx init " << pars.sposx+0.5*pars.sposedge << "\n";
 fout2 << "miny init " << pars.sposx-0.5*pars.sposedge << "\n";
 fout2 << "maxy init " << pars.sposx+0.5*pars.sposedge << "\n";
 fout2 << "J " << pars.J << "\n";
 fout2 << "fxi " << parsFDF.fxi << "\n";
 fout2 << "fxf " << parsFDF.fxf << "\n";
 fout2 << "fyi " << parsFDF.fyi << "\n";
 fout2 << "fyf " << parsFDF.fyf << "\n";
 fout2 << "dx " << parsFDF.dx << "\n";
 fout2 << "dy " << parsFDF.dy << "\n";
 for(int k=0;k<N;k++){fout2 << " agent" << k << "Dx,y= " << pars.D[2*k] << ", " << pars.D[2*k+1] << " Dangle= " << pars.D[(n-2)*N+k] << " Dphi= " << pars.D[(n-1)*N+k] << "\n";}
 fout2 << "if enabled: images are extracted from " << parsFDF.image_path << "\n";
 fout2 << parsFDF.kappa << " gaussian smoothing parameter to interpolate pixels onto internal grid \n";
 fout2 << pars.dir_lag << " directional phase lag parameter \n";
 fout2 << pars.J2 << " directional sensitivity of coupling \n";
 fout2 << pars.J3 << " velocity oscillation parameters \n";
 fout2 << pars.eps2 << " intercation strength of direction \n";
 fout2 << parsFDF.rho << " density of fluid \n";
 fout2 << parsFDF.mu << " viscosity of fluid \n";
 fout2 << parsFDF.beta << " relaxation prameter of pressur solver \n";
 fout2 << parsFDF.toll << " tolerance of pressur solver \n";
 fout2 << parsFDF.dtf << " dt of fluid solver (initial value) \n";
 fout2 << parsFDF.DTr1 << " Diffusion coefficients of first tracer \n";
 fout2 << pars.vel_c << " adaption velocity to counter the mean flow \n";
 fout2 << pars.eps3 << " interaction strength of direction and velocity (counter current swimming) \n";
 fout2 << parsFDF.pos_inlet << " position of inlet \n";
 fout2 << parsFDF.pos_outlet << " position of outlet \n";
 fout2 << parsFDF.width_inlet << " width of outlet \n";
 fout2 << parsFDF.width_outlet << " width of outlet \n";
 fout2 << parsFDF.vel_inlet << " velocity maximum after rampoff at the center of the inlet \n";
 fout2 << parsFDF.length_outlet << " how long is the outlet channel \n";
 fout2 << T2 << " sim time \n";
 fout2 << pars.freq_c << " frequency_activity adaption \n";
 fout2 << pars.mem_damp << " memory damping value \n";
 fout2 << pars.eps4 << " interaction strength of direction and velocity (counter current swimming) \n";
 fout2 << pars.counter_curent << " switcher for counter-curent swimming \n";
 fout2 << pars.osc_activity << " switcher for coupling of frequency and activity \n";
 fout2 << pars.dif_activity << " switcher for coupling of diffusivity and activity \n";
 fout2 << pars.agent_atr << " switcher for attraction of agents \n";
 fout2 << pars.seasons_switc << " switcher for seasons \n";
 fout2 << pars.agent_rep << " switcher for repulsion of agents \n";
 fout2 << pars.x_tracer << " x position of tracer distribution \n";
 fout2 << pars.y_tracer << " y position of tracer distribution \n";
 fout2 << pars.amp_tracer << " amplitude of tracer density \n";
 fout2 << pars.agent_direction << " switcher for directionality of agents (Q function that determines sensitivity of sensing) \n";
 fout2 << pars.Vs << " turning point paramter of counter-current swimming \n"; 
 fout2 << pars.tr_dir3 << " switcher for tracer orientation 1 means on, 0 means off \n";
 fout2 << pars.tr_dir_steepnes << " 1/tr_dir_steepnes determines how steep the switching from 0 to 1 takes place when activity increases \n";
 fout2 << pars.eps5 << " interaction strength of direction and tracer \n";
 fout2 << pars.counter_current_act << " switcher counter curent activity enhancement 1 means on, 0 means off \n"; 
 fout2 << pars.tr_steepnes << " turning point paramter of counter-current swimming direction adaption (switches off cc swimming and swimming towards prey) \n";
 fout2 << pars.act_steepnes_2 << " turning point parameter for switch off of gradient swimming \n"; 
 fout2 << pars.tr_dir2 << " switcher for counter-current swimming reduction when tracer is present 1 means loss of orientation, 0 means no loss of orientation \n";
 fout2 << pars.tr_dir1 << " switcher for vorticity avoidance 1 means avoidance changes with activity, 0 means avoidance is not affected by vorticity \n";

 fout2.close();

 //### CALCULATE REFERENCE OFFSET VALUE OF FORCE ###//TODO: check mechanism of spatial interaction in DGL!
 //NOTE: needed in dgl to calculate the effective interaction radius with continuous cutoff. not pars.cut which is the actual cutoff potential
 pars.offset = pars.agent_rep/(pars.irad*pars.irad) - 1.0/pars.irad;  

 cout << "seed initial positions \n";
 uniform_real_distribution<DOUB> distributionU(0.0,1.0);
 //### Set initial conditions of position and phase ###//
 DOUB x0(0.0), y0(0.0), d0(0.0), rep0(0.0);
 for(int k=0;k<N;k++)
  {
   rep0=0.0;
   while(rep0==0.0)//make sure that agents are not to close initially
    {
     x0 = pars.sposx-0.5*pars.sposedge + pars.sposedge*distributionU(generator); 
     y0 = pars.sposy-0.5*pars.sposedge + pars.sposedge*distributionU(generator); 
     Data.pos[2*k+1] = y0; 
     Data.pos[2*k] = x0;
     for(int l=0;l<N;l++)
      {
       d0= sqrt((Data.pos[2*k]-Data.pos[2*l])*(Data.pos[2*k]-Data.pos[2*l]) + (Data.pos[2*k+1]-Data.pos[2*l+1])*(Data.pos[2*k+1]-Data.pos[2*l+1]));
       if(d0<=0.1){rep0=1.0;}
      }
    } 
   Data.pos2[2*k] = x0;
   Data.pos2[2*k+1] = y0;
  }//x0,y0
  
 for(int k=0;k<N;k++){Data.pos[(n-2)*N+k] = 2.0*pi*distributionU(generator);}//angle of direction
 for(int k=0;k<N;k++){Data.pos[(n-1)*N+k] = 2.0*pi*distributionU(generator);}//internal state (phase)
 //for(int k=0;k<N;k++){cout << "posx[" << k <<"]= " << "posy[" << k <<"]= " << Data.pos[2*k]  << Data.pos[2*k+1] << " internal phase= " << Data.pos[(n-1)*N+k] << "\n";}
 
 //### Set initial noise components of agents dynamics ###//
 normal_distribution<DOUB> Snormal(0.0,1.0);
 for(int k=0;k<n*N;k++){Data.noise[k]=Snormal(generator);}//noise

 //### set CFD grid ###//
 //x direction
 cout << "init CFD grid \n";
 FDF.PrgridX[0] = parsFDF.fxi-0.5*parsFDF.dx;
 for(int k=0;k<parsFDF.Nfx;k++)//x grid
  {
   FDF.gridX[k] = parsFDF.fxi + (DOUB(k)+0.5)*parsFDF.dx;
   FDF.PrgridX[k+1] = parsFDF.fxi + (DOUB(k)+0.5)*parsFDF.dx;
  }
 FDF.PrgridX[parsFDF.Nfx+1] = parsFDF.fxi + (DOUB(parsFDF.Nfx)+0.5)*parsFDF.dx;

 //y direction
 FDF.PrgridY[0] = parsFDF.fyi-0.5*parsFDF.dy;
 for(int k=0;k<parsFDF.Nfy;k++)//y grid
  {
   FDF.gridY[k] = parsFDF.fyi + (DOUB(k)+0.5)*parsFDF.dy;
   FDF.PrgridY[k+1] = parsFDF.fyi + (DOUB(k)+0.5)*parsFDF.dy;
  }
 FDF.PrgridY[parsFDF.Nfy+1] = parsFDF.fyi + (DOUB(parsFDF.Nfy)+0.5)*parsFDF.dy;

 //set CFD initial conditions ###//
#ifdef CFD_field_from_integrator
 cout << "intensity from CFD field \n";
 DOUB distx(0.0), disty(0.0);
 //set tracers and pressur
 for(int k=0;k<parsFDF.Nfx+2;k++)
  {
   for(int l=0;l<parsFDF.Nfy+2;l++)
    {
     distx=(FDF.PrgridX[k]);
     disty=(FDF.PrgridY[l]);
     //### initial conditions of velocity and tracer ###//
#ifdef tracer_distribution
     FDF.intensity[k*(parsFDF.Nfy+2)+l] = exp(-(distx*distx+disty*disty)*0.2);
     //cout << "intensity at t=0: (k,l)= (" << k << ", " << l << "), I= " << FDF.intensity[k*(parsFDF.Nfy+2)+l] << "\n";
#endif
     FDF.Pr[k*(parsFDF.Nfy+2)+l] = 0.0;
    }
  }
 //set x-velocities
 for(int k=0;k<parsFDF.Nfx+1;k++)
  {
   for(int l=0;l<parsFDF.Nfy+2;l++)
    {
     FDF.UX[k*(parsFDF.Nfy+2)+l] = 0.0;
    }
  }
 //set y-velocities
 for(int k=0;k<parsFDF.Nfx+2;k++)
  {
   for(int l=0;l<parsFDF.Nfy+1;l++)
    {
     FDF.UY[k*(parsFDF.Nfy+1)+l] = 0.0;
    }
  }
#endif
#ifdef CFD_field_from_image
  cout << "intensity from images \n";
  stringstream image_name; image_name << parsFDF.image_path << ".0.dat"; // prepare full image name
  string img = image_name.str();                                         // set image name
  Interpolate_Image_to_Grid(img, FDF, parsFDF);                           // interpolate image data onto the grid as specified by parameters
#endif

 //### copy field values to agent grid ###//
#ifdef CFD_field_from_integrator
 CFD_grid_to_agent_grid(FDF, parsFDF);//the function copies the staggered grid values all to the nodes of pressur and omits the boundary cells. If necessary the average is used
#endif

 //### PERFORM SIMULATION FOR PARTICLES ###//
 DOUB t(0.0), tf(0.0);//time of agents simulation (main time) and time of fluid simulation (according to time-step control much shorter steps)
 int step(0), step_img(0);
 
 //### DEFINE FILES FOR OUTPUT ###//
 //agents
 std::stringstream fouts; fouts << argv[1];
 std::string name = fouts.str(); ofstream fout(name,ios::out);
 fout.setf(ios::scientific); fout.precision(6);
 //example agent
 std::stringstream fouts3; fouts3 << "Traject_" << argv[1];
 std::string name3 = fouts3.str(); ofstream fout3(name3,ios::out);
 fout3.setf(ios::scientific); fout3.precision(6);
// //fluid field (at t=0) in the main domain
 std::stringstream fouts4; fouts4 << "FDFS_" << argv[1];
 std::string name4 = fouts4.str(); ofstream fout4(name4,ios::out);
 fout4.setf(ios::scientific); fout4.precision(6);
// //fluid field (at t=0) in the outlet
 std::stringstream fouts5; fouts5 << "FDFS_Outlet_" << argv[1];
 std::string name5 = fouts5.str(); ofstream fout5(name5,ios::out);
 fout5.setf(ios::scientific); fout5.precision(6);
 
 //### Files: fout6: Inflow vector field at boundary                     ###//
 //           fout7: Inflow and outflow rate at inlet and outlet average 
 //           fout8: averaged y velocity as a function of x
 //           fout9: averaged x velocity as a function of y
 std::stringstream fouts6; fouts6 << "Volume_flow_" << argv[1];
 std::string name6 = fouts6.str(); ofstream fout6(name6,ios::out);
 fout6.setf(ios::scientific); fout6.precision(6);
 std::stringstream fouts7; fouts7 << "Volume_flow_out_" << argv[1];
 std::string name7 = fouts7.str(); ofstream fout7(name7,ios::out);
 fout7.setf(ios::scientific); fout7.precision(6);
 std::stringstream fouts8; fouts8 << "V_average_X_" << argv[1];
 std::string name8 = fouts8.str(); ofstream fout8(name8,ios::out);
 fout8.setf(ios::scientific); fout8.precision(6);
 std::stringstream fouts9; fouts9 << "U_average_Y_" << argv[1];
 std::string name9 = fouts9.str(); ofstream fout9(name9,ios::out);
 fout9.setf(ios::scientific); fout9.precision(6);

#ifdef CFD_field_from_integrator

//### Set maximum number of SOR steps ###//
 parsFDF.Npr =3000.0;

 //### Fluid transient ###//
 cout << "start simulation: transient \n";
 tf=-T;//start T time units before simulation
 while(tf<-0.1*T)
  {
   Integrator_FDFS_EULER(FDF, tf, parsFDF, T, T2);
   //### Calculate divergence field ###//
   Divergence_calc(FDF, parsFDF);
   //### Adjust tolerance of pressure convergence ###//
   Toll_set(parsFDF.toll, parsFDF.Npr, parsFDF.Nsor, FDF.divergence);         
  }

//### Set maximum number of SOR steps ###//
 parsFDF.Npr = 250000;

 while(tf<t)
  {
#ifdef Euler
   Integrator_FDFS_EULER(FDF, tf, parsFDF, T, T2);
#else
   Integrator_FDFS_HEUN(FDF, tf, parsFDF, T, T2);
#endif
   //### Calculate divergence field ###//
   Divergence_calc(FDF, parsFDF);
   //### Adjust tolerance of pressure convergence ###//
   Toll_set(parsFDF.toll, parsFDF.Npr, parsFDF.Nsor, FDF.divergence);         
  }

 //### Reset tracer distribution ###//
#ifdef tracer_distribution
 for(int k=0;k<parsFDF.Nfx+2;k++)
  {
   for(int l=0;l<parsFDF.Nfy+2;l++)
    {
     distx=(FDF.PrgridX[k]-pars.x_tracer);//-0.5*FDF.PrgridX[0]);
     disty=(FDF.PrgridY[l]-pars.y_tracer);
     //### initial conditions of velocity and tracer ###//
     FDF.intensity[k*(parsFDF.Nfy+2)+l] = pars.amp_tracer*exp(-(distx*distx+disty*disty)*0.4);
    }
  }
#endif

 //### Calculate divergence field ###//
 Divergence_calc(FDF, parsFDF);
 cout << "start agent simulation \n";

 //### Calculate vorticity field ###//
 Vorticity_calc(FDF, parsFDF);
 //### Calculate divergence field ###//
 Divergence_calc(FDF, parsFDF);
 //### Calculate gradient of tracer and vorticity ###//
 Grads(FDF, parsFDF);

 //### Transfer CFD values to Agent grid values ###//
 CFD_grid_to_agent_grid(FDF, parsFDF);
#endif

 //### find initial index of particle in CFD grid and field values  ###//
 FDF_index(Data.pos, N, FDF.gridX, FDF.gridY, pars.FDF_indexX, pars.FDF_indexY);//finds closest grid points of agents
 FDF_index(Data.pos2, N, FDF.gridX, FDF.gridY, pars.FDF_indexX2, pars.FDF_indexY2);//finds closest grid points of passive agents

 //### interpolate field values onto agent positions
 //### NOTE: Interpolates field values on pressure grid points inside the domain. these points are 0.5dt and 0.5dy shifted from the actual domain walls. Thus, the grid must be finer 
 //          than the distance of wall interaction, which keeps the agents inside the lattice of inner pressure points. 
 //          This is mandatory! If the domain is too sparse, the index finder 
 //          is unable to locate agents in the grid. Thus, there can be no interpolation of field values such that the simulation breaks down.
 Interpolator(Data.pos, pars.intensity, pars.FDF_indexX, pars.FDF_indexY, FDF.intensity_coupler, FDF.gridX, FDF.gridY, parsFDF);            //interpolate intensity
 Interpolator(Data.pos2, pars.intensity2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.intensity_coupler, FDF.gridX, FDF.gridY, parsFDF);        //interpolate intensity passive agents
 Interpolator(Data.pos, pars.UXA, pars.FDF_indexX, pars.FDF_indexY, FDF.UX_coupler, FDF.gridX, FDF.gridY, parsFDF);                         //interpolate velocity in x direction
 Interpolator(Data.pos, pars.UYA, pars.FDF_indexX, pars.FDF_indexY, FDF.UY_coupler, FDF.gridX, FDF.gridY, parsFDF);                         //interpolate velocity in y direction
 Interpolator(Data.pos2, pars.UXA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UX_coupler, FDF.gridX, FDF.gridY, parsFDF);                     //interpolate velocity in x 
																	    //direction (passive agents)
 Interpolator(Data.pos2, pars.UYA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UY_coupler, FDF.gridX, FDF.gridY, parsFDF);                     //interpolate velocity in y 
																	    //direction (passive agents)
 Interpolator(Data.pos, pars.PrA, pars.FDF_indexX, pars.FDF_indexY, FDF.Pr_coupler, FDF.gridX, FDF.gridY, parsFDF);                         //interpolate pressure
 Interpolator(Data.pos, pars.VorticityA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticity_coupler, FDF.gridX, FDF.gridY, parsFDF);           //interpolate vorticity
 Interpolator(Data.pos2, pars.VorticityA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.vorticity_coupler, FDF.gridX, FDF.gridY, parsFDF);       //interpolate vorticity passive agents
 Interpolator(Data.pos, pars.Vorticity_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity gradX
 Interpolator(Data.pos, pars.Vorticity_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity gradY
 Interpolator(Data.pos, pars.TrgradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradX_coupler, FDF.gridX, FDF.gridY, parsFDF);               //interpolate tracer gradient X
 Interpolator(Data.pos, pars.TrgradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradY_coupler, FDF.gridX, FDF.gridY, parsFDF);               //interpolate tracer gradient Y
 
 //print out t=0
 Data_Print(t, Data, N, n, pars, fout, fout3); //write out t=0 for agents
#ifdef FDF_out
 Data_print_FDF(t,FDF,parsFDF.Nfx, parsFDF.Nfy,fout4, 2*parsFDF.width_outlet+1,parsFDF.length_outlet,parsFDF.pos_outlet,fout5);//write out t=0 for fluid fields 
#endif

 fout3 << t << " " << Data.pos[0] << " " << Data.pos[1] << " " << Data.pos[(n-3)*N] << " " << Data.pos[(n-2)*N] << " " << Data.pos[(n-1)*N]; for(int k=0;k<4;k++){fout3 << " " << FDF.gridX[pars.FDF_indexX[0][k]] << " " << FDF.gridY[pars.FDF_indexY[0][k]];} fout3 << " " << pars.intensity[0] << " " << Data.vel[0] << " " << Data.vel[1] << " " << pars.UXA[0] << " " << pars.UYA[0] << " " << Data.pos[(n-3)*N] << " " << pars.TrgradXA[0] << " " << pars.TrgradYA[0] << " " << pars.VorticityA[0] << " " << pars.Vorticity_gradXA[0] << " " << pars.Vorticity_gradYA[0] << " " << Data.pos2[0] << " " << Data.pos2[1] << " " << Data.vel2[0] << " " << Data.vel2[1] << " " << Data.pos[(n-4)*N] << "\n";

  //### MAIN LOOP ###//
 while(t<T2)
  { 
   pt=clock();
   //### simulate new positions ###//
   rk2_stoch(dgl, Data.pos, 1, t, n*N, n, dt, pars, Data.noise, Data.vel);//2nd order stochastic Heun method (active agents)
   t -= dt;//avoid double stepping of t
   rk2(dgl2, Data.pos2, 1, t, n*N, n, dt, pars, Data.vel2);//2nd order Heun method (passive agents)
   t_agent += clock()-pt;//measure time of agent integration
#ifdef CFD_field_from_integrator
   //### update fluid fields ###//
   pt= clock();
   while(tf<t)
    {
#ifdef Euler
     Integrator_FDFS_EULER(FDF, tf, parsFDF, T, T2);
#else
     Integrator_FDFS_HEUN(FDF, tf, parsFDF, T, T2);
#endif
     //### Calculate divergence field ###//
     Divergence_calc(FDF,parsFDF);
     //### Check volume flow ###//
     In_flow_outflow(parsFDF, FDF, tf, T, fout6, fout7, fout8, fout9);
     //### Adjust tolerance of pressure convergence ###//
     Toll_set(parsFDF.toll, parsFDF.Npr, parsFDF.Nsor, FDF.divergence);
    }
   //### Calculate divergence field ###//
   Divergence_calc(FDF,parsFDF);
   //### Calculate vorticity field ###//
   Vorticity_calc(FDF,parsFDF);
   //### Calculate gradient of tracer and vorticity ###//
   Grads(FDF, parsFDF);
   t_cfd += clock() - pt;//measure time of CFD solver
   //cout << "t_cfd= " << t_cfd << "\n";
#endif
#ifdef CFD_field_from_image_dynamic
   //### Update field from png image ###//
   step_img +=1;
   image_name << parsFDF.image_path << "." << step_img << ".dat"; // prepare full image name
   img = image_name.str();
   Interpolate_Image_to_Grid(img,FDF,parsFDF);
#endif

   //### copy field values to agent grid (CFD grid positions of pressur without boundary cells ###//
   pt=clock();//measure time of agents coupling to CFD
#ifdef CFD_field_from_integrator
   CFD_grid_to_agent_grid(FDF, parsFDF);
#endif
   //### interpolate fields onto positions ###//
   Update_FDF_index(Data.pos, N, FDF.gridX, FDF.gridY, pars.FDF_indexX, pars.FDF_indexY);   //active agents
   Update_FDF_index(Data.pos2, N, FDF.gridX, FDF.gridY, pars.FDF_indexX2, pars.FDF_indexY2);//passive agents

   //### NOTE: Interpolates field values on pressure grid points inside the domain. these points are 0.5dt and 0.5dy shifted from the actual domain walls. 
   //          Thus, the grid must be finer than the distance of wall interaction, which keeps the agents inside the lattice of inner pressure points. 
   //          This is mandatory! If the domain is too sparse, the index finder 
   //          is unable to locate agents in the grid. Thus, there can be no interpolation of field values such that the simulation breaks down.
   Interpolator(Data.pos, pars.intensity, pars.FDF_indexX, pars.FDF_indexY, FDF.intensity_coupler, FDF.gridX, FDF.gridY, parsFDF);
   Interpolator(Data.pos2, pars.intensity2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.intensity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate intensity passive agents
   Interpolator(Data.pos, pars.UXA, pars.FDF_indexX, pars.FDF_indexY, FDF.UX_coupler, FDF.gridX, FDF.gridY, parsFDF);//active agents
   Interpolator(Data.pos, pars.UYA, pars.FDF_indexX, pars.FDF_indexY, FDF.UY_coupler, FDF.gridX, FDF.gridY, parsFDF);//active
   Interpolator(Data.pos2, pars.UXA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UX_coupler, FDF.gridX, FDF.gridY, parsFDF);//passive agents
   Interpolator(Data.pos2, pars.UYA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UY_coupler, FDF.gridX, FDF.gridY, parsFDF);//passive
   Interpolator(Data.pos, pars.PrA, pars.FDF_indexX, pars.FDF_indexY, FDF.Pr_coupler, FDF.gridX, FDF.gridY, parsFDF);
   Interpolator(Data.pos, pars.VorticityA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity
   Interpolator(Data.pos2, pars.VorticityA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.vorticity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity passive agents
   Interpolator(Data.pos, pars.Vorticity_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity gradX
   Interpolator(Data.pos, pars.Vorticity_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity gradY
   Interpolator(Data.pos, pars.TrgradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate tracer gradient X
   Interpolator(Data.pos, pars.TrgradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate tracer gradient Y
   t_coupling += clock() - pt;

   //Interpolator(Data.pos, pars.intensity, pars.FDF_indexX, pars.FDF_indexY, FDF.intensity, FDF.gridX, FDF.gridY, parsFDF);//TODO: !!! Intensity has larger aray now correct!!!
   //### update stochastic components ###//
   for(int k=0;k<n*N;k++){Data.noise[k]=Snormal(generator);}//noise
   //### Check for errors in data ###//
   ERROR_OUT(Data, N, pars);
   //### Print data to files ###//
   step+=1;
   if(step%modu==0)
    {
     Data_Print(t, Data, N, n, pars, fout, fout3); //cout << "### write out at t= " << t << "\n";
#ifdef CFD_field_from_integrator
#ifdef FDF_out
     Data_print_FDF(t,FDF,parsFDF.Nfx, parsFDF.Nfy,fout4, 2*parsFDF.width_outlet+1,parsFDF.length_outlet,parsFDF.pos_outlet,fout5); //write out fluid fields for closest moment in time
#endif
#endif
#ifdef CFD_field_from_image_dynamic
#ifdef FDF_out
     Data_print_FDF(t,FDF,parsFDF.Nfx, parsFDF.Nfy,fout4, 2*parsFDF.width_outlet+1,parsFDF.length_outlet,parsFDF.pos_outlet,fout5); //write out fluid fields for closest moment in time
#endif
#endif
     fout3 << t << " " << Data.pos[0] << " " << Data.pos[1] << " " << Data.pos[(n-3)*N] << " " << Data.pos[(n-2)*N] << " " << Data.pos[(n-1)*N]; for(int k=0;k<4;k++){fout3 << " " << FDF.gridX[pars.FDF_indexX[0][k]] << " " << FDF.gridY[pars.FDF_indexY[0][k]];} fout3 << " " << pars.intensity[0] << " " << Data.vel[0] << " " << Data.vel[1] << " " << pars.UXA[0] << " " << pars.UYA[0] << " " << Data.pos[(n-3)*N] << " " << pars.TrgradXA[0] << " " << pars.TrgradYA[0] << " " << pars.VorticityA[0] << " " << pars.Vorticity_gradXA[0] << " " << pars.Vorticity_gradYA[0] << " " << Data.pos2[0] << " " << Data.pos2[1] << " " << Data.vel2[0] << " " << Data.vel2[1] << " " << Data.pos[(n-4)*N] << "\n";
     
    }//off if step
  }//off while
 
 fout.close();
 fout3.close();
 fout4.close();//comment out if several files are used
 fout5.close();
 fout6.close();
 fout7.close();
 fout8.close();
 fout9.close();

 endtime=clock()/CLOCKS_PER_SEC;
 //starttime=omp_get_wtime();
 t_agent /= CLOCKS_PER_SEC;
 t_cfd /= CLOCKS_PER_SEC;
 t_coupling /= CLOCKS_PER_SEC;
 cout << "t_f= " << endtime << "\n";
 cout << "Process time= " <<  endtime-starttime << "\n";
 cout << "agent time= " << t_agent << "\n";
 cout << "CFD time= " << t_cfd << "\n";
 cout << "coupling time= " << t_coupling << "\n";
 cout << "percentage of time: cfd= " << DOUB(t_cfd)/DOUB(endtime-starttime) << " agents= " << DOUB(t_agent)/DOUB(endtime-starttime) << " coupling= " << DOUB(t_coupling)/DOUB(endtime-starttime) << "\n"; 

 return 0;  
}
