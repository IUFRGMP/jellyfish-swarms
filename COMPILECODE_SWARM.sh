 HEADERPATH='HEADERS/'
 CODEPATH='CODE/'

  #g++ $HEADERPATH/baseInclude.h $HEADERPATH/StructDefinitions.h $CODEPATH/Main.cpp -std=c++11 -O2 -o swarming_OMP             # sequential 
  g++ $HEADERPATH/baseInclude.h $HEADERPATH/StructDefinitions.h $CODEPATH/Main.cpp -std=c++11 -O2 -fopenmp -o swarming_OMP    # OMP parallel
