import sys
#import numpy as np
import math
import copy

### functions ###
def Mean_X(X):
    MX=0.0
    for k in range(len(X)):
        MX += X[k]
    MX /= float(len(X))
    return MX

def Var_X2(X,Y):
    MX = Mean_X(X)
    MY = Mean_X(Y)
    V = 0.0
    for k in range(len(X)):
        V += (X[k]-MX)**2. + (Y[k]-MY)**2.
    return V/(float(len(X)))

def Correlation_XY(X,Y):
    C=0.0
    #mean values
    MX = Mean_X(X)#mean x
    MY = Mean_X(Y)#mean y
    XY = []
    X2 = []
    Y2 = []
    for k in range(len(X)):
        XY.append(X[k]*Y[k])
        X2.append(X[k]*X[k])
        Y2.append(Y[k]*Y[k])
    MXY = Mean_X(XY)#mean xy
    MX2 = Mean_X(X2)#mean x^2
    MY2 = Mean_X(Y2)#mean y^2
    if(((MX2-MX*MX)*(MY2-MY*MY))<1e-10):
        return (MXY-MX*MY)/0.0000000001
    else:
        return (MXY-MX*MY)/((math.sqrt(MX2-MX*MX))*(math.sqrt(MY2-MY*MY)))

def Orderparameter_n(X,n):
    S=0.0
    C=0.0
    for k in range(len(X)):
        S += math.sin(float(n)*X[k])
        C += math.cos(float(n)*X[k])
    return math.sqrt(S**2.+C**2)/(float(len(X)))

def OrderparameterX_n(X,n):
    C=0.0
    for k in range(len(X)):
        C += math.cos(float(n)*X[k])/float(len(X))
    return C

def OrderparameterY_n(X,n):
    S=0.0
    for k in range(len(X)):
        S += math.sin(float(n)*X[k])/float(len(X))
    return S

def mymod(x,y):
    return x - math.floor(x/y)*y
    
def Orderparameter_phase_n(X,n):
    S=0.0
    C=0.0
    for k in range(len(X)):
        S += math.sin(float(n)*X[k])/float(len(X))
        C += math.cos(float(n)*X[k])/float(len(X))
    return mymod(math.atan2(S,C),2.0*math.pi)

def Unwrapper(phi,psi,count):
    if phi-psi>math.pi:
        count -= 1.0 
    if phi-psi<-math.pi:
        count += 1.0
    return count

def Char_fkt(X,thresh):
    if X>thresh:
        return 1.0
    else:
        return 0.0

def mean_hex_order_calc(X,Y,n):
    hex_order=[]
    for k in range(len(X)):
        dist2D=[]
        dist2D_save=[]
        for l in range(len(X)):
            if(l!=k):
                dist2D.append(math.sqrt((X[k]-X[l])**2. + (Y[k]-Y[l])**2.))
        dist2D_save=copy.deepcopy(dist2D)
        #print("all distances before are: ")
        #for l in range(len(dist2D)):
        #    print(dist2D_save[l])

        hex_ind=[]
        while len(hex_ind)<6:
            l_opt=0
            min_d=100000.0
            for l in range(len(dist2D)):
                if(dist2D[l]<min_d and dist2D[l]>0.0):
                    min_d=dist2D[l]
                    l_opt=copy.deepcopy(l)
            hex_ind.append(copy.deepcopy(l_opt))
            #print("smallest index= " + str(l_opt))

            del2D=copy.deepcopy(dist2D[l_opt])
            for l in range(len(dist2D)):
                dist2D[l] -= del2D
            dist2D[l_opt] -= del2D
            #print("if"+ str(l) + " " + str(l_opt))
            #print("all distances at are: ")
            #for l in range(len(dist2D)):
            #    print(dist2D[l])
            
 #       print("nearest distances are:")
  #      print(str(dist2D_save[hex_ind[0]]) + " " + str(hex_ind[0]))       
   #     print(str(dist2D_save[hex_ind[1]]) + " " + str(hex_ind[1]))     
   #     print(str(dist2D_save[hex_ind[2]]) + " " + str(hex_ind[2]))
    #    print(str(dist2D_save[hex_ind[3]]) + " " + str(hex_ind[3]))
     #   print(str(dist2D_save[hex_ind[4]]) + " " + str(hex_ind[4]))
      #  print(str(dist2D_save[hex_ind[5]]) + " " + str(hex_ind[5]))

        ### find hexagonal angles and summ them up ###
        hex_sum_cos=0.0 
        hex_sum_sin=0.0 
        dX=0.0
        dY=0.0
        for l in range(len(hex_ind)):
            dX= X[hex_ind[l]]-X[k]
            dY= Y[hex_ind[l]]-Y[k]
            angl=math.atan2(dY,dX)
            hex_sum_cos += math.cos(n*angl)
            hex_sum_sin += math.sin(n*angl)
        hex_order.append(math.sqrt((hex_sum_cos**2.+hex_sum_sin**2.))/6.0)
    return Mean_X(hex_order)

### input arguments ###
path = str(sys.argv[1]) #./ #path of reading
nameFDF= str(sys.argv[2]) #"World_"    <t>_World.<#>.dat" #file name head
en=int(sys.argv[3]) #exp number
tm=int(sys.argv[4]) #maximum time
TR=float(sys.argv[5]) #threshold for characteristic function

f2=open("Corelations_"+nameFDF+str(en)+".dat","w")
print ("perform analysis")
### extract data for averaging ###
Var0=1
V0X=0.0
V0X2=0.0
V0Y=0.0
V0Y2=0.0
MX0=0.0
MY0=0.0
MX20=0.0
MY20=0.0

#define initial counter for unwrappings
count36=0.0
count37=0.0
count38=0.0
count39=0.0

countDa=[]
countDa2=[]
countDfl=[]
countDfl2=[]
countDF=[]
countDV=[]

#define initial old phases
psi36=0.0
psi37=0.0
psi38=0.0
psi39=0.0

psiDa=[]
psiDa2=[]
psiDfl=[]
psiDfl2=[]
psiDV=[]
psiDF=[]

#define intiail unwrapped phases
Daur=[]
Da2ur=[]
Dflur=[]
Dfl2ur=[]
DVur=[]
DFur=[]

for ti in range(1,tm):
    t=0.1*float(ti)
    if ti%10==0:
        t = ti/10
    print path+nameFDF+str(t)+"_World."+str(en)+".dat"
    f=open(path+nameFDF+str(t)+"_World."+str(en)+".dat","r")
    
    ### read in x,y, u,v for one time step ###
    X=[]        # position x
    Y=[]        # position y
    Act=[]      # activity 
    theta=[]    # orientation
    phi=[]      # internal state
    F=[]        # tracer intensity active agents
    F2=[]       # tracer intensity passive agents
    Vort=[]     # vorticity 
    Vort2=[]    # vorticity passive tracer
    U=[]        # velocity x
    V=[]        # velocity y
    Uf=[]       # velocity x fluid active tracers
    Vf=[]       # velocity y fluid active tracers
    Uf2=[]      # velocity x fluid passive tracers
    Vf2=[]      # velocity y fluid passive tracers
    GxF=[]      # gradient x tracer
    GyF=[]      # gradient y tracer
    GxVort=[]   # gradient x vorticity
    GyVort=[]   # gradient y vorticity
    ###passive tracers###
    X2=[]
    Y2=[]
    U2=[]
    V2=[]
    ###directions###
    Da=[]#active
    Da2=[]#passive
    Dfl=[]#direction of fluid
    Dfl2=[]
    DV=[]#direction of vortex gradient
    DF=[]#direction of tracer gradient
    ###characteristic functions###
    CF=[]
    CF2=[]
    CVort=[]
    CVort2=[]
    #CX=[]
    #CX2=[]
    #CF_fix=[]
    #CF2_fix=[]

    #k=0
    #Xval=0.0
    for lines in f:
        l=lines.split()
        if len(l)>0:
            tsim=float(l[0]) #time
            X.append(float(l[1]))
            Y.append(float(l[2]))
            Act.append(float(l[3]))
            theta.append(float(l[4]))
            phi.append(float(l[5]))
            F.append(float(l[6]))
            Vort.append(abs(float(l[7])))
            U.append(float(l[8]))
            V.append(float(l[9]))
            Uf.append(float(l[10]))
            Vf.append(float(l[11]))
            GxF.append(float(l[12]))
            GyF.append(float(l[13]))
            GxVort.append(float(l[14]))
            GyVort.append(float(l[15]))
            ### passive tracers ###
            X2.append(float(l[16]))
            Y2.append(float(l[17]))
            U2.append(float(l[18]))
            V2.append(float(l[19]))
            Uf2.append(float(l[20]))
            Vf2.append(float(l[21]))
            #22 not used this is O-U noise
            F2.append(float(l[23]))
            Vort2.append(abs(float(l[24])))
            ### directions of agents-flow fluid ###
            Da.append(mymod(math.atan2((float(l[9])),(float(l[8]))),2.0*math.pi))#direction of active agents
            Da2.append(mymod(math.atan2((float(l[19])),(float(l[18]))),2.0*math.pi))#direction of passive agents
            Dfl.append(mymod(math.atan2((float(l[11])),(float(l[10]))),2.0*math.pi))#direction of fluid active agents
            Dfl2.append(mymod(math.atan2((float(l[21])),(float(l[20]))),2.0*math.pi))#direction of fluid passive agents
            ### directions of agents-flow vorticity ###
            DV.append(mymod(math.atan2((float(l[15])),(float(l[14]))),2.0*math.pi))#direction of vortex gradient
            ### directions of agents-flow tracer ###
            DF.append(mymod(math.atan2((float(l[13])),(float(l[12]))),2.0*math.pi))#direction of tracer gradient
            ### characteristic funcions ###
            CF.append(Char_fkt(float(l[6]),TR))
            CF2.append(Char_fkt(float(l[23]),TR))
            CVort.append(Char_fkt(abs(float(l[7])),TR))
            CVort2.append(Char_fkt(abs(float(l[24])),TR))
            # characteristic functions for tracer search #
            #CX.append(Char_fkt(abs(float(l[1])),0.0))# chracteristic function of zero line crossing active
            #CX2.append(Char_fkt(abs(float(l[16])),0.0))# passive
            #CF_fix.append(Char_fkt(float(l[6]),TR))# characteristic function of tracer distribution with fixed threshold active
            #CF2_fix.append(Char_fkt(float(l[23]),TR))# passive

    f.close()
    ### unwrapping single angles of direction (not orientation) ###
    if(Var0==1):
        for k in range(len(Da)):
            countDa.append(0.0)
            countDa2.append(0.0)
            countDfl.append(0.0)
            countDfl2.append(0.0)
            countDF.append(0.0)
            countDV.append(0.0)
            psiDa.append(0.0)#old phases
            psiDa2.append(0.0)
            psiDfl.append(0.0)
            psiDfl2.append(0.0)
            psiDF.append(0.0)
            psiDV.append(0.0)
            Daur.append(0.0)#unwrapped phase
            Da2ur.append(0.0)
            Dflur.append(0.0)
            Dfl2ur.append(0.0)
            DVur.append(0.0)
            DFur.append(0.0)
            
        for k in range(len(Da)):#save old phases
            psiDa[k]=copy.deepcopy(Da[k])
            psiDa2[k]=copy.deepcopy(Da2[k])
            psiDfl[k]=copy.deepcopy(Dfl[k])
            psiDfl2[k]=copy.deepcopy(Dfl2[k])
            psiDF[k]=copy.deepcopy(DF[k])
            psiDV[k]=copy.deepcopy(DV[k])
            
    for k in range(len(Da)):#determine unwrapping counter
        countDa[k] = Unwrapper(Da[k],psiDa[k],countDa[k])
        countDa2[k] = Unwrapper(Da2[k],psiDa2[k],countDa2[k])
        countDfl[k] = Unwrapper(Dfl[k],psiDfl[k],countDfl[k])
        countDfl2[k] = Unwrapper(Dfl2[k],psiDfl2[k],countDfl2[k])
        countDF[k] = Unwrapper(DV[k],psiDV[k],countDF[k])
        countDV[k] = Unwrapper(DF[k],psiDF[k],countDV[k])
            
    for k in range(len(Da)):#determine unwrapped phases
        Daur[k] = 2.0*math.pi*countDa[k] + Da[k]
        Da2ur[k] = 2.0*math.pi*countDa2[k] + Da2[k]
        Dflur[k] = 2.0*math.pi*countDfl[k] + Dfl[k]
        Dfl2ur[k] = 2.0*math.pi*countDfl2[k] + Dfl2[k]
        DVur[k] = 2.0*math.pi*countDa[k] + DV[k]
        DFur[k] = 2.0*math.pi*countDa[k] + DF[k]

    for k in range(len(Da)):#save old phases
        psiDa[k]=copy.deepcopy(Da[k])
        psiDa2[k]=copy.deepcopy(Da2[k])
        psiDfl[k]=copy.deepcopy(Dfl[k])
        psiDfl2[k]=copy.deepcopy(Dfl2[k])
        psiDV[k]=copy.deepcopy(DV[k])
        psiDF[k]=copy.deepcopy(DF[k])

    ### difference of angels ###
    Dafl=copy.deepcopy(Daur)
    Dafl2=copy.deepcopy(Da2ur)
    DbV=copy.deepcopy(Daur)
    DcF=copy.deepcopy(Daur)
    for k in range(len(Dafl2)):#calculate differences of angles
        Dafl[k] -= Dflur[k]
        Dafl2[k] -= Dfl2ur[k]
        DbV[k] -= DVur[k]
        DcF[k] -= DFur[k]

    ### Correlation analysis ###
    ###positions###
    C1 = Correlation_XY(X,X2)      #X-X2 correlation
    C2 = Correlation_XY(Y,Y2)      #Y-Y2 correlation
    ###velocities###
    C3 = Correlation_XY(U,Uf)      #X velocity correlation
    C4 = Correlation_XY(V,Vf)      #Y velocity correlation
    C5 = Correlation_XY(U2,Uf2)    #X2 velocity correlation
    C6 = Correlation_XY(V2,Vf2)    #Y2 velocity correlation
    ###variances of positions###
    C7 = 0.5*Var_X2(X,X)           #variance of swarm in x-direction
    C8 = 0.5*Var_X2(Y,Y)           #variance of swarm in y-direction
    C9 = 0.5*Var_X2(X2,X2)         #variance of swarm in x-direction passive tracers
    C10 = 0.5*Var_X2(Y2,Y2)        #variance of swarm in y-direction passive tracers
    if (Var0==1):                  #normalizations
        V0X=Var_X2(X,X)
        V0X2=Var_X2(X2,X2)
        V0Y=Var_X2(Y,Y)
        V0Y2=Var_X2(Y2,Y2)
        MX0=Mean_X(X)
        MY0=Mean_X(Y)
        MX20=Mean_X(X2)
        MY20=Mean_X(Y2)
        Var0=0
        ### orientations-fluid synchrony of swarm angle-field angle at t=0 ###
        psi36 = Orderparameter_phase_n(Dafl,1)
        psi37 = Orderparameter_phase_n(Dafl2,1)
        psi38 = Orderparameter_phase_n(DbV,1)
        psi39 = Orderparameter_phase_n(DcF,1)
     
    ### correlation of positions with tracer>TR and position ###
    C11 = Correlation_XY(F,CF)           #tracer value is larger than TR
    C12 = Correlation_XY(F2,CF2)         #tracer value is larger than TR passive agents
    ### correlations of position with Vorticity>TR and position ###
    C13 = Correlation_XY(Vort,CVort)     #Vorticity value is larger than TR
    C14 = Correlation_XY(Vort2,CVort2)   #Vorticity value is larger than TR passive agents
    ### correlations of activity and tracer/vorticty ###
    C15 = Correlation_XY(Act,F)    #activity-tracer correlation
    C16 = Correlation_XY(Act,Vort) #activity-vorticity correlation
    C17 = Correlation_XY(Vort,F)   #Vorticity-tracer correlation
    ### order parameters ###
    C18 = Orderparameter_n(phi,1)  #1st order Kuramoto parameter phi
    C19 = Orderparameter_n(theta,1)#1st order Kuramoto parameter theta
    C20 = Orderparameter_n(phi,2)  #2nd order Kuramoto parameter phi 
    C21 = Orderparameter_n(theta,2)#2nd order Kuramoto parameter theta
    C22 = Orderparameter_n(phi,3)  #3rd order Kuramoto parameter phi
    C23 = Orderparameter_n(theta,3)#3rd order Kuramoto parameter theta
    ### orientations-fluid synchrony of the swarm ###
    C24 = Orderparameter_n(Dafl,1)   #1st order Kuramoto parameter theta active fluid
    C25 = Orderparameter_n(Dafl2,1)  #1st order Kuramoto parameter theta passive fluid
    C26 = Orderparameter_n(DbV,1)    #1st order Kuramoto parameter theta active vorticity
    C27 = Orderparameter_n(DcF,1)    #1st order Kuramoto parameter theta active tracer
    ### mean positions of swarm and swarm averaged velocity ###
    C28 = Mean_X(X)                #CM X
    C29 = Mean_X(Y)                #CM Y
    C30 = Mean_X(X2)               #CM X2
    C31 = Mean_X(Y2)               #CM Y2
    C32 = Mean_X(U)                #CM U
    C33 = Mean_X(V)                #CM V
    C34 = Mean_X(U2)               #CM U2
    C35 = Mean_X(V2)               #CM V2
    ### orientations-fluid synchrony of swarm angle-field angle ###    

    C36 = Orderparameter_phase_n(Dafl,1)
    C37 = Orderparameter_phase_n(Dafl2,1)
    C38 = Orderparameter_phase_n(DbV,1)
    C39 = Orderparameter_phase_n(DcF,1)

    C40 = Mean_X(Vort)             #mean vorticitY
    #print ("mean active vorticity= " + str(C40))
    C41 = Mean_X(F)                #mean traceR
    C42 = Mean_X(Vort2)            #mean vorticitY
    #print ("mean passive vorticity= " + str(C42))
    C43 = Mean_X(F2)               #mean traceR
    for k in range(len(CF)):
        CF[k] = Char_fkt(F[k],C43)         #calculate chracteristic function with threshold of quantity from mean of passive tracers
        CF2[k] = Char_fkt(F2[k],C43)       #calculate chracteristic function with threshold of quantity from mean of passive tracers
        CVort[k] = Char_fkt(Vort[k],C42)   #calculate chracteristic function with threshold of quantity from mean of passive tracers
        CVort2[k] = Char_fkt(Vort2[k],C42) #calculate chracteristic function with threshold of quantity from mean of passive tracers

    C44 = Mean_X(CF)               #percentage of agents in areas of tracer
    C45 = Mean_X(CF2)              #percentage of agents in areas of tracer passive
    C46 = Mean_X(CVort)            #percentage vorticity
    C47 = Mean_X(CVort2)           #percentage vorticity 2

    ### unwrapping collective angles ###
    count36 = Unwrapper(C36,psi36,count36)
    count37 = Unwrapper(C37,psi37,count37)
    count38 = Unwrapper(C38,psi38,count38)
    count39 = Unwrapper(C39,psi39,count39)
    
    C361=2.0*math.pi*count36 + C36
    C371=2.0*math.pi*count37 + C37
    C381=2.0*math.pi*count38 + C38
    C391=2.0*math.pi*count39 + C39
    
    ### old collective angles ###
    psi36 = copy.deepcopy(C36)
    psi37 = copy.deepcopy(C37)
    psi38 = copy.deepcopy(C38)
    psi39 = copy.deepcopy(C39)    
    
    ### Hexagonal order analysis ###
    C48 = mean_hex_order_calc(X,Y,3.0)   #ensemble mean of the hexagonal order paramter
    C49 = mean_hex_order_calc(X2,Y2,3.0) #ensemble mean of the hexagonal order paramter passive agents
    C50 = mean_hex_order_calc(X,Y,4.0)   #ensemble mean of the hexagonal order paramter
    C51 = mean_hex_order_calc(X2,Y2,4.0) #ensemble mean of the hexagonal order paramter passive agents
    C52 = mean_hex_order_calc(X,Y,5.0)   #ensemble mean of the hexagonal order paramter
    C53 = mean_hex_order_calc(X2,Y2,5.0) #ensemble mean of the hexagonal order paramter passive agents
    C54 = mean_hex_order_calc(X,Y,6.0)   #ensemble mean of the hexagonal order paramter
    C55 = mean_hex_order_calc(X2,Y2,6.0) #ensemble mean of the hexagonal order paramter passive agents

    f2.write(str(tsim) + " " + str(C1) + " " + str(C2) + " " + str(C3) + " " + str(C4) + " " + str(C5) + " " + str(C6) + " " + str(C7) + " " + str(C8) + " " + str(C9) + " " + str(C10) + " " + str(C11) + " " + str(C12) + " " + str(C13) + " " + str(C14) + " " + str(C15) + " " + str(C16) + " " + str(C17) + " " + str(C18) + " " + str(C19) + " " + str(C20) + " " + str(C21) + " " + str(C22) + " " + str(C23) + " " + str(C24) + " " + str(C25) + " " + str(C26) + " " + str(C27) + " " + str(V0X) + " " + str(V0X2) + " " + str(V0Y) + " " + str(V0Y2) + " " + str(C28) + " " + str(C29) + " " + str(C30) + " " + str(C31) + " " + str(C32) + " " + str(C33) + " " + str(C34) + " " + str(C35) + " " + str(C361) + " " + str(C371) + " " + str(C381) + " " + str(C391) + " " + str(OrderparameterX_n(Dafl,1)) + " " + str(OrderparameterY_n(Dafl,1)) + " " + str(OrderparameterX_n(Dafl2,1)) + " " + str(OrderparameterY_n(Dafl2,1)) + " " + str(OrderparameterX_n(DbV,1)) + " " + str(OrderparameterY_n(DbV,1)) + " " + str(OrderparameterX_n(DcF,1)) + " " + str(OrderparameterY_n(DcF,1)) + " " + str(C40) + " " + str(C41) + " " + str(C42) + " " + str(C43) + " " + str(C44) + " " + str(C45) + " " + str(C46) + " " + str(C47) + " " + str(C48) + " " + str(C49) + " " + str(C50) + " " + str(C51) + " " + str(C52) + " " + str(C53) + " " + str(C54) + " " + str(C55) + "\n")

f2.close()
