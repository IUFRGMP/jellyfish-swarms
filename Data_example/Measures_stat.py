import sys
#import numpy as np
import math
import copy

### functions ###
def Mean_X(X):
    MX=0.0
    for k in range(len(X)):
        MX += X[k]
    MX /= float(len(X))
    return MX

def Var_X2(X,Y):
    MX = Mean_X(X)
    MY = Mean_X(Y)
    V = 0.0
    for k in range(len(X)):
        V += (X[k]-MX)**2. + (Y[k]-MY)**2.
    return V/(float(len(X)))

def Smoothing_Kernel(x,X,kappa):      #X is data position, x is auxiliary position, kappa is smoothing parameter
        return 1.0/(math.sqrt(math.pi*kappa))*math.exp(-(X-x)*(X-x)/kappa)  #gaussian bell shaped curve normalized to unity

def mymod(x,y):
    return x - math.floor(x/y)*y

### input arguments ###
path = str(sys.argv[1]) #./ #path of reading
nameFDF= str(sys.argv[2]) #"World_"    <t>_World.<#>.dat" #file name headnameFDF2= str(sys.argv[3]) #"World_"    <t>_World.<#>.dat" #file name head
nameFDF2= str(sys.argv[3]) #"World_"    <t>_World.<#>.dat" #file name headnameFDF2= str(sys.argv[3]) #"World_"    <t>_World.<#>.dat" #file name head
en=int(sys.argv[4]) #exp number start
tm=int(sys.argv[5]) #maximum time
kappa=float(sys.argv[6]) #smoothing parameter

###set averaged vectors###
tsim=[]

f2=open("Averages"+nameFDF2+".dat","w")
print ("perform analysis")
### extract data for averaging ###
CallD=[] # all files
tzero = 1
tzero2= 1
Nprobes=0
CallD_smooth_average=[]
CallD_smooth_variance=[]
for ti in range(en,tm):
    print (path+nameFDF+"_World_"+str(ti)+".dat")
    f=open(path+nameFDF+"_World_"+str(ti)+".dat")
    Cdat=[] # all data of a file
    ### correlations ###
    C1=[]
    C2=[]
    C3=[]
    C4=[]
    C5=[]
    C6=[]
    C7=[]
    C8=[]
    C9=[]
    C10=[]
    C11=[]
    C12=[]
    C13=[]
    C14=[]
    C15=[]
    C16=[]
    C17=[]
    C18=[]
    C19=[]
    C20=[]
    C21=[]
    C22=[]
    C23=[]
    C24=[]
    C25=[]
    C26=[]
    C27=[]
    C28=[]
    C29=[]
    C30=[]
    C31=[]
    C32=[]
    C33=[]
    C34=[]
    C35=[]
    C36=[]
    C37=[]
    C38=[]
    C39=[]
    C40=[]
    C41=[]
    C42=[]
    C43=[]
    C44=[]
    C45=[]
    C46=[]
    C47=[]
    C48=[]
    C49=[]
    C50=[]
    C51=[]
    C52=[]
    C53=[]
    C54=[]
    C55=[]
    C56=[]
    C57=[]
    C58=[]
    C59=[]
    C60=[]
    C61=[]
    C62=[]
    C63=[]
    C64=[]
    C65=[]
    C66=[]
    C67=[]
 #   C68=[]
  #  C69=[]
   # C70=[]
    #C71=[]

    #k=0
    #Xval=0.0
    for lines in f:
        l=lines.split()
        if len(l)>0:
            if(tzero==1):
                Nprobes = len(l)-1
                tsim.append(float(l[0])) #time
            C1.append(float(l[1]))   #xx2
            C2.append(float(l[2]))   #yy2
            C3.append(float(l[3]))   #uuf
            C4.append(float(l[4]))   #vvf
            C5.append(float(l[5]))   #uuf2
            C6.append(float(l[6]))   #vvf2
            C7.append(float(l[7]))   #varx
            C8.append(float(l[8]))   #vary
            C9.append(float(l[9]))   #varx2
            C10.append(float(l[10])) #vary2
            C11.append(float(l[11])) #F,charF
            C12.append(float(l[12])) #F2,charF2
            C13.append(float(l[13])) #Vort,CharVort
            C14.append(float(l[14])) #Vort2,CharVort2
            C15.append(float(l[15])) #A,F
            C16.append(float(l[16])) #A,Vort
            C17.append(float(l[17])) #Vort,F
            C18.append(float(l[18])) #Z1phi
            C19.append(float(l[19])) #Z1theta
            C20.append(float(l[20])) #Z2phi
            C21.append(float(l[21])) #Z2theta
            C22.append(float(l[22])) #Z3phi
            C23.append(float(l[23])) #Z3theta
            C24.append(float(l[24])) #Z1thetaf
            C25.append(float(l[25])) #Z1theta2f
            C26.append(float(l[26])) #Z1thetavort
            C27.append(float(l[27])) #Z1thetaF
            C28.append(float(l[28])) #Var0x
            C29.append(float(l[29])) #Var0x2
            C30.append(float(l[30])) #Var0y
            C31.append(float(l[31])) #Var0y2
            C32.append(float(l[32])) #mean x
            C33.append(float(l[33])) #mean y
            C34.append(float(l[34])) #mean x2
            C35.append(float(l[35])) #mean y2
            C36.append(float(l[36])) #mean U
            C37.append(float(l[37])) #mean V
            C38.append(float(l[38])) #mean U2
            C39.append(float(l[39])) #mean V2
            C40.append(float(l[40])) #psi1thetaf
            C41.append(float(l[41])) #psi1theta2f
            C42.append(float(l[42])) #psi1thetavort
            C43.append(float(l[43])) #psi1thetaF
            C44.append(float(l[44])) #Z1thetafX
            C45.append(float(l[45])) #Z1thetafY
            C46.append(float(l[46])) #Z1theta2fX
            C47.append(float(l[47])) #Z1theta2fY
            C48.append(float(l[48])) #Z1thetaVortX
            C49.append(float(l[49])) #Z1thetaVortY
            C50.append(float(l[50])) #Z1thetaFX
            C51.append(float(l[51])) #Z1thetaFY
            C52.append(float(l[52])) #mean Vort
            C53.append(float(l[53])) #mean F
            C54.append(float(l[54])) #mean Vort2
            C55.append(float(l[55])) #mean F2
            C56.append(float(l[56])) #F percentage
            C57.append(float(l[57])) #F2 percentage
            C58.append(float(l[58])) #Vort percentage
            C59.append(float(l[59])) #Vort2 percentage
            C60.append(float(l[60])) #hexagonal order parameter n=3 active
            C61.append(float(l[61])) #hexagonal order parameter n=3 passive
            C62.append(float(l[62])) #hexagonal order parameter n=4 active
            C63.append(float(l[63])) #hexagonal order parameter n=4 passive
            C64.append(float(l[64])) #hexagonal order parameter n=5 active
            C65.append(float(l[65])) #hexagonal order parameter n=5 passive
            C66.append(float(l[66])) #hexagonal order parameter n=6 active
            C67.append(float(l[67])) #hexagonal order parameter n=6 passive
     #       C68.append(float(l[68])) # tracer fixed threshold active
      #      C69.append(float(l[69])) # tracer fixed threshold passive
       #     C70.append(float(l[70])) # negative x active
        #    C71.append(float(l[71])) # negative x passive
            
    f.close()
    # append all data of a file #
    Cdat.append(C1)
    Cdat.append(C2)
    Cdat.append(C3)
    Cdat.append(C4)
    Cdat.append(C5)
    Cdat.append(C6)
    Cdat.append(C7)
    Cdat.append(C8)
    Cdat.append(C9)
    Cdat.append(C10)
    Cdat.append(C11)
    Cdat.append(C12)
    Cdat.append(C13)
    Cdat.append(C14)
    Cdat.append(C15)
    Cdat.append(C16)
    Cdat.append(C17)
    Cdat.append(C18)
    Cdat.append(C19)
    Cdat.append(C20)
    Cdat.append(C21)
    Cdat.append(C22)
    Cdat.append(C23)
    Cdat.append(C24)
    Cdat.append(C25)
    Cdat.append(C26)
    Cdat.append(C27)
    Cdat.append(C28)
    Cdat.append(C29)
    Cdat.append(C30)
    Cdat.append(C31)
    Cdat.append(C32)
    Cdat.append(C33)
    Cdat.append(C34)
    Cdat.append(C35)
    Cdat.append(C36)
    Cdat.append(C37)
    Cdat.append(C38)
    Cdat.append(C39)
    Cdat.append(C40)
    Cdat.append(C41)
    Cdat.append(C42)#angle swarm tracer
    Cdat.append(C43)
    Cdat.append(C44)
    Cdat.append(C45)
    Cdat.append(C46)
    Cdat.append(C47)
    Cdat.append(C48)
    Cdat.append(C49)
    Cdat.append(C50)
    Cdat.append(C51)
    Cdat.append(C52)
    Cdat.append(C53)
    Cdat.append(C54)
    Cdat.append(C55)
    Cdat.append(C56)
    Cdat.append(C57)
    Cdat.append(C58)
    Cdat.append(C59)
    Cdat.append(C60)
    Cdat.append(C61)
    Cdat.append(C62)
    Cdat.append(C63)
    Cdat.append(C64)
    Cdat.append(C65)
    Cdat.append(C66)
    Cdat.append(C67)#Nprobes=67+1=68
    #Cdat.append(C68)
    #Cdat.append(C69)
    #Cdat.append(C70)
    #Cdat.append(C71)
    if tzero==1:
        tzero=0
    # append a whole file to the collection of data #
    CallD.append(Cdat)
    if tzero2==1:
        tzero2=0
        CallD_smooth_average=copy.deepcopy(Cdat)
        CallD_smooth_variance=copy.deepcopy(Cdat)

### copy structure of all data files ###
CallD_smooth = copy.deepcopy(CallD)
CallD_Norms = copy.deepcopy(CallD)

# calculate r such that kernel has decayed to 0.01#
r = int(math.sqrt(abs(kappa*math.log(0.01*math.sqrt(math.pi*kappa))))/(tsim[1]-tsim[0]))
print ("radius = " + str(r))
print ("len CallD_smooth_average= =" + str(len(CallD_smooth_average)) + " Nprobes = " + str(Nprobes) + " len data = " + str(len(CallD_smooth_average[0])))

# smooth data #
for i in range(len(CallD)):# loop through files
    print ("data file " + str(i))
    for j in range(len(CallD[i])): # loop through datas
 #       print ("data column " + str(j))
        for k in range(len(CallD[i][j])): # loop through a single data line
            CallD_smooth[i][j][k] = 0.0
            CallD_Norms[i][j][k] = 0.0
            lmin = -r+k
            lmax = r+k
            if lmin<0:
                lmin=0
            if lmax>len(tsim)-2:
                lmax=len(tsim)-2
            for L in range(lmin,lmax):
                #print ("t= " + str(tsim[k]) + " tsim_s= " + str(tsim[l]))
                CallD_smooth[i][j][k] += CallD[i][j][L]*Smoothing_Kernel(tsim[k],tsim[L],kappa)
                CallD_Norms[i][j][k] += Smoothing_Kernel(tsim[k],tsim[L],kappa)
            CallD_smooth[i][j][k] /= CallD_Norms[i][j][k]

# average over smoothed data #
for j in range(Nprobes):
    for k in range(len(tsim)):
        CallD_smooth_average[j][k] = 0.0
        CallD_smooth_variance[j][k] = 0.0
        for i in range(len(CallD)):
            CallD_smooth_average[j][k] += CallD_smooth[i][j][k]/(float(tm-en))
            CallD_smooth_variance[j][k] += CallD_smooth[i][j][k]*CallD_smooth[i][j][k]/(float(tm-en))

# variances #
for j in range(Nprobes):
    for k in range(len(tsim)):
        CallD_smooth_variance[j][k] -= CallD_smooth_average[j][k]*CallD_smooth_average[j][k]

for j in range(Nprobes):
    for k in range(len(tsim)):
#        print ("smoothed variance= " + str(abs(CallD_smooth_variance[j][k])))
        CallD_smooth_variance[j][k] = math.sqrt(abs(CallD_smooth_variance[j][k]))

for k in range(len(tsim)):# time
    f2.write(str(tsim[k]) + " ")
    for j in range(Nprobes):# all datas
        f2.write(str(CallD_smooth_average[j][k]) + " ")
    for j in range(Nprobes):# all datas
        f2.write(str(abs(CallD_smooth_variance[j][k])) + " ")
    f2.write("\n")

f2.close()
