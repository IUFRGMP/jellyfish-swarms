import sys

path = str(sys.argv[1])   #./ #path of reading
nameFDF= str(sys.argv[2]) #"FDFS_" #file name head
vn=int(sys.argv[3])       #1 #exnumber
bc=int(sys.argv[4])       #21 #block count
patho = str(sys.argv[5])  #./ #path of output

if nameFDF=="World_":
    f=open(path+"World."+str(vn)+".dat","r")
else:
    f=open(path+nameFDF+"World."+str(vn)+".dat","r")

f2=open(patho+nameFDF+"0_World."+str(vn)+".dat","w")
t=0.0
nf=0
k=1
print ("start of splitting")
for lines in f:
#    print "new line:"
#    print lines
    l=lines.split()
#    print "splitted line:"
#    print l
#    print "len l= " + str(len(l))
    nf=0
    if len(l)==0 and k<=bc:
#        print "regular block change"
        k +=1
        f2.write("\n")
    if len(l)==0 and k==1+bc:
#        print "close file after block " + str(k) + "\n"
        f2.close()
    if len(l)>0 and float(l[0]) != t and k==1+bc:
#        print "open new file"
        if abs(round(float(l[0]))-float(l[0]))<0.001:
            f2=open(patho+nameFDF+str(int(float(l[0])))+"_World."+str(vn)+".dat","w")
        else:
            f2=open(patho+nameFDF+str(round(float(l[0]),1))+"_World."+str(vn)+".dat","w")
        f2.write(lines)
        t=float(l[0])
        nf=1
        k=1
    if len(l)>0 and float(l[0]) == t and nf == 0 and k<=bc:
        t= float(l[0])
#        print "regular print at t=" + str(t)
        f2.write(lines)
        
print ("finished splitting")
f.close()
