import math
import sys
import numpy as np

#fa=0.05 but has not been renamed
fs=[]
fs.append("AveragesREP_CC_VORT_fb002_epsF0005_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF001_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF002_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF004_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF008_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF016_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF032_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF064_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb002_epsF1_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF0005_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF001_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF002_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF004_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF008_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF016_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF032_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF064_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb01_epsF1_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF0005_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF001_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF002_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF004_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF008_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF016_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF032_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF064_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb02_epsF1_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF0005_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF001_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF002_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF004_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF008_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF016_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF032_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF064_fa001.dat")
fs.append("AveragesREP_CC_VORT_fb04_epsF1_fa001.dat")

eps=[0.005,0.01,0.02,0.04,0.08,0.16,0.32,0.64,1.0,0.005,0.01,0.02,0.04,0.08,0.16,0.32,0.64,1.0,0.005,0.01,0.02,0.04,0.08,0.16,0.32,0.64,1.0,0.005,0.01,0.02,0.04,0.08,0.16,0.32,0.64,1.0]

f1=open("TRACER_fb_002_fa_001.dat", 'w')
f2=open("TRACER_fb_01_fa_001.dat", 'w')
f3=open("TRACER_fb_02_fa_001.dat", 'w')
f4=open("TRACER_fb_04_fa_001.dat", 'w')
for i in range(len(fs)):
    print (fs[i])
    t=[]
    x1=[]
    x2=[]
    x3=[]
    x4=[]
    dat=open(fs[i],'r')
    for lines in dat:
        l=lines.split()
        #print(str(l[0]) + " " + str(l[37]) + " " + str(l[39]))
        if(float(l[0])>600.0 and float(l[0])<899.9):
        #if(float(l[0])>6.0 and float(l[0])<8.9):
            t.append(float(l[0]))
            x1.append(math.sqrt(float(l[7])/float(l[28]) + float(l[8])/float(l[30])))#standard deviation active
            x2.append(math.sqrt(float(l[9])/float(l[29]) + float(l[10])/float(l[31])))#passive
            x3.append(float(l[53]))#mean value tracer active
            x4.append(float(l[55]))#passive
    a1=0.0
    a2=0.0
    a3=0.0
    a4=0.0
    for j in range(len(x1)):
        a1 += x1[j]
        a2 += x2[j]
        a3 += x3[j]
        a4 += x4[j]
    a1 /= float(len(x1))
    a2 /= float(len(x2))
    a3 /= float(len(x3))
    a4 /= float(len(x4))

    if(i>=0 and i<=8):
        f1.write(str(eps[i]) + " " + str(a1) + " " + str(a2) + " " + str(a3) + " " + str(a4) + "\n")
    if(i>=9 and i<=17):
        f2.write(str(eps[i]) + " " + str(a1) + " " + str(a2) + " " + str(a3) + " " + str(a4) + "\n")
    if(i>=18 and i<=26):
        f3.write(str(eps[i]) + " " + str(a1) + " " + str(a2) + " " + str(a3) + " " + str(a4) + "\n")
    if(i>=27 and i<=35):
        f4.write(str(eps[i]) + " " + str(a1) + " " + str(a2) + " " + str(a3) + " " + str(a4) + "\n")

f1.close()
f2.close()
f3.close()
