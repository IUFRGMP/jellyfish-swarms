import sys
#import numpy as np
import math
import copy

### functions ###
def Mean_X(X):
    MX=0.0
    for k in range(len(X)):
        MX += X[k]
    MX /= float(len(X))
    return MX

def Char_fkt(X,thresh):
    if X>thresh:
        return 1.0
    else:
        return 0.0
#NOTE: CODE FINDS BOUNDARY LINE OF CHARACTERISTIC REGION WHEN THE COMPUTATIONAL DOMAIN IS QUADRATIC 

### input arguments ###
path = str(sys.argv[1]) #./ #path of reading
nameFDF= str(sys.argv[2]) #"World_    <t>_World.<#>.dat" #file name head
en=int(sys.argv[3]) #exp number
bc=int(sys.argv[4]) #block count for matrix
t=str(sys.argv[5]) #time stamp
TR=float(sys.argv[6]) #threshold for characteristic function

f2=open("region_"+str(t)+"_"+nameFDF+str(en)+".dat","w")
print ("perform analysis")

f=open(path+nameFDF+str(t)+"_World."+str(en)+".dat","r")
 
t=0.0
nf=0
#read lines and use absolute vorticity and compare it to the given threshold.
X=[]
Y=[]
Vort=[]
for lines in f:
    l=lines.split()#l(0)time, l(1)x, l(2)y, l(9)vorticity
    nf=0
    if len(l)>0:
    #        print "open new file"
        X.append(float(l[1]))
        Y.append(float(l[2]))
        Vort.append(abs(float(l[9])))
f.close()
Mx=[]#pos x
My=[]#pos y
Mv=[]#vort

v=[]
for k in range(bc):
    v.append(0.0)
for k in range(bc):
    Mx.append(copy.deepcopy(v))
    My.append(copy.deepcopy(v))
    Mv.append(copy.deepcopy(v))

#fill matrix for gradient calculation
for k in range(bc):
    for l in range(bc):
        Mx[k][l]=X[k*bc+l]
        My[k][l]=Y[k*bc+l]
        if(abs(Vort[k*bc+l])>TR):
            Mv[k][l]=1.0
        else:
            Mv[k][l]=0.0

#calculate gradient and identify boundary of characteristic domains
Ml=copy.deepcopy(Mv)
dx=0.0
dy=0.0
for k in range(1,bc-1):
    for l in range(1,bc-1):
        dx=Mv[k+1][l]-Mv[k-1][l]
        dy=Mv[k][l+1]-Mv[k][l-1]
        if Mv[k][l]==1.0 and (Mv[k-1][l]==0.0 or Mv[k+1][l]==0.0 or Mv[k][l+1]==0.0 or Mv[k][l-1]==0.0):
        #if abs(dx)+abs(dy)>0.0:
            Ml[k][l] = 1.0
        else:
            Ml[k][l] = 0.0

#print to file
for k in range(bc):
    for l in range(bc):
        f2.write(str(Mx[k][l]) + " " + str(My[k][l]) + " " + str(Ml[k][l]) + "\n")
    f2.write("\n")

f2.close()
