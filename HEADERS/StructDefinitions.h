// ###    STRUCTS FOR JELLYFISH CFD SIMULATION        ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### Dtype for precission controlle ###//
typedef long double DOUB ;

const DOUB pi(atan(1.0)*4.0); //pi

//### DEFINITION OF MAIN DATA STRUCTURE ###
typedef struct Vectordata
{
  vector<DOUB> pos;   //positions of agents
  vector<DOUB> pos2;  //positions of passive agents
  vector<DOUB> vel;   //velocity of agents
  vector<DOUB> vel2;  //velocity of passive agents
  vector<DOUB> noise; //component wise noise 
}Vectordata;

typedef struct params
 {
  DOUB freq1;                                     //mean of internal natural frequencies
  DOUB freq2;                                     //standard deviation
  DOUB vel0;                                      //positional velocity
  vector<DOUB> freq;                              //natural frequencies
  DOUB eps;                                       //strength of phase interaction between agents (internal state)
  DOUB eps2;                                      //strength of directional interaction between agents
  DOUB irad;                                      //radius of interaction 
  DOUB xi;                                        //domain left
  DOUB xf;                                        //domain right
  DOUB yi;                                        //domain bottom
  DOUB yf;                                        //domain top
  DOUB thresh;                                    //detection threshold
  vector<DOUB> intensity;                         //intensity field for agents (to be copied from CFD simulation)
  vector<DOUB> intensity2;                        //intensity field for agents (to be copied from CFD simulation) passive agents
  vector<DOUB> UXA;                               // x velocity of agents 
  vector<DOUB> UYA;                               // y velocity of agents 
  vector<DOUB> UXA2;                              // x velocity of passive agents 
  vector<DOUB> UYA2;                              // y velocity of passive agents 
  vector<DOUB> PrA;                               // Pressur agents 
  vector<DOUB> VorticityA;                        // vorticity at agents
  vector<DOUB> VorticityA2;                       // vorticity at passive agents
  vector<DOUB> Vorticity_gradXA;                  // vorticity gradient X at agents
  vector<DOUB> Vorticity_gradYA;                  // vorticity gradient Y at agents
  vector<DOUB> TrgradXA;                          // X gradient of intensity at agents
  vector<DOUB> TrgradYA;                          // Y gradient of intensity at agents
  vector<vector<int>> FDF_indexX;                 //index of CFD vector position of each agent X
  vector<vector<int>> FDF_indexY;                 //index of CFD vector position of each agent Y
  vector<vector<int>> FDF_indexX2;                //index of CFD vector position of each passive agent X
  vector<vector<int>> FDF_indexY2;                //index of CFD vector position of each passive agent Y
  DOUB sposx;                                     //.>X center of initial square of starting positions
  DOUB sposy;                                     //->Y
  DOUB sposedge;                                  //length of edge of starting square
  DOUB dirdamp;                                   //damping of moving direction
  DOUB dir_lag;                                   //phase lag parameter of moving direction
  DOUB J;                                         //coupling parameter between internal state and attractive coupling
  DOUB J2;                                        //sensitivity parameter -> Determines how sensitive the agent is in a given direction
  DOUB J3;                                        //velocity oscillation parameter due to oscillation of bell
  vector<DOUB> D;                                 //component wise Diffusion coefficients
  DOUB vel_c;                                     //adaption velocity of agents to counter the mean background flow
  DOUB eps3;                                      //influence of background flow on direction of swimming
  DOUB eps4;                                      //influence of vorticity gradient on direction of swimming
  DOUB freq_c;                                    //parameter of oscillatory activity increase according to tracer value
  DOUB mem_damp;                                  //parameter of memory value decay
  DOUB counter_curent;                            //switcher for countercurent swimming
  DOUB osc_activity;                              //switcher for coupling of frequency and activity
  DOUB dif_activity;                              //switcher for coupling of diffusivity and activity
  DOUB agent_atr;                                 //switcher for attraction of agents
  DOUB seasons_switc;                             //switcher for seasons
  DOUB agent_rep;                                 // switcher for repulsion of agents
  DOUB agent_direction;                           // switcher for repulsion of agents
  DOUB x_tracer;                                  // x position of tracer distribution
  DOUB y_tracer;                                  // y position of tracer distribution
  DOUB amp_tracer;                                // amplitude of tracer density
  DOUB cut;                                       // constant of potential to be set in code such that at irad for given agent_rep 
						  // there is a second equilibrium force cut*0.5*r^2 is the aditional potential term
  DOUB offset;                                    // fixed offset value of force at a given distance irad from r=0. Needed in dgl to 
						  // calculate the actual interaction radius and cutoff potential
  DOUB Vs;                                        // turning point parameter of counter-current swimming
  DOUB tr_dir3;                                   // switcher of parametric changes for orientation towards prey (dependence on activity)
  DOUB tr_dir_steepnes;                           // parameter that determines how fast happens the switching response when activity increases
  DOUB eps5;                                      //influence of tracer on direction of swimming
  DOUB counter_current_act;                       // switcher counter-current swimming activity
  DOUB tr_steepnes;                               // switching parameter for loss of counter-current swimming orrientation
  DOUB act_steepnes_2;                            //stepp turning point parameter or steepness of switch off of gradient swimming
  DOUB tr_dir2;                                   // switcher of parametric changes from counter-current swimming to pure advection/random orientation (dependence on prey concentration)
  DOUB tr_dir1;                                   // switcher of parametric changes for vorticity avoidance (dependence on activity)
 }params;

typedef struct FDFS
{
 vector<DOUB> intensity;              //trancer intensity
 vector<DOUB> intensityi;             //trancer intensity
 vector<DOUB> intensity_coupler;      //trancer intensity on agent grid (position of pressur nodes)
 vector<DOUB> gridX;                  //position of grid points X in Agent domain (grid without halo cells)
 vector<DOUB> gridY;                  //position of grid points Y in Agent domain (grid without halo cells)
 vector<DOUB> PrgridX;                //position of grid points X in CFD simulation for pressur (including halo cells for boundaries)
 vector<DOUB> PrgridY;                //position of grid points Y in CFD simulation for pressur (including halo cells for boundaries)
 vector<DOUB> UX;                     //fluid velocity in x direction
 vector<DOUB> UY;                     //fluid velocity in y direction
 vector<DOUB> UX_coupler;             //fluid velocity in x direction on agent grid (positions of pressur nodes)
 vector<DOUB> UY_coupler;             //fluid velocity in y direction on agent grid (positions of pressur nodes)
 vector<DOUB> UZ;                     //fluid velocity in z direction 
 vector<DOUB> Pr;                     //pressur of fluid
 vector<DOUB> Pri;                    //intermediate pressur of fluid (for Heuns method in time stepping)
 vector<DOUB> Pri2;                   //intermediate pressur of fluid
 vector<DOUB> Pr_coupler;             //pressur of fluid on agent grid on agent grid (position of pressur nodes)
 vector<DOUB> vorticity;              //vorticity of fluid 
 vector<DOUB> vorticitygradX;         //vorticity gradX of fluid 
 vector<DOUB> vorticitygradY;         //vorticity gradY of fluid 
 vector<DOUB> vorticity_coupler;      //vorticity of fluid on agent grid
 vector<DOUB> vorticitygradX_coupler; //vorticity gradX of fluid on agent grid
 vector<DOUB> vorticitygradY_coupler; //vorticity gradY of fluid on agent grid
 vector<DOUB> divergence;             //divergence of fluid 
 vector<DOUB> divergence_coupler;     //divergence of fluid on agent grid
 vector<DOUB> TrgradX;                //X gradient of fluid on agent grid
 vector<DOUB> TrgradY;                //Y gradient of fluid on agent grid
 vector<DOUB> TrgradX_coupler;        //X gradient of fluid on agent grid
 vector<DOUB> TrgradY_coupler;        //Y gradient of fluid on agent grid
}FDFS;

typedef struct parsf
 {
  int Nfx;            // number of grid points in x direction
  int Nfy;            // in y direction
  DOUB fxi;           // domain left
  DOUB fxf;           // domain right
  DOUB fyi;           // domain bottom
  DOUB fyf;           // domain top
  DOUB dx;            // x step
  DOUB dy;            // y step
  DOUB dz;            // z step 
  DOUB dtf;           // time step of fluid
  string image_path;  // path name to image data files
  DOUB kappa;         // smoothing parameter for interpolation of pixels to internal grid
  DOUB rho;           // fluid density (constant)
  DOUB mu;            // viscosity (constant)
  DOUB beta;          // relaxation parameter for pressur solver
  DOUB toll;          // tolerance for pressur solver
  DOUB DTr1;          // diffusion coefficient of Tracer 1
  vector<DOUB> UU;    // U velocity at upper boundary
  vector<DOUB> VU;    // V velocity at upper boundary
  vector<DOUB> UL;    // U velocity at lower boundary
  vector<DOUB> VL;    // V velocity at lower boundary
  vector<DOUB> ULE;   // U velocity at left boundary
  vector<DOUB> VLE;   // V velocity at left boundary
  vector<DOUB> UR;    // U velocity at left boundary
  vector<DOUB> VR;    // V velocity at left boundary
  int width_inlet;    // width of inlet valve
  int width_outlet;   // width of outlet valve
  int pos_inlet;      // center of inlet valve
  int pos_outlet;     // position of outlet valve
  int length_outlet;  // position of outlet valve
  DOUB vel_inlet;     // maximum velocity of inlet flow 
  DOUB Npr;           // maximal number of SOR steps adjusted if needed
  DOUB Nsor;          // keeping track of the actual SOR steps 
 }parsf;
