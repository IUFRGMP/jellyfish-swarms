// ###    BASE HEADER FOR JELLYFISH CFD SIMULATION    ### //
// ###                                                ### //
// ### AUTHOR: Dr. ERIK GENGEL, Tel Aviv University   ### //
// ###                                                ### //

//#include <mpi.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <cmath>
#include <sstream>
#include <ostream>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <random>
#include <omp.h> 

using namespace std;

//#define CFD_field_from_image              //enables interpolation of external field onto specified grid
//#define CFD_field_from_image_dynamic      //enables interpolation of external field onto specified grid in each time step of simulation
#define CFD_field_from_integrator         //enables dynamic simulation of field in code using CFD solver
//#define Euler                             //if defined first order in time stepping of CFD fields, if not defined 2nd order Heun scheme for time stepping
#define tracer_distribution               //initializes a tracer distribution instead of a tracer injection (otherwise)
//#define Strogatz                          //sets the direction-orientation function to unity, giving essentially back the Strogatz model of swarming
//#define pressure_outlet                   //defines a setting with one/multiple velocity inlets and one pressure outlet
#define seasons                           //to enables square oscillating function mimicing seasons in which the swarm groups together
//#define cavity_upper_wall                 //defines a single moving wall cavity if not defined the left and right walls are moving in the same direction
//#define averageUV_velocity                //calculate the average velocities of U and V in y and x direction for each time step
//#define inflow                            //inlet vector field output
//#define outflow                           //outflow and inflow comparison output
//#define FDF_out                           //output of fluid fields
