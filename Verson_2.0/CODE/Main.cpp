#include "../HEADERS/baseInclude.h"
#include "../HEADERS/StructDefinitions.h"

DOUB inlet_velocity(int k, DOUB &vel_inlet, int &pos_inlet, int &width_inlet, DOUB &tf, DOUB &T)
 {
  //NOTE: This function sets the inlet velocity (if needed)
   return -vel_inlet*(-(DOUB(k-pos_inlet)*(k-pos_inlet))/(DOUB(width_inlet*width_inlet))+1.0)*(tanh(15.0*(tf-10.0+0.8*T)/T)+1.0)*0.5;
  //return -vel_inlet*DOUB(k-pos_inlet+width_inlet)*DOUB(k-pos_inlet+width_inlet)/(DOUB(width_inlet*width_inlet))*exp(-6.0*DOUB(k-pos_inlet+width_inlet)/(DOUB(width_inlet)) + 2.0)*9.0;
 }

void Toll_set(DOUB &toll, DOUB &Npr, DOUB &Nsor, vector<DOUB> &divergence, DOUB &DIVMAX, DOUB &NPRMAX)
 {
  //NOTE: This function sets the tolerance of numerical error in the CFD solver.
  //      - It calculates the average divergence in the domain (D)
  //      - If abs(D) is larger than 1e-6 and if the number of pressure steps is smaller than 8000, the number of maximally usable relaxation steps is increased by 1
  //      - Initially, the number of pressure steps is set to 250000 (see main) and is successively reduced in small steps (-3050).
  //      - In turn, when the number of pressure steps is below 200000, the reduction is just by -1050
  //      - When Npr<1000 reducetion happens with -50
  //      - Usually, at least 20 relaxation iterations are performed each step once the maximal step number has relaxed (this is also the minimal maximal number of steps).
  //        
  //   Npr is set initially anc changes afterwards
  //   ^                                     reduction by 2000 + 1000 + 50 
  //   |*....................................250000
  //   | *  
  //   |  *                                  reduction by 1000 + 50
  //   |   *
  //   |     *...............................100000
  //   |       *
  //   |         *                           reduction by 50
  //   |           *
  //   |               *                     reduction by 1 below NPRMAX
  //   |                   *.................NPRMAX       
  //   |                       *          *
  //   |                            *        at least 20 steps; regulation up and down up in steps of 100
  //   +------------------------------------>
  
  DOUB D(0.0);
  for(int k=0; k<divergence.size();k++)
   {
    D += divergence[k];
   }
  D /= (DOUB(divergence.size()));
  //sor step adjustion
  //if((Npr>8000) & (Nsor>8000)){Npr=(Nsor+Npr)/2.0; cout << "make larger step: Npr= " << Npr << "\n";}//make larger steps if there are many steps needed
  if(Npr>200000){Npr -=2000.0; }// cout << "to high Npr -20, Npr= " << Npr << "\n";}//
  if(Npr>=100000){Npr -=1000.0; }//cout << "to high Npr -10, Npr= " << Npr << "\n";}//
  if(Npr>=NPRMAX){Npr -= 50.0;}
  if(abs(D)>=DIVMAX){if(Npr<=NPRMAX){Npr +=100.0;} }//cout << "to high Div! Npr +1, Npr= " << Npr << "\n";}//div to large increase Npr
  else{if(Npr>=20){Npr -=1.0;} }//cout << "Div is small. Npr-1, Npr=  " << Npr << "\n";}//div small decrease Npr
  //Tolerance adjustion
  //if(abs(D)>1e-6){if(toll>0.001){toll *=0.98;}}//if(toll>5e-4){toll *=0.98;}}//div to large decrease tolerance
  //else {if(toll<0.01){toll *=1.01;}}//if(toll<5e-3){toll *=1.01;}}//div small increase tolerance up to 1e-2
  cout << "|<Div U>| = " << abs(D) << "\n";
 }

void In_flow_outflow(parsf &parsFDF, FDFS &FDF, DOUB &tf, DOUB &T, ofstream &fout6, ofstream &fout7, ofstream &fout8, ofstream &fout9)
 {
  //NOTE: This function calculates several measures for diagnosis of the CFD solver
  DOUB Inflow(0.0), Outflow(0.0), InflowF(0.0), Inflow_bound(0.0), Divergence(0.0), Inflow_tracer(0.0);
  for(int k=parsFDF.pos_inlet-parsFDF.width_inlet;k<=parsFDF.pos_inlet+parsFDF.width_inlet;k++)
   {
    Inflow += parsFDF.dx*FDF.UY[k*(parsFDF.Nfy+1)+parsFDF.Nfy];
    InflowF += parsFDF.dx*inlet_velocity(k, parsFDF.vel_inlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T);
    Inflow_bound += parsFDF.dx*parsFDF.VU[k];
    Inflow_tracer += FDF.intensity[k*(parsFDF.Nfy+2)+parsFDF.Nfy+1];
#ifdef inflow
    fout6 << tf << " " << FDF.gridX[k] << " " << FDF.gridY[FDF.gridY.size()-1] << " " << FDF.UY[k*(parsFDF.Nfy+1)+parsFDF.Nfy] << " "                                                                         << 0.25*(FDF.UX[(k-1)*(parsFDF.Nfy+2)+parsFDF.Nfy]+FDF.UX[(k-1)*(parsFDF.Nfy+2)+parsFDF.Nfy+1]                                                                                                 +FDF.UX[k*(parsFDF.Nfy+2)+parsFDF.Nfy]+FDF.UX[k*(parsFDF.Nfy+2)+parsFDF.Nfy+1])                                                                                                << " " << parsFDF.VU[k] << " " << 0.5*(parsFDF.UU[k-1]+parsFDF.UU[k]) << " "                                                                                                           << inlet_velocity(k, parsFDF.vel_inlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T) << " " << 0.0 << " "                                                                            << FDF.intensity[k*(parsFDF.Nfy+2)+parsFDF.Nfy+1] << "\n";
#endif
   }
  for(int k=0;k<FDF.divergence_coupler.size();k++)
   {
    Divergence += FDF.divergence_coupler[k];
   }
  //Divergence /= (DOUB(FDF.divergence.size()));
  Divergence *= parsFDF.dx*parsFDF.dy;
#ifdef inflow
  fout6 << "\n";
#endif
  /*for(int k=(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+parsFDF.length_outlet; k<FDF.UX.size()-parsFDF.length_outlet; k++)
   {
    //cout << "outlet index k= " << (parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(parsFDF.pos_outlet-parsFDF.width_outlet-1+k)*(parsFDF.length_outlet)+parsFDF.length_outlet-5 << " max size= " << FDF.UY.size() << " size before outlet= " << (parsFDF.Nfx+2)*(parsFDF.Nfy+1) << "\n";
     {
      Outflow += parsFDF.dx*FDF.UY[k]/(DOUB(parsFDF.length_outlet)); //(DOUB(FDF.UY.size()-(parsFDF.Nfx+2)*(parsFDF.Nfy+1)));
     }
   }//*/
  for(int k=1; k<2*parsFDF.width_outlet+2;k++)//outlet flow at 80% of outlet length
   {
    for(int l=10; l<20;l++)
     {
      Outflow += parsFDF.dx*FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*parsFDF.length_outlet+l]/10.0;
     }
   }
#ifdef outflow
  fout7 << tf << " " << Inflow << " " << InflowF << " " << Inflow_bound << " " << Inflow_tracer << " " << Outflow << " " << Divergence << " " << parsFDF.toll << "\n";
#endif
#ifdef averageUV_velocity
  DOUB Vave(0.0), Uave(0.0);
  //calculation of averaged y velocity
  for(int k=0;k<parsFDF.Nfx;k++)
   {
    Vave=0.0;
    for(int l=0;l<parsFDF.Nfy;l++)
     {
      Vave += FDF.UY_coupler[k*parsFDF.Nfy+l];
     }
    fout8 << tf << " " << FDF.gridX[k] << " " << Vave/(DOUB(parsFDF.Nfy)) << "\n";
   }
  fout8 << "\n";
  //calculation of averaged x velocity
  for(int l=0;l<parsFDF.Nfy;l++)
   {
    Uave=0.0;
    for(int k=0;k<parsFDF.Nfx;k++)
     {
      Uave += FDF.UX_coupler[k*parsFDF.Nfy+l];
     }
    fout9 << tf << " " << FDF.gridY[l] << " " << Uave/(DOUB(parsFDF.Nfx)) << "\n";
   }
  fout9 << "\n";
#endif
 }

void CFL(vector<DOUB> &UX, vector<DOUB> &UY, DOUB &dtf, DOUB &dx, DOUB &dy, DOUB &D, DOUB &mu, DOUB &rho, DOUB &dtmax, DOUB &dtmin)
 {
  //NOTE: CFL criteria for time step control
  //### FIND MAXIMUM VELOCITY ###//
  DOUB Vel2(0.0), UXm(0.0), UYm(0.0), dt1(0.0), dt2(0.0), UXs(DOUB(UX.size())), UYs(DOUB(UY.size()));
  //search for largest velocity components
  for(int k=0; k<UX.size();k++)
   {
//    UXm +=UX[k];
    if(abs(UX[k])>=UXm){UXm=UX[k];}
   }
  for(int k=0; k<UY.size();k++)
   {
  //  UYm +=UY[k];
    if(abs(UY[k])>=UYm){UYm=UY[k];}
   }
  //Vel2=UXm*UXm/(UXs*UXs) + UYm*UYm/(UYs*UYs);
  Vel2=UXm*UXm + UYm*UYm;
  if(Vel2<1e-3){Vel2=1e-3;} 
  //cout << "maximal velocity <=  " << Vel2 << "\n";
  dt1=0.8*0.5*dx*dx/D;       //Tracer advection
  //dt2=0.8*0.5*mu/(rho*Vel2); //velocity 
  dt2 = 0.2*dx/sqrt(Vel2);
  if(dt1>dt2)
   {
    if(dt2> dtmin){dtf=dt2; cout << "CFL: dt2= " << dt2 << "\n";}
    else{dtf= dtmin; cout << "CFL: dt2= " << dtf << "\n";}
   }
  else
   {
    if(dt1> dtmin){dtf=dt1; cout << "CFL: dt1= " << dt1 << "\n";}
    else{dtf= dtmin; cout << "CFL: dt1= " << dtf << "\n";}
   }
  if(dtf> dtmax){dtf = dtmax;}
 }

void Damper(vector<DOUB> &UX, vector<DOUB> &UY, DOUB &tf, DOUB &T, parsf &parsFDF)
 {
  //NOTE: This function inhebits too large velocity fluctuations in the intial transient
  DOUB maxU(parsFDF.vel_inlet + 2.0*parsFDF.vel_inlet*(tanh(5.0*(tf-10.0+0.5*T)/T)+1.0)*0.5), nc(0.0);
  for(int k=1;k<parsFDF.Nfx;k++)//x
   {
    for(int l=1;l<parsFDF.Nfy+1;l++)//y
     {
      if(UX[k*(parsFDF.Nfy+2)+l] > maxU){UX[k*(parsFDF.Nfy+2)+l]= maxU*UX[k*(parsFDF.Nfy+2)+l]/(abs(UX[k*(parsFDF.Nfy+2)+l])); nc +=1.0;}
     } 
   }
  //UY
  for(int k=1;k<parsFDF.Nfx+1;k++)//x
   {
    for(int l=1;l<parsFDF.Nfy;l++)//y
     {
      if(UY[k*(parsFDF.Nfy+1)+l] > maxU){UY[k*(parsFDF.Nfy+1)+l]= maxU*UY[k*(parsFDF.Nfy+1)+l]/abs(UY[k*(parsFDF.Nfy+1)+l]); nc +=1.0;}
     }
   }
  //cout << "Damper: " << (tanh(5.0*(tf-10.0+0.5*T)/T)+1.0)*0.5 << " Umax= " << maxU << " corrected " << nc << " values \n";
 }

void NS_boundary_set(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &Pr, parsf &parsFDF, DOUB &tf, DOUB &T, DOUB &rho)
 {
  //NOTE: This function sets the wall values needed for the boundary conditions
  //      - When a complex boundary condition is read in, no-slip boundary conditions are enforced at the walls
  //      - A free-slip boundary condtion can be applied to the right boundary (sea side)
  //      - TODO: Implement additional inlets at the complex boundary
#ifndef complex_boundary
  //### U velocities ###//
  for(int k=0; k<parsFDF.Nfx+1;k++)
   {
    //### upper boundary ###//
    parsFDF.UU[k] = 0.0;
#ifdef cavity_upper_wall
    parsFDF.UU[k] = parsFDF.vel_inlet;//moving wall
#endif
    //### lower boundary ###//
    parsFDF.UL[k]= 0.0;//no slip condition
#ifdef pressure_outlet
    if(abs(k-parsFDF.pos_outlet)<=parsFDF.width_outlet){parsFDF.UL[k]= (UX[k*(parsFDF.Nfy+2)] + UX[k*(parsFDF.Nfy+2)+1])/2.0;}
    if(k-parsFDF.pos_outlet==parsFDF.width_outlet){parsFDF.UL[k]=0.0;};//using field values to change nothing in the lower layer of the flow (if outlet is present)
#endif
   }
  //### V velocities ###//
  for(int k=0; k<parsFDF.Nfx+2;k++)
   {
    //### upper boundary ###//
    parsFDF.VU[k]= 0.0;
#ifdef pressure_outlet
    if(abs(k-parsFDF.pos_inlet)<=parsFDF.width_inlet)
     {
      parsFDF.VU[k] = inlet_velocity(k, parsFDF.vel_inlet, parsFDF.pos_inlet,parsFDF.width_inlet, tf, T) - (Pr[k*(parsFDF.Nfy+2)+parsFDF.Nfy]/abs(Pr[k*(parsFDF.Nfy+2)+parsFDF.Nfy]))*sqrt(2.0*abs(Pr[k*(parsFDF.Nfy+2)+parsFDF.Nfy])/rho);
      //cout << "inlet flow " << k << "= " << parsFDF.VU[k] << "\n";
     }//Poiseulle flow with time rampoff from t=0
#endif
    //### lower boundary ###//
    parsFDF.VL[k]= 0.0;//no slip condition
#ifdef pressure_outlet
    if(abs(k-parsFDF.pos_outlet)<=parsFDF.width_outlet){parsFDF.VL[k]= UY[k*(parsFDF.Nfy+1)];}//using field values to change nothing in the lower layer of the flow (if outlet is present)
#endif
   }
  //### U velocities ### // 
  for(int l=0; l<parsFDF.Nfy+2;l++)
   {
    //### left boundary ###//
    parsFDF.ULE[l] = 0.0;
    //### right boundary ###//
    parsFDF.UR[l] = 0.0;
   }
  //### V velocities ###//
  for(int l=0; l<parsFDF.Nfy+1;l++)
   {
#ifdef pressure_outlet
    //### left boundary ###//
    parsFDF.VLE[l] = 0.0;
    //### right boundary ###//
    parsFDF.VR[l] = 0.0;
#else
#ifdef cavity_upper_wall
    //### left boundary ###//
    parsFDF.VLE[l] = 0.0;
    //### right boundary ###//
    parsFDF.VR[l] = 0.0;
#else
    //### left boundary ###//
    parsFDF.VLE[l] = parsFDF.vel_inlet;
    //### right boundary ###//
    parsFDF.VR[l] = parsFDF.vel_inlet;
#endif
#endif
   }
#else //now comes complex boundary
  //### U velocities ###//
  //### left boundary ###//
  for(int k=0; k<parsFDF.ULE.size(); k++)
   {
    parsFDF.ULE[k] = 0.0;
   }
  //### right boundary ###//
  for(int k=0; k<parsFDF.UR.size(); k++)
   {
    parsFDF.UR[k] = 0.0;
   }
  //### upper boundary ###//
  for(int k=0; k<parsFDF.UU.size(); k++)
   {
    parsFDF.UU[k] = 0.0;
#ifdef cavity_upper_wall
    if(parsFDF.boundary_mapper_UU[k]%(parsFDF.Nfy+2)==(parsFDF.Nfy+1)){parsFDF.UU[k] = parsFDF.vel_inlet;}//moving wall
#endif

   }
  //### lower boundary ###//
  for(int k=0; k<parsFDF.UL.size(); k++)
   {
    parsFDF.UL[k] = 0.0;
#ifdef pressure_outlet
    if(parsFDF.boundary_outlet_UL[k]==1)
     {
      if((parsFDF.boundary_mapper_UL[k]/(parsFDF.Nfy+2)-parsFDF.pos_outlet>=-parsFDF.width_outlet) & (parsFDF.boundary_mapper_UL[k]/(parsFDF.Nfy+2)-parsFDF.pos_outlet<parsFDF.width_outlet)){parsFDF.UL[k] = (UX[parsFDF.boundary_mapper_UL[k]] + UX[parsFDF.boundary_mapper_UL[k]+1])/2.0;}
     }
#endif
   }
  
  //### V velocities ###//
  //### left boundary ###//
  for(int k=0; k<parsFDF.VLE.size(); k++)
   {
    parsFDF.VLE[k] = 0.0;
   }  
  //### right boundary ###//
  for(int k=0; k<parsFDF.VR.size(); k++)
   {
    parsFDF.VR[k] = 0.0;
//TODO: implement propper free slip boundary condition by reading in an array
#ifdef free_slip_right
    parsFDF.VR[k] = UY[parsFDF.boundary_mapper_VR[k]-(parsFDF.Nfy+1)];
#endif
   }
  //### upper boundary ###//
  for(int k=0; k<parsFDF.VU.size(); k++)
   {
    parsFDF.VU[k] = 0.0;
#ifdef pressure_outlet //TODO: this specifies only boundary inlets at the top of the domain by subtracting parsFDF.Nfy, this gives the x index coordinate needed for determining the flow profile
    if(parsFDF.boundary_inlet_VU[k]==1)
     {
      if(abs((parsFDF.boundary_mapper_VU[k]-parsFDF.Nfy)/(parsFDF.Nfy+1)-parsFDF.pos_inlet)<=parsFDF.width_inlet)
       {
        parsFDF.VU[k] = inlet_velocity((parsFDF.boundary_mapper_VU[k]-parsFDF.Nfy)/(parsFDF.Nfy+1), parsFDF.vel_inlet, parsFDF.pos_inlet,parsFDF.width_inlet, tf, T);
        //cout << "inlet flow " << k << "= " << parsFDF.VU[k] << "\n";
       }//Poiseulle flow with time rampoff from t=0
     }
#endif
   }
  //### lower boundary ###//
  for(int k=0; k<parsFDF.VL.size(); k++)
   {
    parsFDF.VL[k] = 0.0;
#ifdef pressure_outlet
    if(parsFDF.boundary_outlet_VL[k]==1)
     {
      if(abs(parsFDF.boundary_mapper_VL[k]/(parsFDF.Nfy+1)-parsFDF.pos_outlet)<=parsFDF.width_outlet){parsFDF.VL[k]= UY[parsFDF.boundary_mapper_VL[k]];}//using field values to change nothing in the lower layer of the flow (if outlet is present)
     }
#endif
   }  
#endif //*/ //end complex boundary 
 }

void NS_STEP0(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &UL, vector<DOUB> &UU, vector<DOUB> &ULE, vector<DOUB> &UR, vector<DOUB> &VL, vector<DOUB> &VU, vector<DOUB> &VLE, vector<DOUB> &VR, int &Nfx, int &Nfy, int &Nfxo, int &Nfyo, int &Xo, parsf &parsFDF)//initial velocity step
 {
  //NOTE: This function updates the boundary conditions
  //      -Ordering is important here at all corners of the domain. As a grid entry can be accessed either from the side or from the top/bottom
  //       Thus there can be double setting. Ordering in the below manner avoids erroneous setting by correct overwriting. (generally the below order enforces no-slip boundary conditions at corners) 
  //### Boundary conditions ###//
#ifndef complex_boundary
  //### U velocities ###//
  for(int k=0; k<Nfx+1;k++)
   {
    //lower boundary:
    UX[k*(Nfy+2)] = 2.0*UL[k] - UX[k*(Nfy+2)+1];
    //upper boundary:
    UX[k*(Nfy+2)+Nfy+1] = 2.0*UU[k] - UX[k*(Nfy+2)+Nfy];
   }
  
  //### U velocities ###//
  for(int l=0; l<Nfy+2;l++)
   {
    //left boundary:
    UX[l] = ULE[l];
    //right boundary:
    UX[Nfx*(Nfy+2)+l] = UR[l];
   }

  //### V velocities ###//
  for(int l=0; l<Nfy+1;l++)
   {
    //left boundary:
    UY[l] = 2.0*VLE[l] - UY[(Nfy+1)+l];
    //right boundary:
    UY[(Nfx+1)*(Nfy+1)+l] = 2.0*VR[l] - UY[Nfx*(Nfy+1)+l];
   }

  //### V velocities ###//
  for(int k=0; k<Nfx+2;k++)
   {
    //lower boundary:
    UY[k*(Nfy+1)] = VL[k];
    //upper boundary:
    UY[k*(Nfy+1)+Nfy] = VU[k];
   }
    
#else
  //### Complex boundary conditions ###//
  //NOTE: To avoide erroneous setting of boundary conditions when a staggered grid cell is assigned twice, the ordering is crucial. 
  // For UX, the lower and upper boundaries have to be assigned first as they can cause reversal of the flow at an index (inside the boundary)
  // At convex corners this can lead to the situation that a cell entry that is positioned on an actual cell surface (thus having actually UX=0) will have reversal flow
  // Thus, one first needs to access all such reversal positions and then proceed with left and right UX boundaries (which are on actual cell boundaries (pressure cells)
  // Similarly for UY with left and right first and then lower and upper indices.
  //### U velocities ###//
  for(int k=0; k<UL.size();k++)
   {
    //lower boundary:
    UX[parsFDF.boundary_mapper_UL[k]] = 2.0*UL[k] - UX[parsFDF.boundary_mapper_UL[k]+1];
   }
  for(int k=0; k<UU.size();k++)
   {
    //upper boundary:
    UX[parsFDF.boundary_mapper_UU[k]] = 2.0*UU[k] - UX[parsFDF.boundary_mapper_UU[k]-1];
   }

  //### U velocities ###//
  for(int l=0; l<ULE.size();l++)
   {
    //left boundary:
    UX[parsFDF.boundary_mapper_ULE[l]] = ULE[l];
   }
  for(int l=0; l<UR.size();l++)
   {
    //right boundary:
    UX[parsFDF.boundary_mapper_UR[l]] = UR[l];
   }   

  //### V velocities ###//
  for(int l=0; l<VLE.size();l++)
   {
    //left boundary:
    UY[parsFDF.boundary_mapper_VLE[l]] = 2.0*VLE[l] - UY[parsFDF.boundary_mapper_VLE[l]+(Nfy+1)];
   } 
  for(int l=0; l<VR.size();l++)
   {
    //right boundary:
    UY[parsFDF.boundary_mapper_VR[l]] = 2.0*VR[l] - UY[parsFDF.boundary_mapper_VR[l]-(Nfy+1)];
   } 
   
  //### V velocities ###//
  for(int k=0; k<VL.size();k++)
   {
    //lower boundary:
    UY[parsFDF.boundary_mapper_VL[k]] = VL[k];
   }
  for(int k=0; k<VU.size();k++)
   {
    //upper boundary:
    UY[parsFDF.boundary_mapper_VU[k]] = VU[k];
   }//*/
     
#endif

#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //left edge
  for(int l=0;l<Nfyo;l++)
   {
    UX[(Nfx+1)*(Nfy+2)+l] = 0.0;
    UY[(Nfx+2)*(Nfy+1)+l] = -UY[(Nfx+2)*(Nfy+1)+Nfyo+l];
   }
  //left edge u velocity in lower boundary at outlet (in main domain)
  UX[(Xo-1-(Nfxo-1)/2)*(Nfy+2)] = 0.0;
  //right edge u velocity in lower boundary at outlet (in main domain)
  UX[(Xo+(Nfxo-1)/2)*(Nfy+2)] = 0.0;
  //right edge
  for(int l=0;l<Nfyo;l++)
   {
    UX[(Nfx+1)*(Nfy+2)+Nfxo*Nfyo+l] = 0.0;
    UY[(Nfx+2)*(Nfy+1)+(Nfxo+1)*Nfyo+l] = -UY[(Nfx+2)*(Nfy+1)+Nfxo*Nfyo+l];
   }
  //bottom edge (outlet)
  for(int k=0;k<Nfxo+1;k++)
   {
    // velocities
    UX[(Nfx+1)*(Nfy+2)+k*Nfyo] = UX[(Nfx+1)*(Nfy+2)+k*Nfyo+1];
    UY[(Nfx+2)*(Nfy+1)+k*Nfyo] = UY[(Nfx+2)*(Nfy+1)+k*Nfyo+1];
   }
#endif
 }

void Pressure_staggered_grid_mapper(parsf &parsFDF)
 {
  cout << "start grid_mapper \n";
  //NOTE: This function assigns to the fluid bounary vectors and to the staggered grid positions their boundary state. 
  //      If a staggered grid position is insight a boundary, it has to enforce the no-slip boundary condition 
  //      scan through cells if a cell has an adjecent boundary, it sets up the boundary vectors accordingly
  //      double counting is possible 
  int Nfx(parsFDF.Nfx), Nfy(parsFDF.Nfy);
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      //### mapping of boundary vectors ###///
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[(k-1)*(Nfy+2)+l]==1))//left cell is boundary 
       {
        parsFDF.ULE.push_back(0.0);                              //boundary X velocity of left boundary
        parsFDF.VLE.push_back(0.0);                              //boundary V velocity of left boundary  
        parsFDF.boundary_mapper_ULE.push_back((k-1)*(Nfy+2)+l);  // maps index of actual field value to the index of the boundary vector
        parsFDF.boundary_mapper_VLE.push_back((k-1)*(Nfy+1)+l-1);// maps index of actual field value to the index of the boundary vector 
        if(parsFDF.boundary_inlet[(k-1)*(Nfy+2)+l]==1){parsFDF.boundary_inlet_ULE.push_back(1); parsFDF.boundary_inlet_VLE.push_back(1);}
        else{parsFDF.boundary_inlet_ULE.push_back(0); parsFDF.boundary_inlet_VLE.push_back(0);}
        if(parsFDF.boundary_outlet[(k-1)*(Nfy+2)+l]==1){parsFDF.boundary_outlet_ULE.push_back(1); parsFDF.boundary_outlet_VLE.push_back(1);}
        else{parsFDF.boundary_outlet_ULE.push_back(0); parsFDF.boundary_outlet_VLE.push_back(0);}
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[k*(Nfy+2)+l-1]==1))//lower cell is boundary
       {
        parsFDF.UL.push_back(0.0);                               //boundary X velocity of lower boundary
        parsFDF.VL.push_back(0.0);                               //boundary V velocity of lower boundary        
        parsFDF.boundary_mapper_UL.push_back(k*(Nfy+2)+l-1);     // maps index of actual field value to the index of the boundary vector    
        parsFDF.boundary_mapper_VL.push_back(k*(Nfy+1)+l-1);     // maps index of actual field value to the index of the boundary vector    
        if(parsFDF.boundary_inlet[k*(Nfy+2)+l-1]==1){parsFDF.boundary_inlet_UL.push_back(1); parsFDF.boundary_inlet_VL.push_back(1);} 
        else{parsFDF.boundary_inlet_UL.push_back(0); parsFDF.boundary_inlet_VL.push_back(0);}
        if(parsFDF.boundary_outlet[k*(Nfy+2)+l-1]==1){parsFDF.boundary_outlet_UL.push_back(1); parsFDF.boundary_outlet_VL.push_back(1);}
        else{parsFDF.boundary_outlet_UL.push_back(0); parsFDF.boundary_outlet_VL.push_back(0);}
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[(k+1)*(Nfy+2)+l]==1))//right cell is boundary
       {
        parsFDF.UR.push_back(0.0);                               //boundary X velocity of right boundary
        parsFDF.VR.push_back(0.0);                               //boundary V velocity of right boundary        
        parsFDF.boundary_mapper_UR.push_back(k*(Nfy+2)+l);       // maps index of actual field value to the index of the boundary vector    
        parsFDF.boundary_mapper_VR.push_back((k+1)*(Nfy+1)+l-1); // maps index of actual field value to the index of the boundary vector    
        if(parsFDF.boundary_inlet[(k+1)*(Nfy+2)+l]==1){parsFDF.boundary_inlet_UR.push_back(1); parsFDF.boundary_inlet_VR.push_back(1);} 
        else{parsFDF.boundary_inlet_UR.push_back(0); parsFDF.boundary_inlet_VR.push_back(0);} 
        if(parsFDF.boundary_outlet[(k+1)*(Nfy+2)+l]==1){parsFDF.boundary_outlet_UR.push_back(1); parsFDF.boundary_outlet_VR.push_back(1);}
        else{parsFDF.boundary_outlet_UR.push_back(0); parsFDF.boundary_outlet_VR.push_back(0);}
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[k*(Nfy+2)+l+1]==1))//upper cell is boundary
       {
        parsFDF.UU.push_back(0.0);                               //boundary X velocity of upper boundary
        parsFDF.VU.push_back(0.0);                               //boundary V velocity of upper boundary
        parsFDF.boundary_mapper_UU.push_back(k*(Nfy+2)+l+1);     // maps index of actual field value to the index of the boundary vector    
        parsFDF.boundary_mapper_VU.push_back(k*(Nfy+1)+l);       // maps index of actual field value to the index of the boundary vector    
        if(parsFDF.boundary_inlet[k*(Nfy+2)+l+1]==1){parsFDF.boundary_inlet_UU.push_back(1); parsFDF.boundary_inlet_VU.push_back(1);} 
        else{parsFDF.boundary_inlet_UU.push_back(0); parsFDF.boundary_inlet_VU.push_back(0);} 
        if(parsFDF.boundary_outlet[k*(Nfy+2)+l+1]==1){parsFDF.boundary_outlet_UU.push_back(1); parsFDF.boundary_outlet_VU.push_back(1);}
        else{parsFDF.boundary_outlet_UU.push_back(0); parsFDF.boundary_outlet_VU.push_back(0);}
       }
     }
   } 
  
  //NOTE: This code part is needed to include also corners into the boundary. 
  //      The vector entries have to be added due to symetry reasons in the simple boundary handling for cavities.
  //      Each corner adds to the staggered grid boundary values two entries at the walls which means that VL,VU, ULE and UR get extra entries
#ifndef complex_boundary
  parsFDF.VL.push_back(0.0);                               //boundary V velocity of lower boundary        
  parsFDF.VL.push_back(0.0);                               //boundary V velocity of lower boundary        
  parsFDF.VU.push_back(0.0);                               //boundary V velocity of upper boundary
  parsFDF.VU.push_back(0.0);                               //boundary V velocity of upper boundary
  parsFDF.ULE.push_back(0.0);                              //boundary X velocity of left boundary
  parsFDF.ULE.push_back(0.0);                              //boundary X velocity of left boundary
  parsFDF.UR.push_back(0.0);                               //boundary X velocity of right boundary
  parsFDF.UR.push_back(0.0);                               //boundary X velocity of right boundary
#endif
  
  //### setting of staggered grid calculation mask ###//
  //UX velocities
  parsFDF.Calc_UX.resize(Nfy+3);//add left boundary UX inicators
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      //### Set mask to enable computation of staggered grid points ###// 
      if(parsFDF.boundary[k*(Nfy+2)+l]==0) 
       {
        if(parsFDF.boundary[(k+1)*(Nfy+2)+l]==1)//right cell is boundary
         {
          parsFDF.Calc_UX.push_back(0.0);
         }
        else{parsFDF.Calc_UX.push_back(1.0);}
       }
      else{parsFDF.Calc_UX.push_back(0.0);}
     }
    parsFDF.Calc_UX.push_back(0.0);
    parsFDF.Calc_UX.push_back(0.0);
   }
  // parsFDF.Calc_UX.resize(Nfy+3);//add right boundary UX inicators
  
  //cout << "len(calc_ux)= " << parsFDF.Calc_UX.size();
  
  //UY velocities
  parsFDF.Calc_UY.resize(Nfy+2);//add left boundary UY inicators
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      //### Set mask to enable computation of staggered grid points ###// 
      if(parsFDF.boundary[k*(Nfy+2)+l]==0)
       {
        if(parsFDF.boundary[k*(Nfy+2)+l+1]==1)//upper cell is boundary
         {
          parsFDF.Calc_UY.push_back(0.0);
         }
        else{parsFDF.Calc_UY.push_back(1.0);}
       }
      else{parsFDF.Calc_UY.push_back(0.0);}
     }
    parsFDF.Calc_UY.push_back(0.0);
   }
  parsFDF.Calc_UY.resize(Nfy+1);//add right boundary UY inicators
  
  //### Setting Pressure calculation mask ###// 
  for(int k=0;k<Nfx+2;k++)
   {
    for(int l=0;l<Nfy+2;l++)
     {
      //### Set mask to enable computation of pressure ###// 
      if(parsFDF.boundary[k*(Nfy+2)+l]==0){parsFDF.Calc_Pr.push_back(1.0); parsFDF.Calc_Tr.push_back(1.0);}
      else{parsFDF.Calc_Pr.push_back(0.0); parsFDF.Calc_Tr.push_back(0.0);}
     }
   }
  
  //### Set pressure gradient multipliers at boundary ###//
  int Nfxo(2*parsFDF.width_outlet+1), Nfyo(parsFDF.length_outlet), Xo(parsFDF.pos_outlet);//outlet boundary dimensions 
  DOUB beta(parsFDF.beta), dx(parsFDF.dx), dy(parsFDF.dy);
  parsFDF.Calc_Pr.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2)+(Nfxo+2)*Nfyo);
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      //setting Poissonian multiplier for type of boundary at position of pressure cell
      if(parsFDF.boundary[k*(Nfy+2)+l]==0)
       {
        //all cells (when no boundary at all)
        parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(2.0/(dx*dx)+2.0/(dy*dy));
        //if one adjacent boundary cells (edges)
        if(parsFDF.boundary[k*(Nfy+2)+l-1]==1)// lower boundary 
         {
#ifdef pressure_outlet
          if(abs(k-Xo)<=(Nfxo-1)/2){parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(2.0/(dx*dx)+2.0/(dy*dy));}//takes into account pressur gradient to the outlet
          else{parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(2.0/(dx*dx)+1.0/(dy*dy));}//no gradient of pressur
#else
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(2.0/(dx*dx)+1.0/(dy*dy));
#endif
         }
        if(parsFDF.boundary[(k+1)*(Nfy+2)+l]==1)// right boundary 
         {
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
         }
        if(parsFDF.boundary[k*(Nfy+2)+l+1]==1)// upper boundary 
         {
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(2.0/(dx*dx)+1.0/(dy*dy));
         }
        if(parsFDF.boundary[(k-1)*(Nfy+2)+l]==1)// left boundary 
         {
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
         }
        //if two adjacent boundary cells (corners) 
        if((parsFDF.boundary[k*(Nfy+2)+l-1]==1) & (parsFDF.boundary[(k+1)*(Nfy+2)+l]==1))//lower right corner
         {
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
         } 
        if((parsFDF.boundary[k*(Nfy+2)+l+1]==1) & (parsFDF.boundary[(k+1)*(Nfy+2)+l]==1))//upper right corner
         {
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
         } 
        if((parsFDF.boundary[k*(Nfy+2)+l+1]==1) & (parsFDF.boundary[(k-1)*(Nfy+2)+l]==1))//upper left corner
         {
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
         } 
        if((parsFDF.boundary[k*(Nfy+2)+l-1]==1) & (parsFDF.boundary[(k-1)*(Nfy+2)+l]==1))//lower left corner
         {
          parsFDF.Calc_Pr[k*(Nfy+2)+l] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
         } 
       }
      else{parsFDF.Calc_Pr[k*(Nfy+2)+l] = 0.0;}//if 0 then no update according to pressure fluxes
     }
   }
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //lower edge l=0 (towards outlet in main domain)
  for(int k=1; k<Nfx+1;k++)
   {
    if(abs(k-Xo)<=(Nfxo-1)/2){parsFDF.Calc_Pr[k*(Nfy+2)] = beta/(2.0/(dx*dx)+2.0/(dy*dy));}//takes into account pressur gradient to the outlet
   }
  //left outlet wall at l=0, k= Xo
  parsFDF.Calc_Pr[(Xo-(Nfxo-1)/2)*(Nfy+2)] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
  //right outlet wall
  parsFDF.Calc_Pr[(Xo+(Nfxo-1)/2)*(Nfy+2)] = beta/(1.0/(dx*dx)+2.0/(dy*dy));

  // outlet main domain (including cells touching the main domain)
  for(int k=1;k<Nfxo+1;k++)
   {
    for(int l=1;l<Nfyo;l++)
     {
      parsFDF.Calc_Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = beta/(2.0/(dx*dx)+2.0/(dy*dy));
     }
   }
  // left edge k=1
  for(int l=2;l<Nfyo;l++)
   {
    parsFDF.Calc_Pr[(Nfx+2)*(Nfy+2)+Nfyo+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  // right edge k=Nfxo
  for(int l=2;l<Nfyo;l++)
   {
  parsFDF.Calc_Pr[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  // bottom edge
  for(int k=2;k<Nfxo;k++)
   {
    parsFDF.Calc_Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+1] = beta/(2.0/(dx*dx)+2.0/(dx*dx));
   }
  // left bottom outlet corner
  parsFDF.Calc_Pr[(Nfx+2)*(Nfy+2)+Nfyo+1] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
  
  // right bottom outlet corner
  parsFDF.Calc_Pr[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+1] = beta/(1.0/(dx*dx)+2.0/(dx*dx));
  
#endif

  //### Setting boundary mapper for tracers ###//
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      //### mapping of boundary vectors ###///
      //NOTE: First entry is the receiving cell, second index is the donating cell
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[(k-1)*(Nfy+2)+l]==1))//left cell is boundary
       {
        parsFDF.boundary_mapper_TrLE.push_back({(k-1)*(Nfy+2)+l,k*(Nfy+2)+l});
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[k*(Nfy+2)+l-1]==1))//lower cell is boundary
       {
        parsFDF.boundary_mapper_TrL.push_back({k*(Nfy+2)+l-1,k*(Nfy+2)+l});
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[(k+1)*(Nfy+2)+l]==1))//right cell is boundary
       {
        parsFDF.boundary_mapper_TrR.push_back({(k+1)*(Nfy+2)+l,k*(Nfy+2)+l});
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & (parsFDF.boundary[k*(Nfy+2)+l+1]==1))//upper cell is boundary
       {
        parsFDF.boundary_mapper_TrU.push_back({k*(Nfy+2)+l+1,k*(Nfy+2)+l});
       }
     }
   }
   
  //### setting pressure index mapping ###//
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      if(parsFDF.boundary[k*(Nfy+2)+l]==0)
       {                        //Pr:   at           left           right            below        above   UX:    left          right  UY: below          above
        parsFDF.Pr_mapper.push_back({k*(Nfy+2)+l,(k-1)*(Nfy+2)+l,(k+1)*(Nfy+2)+l,k*(Nfy+2)+l-1,k*(Nfy+2)+l+1,(k-1)*(Nfy+2)+l,k*(Nfy+2)+l,k*(Nfy+1)+l-1,k*(Nfy+1)+l});
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & ((k*(Nfy+2)+l)%2==0))//defines index map for all even numbers of presssure grid cells
       {                        //Pr:   at           left           right            below        above   UX:    left          right  UY: below          above
        parsFDF.Pr_mapper_even.push_back({k*(Nfy+2)+l,(k-1)*(Nfy+2)+l,(k+1)*(Nfy+2)+l,k*(Nfy+2)+l-1,k*(Nfy+2)+l+1,(k-1)*(Nfy+2)+l,k*(Nfy+2)+l,k*(Nfy+1)+l-1,k*(Nfy+1)+l});
       }
      if((parsFDF.boundary[k*(Nfy+2)+l]==0) & ((k*(Nfy+2)+l)%2!=0))//defines index map for all odd numbers of presssure grid cells
       {                        //Pr:   at           left           right            below        above   UX:    left          right  UY: below          above
        parsFDF.Pr_mapper_odd.push_back({k*(Nfy+2)+l,(k-1)*(Nfy+2)+l,(k+1)*(Nfy+2)+l,k*(Nfy+2)+l-1,k*(Nfy+2)+l+1,(k-1)*(Nfy+2)+l,k*(Nfy+2)+l,k*(Nfy+1)+l-1,k*(Nfy+1)+l});
       }

     }
   }
  //for(int k=0;k<parsFDF.Pr_mapper_even.size();k++){ 
  //for(int l=0;l<9;l++){cout << parsFDF.Pr_mapper_even[k][l] << " ";}
  //cout << "\n";}
  //for(int k=0;k<parsFDF.Pr_mapper_odd.size();k++){ 
  //for(int l=0;l<9;l++){cout << parsFDF.Pr_mapper_odd[k][l] << " ";}
  //cout << "\n";}
   
  //### Setting boundary mapper for large eddy viscosity ###//
 // for(int k=1;k<Nfx+1;k++)
 //  {
 //   for(int l=1;l<Nfy+1;l++)
 //    {
 //     parsFDF.LEV_boundary.push_back();
 //    }
 //  }
   
  //### set map for porpous wall indication ###//
  //UX velocities
  parsFDF.Calc_porousX.resize(Nfy+3); //add left boundary UX inicators
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      if((parsFDF.porous[k*(Nfy+2)+l]==1.0)||(parsFDF.porous[(k+1)*(Nfy+2)+l]==1.0)){parsFDF.Calc_porousX.push_back(1.0);}
      else{parsFDF.Calc_porousX.push_back(0.0);}
     }
    parsFDF.Calc_porousX.push_back(0.0);
    parsFDF.Calc_porousX.push_back(0.0);
   }
     
  //UY velocities
  parsFDF.Calc_porousY.resize(Nfy+2); //add left boundary UY inicators
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      if((parsFDF.porous[k*(Nfy+2)+l]==1.0)||(parsFDF.porous[k*(Nfy+2)+l+1]==1.0)){parsFDF.Calc_porousY.push_back(1.0);}
      else{parsFDF.Calc_porousY.push_back(0.0);}
     }
    parsFDF.Calc_porousY.push_back(0.0);
   }
  parsFDF.Calc_porousY.resize(Nfy+1);//add right boundary UY inicators
  
 }

void NS_STEP1(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> &UXN, vector<DOUB> &UYN, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB dtf, DOUB &rho, DOUB &mu, int &Nfxo, int &Nfyo, int &Xo, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, vector<DOUB> &LEV, vector<DOUB> &Calc_porousX, vector<DOUB> &Calc_porousY)//initial velocity step
 { 
  //+   +   +   +
  //  x   x4  x
  //+   +9  +8  +
  //  x3  x1  x2
  //+   +6  +7  +
  //  x   x5  x
  //+   +   +   +
  //
  //212113134141156721398
  //cout << " UX in main domain \n";
  #pragma omp parallel for //shared(UXN,UYN,UX,UY)
  //### Approximation of velocity ###//
  //UX
  for(int k=1;k<Nfx;k++)//x
  {
   for(int l=1;l<Nfy+1;l++)//y
    {
    //cout << k << " " << l << " x " << k*(Nfy+2)+l << " x " << (k+1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k+1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k-1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k-1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l+1 << " x " << k*(Nfy+2)+l << " y " << (k+1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l-1 << " y " << (k+1)*(Nfy+1)+l-1 << " y " << k*(Nfy+1)+l-1 << " x " << (k+1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k-1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l+1 << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l-1 << "\n";
    // cout << " (k,l)= (" << k << "," << l << ") " << Calc_UX[k*(Nfy+2)+l] << ", " << UXN[k*(Nfy+2)+l] << "\n";
     UXN[k*(Nfy+2)+l] = UX[k*(Nfy+2)+l] + dtf*(Calc_UX[k*(Nfy+2)+l])*(-((UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])*(UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])                                                                                                          -(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])*(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l]))*0.25/dx                                                                                                  -((UX[k*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l])*(UY[(k+1)*(Nfy+1)+l]+UY[k*(Nfy+1)+l])                                                                                                            -(UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])*(UY[(k+1)*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                + (mu/rho + (3.0/8.0)*(LEV[(k+1)*(Nfy+2)+l]+LEV[k*(Nfy+2)+l]) + (LEV[k*(Nfy+2)+l+1]+LEV[k*(Nfy+2)+l-1]+LEV[(k+1)*(Nfy+2)+l+1]+LEV[(k+1)*(Nfy+2)+l-1])/16.0)*((UX[(k+1)*(Nfy+2)+l]-2.0*UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                        + (UX[k*(Nfy+2)+l+1]-2.0*UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])/(dy*dy)) + 0.0000000);
    //UXN[k*(Nfy+2)+l] = UX[k*(Nfy+2)+l] + dtf*(-((UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])*(UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])                                                                                                          -(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])*(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l]))*0.25/dx                                                                                                  -((UX[k*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l])*(UY[(k+1)*(Nfy+1)+l]+UY[k*(Nfy+1)+l])                                                                                                            -(UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])*(UY[(k+1)*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                + mu/rho*((UX[(k+1)*(Nfy+2)+l]-2.0*UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                        + (UX[k*(Nfy+2)+l+1]-2.0*UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])/(dy*dy)));    
    }
  }
  #pragma omp barrier
  //UY
  //cout << " UY in main domain \n";
  #pragma omp parallel for
  for(int k=1;k<Nfx+1;k++)//x
  {
   for(int l=1;l<Nfy;l++)//y
    {
    //cout << k << " " << l << " y " << k*(Nfy+1)+l << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l+1 << " y " << k*(Nfy+1)+l << " y " << (k+1)*(Nfy+1)+l << " x " << (k-1)*(Nfy+2)+l+1 << " x " << (k-1)*(Nfy+2)+l << " y " << k*(Nfy+1)+l << " y " << (k-1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l+1 << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l+1 << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l-1 << "y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l-1 << " y " << (k+1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l << " y " << (k-1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l+1 << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l-1 << "\n";
     UYN[k*(Nfy+1)+l] = UY[k*(Nfy+1)+l] + dtf*(Calc_UY[k*(Nfy+1)+l])*(-((UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l+1])*(UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l])                                                                                                            -(UX[(k-1)*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l])*(UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l]))*0.25/dx                                                                           		    -((UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])*(UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])                                                                                                              -(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])*(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                      + (mu/rho + (3.0/8.0)*(LEV[k*(Nfy+2)+l+1]+LEV[k*(Nfy+2)+l]) + (LEV[(k-1)*(Nfy+2)+l+1]+LEV[(k-1)*(Nfy+2)+l]+LEV[(k+1)*(Nfy+2)+l+1]+LEV[(k+1)*(Nfy+2)+l])/16.0)*((UY[(k+1)*(Nfy+1)+l]-2.0*UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l])/(dx*dx)                                                                                                        + (UY[k*(Nfy+1)+l+1]-2.0*UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])/(dy*dy)));
//    UYN[k*(Nfy+1)+l] = UY[k*(Nfy+1)+l] + dtf*(-((UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l+1])*(UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l])                                                                                                            -(UX[(k-1)*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l])*(UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l]))*0.25/dx                                                                           		    -((UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])*(UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])                                                                                                              -(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])*(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                      + mu/rho*((UY[(k+1)*(Nfy+1)+l]-2.0*UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l])/(dx*dx)                                                                                                        + (UY[k*(Nfy+1)+l+1]-2.0*UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])/(dy*dy)));
    }
  }
  #pragma omp barrier
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //UX
  //cout << "UX in outlet domain \n";
  #pragma omp parallel for  
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      //cout << k << " " << l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l << " " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l+1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l+1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l-1 << "\n";
      UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+l] = UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]                                                                                                                                           + dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])                                               -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]))*0.25/dx                                              -((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                            + (mu/rho + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l]) + (LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l-1])/16.0)*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)));
     }
   }
  #pragma omp barrier
 //coupling to main domain
 //UX in outlet
 //cout << " UX in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo;k++)//x at l=Nfyo-1
  {
   //cout << k << " " << Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1 << "\n";
   UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] = UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                            + dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])                                  -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                        + (mu/rho  + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]) + (LEV[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2]+LEV[(Xo-1-(Nfxo-1)/2+k+1)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-2])/16.0)*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
 
  }
 //UX in main domain
 //cout << " UX in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
  {
   //cout << k << " " << 0 << " x " << k*(Nfy+2) << " x " << (k+1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k+1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k-1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k-1)*(Nfy+2) << " x " << k*(Nfy+2)+1 << " x " << k*(Nfy+2) << " y " << (k+1)*(Nfy+1) << " y " << k*(Nfy+1) << " x " << k*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2+1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " x " << (k+1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k-1)*(Nfy+2) << " x " << k*(Nfy+2)+1 << " x " << k*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << "\n";
   UXN[k*(Nfy+2)] = UX[k*(Nfy+2)] + dtf*(-((UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])*(UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])                                                                                                           -(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])*(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)]))*0.25/dx                                                                                                          -((UX[k*(Nfy+2)+1]+UX[k*(Nfy+2)])*(UY[(k+1)*(Nfy+1)]+UY[k*(Nfy+1)])                                                                                                                    -(UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]                                                             +UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                                                                                                        + (mu/rho + (3.0/8.0)*(LEV[(k+1)*(Nfy+2)]+LEV[k*(Nfy+2)]) + (LEV[k*(Nfy+2)+1]+LEV[(Nfx+2)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]+LEV[(k+1)*(Nfy+2)+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/16.0)*((UX[(k+1)*(Nfy+2)]-2.0*UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                              + (UX[k*(Nfy+2)+1]-2.0*UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
 //UY
 //cout << "UY in outlet domain \n";
   #pragma omp parallel for  
 for(int k=1;k<Nfxo+1;k++)//x
  {
   for(int l=1;l<Nfyo-1;l++)//y
    {
     //cout << k << " " << l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << "\n";
     UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+l] = UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]                                                                                                                                           + dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l])                                                 -(UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l]))*0.25/dx                                        -((UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                          -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                                  + (mu/rho + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l]+LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1]) + (LEV[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l]+LEV[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l])/16.0)*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/(dy*dy)));
    }
  }
  #pragma omp barrier
 //coupling to main domain
 //UY in outlet
 //cout << " UY in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
  {
   //cout << k << " " << Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+2) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1 << " y " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1 << " y " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << "\n";
   UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] = UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]                                                                                                                                  + dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1])                                 -(UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])*(UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                              -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                              + (mu/rho + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]+LEV[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]) + (LEV[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]+LEV[(Xo-1-(Nfxo-1)/2+k+1)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1])/16.0)*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
  }
 //UY in main domain
 //cout << " UY in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
  {
   //cout << k << " " << 0 << " y " << k*(Nfy+1) << " x " << k*(Nfy+2) << " x " << k*(Nfy+2)+1 << " y " << k*(Nfy+1) << " y " << (k+1)*(Nfy+1) << " x " << (k-1)*(Nfy+2)+1 << " x " << (k-1)*(Nfy+2) << " y " << k*(Nfy+1) << " y " << (k-1)*(Nfy+1) << " y " << k*(Nfy+1)+1 << " y " << k*(Nfy+1) << " y " << k*(Nfy+1)+1 << " y " << k*(Nfy+1) << " y " << k*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " y " << k*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " y " << (k+1)*(Nfy+1) << " y " << k*(Nfy+1) << " y " << (k-1)*(Nfy+1) << " y " << k*(Nfy+1)+1 << " y " << k*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << "\n";
   UYN[k*(Nfy+1)] = UY[k*(Nfy+1)] + dtf*(-((UX[k*(Nfy+2)]+UX[k*(Nfy+2)+1])*(UY[k*(Nfy+1)]+UY[(k+1)*(Nfy+1)])                                                                                                                    -(UX[(k-1)*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)])*(UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)]))*0.25/dx                                                                                                    -((UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])*(UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])                                                                                                                      -(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                      + (mu/rho + (3.0/8.0)*(LEV[k*(Nfy+2)+1]+LEV[k*(Nfy+2)]) + (LEV[(k-1)*(Nfy+2)+1]+LEV[(k-1)*(Nfy+2)]+LEV[(k+1)*(Nfy+2)+1]+LEV[(k+1)*(Nfy+2)])/16.0)*((UY[(k+1)*(Nfy+1)]-2.0*UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)])/(dx*dx)                                                                                                              + (UY[k*(Nfy+1)+1]-2.0*UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
#endif
 }

void NS_STEP5_WIND_FORCING(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> UWX, vector<DOUB> UWY, DOUB &tf, parsf &parsFDF, DOUB &T, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, DOUB Vwind, DOUB thwind, vector<DOUB> &Calc_porousX, vector<DOUB> &Calc_porousY)
 {
  //NOTE: The wind forcing is assumed to be according to F/rho_sea=dtau/dz*1/(rho_air*rho_sea)=-C_m*S*(U_sea-U_air)/rho_sea
  //      The coupling constant needs to be estimated S=4 m/s according to "Long-Term Variability of wind speed and direction in the Mediterranean basin"
  //      The momentum coupling C_m= 1.4*10^-3 according to "The effective drag coefficient for evaluating wind stress over the ocean"
  //      The surface wind stress causes an Eckman velocity component. Not in use yet! Since the water density is 1000 and 
  //      Israels Coriolis Parameter is given by f=2*omega*sin(phi) = 2*7.29*10^(-5)*sin(32/180*pi)= 7.7*10^-5
  //      The overall coupling constant is given by C*S/(rho*f)= 0.072
  //      e.e. v_e = -0.072*(UX-UWX), u_e = 0.072*(UY-UWY)
  
  DOUB CmS(0.056/parsFDF.rho), W(Vwind*(tanh(5.0*(tf-10.0+0.5*T)/T) + 1.0)*0.5), a(thwind);//downward windS
  //UX
#pragma omp parallel for 
  for(int k=1;k<parsFDF.Nfx;k++)//x
   {
    for(int l=1;l<parsFDF.Nfy+1;l++)//y
     {
      UWX[k*(parsFDF.Nfy+2)+l] = W*cos(a);
      UX[k*(parsFDF.Nfy+2)+l] -= parsFDF.dtf*Calc_UX[k*(parsFDF.Nfy+2)+l]*(CmS*(UX[k*(parsFDF.Nfy+2)+l]-UWX[k*(parsFDF.Nfy+2)+l]) + Calc_porousX[k*(parsFDF.Nfy+2)+l]*(parsFDF.Cpor1X*UX[k*(parsFDF.Nfy+2)+l] + parsFDF.rho*0.5*parsFDF.Cpor2X*sqrt(UX[k*(parsFDF.Nfy+2)+l]*UX[k*(parsFDF.Nfy+2)+l]+(UY[k*(parsFDF.Nfy+1)+l]+UY[(k+1)*(parsFDF.Nfy+1)+l]+UY[k*(parsFDF.Nfy+1)+l-1]+UY[(k+1)*(parsFDF.Nfy+1)+l-1])*(UY[k*(parsFDF.Nfy+1)+l]+UY[(k+1)*(parsFDF.Nfy+1)+l]+UY[k*(parsFDF.Nfy+1)+l-1]+UY[(k+1)*(parsFDF.Nfy+1)+l-1])/16.0)*UX[k*(parsFDF.Nfy+2)+l]));
     }
   }
#pragma omp barrier
  //UY
#pragma omp parallel for 
  for(int k=1;k<parsFDF.Nfx+1;k++)//x
   {
    for(int l=1;l<parsFDF.Nfy;l++)//y
     {
      UWY[k*(parsFDF.Nfy+1)+l] = W*sin(a);
      UY[k*(parsFDF.Nfy+1)+l] -= parsFDF.dtf*Calc_UY[k*(parsFDF.Nfy+1)+l]*(CmS*(UY[k*(parsFDF.Nfy+1)+l]-UWY[k*(parsFDF.Nfy+1)+l]) + Calc_porousY[k*(parsFDF.Nfy+1)+l]*(parsFDF.Cpor1Y*UY[k*(parsFDF.Nfy+1)+l] + parsFDF.rho*0.5*parsFDF.Cpor2Y*sqrt(UY[k*(parsFDF.Nfy+1)+l]*UY[k*(parsFDF.Nfy+1)+l]+(UX[(k-1)*(parsFDF.Nfy+2)+l]+UX[k*(parsFDF.Nfy+2)+l]+UX[(k-1)*(parsFDF.Nfy+2)+l+1]+UX[k*(parsFDF.Nfy+2)+l+1])*(UX[(k-1)*(parsFDF.Nfy+2)+l]+UX[k*(parsFDF.Nfy+2)+l]+UX[(k-1)*(parsFDF.Nfy+2)+l+1]+UX[k*(parsFDF.Nfy+2)+l+1])/16.0)*UY[k*(parsFDF.Nfy+1)+l]));
     }
   }
#pragma omp barrier
 }

void NS_STEP5_WIND_FORCING_HEUN(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> &UXN, vector<DOUB> &UYN, vector<DOUB> &UWX, vector<DOUB> &UWY, DOUB &tf, parsf &parsFDF, DOUB &T, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, DOUB Vwind, DOUB thwind, vector<DOUB> &Calc_porousX, vector<DOUB> &Calc_porousY)
 {
  //NOTE: The wind forcing is assumed to be according to F/rho_sea=dtau/dz*1/(rho_air*rho_sea)=-C_m*S*(U_sea-U_air)/rho_sea
  //      The coupling constant needs to be estimated S=4 m/s according to "Long-Term Variability of wind speed and direction in the Mediterranean basin"
  //      The momentum coupling C_m= 1.4*10^-3 according to "The effective drag coefficient for evaluating wind stress over the ocean"
  //      The surface wind stress causes an Eckman velocity component. Not in use yet! Since the water density is 1000 and 
  //      Israels Coriolis Parameter is given by f=2*omega*sin(phi) = 2*7.29*10^(-5)*sin(32/180*pi)= 7.7*10^-5
  //      The overall coupling constant is given by C*S/(rho*f)= 0.072
  //      e.e. v_e = -0.072*(UX-UWX), u_e = 0.072*(UY-UWY)
  
  DOUB CmS(0.056/parsFDF.rho), W(Vwind*(tanh(5.0*(tf-10.0+0.5*T)/T) + 1.0)*0.5), a(thwind);//downward windS
  //UX
#pragma omp parallel for 
  for(int k=1;k<parsFDF.Nfx;k++)//x
   {
    for(int l=1;l<parsFDF.Nfy+1;l++)//y
     {
      UWX[k*(parsFDF.Nfy+2)+l] = W*cos(a);
      UXN[k*(parsFDF.Nfy+2)+l] -= parsFDF.dtf*Calc_UX[k*(parsFDF.Nfy+2)+l]*(CmS*(UX[k*(parsFDF.Nfy+2)+l]-UWX[k*(parsFDF.Nfy+2)+l]) + Calc_porousX[k*(parsFDF.Nfy+2)+l]*(parsFDF.Cpor1X*UX[k*(parsFDF.Nfy+2)+l] + parsFDF.rho*0.5*parsFDF.Cpor2X*sqrt(UX[k*(parsFDF.Nfy+2)+l]*UX[k*(parsFDF.Nfy+2)+l]+(UY[k*(parsFDF.Nfy+1)+l]+UY[(k+1)*(parsFDF.Nfy+1)+l]+UY[k*(parsFDF.Nfy+1)+l-1]+UY[(k+1)*(parsFDF.Nfy+1)+l-1])*(UY[k*(parsFDF.Nfy+1)+l]+UY[(k+1)*(parsFDF.Nfy+1)+l]+UY[k*(parsFDF.Nfy+1)+l-1]+UY[(k+1)*(parsFDF.Nfy+1)+l-1])*0.125)*UX[k*(parsFDF.Nfy+2)+l]));
     }
   }
#pragma omp barrier
  //UY
#pragma omp parallel for 
  for(int k=1;k<parsFDF.Nfx+1;k++)//x
   {
    for(int l=1;l<parsFDF.Nfy;l++)//y
     {
      UWY[k*(parsFDF.Nfy+1)+l] = W*sin(a);
      UYN[k*(parsFDF.Nfy+1)+l] -= parsFDF.dtf*Calc_UY[k*(parsFDF.Nfy+1)+l]*(CmS*(UY[k*(parsFDF.Nfy+1)+l]-UWY[k*(parsFDF.Nfy+1)+l]) + Calc_porousY[k*(parsFDF.Nfy+1)+l]*(parsFDF.Cpor1Y*UY[k*(parsFDF.Nfy+1)+l] + parsFDF.rho*0.5*parsFDF.Cpor2Y*sqrt(UY[k*(parsFDF.Nfy+1)+l]*UY[k*(parsFDF.Nfy+1)+l]+(UX[(k-1)*(parsFDF.Nfy+2)+l]+UX[k*(parsFDF.Nfy+2)+l]+UX[(k-1)*(parsFDF.Nfy+2)+l+1]+UX[k*(parsFDF.Nfy+2)+l+1])*(UX[(k-1)*(parsFDF.Nfy+2)+l]+UX[k*(parsFDF.Nfy+2)+l]+UX[(k-1)*(parsFDF.Nfy+2)+l+1]+UX[k*(parsFDF.Nfy+2)+l+1])*0.125)*UY[k*(parsFDF.Nfy+1)+l]));
     }
   }
#pragma omp barrier
 }

void NS_STEP6(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> &UXN, vector<DOUB> &UYN, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB dtf, DOUB &rho, DOUB &mu, int &Nfxo, int &Nfyo, int &Xo, DOUB &damping, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, DOUB &f, DOUB &GravX, DOUB &GravY, DOUB &Vi)//, DOUB &lam)//initial velocity step
 {
  //NOTE: This function implements -a drag in the outlet that is ramped up from start to final outlet layer
  //                               -Corriolis force with constant f

  //### Corriolis forcing ###//
  //### Main domain Corriolis forcing ### //
  //DOUB f(0.00007943); //Corriolis parameter f= 2*sin(th)*7.2921 s^(-1) Haifa (33 degree north)
#pragma omp parallel for 
  for(int k=1;k<Nfx;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      UXN[k*(Nfy+2)+l] -= dtf*Calc_UX[k*(Nfy+2)+l]*f*0.25*(UY[k*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l-1]);
     }
   } 
#pragma omp barrier

#pragma omp parallel for 
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy;l++)//y
     {
      UYN[k*(Nfy+1)+l] += dtf*Calc_UY[k*(Nfy+1)+l]*f*0.25*(UX[(k-1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l+1]);
     }
   } 
#pragma omp barrier
 
    //### Drag in the outlet ###//
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //UX  
  #pragma omp parallel for 
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+l] = UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] + dtf*(-damping*0.5*(1.0+tanh(8.0*(0.5*DOUB(Nfyo-1)-DOUB(l))/DOUB(Nfyo-1)))*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]);
     }
   }
/*  //gravity forcing
  //UY
  for(int k=1;k<Nfxo+1;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {//TODO: gravity!
      UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+l] = UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l];// + dtf*sqrt(GravX*GravX+GravY*GravY)*0.5*(1.0+tanh(8.0*(0.5*DOUB(Nfyo-1)-DOUB(l))/DOUB(Nfyo-1))));
     }
   }//*/ 
  #pragma omp barrier
#endif   
 }
 
void NS_STEP6_HEUN(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> &UXN, vector<DOUB> &UYN, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB dtf, DOUB &rho, DOUB &mu, int &Nfxo, int &Nfyo, int &Xo, DOUB &damping, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, DOUB &f, DOUB &GravX, DOUB &GravY, DOUB &Vi)//, DOUB &lam)//initial velocity step
 {
  //NOTE: This function implements a drag in the outlet that is ramped up from start to final outlet layer

  //### Corriolis forcing ###//
  //### Main domain Corriolis forcing ### //
  //DOUB f(0.00007943); //Corriolis parameter f= 2*sin(th)*7.2921 s^(-1) Haifa (33 degree north)
#pragma omp parallel for 
  for(int k=1;k<Nfx;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      UXN[k*(Nfy+2)+l] -= dtf*Calc_UX[k*(Nfy+2)+l]*f*0.25*(UY[k*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l-1]);
     }
   } 
#pragma omp barrier

#pragma omp parallel for 
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy;l++)//y
     {
      UYN[k*(Nfy+1)+l] += dtf*Calc_UY[k*(Nfy+1)+l]*f*0.25*(UX[(k-1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l+1]);
     }
   } 
#pragma omp barrier
 
    //### Drag in the outlet ###//
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //UX  
  #pragma omp parallel for 
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+l] += dtf*(-damping*0.5*(1.0+tanh(8.0*(0.5*DOUB(Nfyo-1)-DOUB(l))/DOUB(Nfyo-1)))*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]);
     }
   }
  //UY
  //Gravity forcing
/*  for(int k=1;k<Nfxo+1;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {//TODO: gravity
      UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+l] += dtf*sqrt(GravX*GravX+GravY*GravY)*0.5*(1.0+tanh(8.0*(0.5*DOUB(Nfyo-1)-DOUB(l))/DOUB(Nfyo-1)));
     }
   }//*/ 
  #pragma omp barrier
#endif   
 }
  
void NS_STEP7(vector<DOUB> &UX, vector<DOUB> &UY, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, int &Nfxo, int &Nfyo, int &Xo, vector<DOUB> &LEV, DOUB &CSL, vector<DOUB> &boundary)//initial velocity step
 {
  //NOTE: This function calculates the Smagorinsky Large Eddy Viscosity from the strain rate tensor. Values are stored on the pressure nodes.
  //TODO: implement calculation 1. with constant smagorinsky coefficient 2. with variable one. Take care of boundary values for LEV! This might be a tedious because the boundary cells will be provided with erroneous gradients of velocity due to reversal flow...
  
  vector<DOUB> LEVi(LEV.size());
  //main domain
  DOUB muave(0.0), mumax(0.0);
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      LEVi[k*(Nfy+2)+l] = (1.0-boundary[k*(Nfy+2)+l])*CSL*CSL*dx*dy*sqrt((0.25*((UX[k*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l+1])-(UX[k*(Nfy+2)+l-1]+UX[(k-1)*(Nfy+2)+l-1]))/dy + 0.25*((UY[(k+1)*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l-1])-(UY[(k-1)*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l-1]))/dx)*(0.25*((UX[k*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l+1])-(UX[k*(Nfy+2)+l-1]+UX[(k-1)*(Nfy+2)+l-1]))/dy + 0.25*((UY[(k+1)*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l-1])-(UY[(k-1)*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l-1]))/dx) + 2.0*(UX[k*(Nfy+2)+l] - UX[(k-1)*(Nfy+2)+l])*(UX[k*(Nfy+2)+l] - UX[(k-1)*(Nfy+2)+l])/(dx*dx) + 2.0*(UY[k*(Nfy+1)+l] - UY[k*(Nfy+1)+l-1])*(UY[k*(Nfy+1)+l] - UY[k*(Nfy+1)+l-1])/(dy*dy));
      muave += LEV[k*(Nfy+2)+l];
      if(LEVi[k*(Nfy+2)+l]>mumax){mumax=LEVi[k*(Nfy+2)+l];}
     }  
   }
  //### Boundary cells ###//
  //NOTE: Here it is made sure that boundary values are not affected by the reverse flows
  //for(int l=1;l<Nfy+1;l++)//y
  // {
    
  //### outlet domain, main domain
  for(int k=1;k<Nfxo+1;k++)
   {
    for(int l=1;l<Nfyo-1;l++)
     {
      LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = CSL*CSL*dx*dy*sqrt((0.25*((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1])-(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l-1]))/dy + 0.25*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1])-(UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l-1]))/dx)*(0.25*((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1])-(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l-1]))/dy + 0.25*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1])-(UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l-1]))/dx) + 2.0*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx) + 2.0*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/(dy*dy));
      muave += LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l];
      if(LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+l]>mumax){mumax=LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+l];}
     }
   }//*/

  // LEV in outlet at l=Nfyo-1
  for(int k=1;k<Nfxo+1;k++)
   {
    LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = CSL*CSL*dx*dy*sqrt((0.25*((UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)])-(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-2]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-2]))/dy + 0.25*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-2])-(UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-2]))/dx)*(0.25*((UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)])-(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-2]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-2]))/dy + 0.25*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-2])-(UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-2]))/dx) + 2.0*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx) + 2.0*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-2])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-2])/(dy*dy));
    muave += LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1];
    if(LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]>mumax){mumax=LEVi[k*(Nfy+2)+Nfyo-1];}
   }
  // LEV in main domain at l=0
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)
   {
//    LEV[k*(Nfy+2)] = CSL*dx*dy*sqrt((0.25*((UX[k*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)+1])-(UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2-1)*Nfyo+Nfyo-1]))/dy + 0.25*((UY[(k-1)*(Nfy+1)]+UY[(k-1)*(Nfy+1)])-(UY[(k+1)*(Nfy+1)]+UY[(k+1)*(Nfy+1)]))/dx)*(0.25*((UX[k*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)+1])-(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]))/dy + 0.25*((UY[(k-1)*(Nfy+1)]+UY[(k-1)*(Nfy+1)])-(UY[(k+1)*(Nfy+1)]+UY[(k+1)*(Nfy+1)]))/dx) + 2.0*(UX[k*(Nfy+2)] - UX[(k-1)*(Nfy+2)])*(UX[k*(Nfy+2)] - UX[(k-1)*(Nfy+2)])/(dx*dx) + 2.0*(UY[k*(Nfy+1)] - UY[k*(Nfy+1)])*(UY[k*(Nfy+1)] - UY[k*(Nfy+1)])/(dy*dy));

    LEVi[k*(Nfy+2)] = CSL*CSL*dx*dy*sqrt((0.25*((UX[k*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)+1])-(UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))/dy + 0.25*((UY[(k+1)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k+1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]) - (UY[(k-1)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))/dx)*(0.25*((UX[k*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)+1])-(UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))/dy + 0.25*((UY[(k+1)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k+1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])-(UY[(k-1)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))/dx) + 2.0*(UX[k*(Nfy+2)] - UX[(k-1)*(Nfy+2)])*(UX[k*(Nfy+2)] - UX[(k-1)*(Nfy+2)])/(dx*dx) + 2.0*(UY[k*(Nfy+1)] - UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[k*(Nfy+1)] - UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy));
    muave += LEV[k*(Nfy+2)];
    if(LEVi[k*(Nfy+2)]>mumax){mumax=LEVi[k*(Nfy+2)];}
   }//*/
//  cout << "average eddy viscosity= " << muave/(DOUB((Nfx+2)*(Nfy+2) + (Nfxo+2)*Nfyo)) << " maximal eddy viscosity= " << mumax << "\n"; 
  cout << "average eddy viscosity= " << muave/(DOUB((Nfx+2)*(Nfy+2))) << " maximal eddy viscosity= " << mumax << "\n"; 

  //### average main domain ###//
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      LEV[k*(Nfy+2)+l] = 0.2*LEVi[k*(Nfy+2)+l] + (2.0/15.0)*(LEVi[(k-1)*(Nfy+2)+l] + LEVi[(k+1)*(Nfy+2)+l] + LEVi[k*(Nfy+2)+l+1] + LEVi[k*(Nfy+2)+l-1]) + (LEVi[(k-1)*(Nfy+2)+l+1] + LEVi[(k+1)*(Nfy+2)+l+1] + LEVi[(k-1)*(Nfy+2)+l-1] + LEVi[(k+1)*(Nfy+2)+l-1])/15.0;
     }
   }
  //### average outlet domain ###//
  for(int k=1;k<Nfxo+1;k++)
   {
    for(int l=1;l<Nfyo-1;l++)
     {
      LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = 0.2*LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + (2.0/15.0)*(LEVi[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l] + LEVi[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] + LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] + LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1]) + (LEVi[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l+1] + LEVi[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l+1] + LEVi[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l-1] + LEVi[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l-1])/15.0;
     }
   }
  //### average in outlet at l=Nfyo-1 ###//
  for(int k=1;k<Nfxo+1;k++)
   {
    LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = 0.2*LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] + (2.0/15.0)*(LEVi[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1] + LEVi[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] + LEVi[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] + LEVi[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2]) + (LEVi[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)] + LEVi[(Xo-1-(Nfxo-1)/2+k+1)*(Nfy+2)] + LEVi[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-2] + LEVi[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-2])/15.0;
   }  
  //### average in main domain at l=0 ###//   
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)
   {
    LEV[k*(Nfy+2)] = 0.2*LEVi[k*(Nfy+2)] + (2.0/15.0)*(LEVi[(k-1)*(Nfy+2)] + LEVi[(k+1)*(Nfy+2)] + LEVi[k*(Nfy+2)+1] + LEVi[(Nfx+2)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]) + (LEVi[(k-1)*(Nfy+2)+1] + LEVi[(k+1)*(Nfy+2)+1] + LEVi[(Nfx+2)*(Nfy+2)+(k+1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1] + LEVi[(Nfx+2)*(Nfy+2)+(k-1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/15.0;
   } 
 }

void NS_STEP1_HEUN(vector<DOUB> UX, vector<DOUB> UY, vector<DOUB> &UXN, vector<DOUB> &UYN, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB dtf, DOUB &rho, DOUB &mu, int &Nfxo, int &Nfyo, int &Xo, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, vector<DOUB> &LEV, vector<DOUB> &Calc_porousX, vector<DOUB> &Calc_porousY)//initial velocity step
 {
  //+   +   +   +
  //  x   x4  x
  //+   +9  +8  +
  //  x3  x1  x2
  //+   +6  +7  +
  //  x   x5  x
  //+   +   +   +
  //
  //212113134141156721398
  
  #pragma omp parallel for //shared(UX,UY)
  //### Approximation of velocity ###//
  //UX
  //cout << " UX in main domain \n";
  for(int k=1;k<Nfx;k++)//x
  {
   for(int l=1;l<Nfy+1;l++)//y
    {
    //cout << k << " " << l << " x " << k*(Nfy+2)+l << " x " << (k+1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k+1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k-1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k-1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l+1 << " x " << k*(Nfy+2)+l << " y " << (k+1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l-1 << " y " << (k+1)*(Nfy+1)+l-1 << " y " << k*(Nfy+1)+l-1 << " x " << (k+1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l << " x " << (k-1)*(Nfy+2)+l << " x " << k*(Nfy+2)+l+1 << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l-1 << "\n";
     UXN[k*(Nfy+2)+l] = dtf*(Calc_UX[k*(Nfy+2)+l])*(-((UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])*(UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])                                                                                                          -(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])*(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l]))*0.25/dx                                                                                                  -((UX[k*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l])*(UY[(k+1)*(Nfy+1)+l]+UY[k*(Nfy+1)+l])                                                                                                            -(UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])*(UY[(k+1)*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                + (mu/rho + (3.0/8.0)*(LEV[(k+1)*(Nfy+2)+l]+LEV[k*(Nfy+2)+l]) + (LEV[k*(Nfy+2)+l+1]+LEV[k*(Nfy+2)+l-1]+LEV[(k+1)*(Nfy+2)+l+1]+LEV[(k+1)*(Nfy+2)+l-1])/16.0)*((UX[(k+1)*(Nfy+2)+l]-2.0*UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                        + (UX[k*(Nfy+2)+l+1]-2.0*UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])/(dy*dy)));
//    UXN[k*(Nfy+2)+l] = dtf*(-((UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])*(UX[(k+1)*(Nfy+2)+l]+UX[k*(Nfy+2)+l])                                                                                                          -(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])*(UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l]))*0.25/dx                                                                                                  -((UX[k*(Nfy+2)+l+1]+UX[k*(Nfy+2)+l])*(UY[(k+1)*(Nfy+1)+l]+UY[k*(Nfy+1)+l])                                                                                                            -(UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])*(UY[(k+1)*(Nfy+1)+l-1]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                + mu/rho*((UX[(k+1)*(Nfy+2)+l]-2.0*UX[k*(Nfy+2)+l]+UX[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                        + (UX[k*(Nfy+2)+l+1]-2.0*UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l-1])/(dy*dy)));
    }
  }
  #pragma omp barrier
  //UY
  //cout << " UY in main domain \n";
  #pragma omp parallel for
  for(int k=1;k<Nfx+1;k++)//x
  {
   for(int l=1;l<Nfy;l++)//y
    {
    //cout << k << " " << l << " y " << k*(Nfy+1)+l << " x " << k*(Nfy+2)+l << " x " << k*(Nfy+2)+l+1 << " y " << k*(Nfy+1)+l << " y " << (k+1)*(Nfy+1)+l << " x " << (k-1)*(Nfy+2)+l+1 << " x " << (k-1)*(Nfy+2)+l << " y " << k*(Nfy+1)+l << " y " << (k-1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l+1 << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l+1 << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l-1 << "y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l-1 << " y " << (k+1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l << " y " << (k-1)*(Nfy+1)+l << " y " << k*(Nfy+1)+l+1 << " y " << k*(Nfy+1)+l << " y " << k*(Nfy+1)+l-1 << "\n";
     UYN[k*(Nfy+1)+l] = dtf*(Calc_UY[k*(Nfy+1)+l])*(-((UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l+1])*(UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l])                                                                                                            -(UX[(k-1)*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l])*(UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l]))*0.25/dx                                                                           		    -((UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])*(UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])                                                                                                              -(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])*(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                      + (mu/rho + (3.0/8.0)*(LEV[k*(Nfy+2)+l+1]+LEV[k*(Nfy+2)+l]) + (LEV[(k-1)*(Nfy+2)+l+1]+LEV[(k-1)*(Nfy+2)+l]+LEV[(k+1)*(Nfy+2)+l+1]+LEV[(k+1)*(Nfy+2)+l])/16.0)*((UY[(k+1)*(Nfy+1)+l]-2.0*UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l])/(dx*dx)                                                                                                        + (UY[k*(Nfy+1)+l+1]-2.0*UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])/(dy*dy)));
//    UYN[k*(Nfy+1)+l] = dtf*(-((UX[k*(Nfy+2)+l]+UX[k*(Nfy+2)+l+1])*(UY[k*(Nfy+1)+l]+UY[(k+1)*(Nfy+1)+l])                                                                                                            -(UX[(k-1)*(Nfy+2)+l+1]+UX[(k-1)*(Nfy+2)+l])*(UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l]))*0.25/dx                                                                           		    -((UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])*(UY[k*(Nfy+1)+l+1]+UY[k*(Nfy+1)+l])                                                                                                              -(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])*(UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1]))*0.25/dy                                                                                                      + mu/rho*((UY[(k+1)*(Nfy+1)+l]-2.0*UY[k*(Nfy+1)+l]+UY[(k-1)*(Nfy+1)+l])/(dx*dx)                                                                                                        + (UY[k*(Nfy+1)+l+1]-2.0*UY[k*(Nfy+1)+l]+UY[k*(Nfy+1)+l-1])/(dy*dy)));
    }
  }
 #pragma omp barrier 
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //UX
  //cout << "UX in outlet domain \n";
  #pragma omp parallel for
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      //cout << k << " " << l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l << " " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l+1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l+1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l-1 << "\n";
      UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+l] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])                                               -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]))*0.25/dx                                              -((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                            + (mu/rho + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l]) + (LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l-1])/16.0)*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+l]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)));
     }
   }
 #pragma omp barrier   
 //coupling to main domain
 //UX in outlet
 //cout << " UX in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo;k++)//x at l=Nfyo-1
  {
   //cout << k << " " << Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << " x " << (Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1 << "\n";
   UXN[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])                                  -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                        -(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                        + (mu/rho + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]) + (LEV[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2]+LEV[(Xo-1-(Nfxo-1)/2+k+1)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-2])/16.0)*((UX[(Nfx+1)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]-2.0*UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
 
  }
 //UX in main domain
 //cout << " UX in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
  {
   //cout << k << " " << 0 << " x " << k*(Nfy+2) << " x " << (k+1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k+1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k-1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k-1)*(Nfy+2) << " x " << k*(Nfy+2)+1 << " x " << k*(Nfy+2) << " y " << (k+1)*(Nfy+1) << " y " << k*(Nfy+1) << " x " << k*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2+1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " x " << (k+1)*(Nfy+2) << " x " << k*(Nfy+2) << " x " << (k-1)*(Nfy+2) << " x " << k*(Nfy+2)+1 << " x " << k*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << "\n";
   UXN[k*(Nfy+2)] = dtf*(-((UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])*(UX[(k+1)*(Nfy+2)]+UX[k*(Nfy+2)])                                                                                                           -(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])*(UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)]))*0.25/dx                                                                                                          -((UX[k*(Nfy+2)+1]+UX[k*(Nfy+2)])*(UY[(k+1)*(Nfy+1)]+UY[k*(Nfy+1)])                                                                                                                    -(UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]                                                             +UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                                                                                                        + (mu/rho + (3.0/8.0)*(LEV[(k+1)*(Nfy+2)]+LEV[k*(Nfy+2)]) + (LEV[k*(Nfy+2)+1]+LEV[(Nfx+2)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]+LEV[(k+1)*(Nfy+2)+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/16.0)*((UX[(k+1)*(Nfy+2)]-2.0*UX[k*(Nfy+2)]+UX[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                              + (UX[k*(Nfy+2)+1]-2.0*UX[k*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
 //UY
 //cout << "UY in outlet domain \n";
 #pragma omp parallel for
  for(int k=1;k<Nfxo+1;k++)//x
  {
   for(int l=1;l<Nfyo-1;l++)//y
    {
     //cout << k << " " << l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1 << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l+1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << "\n";
     UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+l] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l+1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l])                                                 -(UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l+1]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l]))*0.25/dx                                        -((UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l])                                                          -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]))*0.25/dy                                                  + (mu/rho + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l]+LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1]) + (LEV[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l]+LEV[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l+1]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l])/16.0)*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+l]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+l])/(dx*dx)                                                                 + (UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l+1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/(dy*dy)));
    }
  }
 #pragma omp barrier   
 //coupling to main domain
 //UY in outlet
 //cout << " UY in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
  {
   //cout << k << " " << Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " x " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+2) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1 << " x " << (Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2) << " x " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1 << " y " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << " y " << (Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1 << " y " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " y " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << "\n";
   UYN[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] = dtf*(-((UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]+UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1])                                 -(UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1]))*0.25/dx                          -((UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])*(UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1])                                              -(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1]))*0.25/dy                              + (mu/rho + (3.0/8.0)*(LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]+LEV[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]) + (LEV[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]+LEV[(Xo-1-(Nfxo-1)/2+k+1)*(Nfy+2)]+LEV[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1])/16.0)*((UY[(Nfx+2)*(Nfy+1)+(k+1)*Nfyo+Nfyo-1]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                  + (UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]-2.0*UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]+UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])/(dy*dy)));
  }
 //UY in main domain
 //cout << " UY in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
  {
   //cout << k << " " << 0 << " y " << k*(Nfy+1) << " x " << k*(Nfy+2) << " x " << k*(Nfy+2)+1 << " y " << k*(Nfy+1) << " y " << (k+1)*(Nfy+1) << " x " << (k-1)*(Nfy+2)+1 << " x " << (k-1)*(Nfy+2) << " y " << k*(Nfy+1) << " y " << (k-1)*(Nfy+1) << " y " << k*(Nfy+1)+1 << " y " << k*(Nfy+1) << " y " << k*(Nfy+1)+1 << " y " << k*(Nfy+1) << " y " << k*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " y " << k*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " y " << (k+1)*(Nfy+1) << " y " << k*(Nfy+1) << " y " << (k-1)*(Nfy+1) << " y " << k*(Nfy+1)+1 << " y " << k*(Nfy+1) << " y " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << "\n";
   UYN[k*(Nfy+1)] = dtf*(-((UX[k*(Nfy+2)]+UX[k*(Nfy+2)+1])*(UY[k*(Nfy+1)]+UY[(k+1)*(Nfy+1)])                                                                                                                    -(UX[(k-1)*(Nfy+2)+1]+UX[(k-1)*(Nfy+2)])*(UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)]))*0.25/dx                                                                                                    -((UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])*(UY[k*(Nfy+1)+1]+UY[k*(Nfy+1)])                                                                                                                      -(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])*(UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1]))*0.25/dy                                      + (mu/rho + (3.0/8.0)*(LEV[k*(Nfy+2)+1]+LEV[k*(Nfy+2)]) + (LEV[(k-1)*(Nfy+2)+1]+LEV[(k-1)*(Nfy+2)]+LEV[(k+1)*(Nfy+2)+1]+LEV[(k+1)*(Nfy+2)])/16.0)*((UY[(k+1)*(Nfy+1)]-2.0*UY[k*(Nfy+1)]+UY[(k-1)*(Nfy+1)])/(dx*dx)                                                                                                              + (UY[k*(Nfy+1)+1]-2.0*UY[k*(Nfy+1)]+UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)));
  }
#endif
 }

void NS_STEP2(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &Pr, int &Nfx, int &Nfy, DOUB &beta, DOUB &toll, DOUB &dx, DOUB &dy, DOUB &dtf, DOUB &rho, int &Nfxo, int &Nfyo, int &Xo, DOUB &Npr, DOUB &Nsor, vector<DOUB> &B, vector<vector<int>> &Pr_mapper, vector<vector<int>> &Pr_mapper_even, vector<vector<int>> &Pr_mapper_odd, DOUB &Pr_out)//solve for pressur
 {
  DOUB dPrmax(1.0), Prold(0.0), A(1.0-beta), sornum(0.0); // B(parsFDF.beta/(2.0/(parsFDF.dx*parsFDF.dx)+2.0/(parsFDF.dx*parsFDF.dx))),
  /*vector<DOUB> B((Nfx+2)*(Nfy+2)+(Nfxo+2)*Nfyo);
  
  //fill array
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      B[k*(Nfy+2)+l] = beta/(2.0/(dx*dx)+2.0/(dy*dy));
     }
   }
  //k=1,l=1 lower left corner
  B[(Nfy+2)+1] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //k=1,l=Ny upper left corner
  B[(Nfy+2)+Nfy] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //k=Nx,l=1 lower right corner
  B[Nfx*(Nfy+2)+1] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //k=Nx,l=Ny upper right corner
  B[Nfx*(Nfy+2)+Nfy] = beta/(1.0/(dx*dx)+1.0/(dy*dy));
  //left edge k=1
  for(int l=2; l<Nfy;l++)
   {
    B[(Nfy+2)+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  //right edge k=Nx
  for(int l=2; l<Nfy;l++)
   {
    B[Nfx*(Nfy+2)+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  //lower edge l=1
  for(int k=2; k<Nfx;k++)
   {
#ifdef pressure_outlet
    if(abs(k-Xo)<=(Nfxo-1)/2){B[k*(Nfy+2)+1] = beta/(2.0/(dx*dx)+2.0/(dy*dy));}//takes into account pressur gradient to the outlet
    else{B[k*(Nfy+2)+1] = beta/(2.0/(dx*dx)+1.0/(dy*dy));}//no gradient of pressur
#else
    B[k*(Nfy+2)+1] = beta/(2.0/(dx*dx)+1.0/(dy*dy));
#endif
   }
  //upper edge l=Ny
  for(int k=2; k<Nfx;k++)
   {
    B[k*(Nfy+2)+Nfy] = beta/(2.0/(dx*dx)+1.0/(dy*dy));
   }

#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  //lower edge l=0 (towards outlet in main domain)
  for(int k=1; k<Nfx+1;k++)
   {
    if(abs(k-Xo)<=(Nfxo-1)/2){B[k*(Nfy+2)] = beta/(2.0/(dx*dx)+2.0/(dy*dy));}//takes into account pressur gradient to the outlet
   }
  //left outlet wall at l=0, k= Xo
  B[(Xo-(Nfxo-1)/2)*(Nfy+2)] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
  //right outlet wall
  B[(Xo+(Nfxo-1)/2)*(Nfy+2)] = beta/(1.0/(dx*dx)+2.0/(dy*dy));

  // outlet main domain (including cells touching the main domain)
  for(int k=1;k<Nfxo+1;k++)
   {
    for(int l=1;l<Nfyo;l++)
     {
      B[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = beta/(2.0/(dx*dx)+2.0/(dy*dy));
     }
   }
  // left edge k=1
  for(int l=2;l<Nfyo;l++)
   {
    B[(Nfx+2)*(Nfy+2)+Nfyo+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  // right edge k=Nfxo
  for(int l=2;l<Nfyo;l++)
   {
  B[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+l] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
   }
  // bottom edge
  for(int k=2;k<Nfxo;k++)
   {
    B[(Nfx+2)*(Nfy+2)+k*Nfyo+1] = beta/(2.0/(dx*dx)+2.0/(dx*dx));
   }
  // left bottom outlet corner
  B[(Nfx+2)*(Nfy+2)+Nfyo+1] = beta/(1.0/(dx*dx)+2.0/(dy*dy));
  //B[(Nfx+2)*(Nfy+2)+Nfyo+1] = beta/(2.0/(dx*dx)+2.0/(dy*dy));
  // right bottom outlet corner
  B[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+1] = beta/(1.0/(dx*dx)+2.0/(dx*dx));
  //B[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+1] = beta/(2.0/(dx*dx)+2.0/(dx*dx));
#endif //*/

/*  cout << "Pressur: B=  #################################################################### \n";
  for(int l=0; l<Nfy+2; l++)
   {
    for(int k=0; k<Nfx+2; k++)
     {
      cout << l << " " << k << " " << k*(Nfy+2)+l << " " << B[k*(Nfy+2)+l] << "\n";
     }
    cout << "\n";
   }//*/
  
  vector<DOUB> Pr_old(Pr.size());
  //vector<DOUB> Pr_even(Pr.size());
  //vector<DOUB> Pr_odd(Pr.size());
  
  //cout << Pr.size() << " " << Pr_old.size() << " " << Pr_mapper.size() << "\n";
  
  //### Solve for pressure ###//
  while(dPrmax==1.0)
  //while(sornum <= 4000.0)
   {
    //cout << " SOR step= " << sornum << "\n";
    sornum += 1.0;
    dPrmax = 0.0;
    //#pragma omp parallel for private(Prold)
    //### main domain ###//
    //cout << "Pr in main domain \n";
    
    //Pr:   at           left           right            below        above   UX:    left          right  UY: below          above
    
    //update pressure field
    Pr_old=Pr;
    //Pr_even=Pr;
    //Pr_odd=Pr;
    
   #pragma omp parallel for // parallelize all even pressure grid cells
    for(int k=0;k<Pr_mapper_even.size();k++)
     {
      //for(int l=0;l<9;l++){cout << Pr_mapper_even[k][l] << " ";}
      //cout << "\n";
      //Pr[Pr_mapper_even[k][0]] = A*Pr_even[Pr_mapper_even[k][0]] + B[Pr_mapper_even[k][0]]*((Pr_even[Pr_mapper_even[k][2]] + Pr_even[Pr_mapper_even[k][1]])/(dx*dx)                                                                                               + (Pr_even[Pr_mapper_even[k][4]] + Pr_even[Pr_mapper_even[k][3]])/(dy*dy)                                                                                                                                      - rho/dtf*(UX[Pr_mapper_even[k][6]] - UX[Pr_mapper_even[k][5]])/dx                                                                                                                                   - rho/dtf*(UY[Pr_mapper_even[k][8]] - UY[Pr_mapper_even[k][7]])/dy);
      Pr[Pr_mapper_even[k][0]] = A*Pr_old[Pr_mapper_even[k][0]] + B[Pr_mapper_even[k][0]]*((Pr_old[Pr_mapper_even[k][2]] + Pr_old[Pr_mapper_even[k][1]])/(dx*dx)                                                                                               + (Pr_old[Pr_mapper_even[k][4]] + Pr_old[Pr_mapper_even[k][3]])/(dy*dy)                                                                                                                                      - rho/dtf*(UX[Pr_mapper_even[k][6]] - UX[Pr_mapper_even[k][5]])/dx                                                                                                                                   - rho/dtf*(UY[Pr_mapper_even[k][8]] - UY[Pr_mapper_even[k][7]])/dy);    
     }
    #pragma omp barrier
 //   Pr_odd=Pr;
       
    #pragma omp parallel for // parallelize all odd pressure grid cells
    for(int k=0;k<Pr_mapper_odd.size();k++)
     {
     // for(int l=0;l<9;l++){cout << Pr_mapper[k][l] << " ";}
     // cout << "\n";
//      Pr[Pr_mapper_odd[k][0]] = A*Pr_odd[Pr_mapper_odd[k][0]] + B[Pr_mapper_odd[k][0]]*((Pr_odd[Pr_mapper_odd[k][2]] + Pr_odd[Pr_mapper_odd[k][1]])/(dx*dx)                                                                                               + (Pr_odd[Pr_mapper_odd[k][4]] + Pr_odd[Pr_mapper_odd[k][3]])/(dy*dy)                                                                                                                                      - rho/dtf*(UX[Pr_mapper_odd[k][6]] - UX[Pr_mapper_odd[k][5]])/dx                                                                                                                                   - rho/dtf*(UY[Pr_mapper_odd[k][8]] - UY[Pr_mapper_odd[k][7]])/dy);
      Pr[Pr_mapper_odd[k][0]] = A*Pr_old[Pr_mapper_odd[k][0]] + B[Pr_mapper_odd[k][0]]*((Pr_old[Pr_mapper_odd[k][2]] + Pr_old[Pr_mapper_odd[k][1]])/(dx*dx)                                                                                               + (Pr_old[Pr_mapper_odd[k][4]] + Pr_old[Pr_mapper_odd[k][3]])/(dy*dy)                                                                                                                                      - rho/dtf*(UX[Pr_mapper_odd[k][6]] - UX[Pr_mapper_odd[k][5]])/dx                                                                                                                                   - rho/dtf*(UY[Pr_mapper_odd[k][8]] - UY[Pr_mapper_odd[k][7]])/dy);
     }
    #pragma omp barrier  
    
/*    if(int(sornum)%10==0)
     {
      for(int k=0;k<Pr_mapper.size();k++)
       {
        if(abs(Pr_old[Pr_mapper[k][0]]-Pr[Pr_mapper[k][0]])>toll)//stopping criterion
         {
          dPrmax=1.0;
        //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
         }
       }
     }
    else{dPrmax=1.0;} //*/
    
    //Working old code
 /*   for(int k=1;k<Nfx+1;k++)
     {
      for(int l=1;l<Nfy+1;l++)
       {
	//cout << k << " " << l << " " << k*(Nfy+2)+l << " " << k*(Nfy+2)+l << " " << (k+1)*(Nfy+2)+l << " " << (k-1)*(Nfy+2)+l << " " << k*(Nfy+2)+l+1 << " " << k*(Nfy+2)+l-1 << " " << k*(Nfy+2)+l << " " << (k-1)*(Nfy+2)+l << " " << k*(Nfy+1)+l << " " << k*(Nfy+1)+l-1 << "\n";
	Prold=Pr[k*(Nfy+2)+l];
	Pr[k*(Nfy+2)+l] = A*Pr[k*(Nfy+2)+l] + B[k*(Nfy+2)+l]*((Pr[(k+1)*(Nfy+2)+l] + Pr[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                               + (Pr[k*(Nfy+2)+l+1] + Pr[k*(Nfy+2)+l-1])/(dy*dy)                                                                                                                                      - rho/dtf*(UX[k*(Nfy+2)+l] - UX[(k-1)*(Nfy+2)+l])/dx                                                                                                                                   - rho/dtf*(UY[k*(Nfy+1)+l] - UY[k*(Nfy+1)+l-1])/dy);
	if(abs(Prold-Pr[k*(Nfy+2)+l])>toll)//stopping criterion
	 {
          dPrmax=1.0;
	  //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
	 }
       }
     }//*/

#ifdef pressure_outlet 
    //TODO: gravity pressure at outlet
    //### Outlet boundary cells ###//
    //cout << "Pr in outlet main domain \n";    
    #pragma omp parallel for
    for(int k=1;k<Nfxo+1;k++)
     {
      for(int l=1;l<Nfyo-1;l++)
       {
        //cout << k << " " << l << " " << (Nfx+2)*(Nfy+2)+k*Nfyo+l << " " << (Nfx+2)*(Nfy+2)+k*Nfyo+l << " " << (Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l << " " << (Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l << " " << (Nfx+2)*(Nfy+2)+k*Nfyo+l+1 << " " << (Nfx+2)*(Nfy+2)+k*Nfyo+l-1 << " " << (Nfx+1)*(Nfy+2)+k*Nfyo+l << " " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l << " " << (Nfx+2)*(Nfy+1)+k*Nfyo+l << " " << (Nfx+2)*(Nfy+1)+k*Nfyo+l-1 << "\n";
        /*cout << "####### all quantities for main domain before update ###### \n";
        cout << A << "\n";
        cout << Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] << "\n"; 
        cout << B[(Nfx+2)*(Nfy+2)+k*Nfyo+l] << "\n";
        cout << Pr[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] << "\n";
        cout << Pr[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l] << "\n";
        cout << Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] << "\n";
        cout << Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1] << "\n";
        cout << UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] << "\n";
        cout << UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l] << "\n";
        cout << UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] << "\n";
        cout << UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1] << "\n";
        cout << "outlet: Pr["<< (Nfx+2)*(Nfy+2)+k*Nfyo+l << "]=" << Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] << "\n";//*/
        //Prold=Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l];
 //       Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = A*Pr_even[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + B[(Nfx+2)*(Nfy+2)+k*Nfyo+l]*((Pr_even[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] + Pr_even[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                           + (Pr_even[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] + Pr_even[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)                                                                                                            - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/dx                                                                                                         - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/dy);
        Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = A*Pr_old[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + B[(Nfx+2)*(Nfy+2)+k*Nfyo+l]*((Pr_old[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] + Pr_old[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                           + (Pr_old[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] + Pr_old[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)                                                                                                            - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l])/dx                                                                                                         - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1])/dy);
     //    if(abs(Prold-Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l])>toll)//stopping criterion
    //     {
    //      dPrmax=1.0;
    //      //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
    //     }
 
       }
     }//*/
    //enforce hydrostatic pressure at lower end of pressure outlet
    // at l=1
    //k=1
/*    Pr[(Nfx+2)*(Nfy+2)+1*Nfyo+1] = A*Pr_old[(Nfx+2)*(Nfy+2)+1*Nfyo+1] + (beta/(1.0/(dx*dx)+2.0/(dy*dy)))*((Pr_old[(Nfx+2)*(Nfy+2)+(1+1)*Nfyo+1] + Pr_old[(Nfx+2)*(Nfy+2)+(1-1)*Nfyo+1])/(dx*dx)                                           + (Pr_old[(Nfx+2)*(Nfy+2)+1*Nfyo+1+1] + Pr_out)/(dy*dy)                                                                                                            - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+1*Nfyo+1] - UX[(Nfx+1)*(Nfy+2)+(1-1)*Nfyo+1])/dx                                                                                                         - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+1*Nfyo+1] - UY[(Nfx+2)*(Nfy+1)+1*Nfyo+1-1])/dy);
    for(int k=2;k<Nfxo;k++)
     {
      Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+1] = A*Pr_old[(Nfx+2)*(Nfy+2)+k*Nfyo+1] + (beta/(2.0/(dx*dx)+2.0/(dy*dy)))*((Pr_old[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+1] + Pr_old[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+1])/(dx*dx)                                           + (Pr_old[(Nfx+2)*(Nfy+2)+k*Nfyo+1+1] + Pr_out)/(dy*dy)                                                                                                            - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+1] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+1])/dx                                                                                                         - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+1] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+1-1])/dy);
     }
    //k=Nfxo
    Pr[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+1] = A*Pr_old[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+1] + (beta/(1.0/(dx*dx)+2.0/(dy*dy)))*((Pr_old[(Nfx+2)*(Nfy+2)+(Nfxo+1)*Nfyo+1] + Pr_old[(Nfx+2)*(Nfy+2)+(Nfxo-1)*Nfyo+1])/(dx*dx)                                           + (Pr_old[(Nfx+2)*(Nfy+2)+Nfxo*Nfyo+1+1] + Pr_out)/(dy*dy)                                                                                                            - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+Nfxo*Nfyo+1] - UX[(Nfx+1)*(Nfy+2)+(Nfxo-1)*Nfyo+1])/dx                                                                                                         - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+Nfxo*Nfyo+1] - UY[(Nfx+2)*(Nfy+1)+Nfxo*Nfyo+1-1])/dy);//*/
         
    #pragma omp barrier  
  
    //coupling to main domain
    //Pr in outlet at l=Nfyo-1
    //cout << "Pr in outlet at l=Nfyo-1 \n";
    for(int k=1;k<Nfxo+1;k++)
     {
      //cout << k << " " << Nfyo-1 << " " << (Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1 << " " << (Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1 << " " << (Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1 << " " << (Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " " << (Xo-1-(Nfxo-1)/2+k)*(Nfy+2) << " " << (Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1-1 << " " << (Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1 << " " << (Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1 << " " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1 << " " << (Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1 << "\n";
      /*cout << "####### all quantities for outlet in touching layer before update ###### \n";
      cout << A << "\n";
      cout << Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] << "\n";
      cout << B[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] << "\n";
      cout << Pr[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] << "\n";
      cout << Pr[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1] << "\n";
      cout << Pr[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << "\n";
      cout << Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1-1] << "\n";
      cout << UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] << "\n";
      cout << UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1] << "\n";
      cout << UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] << "\n";
      cout << UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1] << "\n";
      cout << "outlet: Pr["<< (Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1 << "]=" << Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] << "\n";//*/
      //Prold=Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1];
      //Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = A*Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                                                  + B[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]*((Pr[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] + Pr[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                            + (Pr[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] + Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1-1])/(dy*dy)                                                                                                      - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/dx                                                                                               - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])/dy);
      Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = A*Pr_old[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                                                + B[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]*((Pr_old[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] + Pr_old[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                            + (Pr_old[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] + Pr_old[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1-1])/(dy*dy)                                                                                                      - rho/dtf*(UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] - UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/dx                                                                                               - rho/dtf*(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1-1])/dy);
   //   if(abs(Prold-Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1])>toll)//stopping criterion
   //    {
   //     dPrmax=1.0;
   //     //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
   //    }
     }
    // Pr in main domain at l=0
    //cout << "Pr in main domain at l=0 \n";
    for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)
     {
      //cout << k << " " << 0 << " " << k*(Nfy+2) << " " << k*(Nfy+2) << " " << (k+1)*(Nfy+2) << " " << (k-1)*(Nfy+2) << " " << k*(Nfy+2)+1 << " " << (Nfx+2)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << " " << k*(Nfy+2) << " " << (k-1)*(Nfy+2) << " " << k*(Nfy+1) << " " << (Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1 << "\n";
      //Prold=Pr[k*(Nfy+2)];
      //Pr[k*(Nfy+2)] = A*Pr_old[k*(Nfy+2)] + B[k*(Nfy+2)]*((Pr[(k+1)*(Nfy+2)] + Pr[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                      + (Pr[k*(Nfy+2)+1] + Pr[(Nfx+2)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)                                                                                                      - rho/dtf*(UX[k*(Nfy+2)] - UX[(k-1)*(Nfy+2)])/dx                                                                                                                                       - rho/dtf*(UY[k*(Nfy+1)] - UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/dy);//NOTE: in last term before second -1 was missing -> switch to lowest layer of outlet instead of higest layer of outlet. Note: other error nfx+1*nfy+2 but must be the other way round
      Pr[k*(Nfy+2)] = A*Pr_old[k*(Nfy+2)] + B[k*(Nfy+2)]*((Pr_old[(k+1)*(Nfy+2)] + Pr_old[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                      + (Pr_old[k*(Nfy+2)+1] + Pr_old[(Nfx+2)*(Nfy+2)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/(dy*dy)                                                                                                      - rho/dtf*(UX[k*(Nfy+2)] - UX[(k-1)*(Nfy+2)])/dx                                                                                                                                       - rho/dtf*(UY[k*(Nfy+1)] - UY[(Nfx+2)*(Nfy+1)+(k-Xo+1+(Nfxo-1)/2)*Nfyo+Nfyo-1])/dy);
    //  if(abs(Prold-Pr[k*(Nfy+2)])>toll)//stopping criterion
    //   {
    //    dPrmax=1.0;
    //    //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
    //   }
     }//*/
#endif
    //#pragma omp barrier
    if(int(sornum)%10==0)
     {
      for(int k=0;k<Pr.size();k++)
       {
        if(abs(Pr_old[k]-Pr[k])>toll)//stopping criterion
         {
          dPrmax=1.0;
          //cout << " main loop: k,l= " << k << ", " << l << "Pold= " << Prold << "Prnew= " << Pr[k*(parsFDF.Nfy+2)+l] << "\n";
         }
       }
     }
    else{dPrmax=1.0;}
    if(sornum > Npr){dPrmax=0.0;} //blocks to high iterations 
    
   }//off while
  Nsor = sornum; //keeps track of how expensive sor is for adjustment
  cout << "steps needed= " << sornum << " max steps= " << Npr << "\n";
 }

void NS_STEP3(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &Pr, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB &dtf, DOUB &rho, int &Nfxo, int &Nfyo, int &Xo, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, DOUB & GravX, DOUB & GravY, DOUB &tf, DOUB &T)//pressur correction
 {
  //#pragma omp parallel for shared(UX,UY)
  #pragma omp parallel for
  for(int k=1;k<Nfx;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      UX[k*(Nfy+2)+l] += Calc_UX[k*(Nfy+2)+l]*(-dtf/rho*(Pr[(k+1)*(Nfy+2)+l] - Pr[k*(Nfy+2)+l])/dx);// + dtf*GravX*(tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5); // x velocity
      //UX[k*(Nfy+2)+l] += (-dtf/rho*(Pr[(k+1)*(Nfy+2)+l] - Pr[k*(Nfy+2)+l])/dx); // x velocity
     }
   }
  #pragma omp barrier  
    
  #pragma omp parallel for
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy;l++)//y
     {
      UY[k*(Nfy+1)+l] += Calc_UY[k*(Nfy+1)+l]*(-dtf/rho*(Pr[k*(Nfy+2)+l+1] - Pr[k*(Nfy+2)+l])/dy);// + dtf*GravY*(tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5);   // y velocity
      //UY[k*(Nfy+1)+l] += (-dtf/rho*(Pr[k*(Nfy+2)+l+1] - Pr[k*(Nfy+2)+l])/dy);   // y velocity
     }
   }
  #pragma omp barrier  
  //cout << "main domain finished \n";
#ifdef pressure_outlet
  //### Outlet boundary cells ###//
  #pragma omp parallel for
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo;l++)//y (includes touching layer at l=Nfyo-1) but only for UX!
     {
      UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] += -dtf/rho*(Pr[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] - Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l])/dx;// + dtf*GravX*(tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5; // x velocity
     }
   }
  #pragma omp barrier  
  #pragma omp parallel for
  for(int k=1;k<Nfxo+1;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] += -dtf/rho*(Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] - Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l])/dy;// + dtf*GravY*(tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5;   // y velocity
     }
   }   
  #pragma omp barrier  
  //cout << "outlet main domain finished \n";
  //coupling to main domain
  //UX in main domain
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
   {
    UX[k*(Nfy+2)] += -dtf/rho*(Pr[(k+1)*(Nfy+2)] - Pr[k*(Nfy+2)])/dx + dtf*GravX*(tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5;// + GravX; // x velocity
   }
  //cout << "UX in main domain finished \n";
   
  //UY in outlet
  for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
   {
    UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] += -dtf/rho*(Pr[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] - Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1])/dy;// + dtf*GravY*(tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5;   // y velocity
   }
  //cout << "UY in outlet finished \n";

  //UY in main domain
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
   {
    UY[k*(Nfy+1)] += -dtf/rho*(Pr[k*(Nfy+2)+1] - Pr[k*(Nfy+2)])/dy;// + dtf*GravY*(tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5;   // y velocity
   }
  //cout << "UY in main domain finished \n";

#endif
  //#pragma omp barrier
 }

void NS_STEP1_UPDATE(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &UX_old, vector<DOUB> &UY_old, vector<DOUB> &FX, vector<DOUB> &FY, int &Nfx, int &Nfy, DOUB &dx, DOUB &dy, DOUB &dtf, DOUB &rho, int &Nfxo, int &Nfyo, int &Xo, vector<DOUB> &Calc_UX, vector<DOUB> &Calc_UY, vector<DOUB> &Calc_porousX, vector<DOUB> &Calc_porousY)//pressur correction
 {
  #pragma omp parallel for //shared(UX,UY,FX,FY,UX_old,UY_old)
  for(int k=1;k<Nfx;k++)//x
   {
    for(int l=1;l<Nfy+1;l++)//y
     {
      UX[k*(Nfy+2)+l] = (UX_old[k*(Nfy+2)+l] + FX[k*(Nfy+2)+l])*Calc_UX[k*(Nfy+2)+l]; // x velocity
     }
   }
  #pragma omp barrier   
  #pragma omp parallel for 
  for(int k=1;k<Nfx+1;k++)//x
   {
    for(int l=1;l<Nfy;l++)//y
     {
      UY[k*(Nfy+1)+l] = (UY_old[k*(Nfy+1)+l] + FY[k*(Nfy+1)+l])*Calc_UY[k*(Nfy+1)+l]; // y velocity
     }
   }
  #pragma omp barrier  
#ifdef pressure_outlet
 //### Update in outlet domain ###//
 //UX
  #pragma omp parallel for
  for(int k=1;k<Nfxo;k++)//x
   {
    for(int l=1;l<Nfyo-1;l++)//y
     {
      UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l] = UX_old[(Nfx+1)*(Nfy+2)+k*Nfyo+l] + FX[(Nfx+1)*(Nfy+2)+k*Nfyo+l];//x component
     }
   }
  #pragma omp barrier  
 //coupling to main domain
 //UX in outlet
 //cout << " UX in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo;k++)//x at l=Nfyo-1
  {
   UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] = UX_old[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1] + FX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1];//x component
 
  }
 //UX in main domain
 //cout << " UX in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2;k++)//x at l=0
  {
   UX[k*(Nfy+2)] = UX_old[k*(Nfy+2)] + FX[k*(Nfy+2)];//x component
  }
 //UY
 //cout << "UY in outlet domain \n";
 #pragma omp parallel for
 for(int k=1;k<Nfxo+1;k++)//x
  {
   for(int l=1;l<Nfyo-1;l++)//y
    {
     UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l] = UY_old[(Nfx+2)*(Nfy+1)+k*Nfyo+l] + FY[(Nfx+2)*(Nfy+1)+k*Nfyo+l];//x component
    }
  }
 #pragma omp barrier  
 //coupling to main domain
 //UY in outlet
 //cout << " UY in outlet l=Nfyo-1 \n";
 for(int k=1;k<Nfxo+1;k++)//x at l=Nfyo-1
  {
   UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] = UY_old[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1] + FY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1];//x component
  }
 //UY in main domain
 //cout << " UY in main domain l=0 \n";
 for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)//x at l=0
  {
   UY[k*(Nfy+1)] = UY_old[k*(Nfy+1)] + FY[k*(Nfy+1)];//x component
  }
#endif
  //#pragma omp barrier
 }

void NS_STEP4_TRACER1_BOUNDARY_SET(vector<DOUB> &F, int &Nfx, int &Nfy, int &Nfxo, int &Nfyo, int &Xo, int &Xi, int &wi, DOUB &tf, DOUB &T2, parsf &parsFDF) 
 {
  //NOTE: TRACER1 IS THE FOOD TRACER
  //### Boundary ###//
 // cout << "STEP4 boundary 1 \n";
#ifndef complex_boundary
  //k=0
  for(int l=1;l<Nfy+1;l++)
   {
    F[l] = F[Nfy+l];
   }
  //k=parsFDF.Nfx+1
  for(int l=1;l<Nfy+1;l++)
   {
    F[(Nfx+1)*(Nfy+2)+l] = F[Nfx*(Nfy+2)+l];
   }
  //l=0
  for(int k=1;k<Nfx+1;k++)
   {
    if(abs(Xo-k)>(Nfxo-1)/2)//exclude outlet cells from beeing updated
     {
      F[k*(Nfy+2)] = F[k*(Nfy+2)+1];
     }
   }
  //l=parsFDF.Nfy+1
  for(int k=1;k<Nfx+1;k++)
   {
    F[k*(Nfy+2)+Nfy+1] = F[k*(Nfy+2)+Nfy];
   }
#else //now complex boundary
  // cout << "left bounds \n";
   for(int k=0;k<parsFDF.boundary_mapper_TrLE.size();k++)//left boundaries
    {
     //Receiver receives from donator 
     F[parsFDF.boundary_mapper_TrLE[k][0]] = F[parsFDF.boundary_mapper_TrLE[k][1]];
    }
  // cout << "right bounds \n";
   for(int k=0;k<parsFDF.boundary_mapper_TrR.size();k++)//right boundaries
    {
     F[parsFDF.boundary_mapper_TrR[k][0]] = F[parsFDF.boundary_mapper_TrR[k][1]];
    }
  // cout << "lower bounds \n";
   for(int k=0;k<parsFDF.boundary_mapper_TrL.size();k++)//lower boundaries
    {
     if(parsFDF.boundary_outlet[parsFDF.boundary_mapper_TrL[k][0]]==0){F[parsFDF.boundary_mapper_TrL[k][0]] = F[parsFDF.boundary_mapper_TrL[k][1]];}
    }
  // cout << "upper bounds \n";
   for(int k=0;k<parsFDF.boundary_mapper_TrU.size();k++)//upper boundaries
    {
//     cout << "receiver index= " << parsFDF.boundary_mapper_TrU[k][0] << " donator index= " << parsFDF.boundary_mapper_TrU[k][1] << "\n";
     F[parsFDF.boundary_mapper_TrU[k][0]] = F[parsFDF.boundary_mapper_TrU[k][1]];
    }
  // cout << "upper bounds done \n";    
#endif   
#ifdef pressure_outlet
  //cout << "inlet tracer \n";
#ifndef tracer_distribution
  //### tracer inflow ###//
  for(int k=Xi-wi;k<Xi+wi;k++)
   {
//    F[k*(Nfy+2)+Nfy+1] = (-(DOUB(k-Xi)*(k-Xi))/(DOUB(wi*wi))+1.0)*(tanh(0.25*(tf-0.3*T2))+1.0)*0.5*(tanh(-0.5*(tf-0.6*T2))+1.0)*0.25;//highest concentration possible flows into the domain
    F[k*(Nfy+2)+Nfy+1] = (tanh(0.25*(tf-0.3*T2))+1.0)*0.5*(tanh(-0.5*(tf-0.6*T2))+1.0)*0.25;//highest concentration possible flows into the domain
   }
#else
  for(int k=Xi-wi;k<Xi+wi;k++)
   {
    F[k*(Nfy+2)+Nfy+1] = 0.0;//no tracer flows in 
   }
#endif
  //### outlet boundary cells ###//
  //cout << "outlet tracer \n";
  for(int l=0;l<Nfyo;l++)
   {
    //k=0
    F[(Nfx+2)*(Nfy+2) + l] = F[(Nfx+2)*(Nfy+2) + Nfyo+l];
    //k=Nfxo
    F[(Nfx+2)*(Nfy+2) + (Nfxo+1)*Nfyo+l] = F[(Nfx+2)*(Nfy+2) + Nfxo*Nfyo+l];
   }
  //NOTE: l=0 (lower boundary of outlet) is supposed to have zero concentration, no action needed
#endif
 }

void NS_STEP4_TRACER2_BOUNDARY_SET(vector<DOUB> &F, int &Nfx, int &Nfy, int &Nfxo, int &Nfyo, int &Xo, int &Xi, int &wi, DOUB &tf, DOUB &T2, parsf &parsFDF) 
 {
  //NOTE: TRACER2 IS THE SIGNALING TRACER
  //### Boundary ###//
  //cout << "STEP4 boundary 2 \n";
#ifndef complex_boundary
  //k=0
  for(int l=1;l<Nfy+1;l++)
   {
    F[l] = F[Nfy+l];
   }
  //k=parsFDF.Nfx+1
  for(int l=1;l<Nfy+1;l++)
   {
    F[(Nfx+1)*(Nfy+2)+l] = F[Nfx*(Nfy+2)+l];
   }
  //l=0
  for(int k=1;k<Nfx+1;k++)
   {
    if(abs(Xo-k)>(Nfxo-1)/2)//exclude outlet cells from beeing updated
     {
      F[k*(Nfy+2)] = F[k*(Nfy+2)+1];
     }
   }
  //l=parsFDF.Nfy+1
  for(int k=1;k<Nfx+1;k++)
   {
    F[k*(Nfy+2)+Nfy+1] = F[k*(Nfy+2)+Nfy];
   }
#else //now complex boundary
   for(int k=0;k<parsFDF.boundary_mapper_TrLE.size();k++)//left boundaries
    {
     //Receiver receives from donator 
     F[parsFDF.boundary_mapper_TrLE[k][0]] = F[parsFDF.boundary_mapper_TrLE[k][1]];
    }
   for(int k=0;k<parsFDF.boundary_mapper_TrR.size();k++)//right boundaries
    {
     F[parsFDF.boundary_mapper_TrR[k][0]] = F[parsFDF.boundary_mapper_TrR[k][1]];
    }
   for(int k=0;k<parsFDF.boundary_mapper_TrL.size();k++)//lower boundaries
    {
     if(parsFDF.boundary_outlet[parsFDF.boundary_mapper_TrL[k][0]]==0){F[parsFDF.boundary_mapper_TrL[k][0]] = F[parsFDF.boundary_mapper_TrL[k][1]];}
    }
   for(int k=0;k<parsFDF.boundary_mapper_TrU.size();k++)//upper boundaries
    {
     F[parsFDF.boundary_mapper_TrU[k][0]] = F[parsFDF.boundary_mapper_TrU[k][1]];
    }
#endif   
#ifdef pressure_outlet
  for(int k=Xi-wi;k<Xi+wi;k++)
   {
    F[k*(Nfy+2)+Nfy+1] = 0.0;//no tracer flows in 
   }
  //### outlet boundary cells ###//
  for(int l=0;l<Nfyo;l++)
   {
    //k=0
    F[(Nfx+2)*(Nfy+2) + l] = F[(Nfx+2)*(Nfy+2) + Nfyo+l];
    //k=Nfxo
    F[(Nfx+2)*(Nfy+2) + (Nfxo+1)*Nfyo+l] = F[(Nfx+2)*(Nfy+2) + Nfxo*Nfyo+l];
   }
  //NOTE: l=0 (lower boundary of outlet) is supposed to have zero concentration, no action needed
#endif
 }

void NS_STEP4_TRACER(vector<DOUB> &UX, vector<DOUB> &UY, vector<DOUB> &F, vector<DOUB> &Fn, int &Nfx, int &Nfy, int &Nfxo, int &Nfyo, int &Xo, DOUB &dx, DOUB &dy, int &Xi, int &wi, DOUB &dtf, DOUB &DTr1, DOUB &tf, DOUB &T2, vector<DOUB> &Calc_Tr, DOUB &reaction) //parsf &parsFDF)
 {
  //NOTE: This function contains the advection-diffusion scheme for scalar tracers

 // cout << "STEP4 advection  \n";
  //### main latice ###//
  #pragma omp parallel for 
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      //Fn[k*(Nfy+2)+l] = F[k*(Nfy+2)+l] + dtf*((UX[(k-1)*(Nfy+2)+l]*(F[(k-1)*(Nfy+2)+l]+F[k*(Nfy+2)+l]) - UX[k*(Nfy+2)+l]*(F[(k+1)*(Nfy+2)+l]+F[k*(Nfy+2)+l]))/(2.0*dx)                                                              +(UY[k*(Nfy+1)+l-1]*(F[k*(Nfy+2)+l-1]+F[k*(Nfy+2)+l]) - UY[k*(Nfy+1)+l]*(F[k*(Nfy+2)+l+1]+F[k*(Nfy+2)+l]))/(2.0*dy)                                                              +DTr1*((F[(k+1)*(Nfy+2)+l] - 2*F[k*(Nfy+2)+l] + F[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                                  +(F[k*(Nfy+2)+l+1] - 2*F[k*(Nfy+2)+l] + F[k*(Nfy+2)+l-1])/(dy*dy)));
      Fn[k*(Nfy+2)+l] = F[k*(Nfy+2)+l] + dtf*Calc_Tr[k*(Nfy+2)+l]*((UX[(k-1)*(Nfy+2)+l]*F[(k-1)*(Nfy+2)+l] - UX[k*(Nfy+2)+l]*F[(k+1)*(Nfy+2)+l])/(2.0*dx)                                                                                                +(UY[k*(Nfy+1)+l-1]*F[k*(Nfy+2)+l-1] - UY[k*(Nfy+1)+l]*F[k*(Nfy+2)+l+1])/(2.0*dy)                                                                                                      +DTr1*((F[(k+1)*(Nfy+2)+l] - 2*F[k*(Nfy+2)+l] + F[(k-1)*(Nfy+2)+l])/(dx*dx)                                                                                                                  +(F[k*(Nfy+2)+l+1] - 2*F[k*(Nfy+2)+l] + F[k*(Nfy+2)+l-1])/(dy*dy)) + reaction*F[k*(Nfy+2)+l]);
      //cout << "F[" << k*(Nfy+2)+l << "] = " << F[k*(Nfy+2)+l] << " Fn = " << Fn[k*(Nfy+2)+l] << "\n";
     }
   }
  #pragma omp barrier  
#ifdef pressure_outlet
  //### outlet main domain ###//
  #pragma omp parallel for 
  for(int k=1;k<Nfxo+1;k++)
   {
    for(int l=1;l<Nfyo-1;l++)
     {
      //Fn[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = F[(Nfx+2)*(Nfy+2)+k*Nfyo+l]                                                                                                                                                          + dtf*((UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]*(F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+l])                                                                                       - UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]*(F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+l]))/(2.0*dx)                                                                                 +(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]*(F[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+l])                                                                                           - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]*(F[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+l]))/(2.0*dy)                                                                             +DTr1*((F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                                                           +(F[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + F[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)));
      Fn[(Nfx+2)*(Nfy+2)+k*Nfyo+l] = F[(Nfx+2)*(Nfy+2)+k*Nfyo+l]                                                                                                                                                          + dtf*((UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]*F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l]                                                                                                                     - UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]*F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l])/(2.0*dx)                                                                                                               +(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1]                                                                                                                         - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1])/(2.0*dy)                                                                                                           +DTr1*((F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+l] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+l])/(dx*dx)                                                                           +(F[(Nfx+2)*(Nfy+2)+k*Nfyo+l+1] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l] + F[(Nfx+2)*(Nfy+2)+k*Nfyo+l-1])/(dy*dy)) + reaction*F[(Nfx+2)*(Nfy+2)+k*Nfyo+l]);
     }
   }
  #pragma omp barrier   
  //cells in main domain at l=0
  for(int k=Xo-(Nfxo-1)/2;k<Xo+(Nfxo-1)/2+1;k++)
   {
    Fn[k*(Nfy+2)] = F[k*(Nfy+2)] + dtf*((UX[(k-1)*(Nfy+2)]*(F[(k-1)*(Nfy+2)]+F[k*(Nfy+2)]) - UX[k*(Nfy+2)]*(F[(k+1)*(Nfy+2)]+F[k*(Nfy+2)]))/(2.0*dx)                                                                          +(UY[(Nfx+2)*(Nfy+1)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]+F[k*(Nfy+2)])                                                                - UY[k*(Nfy+1)]*(F[k*(Nfy+2)+1]+F[k*(Nfy+2)]))/(2.0*dy)                                                                                                                          +DTr1*((F[(k+1)*(Nfy+2)] - 2*F[k*(Nfy+2)] + F[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                                  +(F[k*(Nfy+2)+1] - 2*F[k*(Nfy+2)] + F[(Nfx+2)*(Nfy+2)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1])/(dy*dy)) + reaction*F[k*(Nfy+2)]);
    //Fn[k*(Nfy+2)] = F[k*(Nfy+2)] + dtf*((UX[(k-1)*(Nfy+2)]*F[(k-1)*(Nfy+2)] - UX[k*(Nfy+2)]*F[(k+1)*(Nfy+2)])/(2.0*dx)                                                                                                        +(UY[(Nfx+2)*(Nfy+1)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]*F[(Nfx+2)*(Nfy+2)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1]                                                                               - UY[k*(Nfy+1)]*F[k*(Nfy+2)+1])/(2.0*dy)                                                                                                                                         +DTr1*((F[(k+1)*(Nfy+2)] - 2*F[k*(Nfy+2)] + F[(k-1)*(Nfy+2)])/(dx*dx)                                                                                                                  +(F[k*(Nfy+2)+1] - 2*F[k*(Nfy+2)] + F[(Nfx+2)*(Nfy+2)+(k-Xo+(Nfxo-1)/2+1)*Nfyo+Nfyo-1])/(dy*dy)));
   }
  //cells in outlet domain at l=Nfyo-1
  for(int k=1;k<Nfxo+1;k++)
   {
    //Fn[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                                                     + dtf*((UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1])                                                                  - UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]))/(2.0*dx)                                                                  +(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-2]*(F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1])                                                                                - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]*(F[(Xo-(Nfxo-1)/2-1+k)*(Nfy+2)]+F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]))/(2.0*dy)                                                                        +DTr1*((F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] + F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                            +(F[(Xo-(Nfxo-1)/2-1+k)*(Nfy+2)] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] + F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2])/(dy*dy)));
    Fn[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] = F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]                                                                                                                                                     + dtf*((UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])                                                                                                         - UX[(Nfx+1)*(Nfy+2)+k*Nfyo+Nfyo-1]*(F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1]))/(2.0*dx)                                                                                                   +(UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-2]*(F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2])                                                                                                                 - UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]*(F[(Xo-(Nfxo-1)/2-1+k)*(Nfy+2)]))/(2.0*dy)                                                                                                   +DTr1*((F[(Nfx+2)*(Nfy+2)+(k+1)*Nfyo+Nfyo-1] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] + F[(Nfx+2)*(Nfy+2)+(k-1)*Nfyo+Nfyo-1])/(dx*dx)                                                            +(F[(Xo-(Nfxo-1)/2-1+k)*(Nfy+2)] - 2*F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1] + F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-2])/(dy*dy)) + reaction*F[(Nfx+2)*(Nfy+2)+k*Nfyo+Nfyo-1]);
   }
#endif
 }

void CFL(FDFS &FDF, parsf &parsFDF, DOUB &mu, DOUB &rho, DOUB &dtmax, DOUB &dtmin, DOUB &tf, DOUB &T2, DOUB &T, DOUB &Pr_out)
 {
  //NOTE: CFL criteria for time step control
  cout << "\n";
  cout << "### CFL Testing ### \n"; 
  
  int Nfxo(2*parsFDF.width_outlet+1), Nfyo(parsFDF.length_outlet);
  vector<DOUB> UX_test = FDF.UX; 
  vector<DOUB> UY_test = FDF.UY;
  vector<DOUB> Pr_test = FDF.Pr;
  vector<DOUB> LEV_test = FDF.LEV;
  DOUB DD1(0.0), DD2(0.0), ND(0.0);  
  DOUB Vel2(0.0), UXm(0.0), UYm(0.0), dt1(0.0), dt11(0.0), dt12(0.0), dt2(0.0);
  for(int k=0; k<FDF.UX.size();k++)
   {
    if(abs(FDF.UX[k])>=UXm){UXm=FDF.UX[k];}
   }
  for(int k=0; k<FDF.UY.size();k++)
   {
    if(abs(FDF.UY[k])>=UYm){UYm=FDF.UY[k];}
   }
  //Vel2=UXm*UXm/(UXs*UXs) + UYm*UYm/(UYs*UYs);
  Vel2=UXm*UXm + UYm*UYm;
  if(Vel2<1e-3){UXm=1e-3; UYm=1e-3;} 
  //cout << "maximal velocity <=  " << Vel2 << "\n";
  dt11 = 0.8*0.5*parsFDF.dx*parsFDF.dx/parsFDF.DTr1;       //Tracer advection
  dt12 = 0.8*0.5*parsFDF.dx*parsFDF.dx/parsFDF.DTr_sig;    //Tracer advection
  //dt2=0.8*0.5*mu/(rho*Vel2); //velocity 
  //dt2 = 0.2*parsFDF.dx/sqrt(Vel2);
  dt2 = parsFDF.CFL/(UXm/parsFDF.dx+UYm/parsFDF.dy);
  if(dt12>dt11){dt1=dt11;}//select smallest time step of tracers
  else{dt1=dt12;}
  if(dt1>dt2)//select smallest time step of fluid or tracer
   {
    if(dt2> dtmin){parsFDF.dtf=dt2; cout << "CFL: dt2= " << dt2 << "\n";}
    else{parsFDF.dtf= dtmin; cout << "CFL: dt2= " << parsFDF.dtf << "\n";}
   }
  else
   {
    if(dt1> dtmin){parsFDF.dtf=dt1; cout << "CFL: dt1= " << dt1 << "\n";}
    else{parsFDF.dtf= dtmin; cout << "CFL: dt1= " << parsFDF.dtf << "\n";}
   }
  if(parsFDF.dtf> dtmax){parsFDF.dtf = dtmax;}
    
  DD2=0.0;
  ND=0.0;
  for(int k=1; k<parsFDF.Nfx+1; k++)
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      DD2 += (FDF.UX[k*(parsFDF.Nfy+2)+l]-FDF.UX[(k-1)*(parsFDF.Nfy+2)+l])/parsFDF.dx + (FDF.UY[k*(parsFDF.Nfy+1)+l]-FDF.UY[k*(parsFDF.Nfy+1)+l-1])/parsFDF.dy;
      ND += (1.0-parsFDF.boundary[k*(parsFDF.Nfy+2)+l]);
     }
   }
  DD2 /= ND; 
  
  if(abs(DD2)>0.000001){parsFDF.dtf *= 0.5; cout << "step reduction: Div= " << abs(DD2) << " dtf= " << parsFDF.dtf << "\n";} //TODO: Check if this produces to small time steps.
      
  //### check if found time step is sufficiently small ###//   
  DD1=0.1;
  DD2=0.0;
  while(abs(abs(DD1)-abs(DD2))>0.0001)
   {
    DD1=DD2;
    //### first test step
    //cout << "UX " << FDF.UX[10000] << " UX_test "<< UX_test[10000] << "\n";
    NS_STEP1(UX_test,UY_test,UX_test,UY_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, LEV_test, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
    //### Coupling to wind field ###//
    NS_STEP5_WIND_FORCING(UX_test, UY_test, FDF.WindX, FDF.WindY, tf, parsFDF, T2, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Vwind, parsFDF.thwind, parsFDF.Calc_porousX, parsFDF.Calc_porousY);//### Pressure solver ###// 
    //### Enforce stright flow in outlet ###//  
    NS_STEP6(UX_test,UY_test,UX_test,UY_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.damp, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Corriolis, parsFDF.GravX, parsFDF.GravY, parsFDF.vel_inlet);
    //cout << "NS: STEP2 \n";
    //### Pressure solver ###///
    NS_STEP2(UX_test,UY_test,Pr_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr,parsFDF.Nsor, parsFDF.Calc_Pr, parsFDF.Pr_mapper, parsFDF.Pr_mapper_even, parsFDF.Pr_mapper_odd, Pr_out);
    //### Pressure correction ###//
    //cout << "NS: STEP3 \n";
    NS_STEP3(UX_test,UY_test,Pr_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.GravX, parsFDF.GravY, tf, T);

    //### Calculate large eddy viscosity ###//
    NS_STEP7(UX_test, UY_test, parsFDF.Nfx, parsFDF.Nfy, parsFDF.dx, parsFDF.dy, Nfxo, Nfyo, parsFDF.pos_outlet, LEV_test, parsFDF.CSL, parsFDF.boundary);

    //### second test step
  //  NS_STEP1(UX_test,UY_test,UX_test,UY_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY);
    //### Coupling to wind field ###//
  //  NS_STEP5_WIND_FORCING(UX_test, UY_test, FDF.WindX, FDF.WindY, tf, parsFDF, T2, parsFDF.Calc_UX, parsFDF.Calc_UY);//### Pressure solver ###// 
    //### Enforce stright flow in outlet ###//  
  //  NS_STEP6(UX_test,UY_test,UX_test,UY_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY);
    //cout << "NS: STEP2 \n";
    //### Pressure solver ###///
    //NS_STEP2(UX_test,UY_test,Pr_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr,parsFDF.Nsor, parsFDF.Calc_Pr, parsFDF.Pr_mapper, parsFDF.Pr_mapper_even, parsFDF.Pr_mapper_odd);
    //### Pressure correction ###//
    //cout << "NS: STEP3 \n";
//    NS_STEP3(UX_test,UY_test,Pr_test,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY);

    
    //cout << "UX " << FDF.UX[10000] << " UX_test "<< UX_test[10000] << "\n";
   /*for(int k=0; k<FDF.UX.size();k++)
    {
     if(abs(UX_test[k])>=UXm){UXm=UX_test[k];}
    }
   for(int k=0; k<FDF.UY.size();k++)
    {
     if(abs(UY_test[k])>=UYm){UYm=UY_test[k];}
    }
   //Vel2=UXm*UXm/(UXs*UXs) + UYm*UYm/(UYs*UYs);
   Vel2=UXm*UXm + UYm*UYm;
   if(Vel2<1e-3){Vel2=1e-3;} 
   //cout << "maximal velocity <=  " << Vel2 << "\n";
   dt1=0.8*0.5*parsFDF.dx*parsFDF.dx/D;       //Tracer advection
   //dt2=0.8*0.5*mu/(rho*Vel2); //velocity 
   dt2 = 0.2*parsFDF.dx/sqrt(Vel2);
   if(dt1>dt2)
    {
     if(dt2> dtmin){parsFDF.dtf=dt2; cout << "CFL: dt2= " << dt2 << "\n";}
     else{parsFDF.dtf= dtmin; cout << "CFL: dt2= " << parsFDF.dtf << "\n";}
    }
   else
    {
     if(dt1> dtmin){parsFDF.dtf=dt1; cout << "CFL: dt1= " << dt1 << "\n";}
     else{parsFDF.dtf= dtmin; cout << "CFL: dt1= " << parsFDF.dtf << "\n";}
    }
   if(parsFDF.dtf> dtmax){parsFDF.dtf = dtmax;}//*/

    //### calculate divergence ###//
    DD2=0.0;
    ND=0.0;
    for(int k=1; k<parsFDF.Nfx+1; k++)
     {
      for(int l=1; l<parsFDF.Nfy+1; l++)
       {
        DD2 += (UX_test[k*(parsFDF.Nfy+2)+l]-UX_test[(k-1)*(parsFDF.Nfy+2)+l])/parsFDF.dx + (UY_test[k*(parsFDF.Nfy+1)+l]-UY_test[k*(parsFDF.Nfy+1)+l-1])/parsFDF.dy;
        ND += (1.0-parsFDF.boundary[k*(parsFDF.Nfy+2)+l]);
       }
     }
    DD2 /= ND;
    cout << " ### CFL: testing time step: Div_new-Div_old= " << abs(abs(DD2)-abs(DD1)) << " dtf= " << parsFDF.dtf << "\n";//*/   
    if(abs(abs(DD1)-abs(DD2))>0.0001)
     {
      UX_test = FDF.UX; 
      UY_test = FDF.UY;
      Pr_test = FDF.Pr;
      LEV_test = FDF.LEV;
      parsFDF.dtf *=0.5;
//      parsFDF.Npr = 3000.0;
     }
   }
  cout << "### CFL testing finished ### \n"; 
  cout << "\n";
 }

void Integrator_FDFS_EULER(FDFS &FDF, DOUB &tf, parsf &parsFDF, DOUB &T, DOUB &T2)
 {
  //NOTE: second order in space, first order in time method:
  //      Workflow:
  //      1. Update velocities
  //      2. Solve for new pressure field
  //      3. Correct new velocity field by pressure gradient force
  //      4. Advect tracers
  //      5. Update time, set new wall values, update boundary conditions
  //### higher viscosity at the start ###//
  //if (tf<=0.1*T){parsFDF.mu -= 4.0/(0.1*T);}
  
  int Nfxo(2*parsFDF.width_outlet+1), Nfyo(parsFDF.length_outlet);//outlet boundary dimensions

  //integrates fluid fields
  //cout << "NS: STEP1 \n";
  vector<DOUB> UX_old(FDF.UX.size());
  vector<DOUB> UY_old(FDF.UY.size());
  vector<DOUB> UXi(FDF.UX.size());
  vector<DOUB> UYi(FDF.UY.size());
  DOUB hight(parsFDF.dy*DOUB(parsFDF.Nfy+Nfyo));
  DOUB Pr_out(parsFDF.rho*parsFDF.GravY*((tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5)*hight);
  UX_old = FDF.UX;
  UY_old = FDF.UY;
  
  //### Intermediate velocities ###//
  //first step (Euler) with dt
  //cout << "1 \n";
  NS_STEP1(UX_old,UY_old,FDF.UX,FDF.UY,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, FDF.LEV, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
  //### Coupling to wind field ###//
  //cout << "5 \n";
  NS_STEP5_WIND_FORCING(FDF.UX, FDF.UY, FDF.WindX, FDF.WindY, tf, parsFDF, T2, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Vwind, parsFDF.thwind, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
  
  //### Enforce stright flow in outlet ###//  
  //cout << "6 \n";
  NS_STEP6(FDF.UX,FDF.UY,FDF.UX,FDF.UY,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.damp, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Corriolis, parsFDF.GravX, parsFDF.GravY, parsFDF.vel_inlet);
  //### Check velocity for too large values ###//
#ifdef flow_damper
  Damper(FDF.UX, FDF.UY, tf, T, parsFDF);
#endif
  //cout << "NS: STEP2 \n";
  //### Pressure solver ###///
  NS_STEP2(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr,parsFDF.Nsor, parsFDF.Calc_Pr, parsFDF.Pr_mapper, parsFDF.Pr_mapper_even, parsFDF.Pr_mapper_odd, Pr_out);
  //### Pressure correction ###//
  //cout << "NS: STEP3 \n";
  NS_STEP3(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.GravX, parsFDF.GravY,tf,T);
  
  //### Check velocity for too large values ###//
#ifdef flow_damper
  Damper(FDF.UX, FDF.UY, tf, T, parsFDF);
#endif  
  //### Signaling tracer forcing ###//
  //NS_FORCE_TRACER(FDF.intensity, FDF.intensity_sig, FDF.intensity_sig_back_coupler, parsFDF.Nfx, parsFDF.Nfy); 
  //### Tracer advection ###//
  NS_STEP4_TRACER1_BOUNDARY_SET(FDF.intensity, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T2, parsFDF);
  NS_STEP4_TRACER(FDF.UX,FDF.UY,FDF.intensity,FDF.intensity, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr1, tf, T2, parsFDF.Calc_Tr,parsFDF.reaction_tracer);
  //### Signaling tracer advection ###//
  NS_STEP4_TRACER2_BOUNDARY_SET(FDF.intensity_sig, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T2, parsFDF);
  NS_STEP4_TRACER(FDF.UX,FDF.UY,FDF.intensity_sig,FDF.intensity_sig, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr_sig, tf, T2, parsFDF.Calc_Tr,parsFDF.reaction_tracer_sig);
  //### boundary conditions for next time step for the main domain ###//
  //cout << "set new boundary condition tf= " << tf << "\n";
  tf += parsFDF.dtf;
#ifdef CFL_cr  
  //CFL(FDF.UX, FDF.UY, parsFDF.dtf, parsFDF.dx, parsFDF.dy, parsFDF.DTr1, parsFDF.mu, parsFDF.rho, parsFDF.dtmax, parsFDF.dtmin);//NOTE: leads to longer pressure iterations and non-equidistant time stepping.
  CFL(FDF, parsFDF, parsFDF.mu, parsFDF.rho, parsFDF.dtmax, parsFDF.dtmin, tf,T2, T, Pr_out);
#endif  
  NS_boundary_set(FDF.UX, FDF.UY, FDF.Pr, parsFDF, tf, T, parsFDF.rho);
  NS_STEP0(FDF.UX, FDF.UY, parsFDF.UL, parsFDF.UU, parsFDF.ULE, parsFDF.UR, parsFDF.VL, parsFDF.VU, parsFDF.VLE, parsFDF.VR, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF);
  
  //### Calculate large eddy viscosity ###//
   NS_STEP7(FDF.UX, FDF.UY, parsFDF.Nfx, parsFDF.Nfy, parsFDF.dx, parsFDF.dy, Nfxo, Nfyo, parsFDF.pos_outlet, FDF.LEV, parsFDF.CSL, parsFDF.boundary);
  
  //*/
  //cout << "tf = " << tf << "\n";
 }

void Integrator_FDFS_HEUN(FDFS &FDF, DOUB &tf, parsf &parsFDF, DOUB &T, DOUB &T2)
 {
  //NOTE: second order in time and space CFD integrator
  //      Workflow: 
  //      1. Calculate intermediate velocities, store intermediate values
  //      2. Solve for pressure field based on intermediate velocities
  //      3. Correct intermediate velocities by pressure gradient force
  //      4. Advect tracers
  //      5. Update time, set new wall values, update boundary conditions
  //      6. Second time Step of velocity starting from intermediate velocities
  //      7. Advect tracers
  //      8. Update each field value as average of the value at time t and t+2dt to get values at time t+dt
  //         X(t+dt) = 0.5*(X(t)+X(t+2dt))
  //      9. Solve for final pressure field P(x,t+dt)
  //      10. Correct final velocity field
  //      11. Set/update boundary conditions (as the velocity field as changed, the no-slip boundary condition needs to be updated)
  
  int Nfxo(2*parsFDF.width_outlet+1), Nfyo(parsFDF.length_outlet);//outlet boundary dimensions

  //integrates fluid fields
  //cout << "NS: STEP1 \n";
  vector<DOUB> UX_old(FDF.UX.size());
  vector<DOUB> UY_old(FDF.UY.size());
  vector<DOUB> UXi(FDF.UX.size());
  vector<DOUB> UYi(FDF.UY.size());
  vector<DOUB> FX1(FDF.UX.size());
  vector<DOUB> FY1(FDF.UY.size());
  vector<DOUB> FX2(FDF.UX.size());
  vector<DOUB> FY2(FDF.UY.size());
  vector<DOUB> intensity_old(FDF.intensity.size());
  vector<DOUB> intensity_sig_old(FDF.intensity_sig.size());
  vector<DOUB> intensityi(FDF.intensity.size());
  vector<DOUB> intensityi_sig(FDF.intensity_sig.size());
  UX_old = FDF.UX;
  UY_old = FDF.UY;
  intensity_old=FDF.intensity;
  intensity_sig_old=FDF.intensity_sig;
  DOUB hight(parsFDF.dy*DOUB(parsFDF.Nfy+Nfyo));
  DOUB Pr_out(parsFDF.rho*parsFDF.GravY*((tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5)*hight);
#ifdef CFL_cr
//  CFL(FDF.UX, FDF.UY, parsFDF.dtf, parsFDF.dx, parsFDF.dy, parsFDF.DTr1, parsFDF.mu, parsFDF.rho, parsFDF.dtmax, parsFDF.dtmin);
  CFL(FDF, parsFDF, parsFDF.mu, parsFDF.rho, parsFDF.dtmax, parsFDF.dtmin, tf,T2,T, Pr_out);
#endif  
  
  //### FIRST HEUN STEP ###//
  //cout << "fist Heun step \n";
  //### Intermediate velocities ###//
  //first step (Euler) with dt
  NS_STEP1_HEUN(UX_old,UY_old,FX1,FY1,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, FDF.LEV, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
  //### Coupling to wind field ###//
  NS_STEP5_WIND_FORCING_HEUN(UX_old, UY_old, FX1, FY1, FDF.WindX, FDF.WindY, tf, parsFDF, T2, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Vwind, parsFDF.thwind, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
  //### Enforce stright flow in outlet ###//  
  //cout << "6 \n";
  NS_STEP6_HEUN(UX_old, UY_old, FX1, FY1, parsFDF.Nfx, parsFDF.Nfy, parsFDF.dx, parsFDF.dy, parsFDF.dtf, parsFDF.rho, parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.damp, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Corriolis, parsFDF.GravX, parsFDF.GravY, parsFDF.vel_inlet);
   
  NS_STEP1_UPDATE(UXi,UYi,UX_old,UY_old,FX1,FY1,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
  
    
  //### Check velocity for too large values ###//
#ifdef flow_damper  
  Damper(UXi, UYi, tf, T, parsFDF);//### Pressure solver ###//
#endif
  //cout << "NS: STEP2 \n";
  NS_STEP2(UXi,UYi,FDF.Pri,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr,parsFDF.Nsor, parsFDF.Calc_Pr, parsFDF.Pr_mapper, parsFDF.Pr_mapper_even, parsFDF.Pr_mapper_odd, Pr_out);
  //### Pressure correction ###//
  //cout << "NS: STEP3 \n";
  NS_STEP3(UXi,UYi,FDF.Pri,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.GravX, parsFDF.GravY, tf, T);

  //### Check velocity for too large values ###//
#ifdef flow_damper
  Damper(UXi, UYi, tf, T, parsFDF);
#endif
  //### Signaling tracer forcing ###//
  //NS_FORCE_TRACER(intensity_old, intensity_sig_old, FDF.intensity_sig_back_coupler, parsFDF.Nfx, parsFDF.Nfy); //NOTE: For several tracers, add more exchange vectors as arguments
  //### Tracer advection ###//
  NS_STEP4_TRACER1_BOUNDARY_SET(intensity_old, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T2, parsFDF);
  NS_STEP4_TRACER(UX_old,UY_old,intensity_old,intensityi, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr1, tf, T2, parsFDF.Calc_Tr,parsFDF.reaction_tracer);
  //### Signaling tracer advection ###//
  NS_STEP4_TRACER2_BOUNDARY_SET(intensity_sig_old, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T2, parsFDF);
  NS_STEP4_TRACER(UX_old,UY_old,intensity_sig_old,intensityi_sig, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr1, tf, T2, parsFDF.Calc_Tr,parsFDF.reaction_tracer_sig);
  //### boundary conditions for next time step for the main domain ###//
  //cout << "set new boundary condition tf= " << tf << "\n";
  tf += parsFDF.dtf;
  NS_boundary_set(UXi, UYi, FDF.Pr, parsFDF, tf, T, parsFDF.rho);
  NS_STEP0(UXi, UYi, parsFDF.UL, parsFDF.UU, parsFDF.ULE, parsFDF.UR, parsFDF.VL, parsFDF.VU, parsFDF.VLE, parsFDF.VR, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF);
  
  //### Calculate large eddy viscosity ###//
  NS_STEP7(UXi, UYi, parsFDF.Nfx, parsFDF.Nfy, parsFDF.dx, parsFDF.dy, Nfxo, Nfyo, parsFDF.pos_outlet, FDF.LEV, parsFDF.CSL, parsFDF.boundary);
  
  //### SECOND HEUN STEP ###//
  //cout << "second Heun step \n";
  //### Intermediate velocities ###//
  NS_STEP1_HEUN(UXi,UYi,FX2,FY2,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, FDF.LEV, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
  //### Coupling to wind field ###//
  NS_STEP5_WIND_FORCING_HEUN(UXi, UYi, FX2, FY2, FDF.WindX, FDF.WindY, tf, parsFDF, T2, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Vwind, parsFDF.thwind, parsFDF.Calc_porousX, parsFDF.Calc_porousY);
  //### Enforce stright flow in outlet ###//  
  //cout << "6 \n";
  NS_STEP6_HEUN(UXi,UYi,FX2,FY2,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho,parsFDF.mu, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.damp, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.Corriolis, parsFDF.GravX, parsFDF.GravY, parsFDF.vel_inlet);

  //### Check velocity for too large values ###//
#ifdef flow_damper
  Damper(UXi, UYi, tf, T, parsFDF);//### Pressure solver ###//
#endif

  //### Pressure solver ###//
  //cout << "NS: STEP2 \n";
  //NS_STEP2(UXi2,UYi2,FDF.Pri2,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### Pressure correction ###//
  //cout << "NS: STEP3 \n";
  //NS_STEP3(UXi2,UYi2,FDF.Pri2,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet);
  //### Tracer advection ###//
  NS_STEP4_TRACER1_BOUNDARY_SET(intensityi, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T2, parsFDF);  
  NS_STEP4_TRACER(UXi,UYi,intensityi,FDF.intensity, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr1, tf, T2, parsFDF.Calc_Tr,parsFDF.reaction_tracer);
  //### Signaling tracer advection ###//
  NS_STEP4_TRACER2_BOUNDARY_SET(intensityi_sig, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.pos_inlet, parsFDF.width_inlet, tf, T2, parsFDF);  
  NS_STEP4_TRACER(UXi,UYi,intensityi_sig,FDF.intensity_sig, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.dx, parsFDF.dy, parsFDF.pos_inlet, parsFDF.width_inlet, parsFDF.dtf, parsFDF.DTr_sig, tf, T2, parsFDF.Calc_Tr,parsFDF.reaction_tracer_sig);

  //### Heuns method ###//
//  cout << "final step of Heuns method \n";
  for(int k=0; k<UX_old.size(); k++)
   {
    FDF.UX[k] = UX_old[k] + 0.5*(FX1[k]+FX2[k]); //0.5*(UXi2[k] + UX_old[k]);//both derivative terms are calculated in terms of the obtained approximations
   }  
  for(int k=0; k<UY_old.size(); k++)
   {
    FDF.UY[k] = UY_old[k] + 0.5*(FY1[k]+FY2[k]); //0.5*(UYi2[k] + UY_old[k]);
   }  
  for(int k=0; k<FDF.intensity.size(); k++)
   {
    FDF.intensity[k] = + 0.5*(FDF.intensity[k] + intensity_old[k]);             //food tracer
    FDF.intensity_sig[k] = + 0.5*(FDF.intensity_sig[k] + intensity_sig_old[k]); //signaling tracer
   }  
   
  //### Check velocity for too large values ###//
#ifdef flow_damper
  Damper(FDF.UX, FDF.UY, tf, T, parsFDF);
#endif
  
  //### Pressure solver of final velocity ###//
//  cout << "NS: STEP2 \n";
  Pr_out = parsFDF.rho*parsFDF.GravY*((tanh(15.0*(tf-10.0+0.4*T)/T)+1.0)*0.5)*hight;
  NS_STEP2(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.beta,parsFDF.toll,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Npr, parsFDF.Nsor, parsFDF.Calc_Pr, parsFDF.Pr_mapper, parsFDF.Pr_mapper_even, parsFDF.Pr_mapper_odd, Pr_out);
  //### Pressure correction of final velocity ###//
//  cout << "NS: STEP3 \n";
  NS_STEP3(FDF.UX,FDF.UY,FDF.Pr,parsFDF.Nfx,parsFDF.Nfy,parsFDF.dx,parsFDF.dy,parsFDF.dtf,parsFDF.rho, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF.Calc_UX, parsFDF.Calc_UY, parsFDF.GravX, parsFDF.GravY, tf, T);
#ifdef flow_damper
  //### Check velocity for too large values ###//
  Damper(FDF.UX, FDF.UY, tf, T, parsFDF);//### Pressure solver ###//
#endif
  //### boundary conditions for next time step for the main domain ###//
  //cout << "set new boundary condition tf= " << tf << "\n";
  NS_boundary_set(FDF.UX, FDF.UY, FDF.Pr, parsFDF, tf, T, parsFDF.rho);
  NS_STEP0(FDF.UX, FDF.UY, parsFDF.UL, parsFDF.UU, parsFDF.ULE, parsFDF.UR, parsFDF.VL, parsFDF.VU, parsFDF.VLE, parsFDF.VR, parsFDF.Nfx, parsFDF.Nfy, Nfxo, Nfyo, parsFDF.pos_outlet, parsFDF);
//  cout << "tf = " << tf << "\n";

  //### Calculate large eddy viscosity ###//
  NS_STEP7(FDF.UX, FDF.UY, parsFDF.Nfx, parsFDF.Nfy, parsFDF.dx, parsFDF.dy, Nfxo, Nfyo, parsFDF.pos_outlet, FDF.LEV, parsFDF.CSL, parsFDF.boundary);

 }

void soft_boundary(DOUB &wallx, DOUB &wally, DOUB &X, DOUB &Y, params &pars, int k)
 {
  //NOTE: This function calculates a 1D repulsion force for the active agents and passive tracers
  //      - Has become obsolete if a propper immersed boundary is read in. Treated by soft_boundary_coast
  //DOUB wrad(0.125);
  if(abs(X-pars.xi)<pars.wrad)
   {
//    wallx = -1.0 - wrad/(pars.xi-X);//if in xi range
    wallx = (-1.0 + pars.wrad*pars.wrad/((pars.xi-X)*(pars.xi-X)))*pars.wrad*pars.wrad*10.0;//if in xi range
    //cout << "close to xi: del= " << X-pars.xi << " xi= " << pars.xi << " Fxi= " << -1.0 - wrad/(pars.xi-X) << "\n";
   }
  if(abs(X-pars.xf)<pars.wrad)
   {
//    wallx = 1.0 - wrad/(pars.xf-X);//if in xf range
    wallx = (1.0 - pars.wrad*pars.wrad/((pars.xf-X)*(pars.xf-X)))*pars.wrad*pars.wrad*10.0;//if in xf range
    //cout << "close to xf: del= " << X-pars.xi << " xf= " << pars.xi << " Fxf= " << 1.0 - wrad/(pars.xf-X) << "\n";
   }
  if(abs(Y-pars.yi)<pars.wrad)
   {
//    wally = -1.0 - wrad/(pars.yi-Y);//if in xf range
//    wally = (-1.0 + pars.wrad*pars.wrad/((pars.yi-Y)*(pars.yi-Y)))*pars.wrad*pars.wrad*10.0;//if in xf range
    wally = (-1.0 + pars.wrad*pars.wrad/((pars.yi-Y)*(pars.yi-Y)))*pars.wrad*pars.wrad*10.0;//if in xf range
    //cout << "close to yi: del= " << X-pars.xi << " yi= " << pars.xi << " Fyi= " << -1.0 - wrad/(pars.yi-Y) << "\n";
   }
  if(abs(Y-pars.yf)<pars.wrad)
   {
//    wally = 1.0 - wrad/(pars.yf-Y);//if in xf range
    wally = (1.0 - pars.wrad*pars.wrad/((pars.yf-Y)*(pars.yf-Y)))*pars.wrad*pars.wrad*10.0;//if in xf range
    //cout << "close to yf: del= " << X-pars.xi << " yf= " << pars.xi << " Fyf= " << 1.0 - wrad/(pars.yf-Y) << "\n";
   }
 }
 
/*void soft_boundary_coast(DOUB &wallx, DOUB &wally, DOUB &X, DOUB &Y, params &pars, int k)
 {
 //TODO: Check if agents get propperly repelled at the outer boundaries
  //soft boundary with complex shape from coastal map
  //NOTE: The force is calculated by checking the halo of surrounding cells
  //1(1,2)   2(2,2)
  //#--------# 
  //|   P    |
  //|   +    |
  //|        |
  //#--------#
  //0(1,1)   3(2,1)
  DOUB delx(0.0), dely(0.0), R(0.0);
  //corners
  if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==0))
   {
    delx = pars.boundaryX[k][0]-X;
    dely = pars.boundaryY[k][0]-Y;
    R=sqrt(delx*delx + dely*dely);
    cout << 1 << " " << k << "\n";
   // if(R<pars.wrad){wallx += (delx/R - pars.wrad*delx/(R*R))*pars.wrad*10.0; wally += (dely/R - pars.wrad*dely/(R*R))*pars.wrad*10.0; }
    {wallx += -delx*pars.dx/(R*pars.dt); wally += -dely*pars.dy/(R*pars.dt);}
   }
  if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==0))
   {
    delx = pars.boundaryX[k][1]-X;
    dely = pars.boundaryY[k][1]-Y;
    R=sqrt(delx*delx + dely*dely);
    cout << 2 << " " << k << "\n";
   // if(R<pars.wrad){wallx += (delx/R - pars.wrad*delx/(R*R))*pars.wrad*10.0; wally += (dely/R - pars.wrad*dely/(R*R))*pars.wrad*10.0; }
    {wallx += -delx*pars.dx/(R*pars.dt); wally += -dely*pars.dy/(R*pars.dt);}
   }
  if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==0))
   {
    delx = pars.boundaryX[k][2]-X;
    dely = pars.boundaryY[k][2]-Y;
    R=sqrt(delx*delx + dely*dely);
    cout << 3 << " " << k << "\n";
   // if(R<pars.wrad){wallx += (delx/R - pars.wrad*delx/(R*R))*pars.wrad*10.0; wally += (dely/R - pars.wrad*dely/(R*R))*pars.wrad*10.0; }
    {wallx += -delx*pars.dx/(R*pars.dt); wally += -dely*pars.dy/(R*pars.dt);}
   } 
  if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==1))
   {
    delx = pars.boundaryX[k][3]-X;
    dely = pars.boundaryY[k][3]-Y;
    R=sqrt(delx*delx + dely*dely);
    cout << 4 << " " << k << "\n";
   // if(R<pars.wrad){wallx += (delx/R - pars.wrad*delx/(R*R))*pars.wrad*10.0; wally += (dely/R - pars.wrad*dely/(R*R))*pars.wrad*10.0; }
    {wallx += -delx*pars.dx/(R*pars.dt); wally += -dely*pars.dy/(R*pars.dt);}
   } 

  //if two points are nearest neighbours
  if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==0))
   {
    delx = pars.boundaryX[k][0]-X;
    cout << k << "left \n";
   // if(abs(delx)<pars.wrad){wallx += (delx/abs(delx)*(1.0 - pars.wrad*pars.wrad/(delx*delx)))*pars.wrad*10.0; }
    {wallx += 1.0*pars.dx/pars.dt;}
   }
  if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==0))
   {
    dely = pars.boundaryY[k][1]-Y;   
    cout << k << "upper \n";
   // if(abs(dely)<pars.wrad){wally += (dely/abs(dely)*(1.0 - pars.wrad*pars.wrad/(dely*dely)))*pars.wrad*10.0; }
    {wally += -1.0*pars.dy/pars.dt;}
   } 
  if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==1))
   {
    delx = pars.boundaryX[k][2]-X;
    cout << k << "right \n";
   // if(abs(delx)<pars.wrad){wallx += (delx/abs(delx)*(1.0 - pars.wrad*pars.wrad/(delx*delx)))*pars.wrad*10.0; }
    {wallx += -1.0*pars.dx/pars.dt;}
   }
  if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==1))
   {
    dely = pars.boundaryY[k][3]-Y;   
    cout << k << "lower \n";
   // if(abs(dely)<pars.wrad){wally += (dely/abs(dely)*(1.0 - pars.wrad*pars.wrad/(dely*dely)))*pars.wrad*10.0; }
    {wally += 1.0*pars.dy/pars.dt;}
   } 
   
  //if three points are nearest neighbours
  if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==0))
   {
    delx = pars.boundaryX[k][0]-X;
    dely = pars.boundaryY[k][1]-Y;   
    cout << k << "left up \n";
    {wallx += 1.0*pars.dx/(sqrt(2.0)*pars.dt);}
    {wally += -1.0*pars.dy/(sqrt(2.0)*pars.dt);}
   }
  if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==1))
   {
    delx = pars.boundaryX[k][2]-X;
    dely = pars.boundaryY[k][1]-Y;   
    cout << k << "right up \n";
    {wallx += -1.0*pars.dx/(sqrt(2.0)*pars.dt);}
    {wally += -1.0*pars.dy/(sqrt(2.0)*pars.dt);}
   } 
  if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==1))
   {   
    delx = pars.boundaryX[k][2]-X;
    dely = pars.boundaryY[k][3]-Y;   
    cout << k << "right lower \n";
    {wallx += -1.0*pars.dx/(sqrt(2.0)*pars.dt);}
    {wally += 1.0*pars.dy/(sqrt(2.0)*pars.dt);}
   }
  if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==1))
   {
    delx = pars.boundaryX[k][0]-X;
    dely = pars.boundaryY[k][3]-Y;   
    cout << k << "left lower \n";
    {wallx += 1.0*pars.dx/(sqrt(2.0)*pars.dt);}
    {wally += 1.0*pars.dy/(sqrt(2.0)*pars.dt);}
   }  
 }//*/
 
void reflect_Boundary(Vectordata &Data, params &pars, parsf &parsFDF, DOUB &dt)
 {
  //hard boundary with complex shape from coastal map
  //NOTE: The force is calculated by checking the halo of surrounding cells
  //1(1,2)   2(2,2)
  //#--------# 
  //|   P    |
  //|   +    |
  //|        |
  //#--------#
  //0(1,1)   3(2,1)
  //cout << "reflect agents \n";
  #pragma omp parallel for
  for(int k=0; k<pars.UXA.size(); k++)
   {
    //if(pars.is_boundary[k][0] + pars.is_boundary[k][1] + pars.is_boundary[k][2] + pars.is_boundary[k][3]>0.0)
    // {
    //  Data.pos[2*k] -= 0.5*Data.vel[2*k]*dt; Data.pos[2*k+1] -= 0.5*Data.vel[2*k+1]*dt;
    // }
    if(pars.is_boundary[k][0] + pars.is_boundary[k][1] + pars.is_boundary[k][2] + pars.is_boundary[k][3]==4.0)
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     }
   }//*/
   #pragma omp barrier
/*
    //corners
    if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==0))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     }
    if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==0))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     }
    if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==0))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     } 
    if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==1))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     } 

    //if two points are nearest neighbours
    if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==0))
     {
      Data.pos[2*k] -= 0.5*Data.vel[2*k]*dt; 
     }
    if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==0))
     {
      Data.pos[2*k+1] -= 0.5*Data.vel[2*k+1]*dt;
     } 
    if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==1))
     {
      Data.pos[2*k] -= 0.5*Data.vel[2*k]*dt;
     }
    if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==1))
     {
      Data.pos[2*k+1] -= 0.5*Data.vel[2*k+1]*dt;
     } 
   
    //if three points are nearest neighbours
    if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==0))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     }
    if((pars.is_boundary[k][0]==0) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==1))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     } 
    if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==0) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==1))
     {   
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     }
    if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==0) & (pars.is_boundary[k][3]==1))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     }
    //### too far inside boundary ###// 
    if((pars.is_boundary[k][0]==1) & (pars.is_boundary[k][1]==1) & (pars.is_boundary[k][2]==1) & (pars.is_boundary[k][3]==1))
     {
      Data.pos[2*k] -= Data.vel[2*k]*dt; Data.pos[2*k+1] -= Data.vel[2*k+1]*dt;
     }
   }  //*/   
 } 

void Coast_update(parsf &parsFDF, params &pars, int &N)
 {
  //NOTE: updates the nearest coastal cells for active and passive agents after new interpolation indices have been updated
  //      Since FDF_index vectors hold index positions in the exchange grid and not in the actual CFD grid
  //      but parsFDF.boundary is defined on pressure nodes of the actual CFD grid the indices are shifted by 
  //      1 (exchange grid is shifted by one compared to the CFD grid
  //      porous cells are also considered boundary cells for the active and passive agents 
  //      they can be added below because a cell is either a hard boundary or a porous boundary. Never both.
  //cout << "coast update \n";
  for(int l=0; l<4;l++)
   {
    //active agents
    for(int k=0; k<N;k++)
     {
      pars.is_boundary[k][l] = parsFDF.boundary[(pars.FDF_indexX[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY[k][l]+1)] + parsFDF.porous[(pars.FDF_indexX[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY[k][l]+1)];
      pars.boundaryX[k][l] = parsFDF.boundaryX[(pars.FDF_indexX[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY[k][l]+1)];
      pars.boundaryY[k][l] = parsFDF.boundaryY[(pars.FDF_indexX[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY[k][l]+1)];
     }
    //passive agents
    for(int k=0; k<N;k++)
     {
      pars.is_boundary2[k][l] = parsFDF.boundary[(pars.FDF_indexX2[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY2[k][l]+1)] + parsFDF.porous[(pars.FDF_indexX2[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY2[k][l]+1)];
      pars.boundaryX2[k][l] = parsFDF.boundaryX[(pars.FDF_indexX2[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY2[k][l]+1)];
      pars.boundaryY2[k][l] = parsFDF.boundaryY[(pars.FDF_indexX2[k][l]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY2[k][l]+1)];
     }
   }
 }

DOUB Kernel_fit(DOUB &xg, DOUB &yg, DOUB xi, DOUB yi, DOUB kappa)
 {
  return exp(-((xg-xi)*(xg-xi)+(yg-yi)*(yg-yi))*kappa);
 }

void Interpolate_Image_to_Grid(string img_tr, string img_U, string img_V, FDFS &FDF, parsf &parsFDF)
 {
  //TODO: this interfact with several input images has never been tested; TEST NEEDED
  //NOTE: This function interpolates png image data in matrix data form to the agent coupling grid.
  //      The spacial dimensions of the data and the dimensions of the exchange grid can differ 
  //      The read data has to have the form x0,y0, data
  //                                         x0,y1, data
  //                                          .  .    .
  //                                         x0,yN, data
  //                                         x1,y0, data
  //                                         x1,y1, data
  //                                          .  .    .
  //                                         x1,yN, data
  //                                          .  .    .
  //                                         xN,yN, data
  
  //interpolate image data onto the simulation grid as specified by parameters //
  
  vector<DOUB> imgx;//xaxis
  vector<DOUB> imgy;//yaxis
  vector<DOUB> imgF1;//field value tracer
  vector<DOUB> imgF2;//field value U
  vector<DOUB> imgF3;//field value V
  
  string line;
  int cx(0);        //counts blocks of changing y axis
  //### EXTRACT FILE CONTENT OF FORM data <SPACE> time \n to struct members ###//
  
  //Tracer
  cout << "interpolating data from file " << img_tr << "\n";
  ifstream imgdat1 (img_tr);
  if(imgdat1.is_open())
   {
    DOUB a(0.0);
    while(getline(imgdat1,line))
     {
      istringstream is(line);
      vector<DOUB> vecline;
      int jj(0);
      while(is >> a)
       {
        if(jj==0)//x axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==1)//y axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==2)//field data
         {
          vecline.push_back(DOUB(a));
         }
        jj +=1;
       }
      if(jj==0){cx +=1;}//counts the number of blocks in which y axis is changed
      cout << "data extracted x,y,F= "; for(int k=0; k<vecline.size();k++){cout << vecline[k] << ", ";} cout << "length of elementsin line is " << jj << "\n";
      if(jj>0){imgF1.push_back(vecline[2]);}//index 2 for image intensity!!!
     }
    imgdat1.close();
   }

  //U velocity
  cout << "interpolating data from file " << img_U << "\n";
  ifstream imgdat2 (img_U);
  if(imgdat2.is_open())
   {
    DOUB a(0.0);
    while(getline(imgdat2,line))
     {
      istringstream is(line);
      vector<DOUB> vecline;
      int jj(0);
      while(is >> a)
       {
        if(jj==0)//x axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==1)//y axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==2)//field data
         {
          vecline.push_back(DOUB(a));
         }
        jj +=1;
       }
      if(jj==0){cx +=1;}//counts the number of blocks in which y axis is changed
      cout << "data extracted x,y,F= "; for(int k=0; k<vecline.size();k++){cout << vecline[k] << ", ";} cout << "length of elementsin line is " << jj << "\n";
      if(jj>0){imgF2.push_back(vecline[2]);}//index 2 for image intensity!!!
     }
    imgdat2.close();
   }
   
  //V velocity
  cout << "interpolating data from file " << img_V << "\n";
  ifstream imgdat3 (img_V);
  if(imgdat3.is_open())
   {
    DOUB a(0.0);
    while(getline(imgdat3,line))
     {
      istringstream is(line);
      vector<DOUB> vecline;
      int jj(0);
      while(is >> a)
       {
        if(jj==0)//x axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==1)//y axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==2)//field data
         {
          vecline.push_back(DOUB(a));
         }
        jj +=1;
       }
      if(jj==0){cx +=1;}//counts the number of blocks in which y axis is changed
      cout << "data extracted x,y,F= "; for(int k=0; k<vecline.size();k++){cout << vecline[k] << ", ";} cout << "length of elementsin line is " << jj << "\n";
      if(jj>0){imgF3.push_back(vecline[2]);}//index 2 for image intensity!!!
     }
    imgdat3.close();
   }   
   
  int cy(imgF1.size()/cx);
  cout << "extracted image with " << cx << " points in x direction and " << cy << " points in y direction \n";
  //### interpolate image onto simulational grid by means of gaussian smoothing ###//
  DOUB dximg((parsFDF.fxf-parsFDF.fxi)/cx), dyimg((parsFDF.fyf-parsFDF.fyi)/cy);//set axis steps of image as given by parameters of CFD grid

  DOUB knorm1(0.0), knorm2(0.0), knorm3(0.0); //normalization of kernel
  for(int k=0; k<FDF.gridX.size();k++)//iterate CFD x axis
   {
    for(int l=0; l<FDF.gridY.size();l++)//iterate CFD y axis
     {
      knorm1=0.0;
      knorm2=0.0;
      knorm3=0.0;
      for(int m=0; m<cx; m++)//iterate image x axis
       {
        for(int n=0; n<cy; n++)//iterate image y axis
	 {
          //Tracer
          FDF.intensity_coupler[k*parsFDF.Nfy+l] += imgF1[m*cy+n]*Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);
	  knorm1 += Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);
          //U velocity
          FDF.UX_coupler[k*parsFDF.Nfy+l] += imgF2[m*cy+n]*Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);
	  knorm2 += Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);
          //V velocity
          FDF.UY_coupler[k*parsFDF.Nfy+l] += imgF3[m*cy+n]*Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);
	  knorm3 += Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa);

        //  cout << " xg,yg= " << FDF.gridX[k] << ", " << FDF.gridY[l] << " pixelgrid x,y= " << parsFDF.fxi+DOUB(m)*dximg+0.5*dximg << ", " << parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg << " data value= " << imgF[m*cy+n] << " kernel value= " << Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa) << " result= " << imgF[m*cy+n]*Kernel_fit(FDF.gridX[k],FDF.gridY[l],parsFDF.fxi+DOUB(m)*dximg+0.5*dximg,parsFDF.fyi+DOUB(n)*dyimg+0.5*dyimg,parsFDF.kappa) << " summation= " << FDF.intensity[k*parsFDF.Nfy+l] << " norm= " << knorm << "\n";
	 }
       }
      if(knorm1<1e-6){knorm1=1.0;}//avoid division by zero
      if(knorm2<1e-6){knorm2=1.0;}//avoid division by zero
      if(knorm3<1e-6){knorm3=1.0;}//avoid division by zero
      //cout << " before: FDF.intensity[" << k*parsFDF.Nfy+l << "]= " << FDF.intensity[k*parsFDF.Nfy+l] << "\n";
      FDF.intensity_coupler[k*parsFDF.Nfy+l] /= knorm1;
      FDF.UX_coupler[k*parsFDF.Nfy+l] /= knorm2;
      FDF.UY_coupler[k*parsFDF.Nfy+l] /= knorm3;
//      cout << " after: norm kernel= " << knorm << " FDF.intensity[" << k*parsFDF.Nfy+l << "]= " << FDF.intensity_coupler[k*parsFDF.Nfy+l] << "\n";
     }
   }
 }

void set_Interpolation_indices(vector<int> &FDF_indexXK, vector<int> &FDF_indexYK, int &nx, int &ny, DOUB &x, DOUB &y, DOUB &FDFx, DOUB &FDFy)
 {
  //NOTE: This function updates the interpolation indices as given by the index update function
  //  closest point seen from grid  ... from particle
  if((x<FDFx)&(y<FDFy))//lower left ... upper right
   {
    FDF_indexXK[0]=-1+nx;
    FDF_indexXK[1]=-1+nx;
    FDF_indexXK[2]=nx;
    FDF_indexXK[3]=nx;
    FDF_indexYK[0]=-1+ny;
    FDF_indexYK[1]=ny;
    FDF_indexYK[2]=ny;
    FDF_indexYK[3]=-1+ny;
   }
  if((x<FDFx)&(y>FDFy))//upper left ... lower right
   {
    FDF_indexXK[0]=-1+nx;
    FDF_indexXK[1]=-1+nx;
    FDF_indexXK[2]=nx;
    FDF_indexXK[3]=nx;
    FDF_indexYK[0]=ny;
    FDF_indexYK[1]=1+ny;
    FDF_indexYK[2]=1+ny;
    FDF_indexYK[3]=ny;
   }
  if((x>FDFx)&(y>FDFy))//upper right ... lower left
   {
    FDF_indexXK[0]=nx;
    FDF_indexXK[1]=nx;
    FDF_indexXK[2]=1+nx;
    FDF_indexXK[3]=1+nx;
    FDF_indexYK[0]=ny;
    FDF_indexYK[1]=1+ny;
    FDF_indexYK[2]=1+ny;
    FDF_indexYK[3]=ny;
   }
  if((x>FDFx)&(y<FDFy))//lower right ... upper left
   {
    FDF_indexXK[0]=nx;
    FDF_indexXK[1]=nx;
    FDF_indexXK[2]=1+nx;
    FDF_indexXK[3]=1+nx;
    FDF_indexYK[0]=-1+ny;
    FDF_indexYK[1]=ny;
    FDF_indexYK[2]=ny;
    FDF_indexYK[3]=-1+ny;
   }
 }

void Update_FDF_index(vector<DOUB> &y, int &N, vector<DOUB> &gridX, vector<DOUB> &gridY, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY)
 {
  //NOTE: finds the vector index corresponding to closest grid point of each agents during simulation
  //      Particle needs to be in the domain!
  //      Uses lower left corner of current containing grid cell and iterates through the 9 nearest grid points arround the agent. 
  //      - If an other grid point is closer than the current one, updtate the grid indicees.
  //      - The process is stopped when there is no new grid point that is closer than the previous one. 
  //        This last step allows to also consider particle trajectories which jump over several grid cells within one time step.

  // finds the vector index corresponding to closest grid point of each agents
  DOUB distold(0.0),dist(0.0);
  int nx(0), ny(0), fx(0), fy(0); //how many steps in x direction, how many steps in y direction
  #pragma omp parallel for private (distold, dist, nx, ny, fx, fy)
  for(int k=0;k<N; k++)
   {
    nx=FDF_indexX[k][0];//current x index of agents in CFD grid
    ny=FDF_indexY[k][0];//y index
    distold=sqrt((gridX[nx]-y[2*k])*(gridX[nx]-y[2*k]) + (gridY[ny]-y[2*k+1])*(gridY[ny]-y[2*k+1]));
    int goOn(1); //how many layers arround current grid index position -> at least one
    fx=FDF_indexX[k][0];
    fy=FDF_indexY[k][0];
    while(goOn==1)
     {
      goOn=0;
      //iterate arround an agent to find closest CFD grid point
      for(int l=nx-1;l<=nx+1;l++)
       {
        for(int m=ny-1;m<=ny+1;m++)
 	 {
          dist=sqrt((gridX[l]-y[2*k])*(gridX[l]-y[2*k]) + (gridY[m]-y[2*k+1])*(gridY[m]-y[2*k+1]));
          if((!((l==nx)&(m==ny)))&(dist<distold))
	   {
  	    distold=dist;
	    fx=l;
	    fy=m;
	    goOn=1;
	   }
  	 }
       }
      nx=fx;
      ny=fy;
     }//off while
    set_Interpolation_indices(FDF_indexX[k], FDF_indexY[k], nx, ny, y[2*k], y[2*k+1], gridX[nx], gridY[ny]);
   }
  #pragma omp barrier
 }

void Update_FDF_index_ghost(vector<DOUB> &y, int &N, vector<DOUB> &gridX, vector<DOUB> &gridY, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY, DOUB &dx, DOUB &dy)
 {
  //NOTE: finds the vector index corresponding to closest grid point of each agents during simulation
  //      Particle needs to be in the domain!
  //      Uses lower left corner of current containing grid cell and iterates through the 9 nearest grid points arround the agent. 
  //      - If an other grid point is closer than the current one, updtate the grid indicees.
  //      - The process is stopped when there is no new grid point that is closer than the previous one. 
  //        This last step allows to also consider particle trajectories which jump over several grid cells within one time step.
  DOUB distold(0.0),dist(0.0);
  int nx(0), ny(0), fx(0), fy(0); //how many steps in x direction, how many steps in y direction
  #pragma omp parallel for private (distold, dist, nx, ny, fx, fy)
  for(int k=0;k<N; k++)
   {
    nx=FDF_indexX[k][0];//current x index of agents in CFD grid
    ny=FDF_indexY[k][0];//        y index
    distold=sqrt((gridX[nx]-y[2*k])*(gridX[nx]-y[2*k]) + (gridY[ny]-y[2*k+1])*(gridY[ny]-y[2*k+1]));
    int goOn(1);   // how many layers arround current grid index position -> at least one
    int dontDo(0); // do not do something if the agents are outside of the interpolation domain
    
    //check if agents are outside of grid -> if true, no update of indicees
    if((y[2*k]<gridX[0]) || (y[2*k]>gridX[gridX.size()-1]) || (y[2*k+1]<gridY[0]) || (y[2*k+1]>gridY[gridY.size()-1])){dontDo=1;}
    
    if(dontDo==0)
     {
      fx=FDF_indexX[k][0];//set initial index to be the lower left corner of the confininf cell
      fy=FDF_indexY[k][0];
      while(goOn==1)
       {
        goOn=0;
        //iterate arround an agent to find closest CFD grid point
        for(int l=nx-1;l<=nx+1;l++)
         {
          for(int m=ny-1;m<=ny+1;m++)
     	   {
            dist=sqrt((gridX[l]-y[2*k])*(gridX[l]-y[2*k]) + (gridY[m]-y[2*k+1])*(gridY[m]-y[2*k+1]));
            if((!((l==nx)&(m==ny)))&(dist<distold))//update index if new index found
	     {
	      distold=dist;
 	      fx=l;
	      fy=m;
	      goOn=1;
	     }
	   }
         }
        nx=fx;
        ny=fy;
       }//off while
      set_Interpolation_indices(FDF_indexX[k], FDF_indexY[k], nx, ny, y[2*k], y[2*k+1], gridX[nx], gridY[ny]);//set interpolation indices
     }
   }
  #pragma omp barrier 
 }

void FDF_index(vector<DOUB> &y, int &N, vector<DOUB> &gridX, vector<DOUB> &gridY, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY)
 {
  //NOTE: This function finds the vector index corresponding to closest grid point of each agents
  //      Function is called just once in the beginning
  DOUB distold(0.0),dist(0.0);
  int nx(0), ny(0);
  #pragma omp parallel for private (distold, dist, nx, ny)
  for(int k=0;k<N; k++)
   {
    distold=10000000.0;//large initial number
    nx=0;
    ny=0;
    for(int l=0; l<gridX.size();l++)
     {
      for(int m=0; m<gridY.size();m++)
       {
	dist= sqrt((gridX[l]-y[2*k])*(gridX[l]-y[2*k]) + (gridY[m]-y[2*k+1])*(gridY[m]-y[2*k+1]));
        if(distold>dist)
	 {
          distold=dist;
          //FDF_index[k] = l*gridY.size()+m;
          nx = l;//index in x
          ny = m;//index in y
	 }
       }
     }
    //set surrounding integers of nodes -> interpolation of fields
    set_Interpolation_indices(FDF_indexX[k], FDF_indexY[k], nx, ny, y[2*k], y[2*k+1], gridX[nx], gridY[ny]);
   }//off for k
  #pragma omp barrier 
 }

void Interpolator(vector<DOUB> &y, vector<DOUB> &F_inter, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY, vector<DOUB> &F, vector<DOUB> &gridX, vector<DOUB> &gridY, parsf &parsFDF)
 {
  //NOTE: This function interpolates CFD field values onto the position of agents
  //1(1,2)   2(2,2)
  //#--------# 
  //|   P    |
  //|   +    |
  //|        |
  //#--------#
  //0(1,1)   3(2,1)
  //update field values (F) at position of agents (y) stored in F_inter
  #pragma omp parallel for
  for(int k=0; k<F_inter.size(); k++)
   {
    F_inter[k] = (F[FDF_indexX[k][0]*parsFDF.Nfy+FDF_indexY[k][0]]*(gridX[FDF_indexX[k][2]]-y[2*k])*(gridY[FDF_indexY[k][2]]-y[2*k+1]))                                                               + (F[FDF_indexX[k][3]*parsFDF.Nfy+FDF_indexY[k][3]]*(-gridX[FDF_indexX[k][0]]+y[2*k])*(gridY[FDF_indexY[k][2]]-y[2*k+1]))                                                              + (F[FDF_indexX[k][1]*parsFDF.Nfy+FDF_indexY[k][1]]*(gridX[FDF_indexX[k][2]]-y[2*k])*(-gridY[FDF_indexY[k][0]]+y[2*k+1]))                                                              + (F[FDF_indexX[k][2]*parsFDF.Nfy+FDF_indexY[k][2]]*(-gridX[FDF_indexX[k][0]]+y[2*k])*(-gridY[FDF_indexY[k][0]]+y[2*k+1]));
    F_inter[k] /= ((gridX[FDF_indexX[k][2]]-gridX[FDF_indexX[k][0]])*(gridY[FDF_indexY[k][2]]-gridY[FDF_indexY[k][0]]));
    //F_inter[k]=F[FDF_indexX[k][0]*parsFDF.Nfy+FDF_indexY[k][0]];//value of field at position given by closest grid point
   }
  #pragma omp barrier
 }

void Interpolator_ghost(vector<DOUB> &y, vector<DOUB> &F_inter, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY, vector<DOUB> &F, vector<DOUB> &gridX, vector<DOUB> &gridY, parsf &parsFDF)
 {
  //NOTE: This function interpolates CFD field values onto the position of agents
  //      -When an agent has left the domain, interpolation stops
  //1(1,2)   2(2,2)
  //#--------# 
  //|   P    |
  //|   +    |
  //|        |
  //#--------#
  //0(1,1)   3(2,1)
  //update field values (F) at position of agents (y) stored in F_inter
  #pragma omp parallel for
  for(int k=0; k<F_inter.size(); k++)
   {
    //check if agents are outside of grid -> if true, no update of indicees
    if((y[2*k]>gridX[0]) && (y[2*k]<gridX[gridX.size()-1]) && (y[2*k+1]>gridY[0]) && (y[2*k+1]<gridY[gridY.size()-1]))
     {
      F_inter[k] = (F[FDF_indexX[k][0]*parsFDF.Nfy+FDF_indexY[k][0]]*(gridX[FDF_indexX[k][2]]-y[2*k])*(gridY[FDF_indexY[k][2]]-y[2*k+1]))                                                               + (F[FDF_indexX[k][3]*parsFDF.Nfy+FDF_indexY[k][3]]*(-gridX[FDF_indexX[k][0]]+y[2*k])*(gridY[FDF_indexY[k][2]]-y[2*k+1]))                                                              + (F[FDF_indexX[k][1]*parsFDF.Nfy+FDF_indexY[k][1]]*(gridX[FDF_indexX[k][2]]-y[2*k])*(-gridY[FDF_indexY[k][0]]+y[2*k+1]))                                                              + (F[FDF_indexX[k][2]*parsFDF.Nfy+FDF_indexY[k][2]]*(-gridX[FDF_indexX[k][0]]+y[2*k])*(-gridY[FDF_indexY[k][0]]+y[2*k+1]));
      F_inter[k] /= ((gridX[FDF_indexX[k][2]]-gridX[FDF_indexX[k][0]])*(gridY[FDF_indexY[k][2]]-gridY[FDF_indexY[k][0]]));
      //F_inter[k]=F[FDF_indexX[k][0]*parsFDF.Nfy+FDF_indexY[k][0]];//value of field at position given by closest grid point
     }
   }
  #pragma omp barrier 
 }


DOUB detector(DOUB &fieldval, DOUB &thresh)
 {
  DOUB detect(0.0);
  if(fieldval>=thresh){detect=1.0;}
  return detect;
 }

DOUB Coupling_internal_state_Agent_attraction(DOUB &phi, DOUB &J)
 {
//  return 1.0 + pars.J*cos(y[(n-1)*N/n+l]-y[(n-1)*N/n+k]);
  return 1.0 + J*cos(phi);
 }

DOUB Coupling_direction(DOUB &del, DOUB &theta, DOUB &agent_direction, DOUB &J2)
 {
#ifdef Strogatz
  return 1.0;
#else
  return 1.0 - agent_direction + agent_direction*((1.0-cos(2.0*(del - theta)))*J2*0.5 + (1.0-J2));//detection sensitivity of positional information
#endif
 }

DOUB Coupling_direction_farfield(DOUB &del, DOUB &theta, DOUB &agent_direction, DOUB &J2)
 {
  return 1.0 - agent_direction + agent_direction*((1.0-cos(2.0*(del - theta)))*J2*0.5 + (1.0-J2)); // detection sensitivity of positional information
 }
 
DOUB Coupling_transmission(DOUB &Tgstartl, DOUB &x3tl, DOUB &theta3tl, DOUB &y3tl , DOUB &xk , DOUB &yk, DOUB &t, DOUB &irad, DOUB &mu2)
 {
  //NOTE: This fucntion defines the wake geometry.
  //       #
  //       |
  //       X
  //       |
  //       #
  // # means sub position of each vortex in the wake.
  //   To avoid numerical overflow, an initial time offset of 0.01 is assumed
  DOUB P(0.5*irad), S1(4.0*mu2*(t-Tgstartl+0.01));
  DOUB xp1(x3tl-P*sin(theta3tl)), yp1(y3tl+P*cos(theta3tl));
  DOUB xm1(x3tl+P*sin(theta3tl)), ym1(y3tl-P*cos(theta3tl));
  //DOUB xp2(x3tl-P*sin(theta3tl) + 0.05*cos(theta3tl)), yp2(y3tl+P*cos(theta3tl) + 0.05*sin(theta3tl));
  //DOUB xm2(x3tl+P*sin(theta3tl) + 0.05*cos(theta3tl)), ym2(y3tl-P*cos(theta3tl) + 0.05*sin(theta3tl)); 
  //return (abs((exp((-(xk-xp1)*(xk-xp1)-(yk-yp1)*(yk-yp1))/S1) - exp((-(xk-xm1)*(xk-xm1)-(yk-ym1)*(yk-ym1))/S1) - exp((-(xk-xp2)*(xk-xp2)-(yk-yp2)*(yk-yp2))/S1) + exp((-(xk-xm2)*(xk-xm2)-(yk-ym2)*(yk-ym2))/S1))/(S1*pi)));
  return (abs((exp((-(xk-xp1)*(xk-xp1)-(yk-yp1)*(yk-yp1))/S1) - exp((-(xk-xm1)*(xk-xm1)-(yk-ym1)*(yk-ym1))/S1)))/(S1*pi));
  //return (1.0+cos(y[(n-2)*N/n+l]-del))*0.5//transmission of positional information (intstantaneous)
 }

DOUB Int_Phase_coupling(DOUB &phil, DOUB &phik, DOUB &dist, DOUB &eps, DOUB &phase_lag, DOUB &Ri)
 {
  return eps*sin(phil - phik + phase_lag)*exp(-dist*dist/(Ri*Ri));// /dist 1/r, agents synchronize their phases
//   return pars.eps*sin(y[(n-1)*N/n+l]-y[(n-1)*N/n+k])/((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));//r^-2
 }

DOUB Int_Phase_coupling_farfield(DOUB &phik, DOUB &eps, DOUB &phase_lag_far)
 {
  return eps*sin(phase_lag_far - phik);// NOTE: by definition the internal phase transported with the wake field is 2pi, agents synchronize their phases
 }

DOUB Int_Direction_coupling(DOUB thetal, DOUB &thetak, DOUB &dist, DOUB &eps2, DOUB &dir_lag, DOUB &Ri)
 {
  //DOUB del(atan2((y[2*l+1] - y[2*k+1]),(y[2*l] - y[2*k])));//relative angle in between agents
  return eps2*sin(thetal - thetak + dir_lag)*exp(-dist*dist/(Ri*Ri)); ///dist;//1/r, agents try to align their orientations
//  return -pars.eps2*sin(y[(n-2)*N/n+l] - y[(n-2)*N/n+k] + pars.dir_lag*sin(del))/sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));//1/r
//  return -pars.eps2*sin(y[(n-2)*N/n+l] - y[(n-2)*N/n+k] + pars.dir_lag*sin(del))/((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]));//r^-2
 }

DOUB Int_Direction_coupling_farfield(DOUB &theta3tl, DOUB &thetak, DOUB &eps2, DOUB &dir_lag_far)
 {
  return -eps2*sin(theta3tl - thetak + dir_lag_far);//NOTE: wake transports the angle of its own orientation to the other agents, agents try to align against the orientation (to swimm towards the generating active agent)
 }

DOUB Int_Direction_coupling_velocity(DOUB &UYAk, DOUB &UXAk, DOUB &thetak, DOUB &eps3)
 {
  DOUB angU(atan2(UYAk,UXAk));//angle of flow velocity measured from positive x axis
  return eps3*sin(thetak-angU);//agent 
 }

DOUB Vel_Direction_switch(DOUB &UXAk, DOUB &UYAk, DOUB &vel_dir)
 {
  // NOTE: This function switches on gradually the ability to swimm against the flow
  //       avoids zero angle if fluid flow is zero
  DOUB U(sqrt(UXAk*UXAk + UYAk*UYAk)); 
  return 0.5*(1.0+tanh(400.0*(U-vel_dir))); //(U/(vel_dir + U)); 
 }

DOUB Vort_Direction_switch(DOUB &VorticityAk, DOUB &vort_dir)
 {
  // NOTE: This function switches on gradually the ability to swimm against the vorticity gradient
  //       avoids zero angle if fluid flow is zero/ vorticity is zero
  DOUB V(abs(VorticityAk));             
  return 0.5*(1.0+tanh(400.0*(V-vort_dir))); //(V/(vort_dir + V)); 
 }

DOUB Signal_Direction_switch(DOUB &intensity_sig, DOUB &sig_dir, DOUB &tr_dir_steepnes2)
 {
  DOUB SigTr(intensity_sig);//sqrt(Tr_Sig_gradXAk*Tr_Sig_gradXAk + Tr_Sig_gradYAk*Tr_Sig_gradYAk));
  return 0.25*(1.0+tanh(tr_dir_steepnes2*(SigTr-sig_dir)))*(1.0+tanh(tr_dir_steepnes2*(1.0-SigTr-sig_dir))); //(SigTr/(sig_dir + SigTr)); 
 }

DOUB Int_Direction_coupling_vorticity(DOUB &Vorticity_gradYAk, DOUB &Vorticity_gradXAk, DOUB thetak, DOUB &eps4)
 {
  DOUB angGV(atan2(Vorticity_gradYAk,Vorticity_gradXAk));//angle of vorticity measured from positive x axis
  return eps4*sin(thetak-angGV);//agent
 }

DOUB Act_Direction_switch(DOUB &Ak, DOUB &tr_dir1, DOUB &tr_dir_steepnes)
 {
  return tr_dir1*(1.0-(Ak/(tr_dir_steepnes + Ak)));
 }
 
DOUB Tracer_Direction_switch(DOUB &intensityk, DOUB &tr_dir2, DOUB &tr_steepnes)
 {
  return tr_dir2*(1.0-(intensityk/(tr_steepnes + intensityk)));
 }
  
DOUB Activity_Direction_step(DOUB &Ak, DOUB &tr_dir3, DOUB &act_steepnes_2)
 {
  return tr_dir3*(1.0-(Ak/(act_steepnes_2 + Ak)));
  //return pars.tr_dir*0.5*(1.0-tanh(10.0*(y[(n-3)*N/n+k]-pars.act_steepnes_2)));
 }

DOUB Int_Direction_coupling_tracer(DOUB &TrgradYAk, DOUB &TrgradXAk, DOUB &thetak, DOUB &Ak, DOUB &eps5, DOUB &tr_dir3, DOUB &tr_dir_steepnes)
 {
  DOUB angGV(atan2(TrgradYAk,TrgradXAk));//angle of food tracer measured from positive x axis
  return -eps5*sin(thetak-angGV)*(Ak/(tr_dir_steepnes + Ak));
 }

DOUB Int_Direction_coupling_tracer_Sig(DOUB &Tr_Sig_gradYAk, DOUB &Tr_Sig_gradXAk, DOUB &thetak, DOUB &eps6, DOUB &tr_dir4, DOUB &tr_dir_steepnes2)
 {
  DOUB angGV(atan2(Tr_Sig_gradYAk,Tr_Sig_gradXAk)), TrG(sqrt(Tr_Sig_gradYAk*Tr_Sig_gradYAk + Tr_Sig_gradXAk*Tr_Sig_gradXAk));//angle of signaling tracer measured from positive x axis
  return -eps6*sin(thetak-angGV);//*tr_dir4*(TrG/(tr_dir_steepnes2 + TrG));//*(y[(n-3)*N/n+k]/(pars.tr_dir_steepnes2 + y[(n-3)*N/n+k]));
 }

DOUB Agent_attractionX(DOUB &xk, DOUB &xl, DOUB &dist, DOUB &agent_atr)
 {
//  return pars.agent_atr*(y[2*l]-y[2*k])/(sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1])));//1/r
  return agent_atr*(xl-xk)/dist;//1/r
 }

DOUB Agent_repulsionX(DOUB &xk, DOUB &xl, DOUB &dist2, DOUB &agent_rep)
 {
//  return -(y[2*l]-y[2*k])/((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]))*pars.agent_rep;//r^-2
  return -(xl-xk)*(1.0/dist2)*agent_rep;//r^-2 + cutting potential term
 }

DOUB Agent_attractionY(DOUB &yk, DOUB &yl, DOUB &dist, DOUB &agent_atr)
 {
//  return pars.agent_atr*(y[2*l+1]-y[2*k+1])/(sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1])));//1/r
  return agent_atr*(yl-yk)/dist;//1/r
 }

DOUB Agent_repulsionY(DOUB &yk, DOUB &yl, DOUB &dist2, DOUB &agent_rep)
 {
 // return -(y[2*l+1]-y[2*k+1])/((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]))*pars.agent_rep;//r^-2
  return -(yl-yk)*(1.0/dist2)*agent_rep;//r^-2
 }

DOUB Velocity(DOUB &UXAk, DOUB &UYAk, DOUB &Ak, DOUB &phik, DOUB &counter_current_act, DOUB &vel0, DOUB &counter_curent, DOUB &vel_c, DOUB &Vs, DOUB &J3)
 {
//  DOUB U2((pars.UXA[k]*pars.UXA[k]+pars.UYA[k]*pars.UYA[k]));//magnitude square of local velocity of agents - measure of kinetic energy of fluid motion
  DOUB U2(sqrt(UXAk*UXAk+UYAk*UYAk) + counter_current_act*Ak);//magnitude of local velocity of agents - measure of kinetic energy of fluid motion
  return (vel0 + counter_curent*vel_c*U2/(Vs + U2))*exp(J3*cos(phik)-J3);//self propulsion as function of internal state (phase) and velocity of background flow
 }

DOUB freq_activity(DOUB &tr, DOUB &osc_activity, DOUB &freq_c)
 {
  return 1.0 + osc_activity*freq_c*tr/(freq_c + tr);
 }

DOUB dir_diff(DOUB &tr, DOUB &dif_activity, DOUB &freq_c)
 {
  return 1.0 + dif_activity*freq_c*tr/(freq_c + tr);
 }

DOUB Season(DOUB &t, DOUB &P, DOUB &switcher)
 {
#ifdef seasons
  return (1.0-switcher) + switcher*0.5*(1.0+tanh(0.25*(t-P)))*0.5*(1.0+tanh(10.0*sin(2.0*pi*t/P)));//ramps up seasons at P and lets oscillate at frequency 1/P
#else
  return 1.0;
#endif
 }

DOUB ApowB(DOUB &A, DOUB &B)
 {
  return exp(B*log(A));
 }

DOUB Agent_attraction_neafieldX(vector<DOUB> &y, int k, int l, int N, int n, DOUB &irad, DOUB &J, DOUB &G)
 {
  DOUB del(atan2((y[2*l+1] - y[2*k+1]),(y[2*l] - y[2*k]))), r(sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1])));//relative angle between the two agents
  return (y[2*l]-y[2*k])*ApowB(r, G)*(r/irad-1.0)*(Coupling_internal_state_Agent_attraction(y[(n-1)*N/n+l],J)*(cos(2.0*(y[(n-2)*N/n+l]-del))) + Coupling_internal_state_Agent_attraction(y[(n-1)*N/n+k],J)*(cos(2.0*(y[(n-2)*N/n+k]-del))))*0.5; // *Coupling_internal_state_Agent_attraction(y[(n-1)*N/n+l], J)*(cos(2.0*(y[(n-2)*N/n+l]-del)));
  //mimics simple suction forces X, depending on the relative orientation of agent j and k and depending on the relative state (assumed is that there can be attraction/repulsion to the side when phase difference of bells is pi/zero)
 }

DOUB Agent_attraction_neafieldY(vector<DOUB> &y, int k, int l, int N, int n, DOUB &irad, DOUB &J, DOUB &G)
 {
  DOUB del(atan2((y[2*l+1] - y[2*k+1]),(y[2*l] - y[2*k]))), r(sqrt((y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1])));//relative angle between the two agents
  return (y[2*l+1]-y[2*k+1])*ApowB(r, G)*(r/irad-1.0)*(Coupling_internal_state_Agent_attraction(y[(n-1)*N/n+l],J)*(cos(2.0*(y[(n-2)*N/n+l]-del))) + Coupling_internal_state_Agent_attraction(y[(n-1)*N/n+k],J)*(cos(2.0*(y[(n-2)*N/n+k]-del))));
  //mimics simple suction forces Y
 }

DOUB Int_wall(DOUB &is_boundary0, DOUB &is_boundary1, DOUB &is_boundary2, DOUB &is_boundary3, DOUB &theta)
 {
  //NOTE: The angle maps the inward normal directions (into the fluid domain) 
  //      depending on the four digit binary number that signifies the boundary status 
  //      of corners of the bounding box. Important is that convex and concave corners have the same outcome 
  //      The magnitude of the angular coupling is mapped to unity in all cases using an other boolean function
  //      The couplin constant is such that an agent at the wall turns pi radians per second
  DOUB del(0.0), mag(0.0);
  del = (is_boundary0)*0.25*pi - DOUB(is_boundary1)*0.25*pi + DOUB(is_boundary2)*1.25*pi - DOUB(is_boundary3)*1.25*pi + DOUB(is_boundary0)*DOUB(is_boundary3)*1.5*pi - DOUB(is_boundary1)*DOUB(is_boundary2)*1.5*pi - DOUB(is_boundary2)*DOUB(is_boundary3)*pi;
  mag = (is_boundary0 + is_boundary1 + is_boundary2 + is_boundary3 + is_boundary0*(is_boundary1*(-1+is_boundary2+is_boundary3)+is_boundary2*(-1+is_boundary3)-is_boundary3) - is_boundary1*(is_boundary2+is_boundary3) - is_boundary2*is_boundary3 + is_boundary1*is_boundary2*is_boundary3);
  //cout << "| " << is_boundary0 << ", " << is_boundary1 << ", " << is_boundary2 << ", " << is_boundary3 << " | " << mag << " " << del <<"\n";
  return sin(del-theta)*mag*pi;
 }

void dgl3(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars)//#dgl for the ghost particles
 {
  //NOTE: time update of wakes/ghosts.
  //      considers advection, passive rotation and active drifting.
  for(int k=0; k<pars.N3;k++)
   {
    //### update position ###//
    dydx[2*k] = pars.UXA3[k] + pars.absvel3[k]*cos(y[2*pars.N3+k]);   // xpositions
    dydx[2*k+1] = pars.UYA3[k] + pars.absvel3[k]*sin(y[2*pars.N3+k]); // ypositions
    dydx[2*pars.N3+k] = pars.VorticityA3[k];                          // Vortex pair rotates at the rate given by the rotation of the background flow at the position of the ghost particles
   }
 }

void dgl3_omp(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars)//#dgl for the ghost particles
 {
  //NOTE: time update of wakes/ghosts.
  //      considers advection, passive rotation and active drifting.
  //      OMP version when number of wakes gets very large
  {
   #pragma omp parallel for // shared(y, pars)
   for(int k=0; k<pars.N3;k++)
    {
     //### update position ###//
     dydx[2*k] = pars.UXA3[k] + pars.absvel3[k]*cos(y[2*pars.N3+k]);   // xpositions
     dydx[2*k+1] = pars.UYA3[k] + pars.absvel3[k]*sin(y[2*pars.N3+k]); // ypositions
     dydx[2*pars.N3+k] = pars.VorticityA3[k];                          // NOTE: + = rotation towards the direction of the flow; -= rotation against or into the flow. This might happen due to differential rotation of a vortex pair
     //cout << dydx[2*pars.N3+k] << "\n";
    }
   #pragma omp barrier   
  }
 }

void dgl2(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars)//#dgl for the passive tracers
 {
  //NOTE: time update of passive tracers.
  //      Considers advection and wall effects
  {
  //### update interactions ###//
  #pragma omp parallel for //shared(y, pars)
  for(int k=0; k<N/n;k++)
   {
    DOUB repulsivex(0.0), repulsivey(0.0), dist(0.0), nosc_radius(0.0),  wallx(0.0), wally(0.0);
    repulsivex=0.0;
    repulsivey=0.0;
#ifndef Jelly_add 
    soft_boundary(wallx, wally, y[2*k],y[2*k+1], pars, k);  // confinement by complex coastal boundary
#endif
    //soft_boundary(wallx,wally,y[2*k],y[2*k+1],pars,k);
    //### update position ###//
    dydx[2*k] = pars.UXA2[k] + wallx;   //xpositions
    dydx[2*k+1] = pars.UYA2[k] + wally; //ypositions
   }
  #pragma omp barrier
  }//off pragma
 }

//NOTE: OLD CODE BEFORE MORE PARALLELIZATION
/*!!!!void dgl(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars)//#dgl for the active particles (jellyfish)
 {
  //NOTE: time update of active agents.
  //      - iterates through all agents
  //        - For each agent checks if other active agents are near, calculate respective interaction terms
  //          This step separates the loops into twoo parts to avoid self-coupling
  //        - If no agent is found to be near, nosc_radius=1
  //      - iterates through all ghosts present.
  //        - If vortex intensity is above threshold, the agents couple otherwise not
  //        - If no wake is found to be near, nosc_ghost=1
  //      - Sets boundary forces to contain agents
  //      - Update of all variables (x,y,O-U noise,A,theta,phi)
  {
  //### update interactions ###//
  //DOUB repulsivex(0.0), repulsivey(0.0), interaction_direction(0.0), dist(0.0), dist2(0.0), nosc_radius(0.0), attractivex(0.0), attractivey(0.0), internalstate_interaction(0.0), wallx(0.0), wally(0.0), delTr(0.0);
  //DOUB interaction_direction_farfield(0.0), internalstate_interaction_farfield(0.0), nosc_ghost(0.0), vortex_intensity_farfield(0.0), delA(0.0), absVel(0.0);
  #pragma omp parallel for //private (repulsivex, repulsivey, interaction_direction, dist, dist2, nosc_radius, attractivex, attractivey, internalstate_interaction, wallx, wally, delTr, interaction_direction_farfield, internalstate_interaction_farfield, nosc_ghost, vortex_intensity_farfield, delA, absVel)
  for(int k=0; k<N/n;k++)
   {
    DOUB repulsivex(0.0), repulsivey(0.0), interaction_direction(0.0), dist(0.0), dist2(0.0), nosc_radius(0.0), attractivex(0.0), attractivey(0.0), internalstate_interaction(0.0), wallx(0.0), wally(0.0), delTr(0.0);
    DOUB interaction_direction_farfield(0.0), internalstate_interaction_farfield(0.0), nosc_ghost(0.0), vortex_intensity_farfield(0.0), delA(0.0), absVel(0.0), indicator(0.0), Coupdir(0.0);
   /* repulsivex = 0.0;
    repulsivey = 0.0;
    interaction_direction = 0.0;
    dist = 0.0;
    dist2 = 0.0;
    nosc_radius = 0.0;
    attractivex = 0.0;
    attractivey = 0.0;
    internalstate_interaction = 0.0;
    delTr = 0.0;
    interaction_direction_farfield = 0.0;
    internalstate_interaction_farfield = 0.0;
    nosc_ghost = 0.0; 
    vortex_intensity_farfield = 0.0;
    delA=0.0;
    absVel=0.0;//*/
    
    //### near field coupling of agents ###//
/*!!!!    for(int l=0; l<k; l++) //l<k
     {
      dist2 = (y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]); //square Distance between two agents
      dist = sqrt(dist2);
    //  delA = atan2(y[2*l+1]-y[2*k+1], y[2*l]-y[2*k]);
    //  Coupdir = Coupling_direction(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2);
    //  indicator= 0.5*((pars.irad-dist)/abs(pars.irad-dist) + 1.0); //avoids if statement 
      if(dist<pars.irad)
       {//indicator*
	delA = atan2(y[2*l+1]-y[2*k+1], y[2*l]-y[2*k]);
        Coupdir = Coupling_direction(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2);
        //interaction_direction += y[(n-2)*N/n+l];//interaction of direction angle -> Viscek model
	interaction_direction += Int_Direction_coupling(y[(n-2)*N/n+l], y[(n-2)*N/n+k], dist, pars.eps2, pars.dir_lag)*Coupdir; //interaction of direction angle -> Kuramoto like // assumption is that transmission of information has no time delay
	internalstate_interaction += Int_Phase_coupling(y[(n-1)*N/n+l], y[(n-1)*N/n+k], dist, pars.eps, pars.phase_lag)*Coupdir; //interaction of internal states
	attractivex += (Agent_attractionX(y[2*k], y[2*l], dist, pars.agent_atr) + Agent_attraction_neafieldX(y, k, l, N, n, pars.irad, pars.J))*Coupdir;
        attractivey += (Agent_attractionY(y[2*k+1], y[2*l+1], dist2, pars.agent_atr) + Agent_attraction_neafieldY(y, k, l, N, n, pars.irad, pars.J))*Coupdir;
        repulsivex += Agent_repulsionX(y[2*k], y[2*l], dist, pars.agent_rep)*Coupdir;
        repulsivey += Agent_repulsionY(y[2*k+1], y[2*l+1], dist2, pars.agent_rep)*Coupdir;
  	nosc_radius +=1.0;
       }
     }
    for(int l=k+1; l<N/n; l++)//l>k
     { 
      dist2 = (y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]);
      dist = sqrt(dist2);
    //  delA = atan2(y[2*l+1]-y[2*k+1], y[2*l]-y[2*k]);                                         //relative angle between the two agents
    //  Coupdir = Coupling_direction(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2);
    //  indicator= 0.5*((pars.irad-dist)/abs(pars.irad-dist) + 1.0); //avoids if statement      
      if(dist<pars.irad)
       {//indicator*
        delA = atan2(y[2*l+1]-y[2*k+1], y[2*l]-y[2*k]);                                         //relative angle between the two agents
        Coupdir = Coupling_direction(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2);
        //interaction_direction += y[(n-2)*N/n+l];//interaction of direction angle -> Viscek model
        interaction_direction += Int_Direction_coupling(y[(n-2)*N/n+l], y[(n-2)*N/n+k], dist, pars.eps2, pars.dir_lag)*Coupdir; //interaction of direction angle -> Kuramoto like
        internalstate_interaction += Int_Phase_coupling(y[(n-1)*N/n+l], y[(n-1)*N/n+k], dist, pars.eps, pars.phase_lag)*Coupdir; //interaction of internal states
        attractivex += (Agent_attractionX(y[2*k], y[2*l], dist, pars.agent_atr) + Agent_attraction_neafieldX(y, k, l, N, n, pars.irad, pars.J))*Coupdir;
        attractivey += (Agent_attractionY(y[2*k+1], y[2*l+1], dist2, pars.agent_atr) + Agent_attraction_neafieldY(y, k, l, N, n, pars.irad, pars.J))*Coupdir;
        repulsivex += Agent_repulsionX(y[2*k], y[2*l], dist, pars.agent_rep)*Coupdir;
        repulsivey += Agent_repulsionY(y[2*k+1], y[2*l+1], dist2, pars.agent_rep)*Coupdir;
  	nosc_radius +=1.0;
       }
     }
    if(nosc_radius==0.0){nosc_radius=1.0;}
    
    //### far field communication due to wake traveling ###//
    for(int l=0; l<pars.N3; l++)
     {
      vortex_intensity_farfield = Coupling_transmission(pars.Tgstart[l], pars.x3t[l], pars.theta3t[l], pars.y3t[l] , y[2*k] , y[2*k+1], t, pars.irad, pars.mu2); //Amplitude of vortex ring at position of an agent
      if((vortex_intensity_farfield>=pars.omegmin) && (l!= pars.start_agent[l]))
       {
        delA = atan2(pars.y3t[l]-y[2*k+1],pars.x3t[l]-y[2*k]);                                                                                                     //relative angle between agent and vortex
        internalstate_interaction_farfield += Int_Phase_coupling_farfield(y[(n-1)*N/n+k], pars.eps, pars.phase_lag_far)*Coupling_direction_farfield(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2)*tanh(vortex_intensity_farfield);
        interaction_direction_farfield += Int_Direction_coupling_farfield(pars.theta3t[l], y[(n-2)*N/n+k], pars.eps2, pars.dir_lag_far)*Coupling_direction_farfield(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2)*tanh(vortex_intensity_farfield);
        nosc_ghost += 1.0; // counter of ghosts (wakes) in a vicinity that have at least an ambient magnitude of omegmin
       }
     }
    //nosc_ghost=DOUB(pars.N3); // use the number of vorteces present for normalization. Reason is that the ambient field amplitude can be extremely small amplifying the contribution too much
    if(nosc_ghost==0.0){nosc_ghost=1.0;}
    
    soft_boundary(wallx,wally,y[2*k],y[2*k+1],pars,k);            // avoiding excape from overall domain
    soft_boundary_coast(wallx, wally, y[2*k],y[2*k+1], pars, k);  // confinement by complex coastal boundary
    delTr = atan2(pars.TrgradYA[k],pars.TrgradXA[k]);             //angle of tracer gradient
    absVel = Velocity(pars.UXA[k], pars.UYA[k], y[(n-3)*N/n+k], y[(n-1)*N/n+k], pars.counter_current_act, pars.vel0, pars.counter_curent, pars.vel_c, pars.Vs, pars.J3); //magnitude of swimming velocity of jellyfish
    //### update position ###//
    dydx[2*k] = cos(y[(n-2)*N/n+k])*absVel + pars.UXA[k] + repulsivex/nosc_radius + attractivex/nosc_radius + wallx;   //xpositions
    dydx[2*k+1] = sin(y[(n-2)*N/n+k])*absVel + pars.UYA[k] + repulsivey/nosc_radius + attractivey/nosc_radius + wally; //ypositions
    //### Ornstein-Uhlenbeck noise ###//
    dydx[(n-4)*N/n+k] = pars.dirdamp*y[(n-4)*N/n+k];
    //### update memory of agents ###//
    dydx[(n-3)*N/n+k] = sqrt(pars.TrgradXA[k]*pars.TrgradXA[k] + pars.TrgradYA[k]*pars.TrgradYA[k])*0.5*(abs(cos(y[(n-2)*N/n+k]-delTr)) +cos(y[(n-2)*N/n+k]-delTr)) + pars.mem_damp*y[(n-3)*N/n+k];
    //### update direction angle ###//
    dydx[(n-2)*N/n+k] = y[(n-4)*N/n+k] + interaction_direction/nosc_radius + interaction_direction_farfield/nosc_ghost + Int_Direction_coupling_velocity(pars.UYA[k], pars.UXA[k], y[(n-2)*N/n+k], pars.eps3)*(Tracer_Direction_switch(pars.intensity[k], pars.tr_dir2, pars.tr_steepnes) + 1.0-pars.tr_dir2)*(Vel_Direction_switch(pars.UXA[k], pars.UYA[k], pars.vel_dir)*pars.vel_dir2 + 1.0 - pars.vel_dir2)                           + Int_Direction_coupling_vorticity(pars.Vorticity_gradYA[k], pars.Vorticity_gradXA[k], y[(n-2)*N/n+k], pars.eps4)*(Act_Direction_switch(y[(n-3)*N/n+k], pars.tr_dir1, pars.tr_dir_steepnes) + 1.0-pars.tr_dir1)*(Vort_Direction_switch(pars.VorticityA[k], pars.vort_dir)*pars.vort_dir2 + 1.0 - pars.vort_dir2)                                                + Int_Direction_coupling_tracer(pars.TrgradYA[k], pars.TrgradXA[k], y[(n-2)*N/n+k], y[(n-3)*N/n+k], pars.eps5, pars.tr_dir3, pars.tr_dir_steepnes)*(Activity_Direction_step(y[(n-3)*N/n+k], pars.tr_dir3, pars.act_steepnes_2) + 1.0 - pars.tr_dir3)                                                + Int_Direction_coupling_tracer_Sig(pars.Tr_Sig_gradYA[k], pars.Tr_Sig_gradXA[k], y[(n-2)*N/n+k], pars.eps6, pars.tr_dir4, pars.tr_dir_steepnes2)*Season(t, pars.thresh, pars.seasons_switch)*(Signal_Direction_switch(pars.Tr_Sig_gradXA[k], pars.Tr_Sig_gradYA[k], pars.sig_dir)*pars.sig_dir2 + 1.0 - pars.sig_dir2);
    //### update internal state ###//
    dydx[(n-1)*N/n+k] = pars.freq[k]*freq_activity(y[(n-3)*N/n+k], pars.osc_activity, pars.freq_c) + internalstate_interaction/nosc_radius + internalstate_interaction_farfield/nosc_ghost;
    //cout << "memory " << k << "= " << freq_tracer(y[(n-3)*N/n+k],pars) << "\n";
   }
  #pragma omp barrier
  }//off pragma
 }//*/

 
void dgl(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars)//#dgl for the active particles (jellyfish)
 {
  //NOTE: time update of active agents.
  //      - iterates through all agents
  //        - For each agent checks if other active agents are near, calculate respective interaction terms
  //          This step separates the loops into twoo parts to avoid self-coupling
  //        - If no agent is found to be near, nosc_radius=1
  //      - iterates through all ghosts present.
  //        - If vortex intensity is above threshold, the agents couple otherwise not
  //        - If no wake is found to be near, nosc_ghost=1
  //      - Sets boundary forces to contain agents
  //      - Update of all variables (x,y,O-U noise,A,theta,phi)

  //### update interactions ###//
  //DOUB repulsivex(0.0), repulsivey(0.0), interaction_direction(0.0), dist(0.0), dist2(0.0), nosc_radius(0.0), attractivex(0.0), attractivey(0.0), internalstate_interaction(0.0), wallx(0.0), wally(0.0), delTr(0.0);
  //DOUB interaction_direction_farfield(0.0), internalstate_interaction_farfield(0.0), nosc_ghost(0.0), vortex_intensity_farfield(0.0), delA(0.0), absVel(0.0);
  
  //Vectors to make code faster
  vector<DOUB> repulsivex(N/n);
  vector<DOUB> repulsivey(N/n);
  vector<DOUB> interaction_direction(N/n);
  vector<DOUB> interaction_direction_farfield(N/n);
  vector<DOUB> nosc_radius(N/n);
  vector<DOUB> attractivex(N/n);
  vector<DOUB> attractivey(N/n);
  vector<DOUB> internalstate_interaction(N/n);
  vector<DOUB> internalstate_interaction_farfield(N/n);
  vector<DOUB> nosc_ghost(N/n);
   
  #pragma omp parallel for // collapse(2) //private (repulsivex, repulsivey, interaction_direction, dist, dist2, nosc_radius, attractivex, attractivey, internalstate_interaction, wallx, wally, delTr, interaction_direction_farfield, internalstate_interaction_farfield, nosc_ghost, vortex_intensity_farfield, delA, absVel)
  //### near field coupling of agents ###//
  for(int k=0; k<N/n;k++)//all pairs 
   {
    interaction_direction[k] = 0.0;
    internalstate_interaction[k] = 0.0;
    for(int l=0; l<N/n; l++) //l<k
     {
      DOUB dist(0.0), dist2(0.0), delA(0.0), Coupdir(0.0);
      dist2 = (y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]); //square Distance between two agents
      dist = sqrt(dist2);
      if((dist<pars.irad) & (l!=k))
       {//indicator*
        delA = atan2(y[2*l+1]-y[2*k+1], y[2*l]-y[2*k]);
        Coupdir = Coupling_direction(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2);
        interaction_direction[k] +=  Int_Direction_coupling(y[(n-2)*N/n+l], y[(n-2)*N/n+k], dist, pars.eps2, pars.dir_lag, pars.irad)*Coupdir; //interaction of direction angle -> Kuramoto like // assumption is that transmission of information has no time delay
        internalstate_interaction[k] += Int_Phase_coupling(y[(n-1)*N/n+l], y[(n-1)*N/n+k], dist, pars.eps, pars.phase_lag, pars.irad)*Coupdir; //interaction of internal states
#ifdef near_field_interaction_static
#ifndef swarmalator
        attractivex[k] += (Agent_attractionX(y[2*k], y[2*l], dist, pars.agent_atr))*Coupdir;
        attractivey[k] += (Agent_attractionY(y[2*k+1], y[2*l+1], dist, pars.agent_atr))*Coupdir;
#else
        attractivex[k] += (Agent_attractionX(y[2*k], y[2*l], dist, pars.agent_atr)*(1.0+cos(y[(n-1)*N/n+k]-y[(n-1)*N/n+l])))*Coupdir;
        attractivey[k] += (Agent_attractionY(y[2*k+1], y[2*l+1], dist, pars.agent_atr)*(1.0+cos(y[(n-1)*N/n+k]-y[(n-1)*N/n+l])))*Coupdir;
#endif
        repulsivex[k] += Agent_repulsionX(y[2*k], y[2*l], dist2, pars.agent_rep)*Coupdir;
        repulsivey[k] += Agent_repulsionY(y[2*k+1], y[2*l+1], dist2, pars.agent_rep)*Coupdir;
#endif
#ifdef near_field_interaction_oscillating
        attractivex[k] += (Agent_attraction_neafieldX(y, k, l, N, n, pars.irad, pars.J, pars.G))*Coupdir;
        attractivey[k] += (Agent_attraction_neafieldY(y, k, l, N, n, pars.irad, pars.J, pars.G))*Coupdir;
#endif
  	nosc_radius[k] +=1.0;
       }
     }
   }
//  cout << repulsivex[0] << " " << repulsivey[0] << "\n";
  #pragma omp barrier//*/
    
/*  #pragma omp parallel for collapse(2)
  for(int k=0; k<N/n;k++)//all pairs where l>k
   {
    for(int l=0; l<N/n; l++)//l>k
     { 
      DOUB dist(0.0), dist2(0.0), delA(0.0), Coupdir(0.0);
      dist2 = (y[2*k]-y[2*l])*(y[2*k]-y[2*l]) + (y[2*k+1]-y[2*l+1])*(y[2*k+1]-y[2*l+1]);
      dist = sqrt(dist2);
    //  delA = atan2(y[2*l+1]-y[2*k+1], y[2*l]-y[2*k]);                                         //relative angle between the two agents
    //  Coupdir = Coupling_direction(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2);
    //  indicator= 0.5*((pars.irad-dist)/abs(pars.irad-dist) + 1.0); //avoids if statement      
      if((dist<pars.irad) & (l!=k))
       {//indicator*
        delA = atan2(y[2*l+1]-y[2*k+1], y[2*l]-y[2*k]);                                         //relative angle between the two agents
        Coupdir = Coupling_direction(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2);
        //interaction_direction += y[(n-2)*N/n+l];//interaction of direction angle -> Viscek model
        interaction_direction[k] += Int_Direction_coupling(y[(n-2)*N/n+l], y[(n-2)*N/n+k], dist, pars.eps2, pars.dir_lag)*Coupdir; //interaction of direction angle -> Kuramoto like
        internalstate_interaction[k] += Int_Phase_coupling(y[(n-1)*N/n+l], y[(n-1)*N/n+k], dist, pars.eps, pars.phase_lag)*Coupdir; //interaction of internal states
        attractivex[k] += (Agent_attractionX(y[2*k], y[2*l], dist, pars.agent_atr) + Agent_attraction_neafieldX(y, k, l, N, n, pars.irad, pars.J))*Coupdir;
        attractivey[k] += (Agent_attractionY(y[2*k+1], y[2*l+1], dist2, pars.agent_atr) + Agent_attraction_neafieldY(y, k, l, N, n, pars.irad, pars.J))*Coupdir;
        repulsivex[k] += Agent_repulsionX(y[2*k], y[2*l], dist, pars.agent_rep)*Coupdir;
        repulsivey[k] += Agent_repulsionY(y[2*k+1], y[2*l+1], dist2, pars.agent_rep)*Coupdir;
  	nosc_radius[k] +=1.0;
       }
     }
   }
  #pragma omp barrier //*/
  
#ifdef wake_interaction
  #pragma omp parallel for //collapse(2)
  //### far field communication due to wake traveling ###//
  for(int k=0; k<N/n;k++)
   {
    for(int l=0; l<pars.N3; l++)
     {
      DOUB vortex_intensity_farfield(0.0), delA(0.0);
      vortex_intensity_farfield = Coupling_transmission(pars.Tgstart[l], pars.x3t[l], pars.theta3t[l], pars.y3t[l] , y[2*k] , y[2*k+1], t, pars.irad, pars.mu2); //Amplitude of vortex ring at position of an agent
      if((vortex_intensity_farfield>=pars.omegmin) && (pars.agent_name[k] != pars.start_agent[l]))
       {
        delA = atan2(pars.y3t[l]-y[2*k+1],pars.x3t[l]-y[2*k]);                                                                                                     //relative angle between agent and vortex
        internalstate_interaction_farfield[k] += Int_Phase_coupling_farfield(y[(n-1)*N/n+k], pars.eps, pars.phase_lag_far)*Coupling_direction_farfield(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2)*tanh(vortex_intensity_farfield);
        interaction_direction_farfield[k] += Int_Direction_coupling_farfield(pars.theta3t[l], y[(n-2)*N/n+k], pars.eps2, pars.dir_lag_far)*Coupling_direction_farfield(delA, y[(n-2)*N/n+k], pars.agent_direction, pars.J2)*tanh(vortex_intensity_farfield);
        nosc_ghost[k] += 1.0; // counter of ghosts (wakes) in a vicinity that have at least an ambient magnitude of omegmin
       }
     }
   }
  #pragma omp barrier//*/
#endif

  #pragma omp parallel for 
  //### Update derivatives ###//
  for(int k=0; k<N/n;k++)
   {
    DOUB wallx(0.0), wally(0.0), delTr(0.0), absVel(0.0);
   
    if(nosc_ghost[k]==0.0){nosc_ghost[k]=1.0;}
    if(nosc_radius[k]==0.0){nosc_radius[k]=1.0;}
    
    //soft_boundary(wallx,wally,y[2*k],y[2*k+1],pars,k);            // avoiding excape from overall domain
#ifndef Jelly_add 
//#ifdef complex_boundary 
//    soft_boundary_coast(wallxC, wallyC, y[2*k],y[2*k+1], pars, k);  // confinement by complex coastal boundary
//#endif
    soft_boundary(wallx, wally, y[2*k],y[2*k+1], pars, k);  // confinement by complex coastal boundary
#endif
    delTr = atan2(pars.TrgradYA[k],pars.TrgradXA[k]);             //angle of tracer gradient
    absVel = Velocity(pars.UXA[k], pars.UYA[k], y[(n-3)*N/n+k], y[(n-1)*N/n+k], pars.counter_current_act, pars.vel0, pars.counter_curent, pars.vel_c, pars.Vs, pars.J3); //magnitude of swimming velocity of jellyfish
    //### update position ###//
    dydx[2*k] =  cos(y[(n-2)*N/n+k])*absVel + pars.UXA[k] +  (repulsivex[k] + attractivex[k])/(nosc_radius[k]) + wallx;   //xpositions
    dydx[2*k+1] = sin(y[(n-2)*N/n+k])*absVel + pars.UYA[k] +  (repulsivey[k] + attractivey[k])/(nosc_radius[k]) + wally; //ypositions
#ifdef buoyancy_jelly
    dydx[2*k+1] += pars.rho_jelly; //buoyancy only in the y-components
#endif
    //### Ornstein-Uhlenbeck noise ###//
    dydx[(n-4)*N/n+k] = pars.dirdamp*y[(n-4)*N/n+k];
    //### update memory of agents ###//
    dydx[(n-3)*N/n+k] = sqrt(pars.TrgradXA[k]*pars.TrgradXA[k] + pars.TrgradYA[k]*pars.TrgradYA[k])*0.5*(abs(cos(y[(n-2)*N/n+k]-delTr)) +cos(y[(n-2)*N/n+k]-delTr)) + pars.mem_damp*y[(n-3)*N/n+k]; //*(0.5+0.5*pars.intensity[k]/(0.01+pars.intensity[k]))*y[(n-3)*N/n+k];
    //### update direction angle ###//
    dydx[(n-2)*N/n+k] = y[(n-4)*N/n+k] + interaction_direction[k]/nosc_radius[k] + interaction_direction_farfield[k]/nosc_ghost[k] + Int_Direction_coupling_velocity(pars.UYA[k], pars.UXA[k], y[(n-2)*N/n+k], pars.eps3)*(Tracer_Direction_switch(pars.intensity[k], pars.tr_dir2, pars.tr_steepnes) + 1.0-pars.tr_dir2)*(Vel_Direction_switch(pars.UXA[k], pars.UYA[k], pars.vel_dir)*pars.vel_dir2 + 1.0 - pars.vel_dir2)                           + Int_Direction_coupling_vorticity(pars.Vorticity_gradYA[k], pars.Vorticity_gradXA[k], y[(n-2)*N/n+k], pars.eps4)*(Act_Direction_switch(y[(n-3)*N/n+k], pars.tr_dir1, pars.tr_dir_steepnes) + 1.0-pars.tr_dir1)*(Vort_Direction_switch(pars.VorticityA[k], pars.vort_dir)*pars.vort_dir2 + 1.0 - pars.vort_dir2)                                                + Int_Direction_coupling_tracer(pars.TrgradYA[k], pars.TrgradXA[k], y[(n-2)*N/n+k], y[(n-3)*N/n+k], pars.eps5, pars.tr_dir3, pars.tr_dir_steepnes)*(Activity_Direction_step(y[(n-3)*N/n+k], pars.tr_dir3, pars.act_steepnes_2) + 1.0 - pars.tr_dir3)                                                + Int_Direction_coupling_tracer_Sig(pars.Tr_Sig_gradYA[k], pars.Tr_Sig_gradXA[k], y[(n-2)*N/n+k], pars.eps6, pars.tr_dir4, pars.tr_dir_steepnes2)*Season(t, pars.thresh, pars.seasons_switch)*(Signal_Direction_switch(pars.intensity_sig[k], pars.sig_dir, pars.tr_dir_steepnes2)*pars.sig_dir2 + 1.0 - pars.sig_dir2) + Int_wall(pars.is_boundary[k][0], pars.is_boundary[k][1], pars.is_boundary[k][2], pars.is_boundary[k][3], y[(n-2)*N/n+k]);
    //### update internal state ###//
    dydx[(n-1)*N/n+k] = pars.freq[k]*freq_activity(y[(n-3)*N/n+k], pars.osc_activity, pars.freq_c) + internalstate_interaction[k]/nosc_radius[k] + internalstate_interaction_farfield[k]/nosc_ghost[k];
    //cout << "memory " << k << "= " << freq_tracer(y[(n-3)*N/n+k],pars) << "\n";
    //cout << Int_Direction_coupling_tracer(pars.TrgradYA[k], pars.TrgradXA[k], y[(n-2)*N/n+k], y[(n-3)*N/n+k], pars.eps5, pars.tr_dir3, pars.tr_dir_steepnes)*(Activity_Direction_step(y[(n-3)*N/n+k], pars.tr_dir3, pars.act_steepnes_2) + 1.0 - pars.tr_dir3) << "\n";
   }
  #pragma omp barrier //*/
 // for(int k=0; k<N/n;k++)
 //  {
 //   cout << k << " " << dydx[2*k] << " " << dydx[2*k+1] << " " << dydx[(n-4)*N/n + k] << " " << dydx[(n-3)*N/n + k] << " " << dydx[(n-2)*N/n + k]  << " " << dydx[(n-1)*N/n + k] << "\n";
 //  }
 }

void rk1(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB &ddt, params &pars)
 {
  //NOTE: Euler method
  vector<DOUB> dydx1(N);
  cout << "start rk1 \n";
  for(int j=0; j<Nsim; j++)
   {
    (*derives)(y, dydx1,N,n,t, pars);
    //cout << "after function \n";
    for(int i=0; i<N; i++)
     {
      y[i] = y[i] + ddt*dydx1[i];
     }
    t = t + ddt;
   }
  }

void rk4(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB dt, params &pars, vector<DOUB> &vel)
 {
  //NOTE: conventional RK-4 method, non stochastic. Due to data ordering the last update is done explicitly
  vector<DOUB> dydx1(N);
  vector<DOUB> dydx2(N);
  vector<DOUB> dydx3(N);
  vector<DOUB> dydx4(N);
  vector<DOUB> y1(N);
  vector<DOUB> y2(N);
  vector<DOUB> y3(N);
  DOUB xold(0.0), yold(0.0);
  for(int j=0; j<Nsim; j++)
   {
    (*derives)(y, dydx1,N,n,t,pars);//k1
    for(int i=0; i<N; i++)
     {
      y1[i] = y[i] + 0.5*dt*dydx1[i];//for k2
     }
    t = t + 0.5*dt;
    (*derives)(y1,dydx2,N,n,t,pars);//k2
    for(int i=0; i<N; i++)
     {
      y2[i] = y[i] + 0.5*dt*dydx2[i] ;//for k3
     }
    (*derives)(y2,dydx3,N,n,t,pars);
    for(int i=0; i<N; i++)
     {
      y3[i] = y[i] + dt*dydx3[i];
     }
    t = t + 0.5*dt;
    (*derives)(y3,dydx4,N,n,t,pars);
    for(int i=0; i<N; i++)//spatial components
     {
      y[i] = y[i] + ((dt/6.0)*dydx1[i] + (dt/3.0)*dydx2[i] + (dt/3.0)*dydx3[i] + (dt/6.0)*dydx4[i]);
      vel[i] = ((1.0/6.0)*dydx1[i] + (1.0/3.0)*dydx2[i] + (1.0/3.0)*dydx3[i] + (1.0/6.0)*dydx4[i]);
    //  xold=y[2*i];
    //  yold=y[2*i+1];
    //  y[2*i] = y[2*i] + ((dt/6.0)*dydx1[2*i] + (dt/3.0)*dydx2[2*i] + (dt/3.0)*dydx3[2*i] + (dt/6.0)*dydx4[2*i]);//posx
    //  y[2*i+1] = y[2*i+1] + ((dt/6.0)*dydx1[2*i+1] + (dt/3.0)*dydx2[2*i+1] + (dt/3.0)*dydx3[2*i+1] + (dt/6.0)*dydx4[2*i+1]);//posy
    //  y[(n-2)*N/n+i] = y[(n-2)*N/n+i] + ((dt/6.0)*dydx1[(n-2)*N/n+i] + (dt/3.0)*dydx2[(n-2)*N/n+i] + (dt/3.0)*dydx3[(n-2)*N/n+i] + (dt/6.0)*dydx4[(n-2)*N/n+i]);//direction
    //  y[(n-1)*N/n+i] = y[(n-1)*N/n+i] + ((dt/6.0)*dydx1[(n-1)*N/n+i] + (dt/3.0)*dydx2[(n-1)*N/n+i] + (dt/3.0)*dydx3[(n-1)*N/n+i] + (dt/6.0)*dydx4[(n-1)*N/n+i]);//internal state
     }
   }
 }

void rk2_stoch(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB dt, params &pars, vector<DOUB> &noise, vector<DOUB> &vel)
 {
  //NOTE: Stochastic Heun method. Data ordering requires explicit writing of lines below. 
  //      - Noise is added in the second step, the diffusion constants can change over time if needed (see O-U noise)
  vector<DOUB> dydx1(N);
  vector<DOUB> dydx2(N);
  vector<DOUB> y1(N);
  DOUB xold(0.0), yold(0.0);
  for(int j=0; j<Nsim; j++)
   {
    //cout << "k1 \n";
    (*derives)(y, dydx1,N,n,t,pars);//k1
    #pragma omp parallel for
    //for(int i=0; i<N/n; i++)
    for(int i=0; i<N; i++)
     {
      y1[i] = y[i] + dt*dydx1[i] + sqrt(2.0*pars.D[i]*dt)*noise[i];//for k2
     }
    #pragma omp barrier  
    t = t + dt;
    //cout << "k2 \n";
    (*derives)(y1,dydx2,N,n,t,pars);//k2
  //  cout << "2nd step \n";
    #pragma omp parallel for 
//    for(int i=0; i<N/n; i++)//spatial components
    for(int i=0; i<N; i++)
     {
//      cout << "rk2 stoch" << i << " " << (dt/2.0)*(dydx1[i]+dydx2[i]) <<  "\n";
      y[i] = y[i] + (dt/2.0)*(dydx1[i]+dydx2[i]) + sqrt(2.0*pars.D[i]*dt)*noise[i];
      vel[i] = (1.0/2.0)*(dydx1[i]+dydx2[i]) + sqrt(2.0*pars.D[i]/dt)*noise[i];      
    //  cout << "rk2 stoch" << i << " noise= " << noise[2*i] << " " << noise[2*i+1] << " " << noise[(n-4)*N/n+i] << " " << noise[(n-3)*N/n+i] << " " << noise[(n-2)*N/n+i] << " " << noise[(n-1)*N/n+i] << " " << (dt/2.0)*(dydx1[2*i]+dydx2[2*i]) + sqrt(2.0*pars.D[2*i]*dt)*noise[2*i] << " " << (dt/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]) + sqrt(2.0*pars.D[2*i+1]*dt)*noise[2*i+1] << " " << (dt/2.0)*(dydx1[(n-4)*N/n+i]+dydx2[(n-4)*N/n+i]) + sqrt(2.0*pars.D[(n-4)*N/n+i]*dir_diff(y[(n-3)*N/n+i], pars.dif_activity, pars.freq_c)*dt)*noise[(n-4)*N/n+i] << " " << (dt/2.0)*(dydx1[(n-3)*N/n+i]+dydx2[(n-3)*N/n+i]) + sqrt(2.0*pars.D[(n-3)*N/n+i]*dt)*noise[(n-3)*N/n+i] << " " << (dt/2.0)*(dydx1[(n-2)*N/n+i]+dydx2[(n-2)*N/n+i]) + sqrt(2.0*pars.D[(n-2)*N/n+i]*dt)*noise[(n-2)*N/n+i] << " " << (dt/2.0)*(dydx1[(n-1)*N/n+i]+dydx2[(n-1)*N/n+i]) + sqrt(2.0*pars.D[(n-1)*N/n+i]*dt)*noise[(n-1)*N/n+i] <<  "\n";
    //  xold=y[2*i];
    //  yold=y[2*i+1];
    //  y[2*i] = y[2*i] + (dt/2.0)*(dydx1[2*i]+dydx2[2*i]) + sqrt(2.0*pars.D[2*i]*dt)*noise[2*i];//x
    //  y[2*i+1] = y[2*i+1] + (dt/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]) + sqrt(2.0*pars.D[2*i+1]*dt)*noise[2*i+1];//y
    //  y[(n-4)*N/n+i] = y[(n-4)*N/n+i] + (dt/2.0)*(dydx1[(n-4)*N/n+i]+dydx2[(n-4)*N/n+i]) + sqrt(2.0*pars.D[(n-4)*N/n+i]*dir_diff(y[(n-3)*N/n+i], pars.dif_activity, pars.freq_c)*dt)*noise[(n-4)*N/n+i];//n-4: O-U noise
    //  y[(n-3)*N/n+i] = y[(n-3)*N/n+i] + (dt/2.0)*(dydx1[(n-3)*N/n+i]+dydx2[(n-3)*N/n+i]) + sqrt(2.0*pars.D[(n-3)*N/n+i]*dt)*noise[(n-3)*N/n+i];//n-3: memory has no noise
    //  y[(n-2)*N/n+i] = y[(n-2)*N/n+i] + (dt/2.0)*(dydx1[(n-2)*N/n+i]+dydx2[(n-2)*N/n+i]) + sqrt(2.0*pars.D[(n-2)*N/n+i]*dt)*noise[(n-2)*N/n+i];//direction
    //  y[(n-1)*N/n+i] = y[(n-1)*N/n+i] + (dt/2.0)*(dydx1[(n-1)*N/n+i]+dydx2[(n-1)*N/n+i]) + sqrt(2.0*pars.D[(n-1)*N/n+i]*dt)*noise[(n-1)*N/n+i];//internal state
      //### keep velocities ###//
    //  vel[2*i] = (1.0/2.0)*(dydx1[2*i]+dydx2[2*i]) + sqrt(2.0*pars.D[2*i]/dt)*noise[2*i];
    //  vel[2*i+1] = (1.0/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]) + sqrt(2.0*pars.D[2*i+1]/dt)*noise[2*i+1];
     }
    #pragma omp barrier 
   }
 }

void rk2(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, int n, DOUB &t, params &pars), vector<DOUB> &y, int Nsim, DOUB &t, int N, int n, DOUB dt, params &pars, vector<DOUB> &vel)
 {
  //NOTE: Heun scheme, non-stochastic for passive agents
  vector<DOUB> dydx1(N);
  vector<DOUB> dydx2(N);
  vector<DOUB> y1(N);
  DOUB xold(0.0), yold(0.0);
  for(int j=0; j<Nsim; j++)
   {
    //cout << "k1 \n";
    (*derives)(y, dydx1,N,n,t,pars);//k1
    #pragma omp parallel for
    for(int i=0; i<N; i++)
     {
      y1[i] = y[i] + dt*dydx1[i];//for k2
     }
    #pragma omp barrier 
    t = t + dt;
    //cout << "k2 \n";
    (*derives)(y1,dydx2,N,n,t,pars);//k2
    #pragma omp parallel for
    //for(int i=0; i<N/n; i++)//spatial components
    for(int i=0; i<N; i++)//spatial components
     {
      //cout << "rk2 stoch" << i << " " << (dt/2.0)*(dydx1[i]+dydx2[i]) <<  "\n";
      y[i] = y[i] + (dt/2.0)*(dydx1[i]+dydx2[i]);//x
//      y[2*i] = y[2*i] + (dt/2.0)*(dydx1[2*i]+dydx2[2*i]);//x
//      y[2*i+1] = y[2*i+1] + (dt/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]);//x
      //### keep velocities ###//
      vel[i] = (1.0/2.0)*(dydx1[i]+dydx2[i]);
//      vel[2*i] = (1.0/2.0)*(dydx1[2*i]+dydx2[2*i]);
//      vel[2*i+1] = (1.0/2.0)*(dydx1[2*i+1]+dydx2[2*i+1]);
     }
    #pragma omp barrier 
   }
 }

void ERROR_OUT(Vectordata &Data, FDFS &FDF, int &N, params &pars, DOUB &t, DOUB &T2)
 {
  //NOTE: This function keeps track of certain error events as given below. 
  //      Then an event occurs, the simulation time is set to a large value such that the integrator stops.
  int stoper(0);
  for(int k=0;k<N;k++)
   {
//    for(int l=0; l<N; l++)
//     {
//      if((l!=k) && (sqrt((Data.pos[2*k]-Data.pos[2*l])*(Data.pos[2*k]-Data.pos[2*l])+(Data.pos[2*k+1]-Data.pos[2*l+1])*(Data.pos[2*k+1]-Data.pos[2*l+1]))<0.01))
//       {
//        //cout << "ERR1: close encounter " << k << ", " << l << " distance= " << sqrt((Data.pos[2*k]-Data.pos[2*l])*(Data.pos[2*k]-Data.pos[2*l])+(Data.pos[2*k+1]-Data.pos[2*l+1])*(Data.pos[2*k+1]-Data.pos[2*l+1])) << "\n";
//       }
//     }
    //if(Data.pos[2*k]<pars.xi){cout << " ERR2.1: agent outside domain x<xi for unit " << k << "\n"; stoper=1;}
    //if(Data.pos[2*k]>pars.xf){cout << " ERR2.2: agent outside domain x>xf for unit " << k << "\n"; stoper=1;}
    //if(Data.pos[2*k+1]<pars.yi){cout << " ERR2.3: agent outside domain y<yi for unit " << k << "\n"; stoper=1;}
    //if(Data.pos[2*k+1]>pars.yf){cout << " ERR2.4: agent outside domain y>yf for unit " << k << "\n"; stoper=1;}
    if(isnan(Data.pos[2*k])){cout << " ERR3.1: overflow x for unit " << k << "\n"; stoper=1;}
    if(isnan(Data.pos[2*k+1])){cout << " ERR3.2: overflow y for unit " << k << "\n"; stoper=1;}   
    if(isnan(Data.pos2[2*k])){cout << " ERR4.1: overflow x for passive unit " << k << "\n"; stoper=1;}
    if(isnan(Data.pos2[2*k+1])){cout << " ERR4.2: overflow y for passive unit " << k << "\n"; stoper=1;}
   }
  for(int k=0;k<pars.N3;k++)
   {
    if(isnan(Data.pos3[2*k])){cout << " ERR5.1: overflow x for ghost unit " << k << " at t= " << t << "\n"; stoper=1;}
    if(isnan(Data.pos3[2*k+1])){cout << " ERR5.2: overflow y for ghost unit " << k << " at t= " << t << "\n"; stoper=1;}   
   } 
  for(int k=0; k<FDF.UX.size(); k++)
   {
    if(isnan(FDF.UX[k])){cout << " ERR6.1: overflow UX fluid velocity at index " << k << "\n"; stoper=1;}
   }
  for(int k=0; k<FDF.UY.size(); k++)
   {
    if(isnan(FDF.UY[k])){cout << " ERR6.2: overflow UY fluid velocity at index " << k << "\n"; stoper=1;}
   }   
  for(int k=0; k<FDF.Pr.size(); k++)
   {
    if(isnan(FDF.Pr[k])){cout << " ERR7: overflow Pressure at index " << k << "\n"; stoper=1;}
   }   
  for(int k=0; k<FDF.intensity.size(); k++)
   {
    if(isnan(FDF.intensity[k])){cout << " ERR8: overflow food tracer at index " << k << "\n"; stoper=1;}
   }   
  for(int k=0; k<FDF.intensity_sig.size(); k++)
   {
    if(isnan(FDF.intensity_sig[k])){cout << " ERR8.1: overflow signaling tracer at index " << k << "\n"; stoper=1;}
   }   
  for(int k=0; k<FDF.intensity_sig_back_coupler.size(); k++)
   {
    if(isnan(FDF.intensity_sig_back_coupler[k])){cout << " ERR9: overflow coupler array signaling tracer at index " << k << "\n"; stoper=1;}
   }   
  for(int k=0; k<FDF.intensity_back_coupler.size(); k++)
   {
    if(isnan(FDF.intensity_back_coupler[k])){cout << " ERR9.1: overflow coupler array food tracer at index " << k << "\n"; stoper=1;}
   }   
  if(stoper==1){cout << "Error at simulation time t= " << t << "\n"; t += T2;}
 }

void Data_Print(DOUB &t, Vectordata &Data, int N, int n, params &pars, ofstream &fout, ofstream &fout3, vector<int> &Cols)
 {
  //NOTE: This function prints the information for the active and passive agents to a single file as given below.
  //cout << "print data agents \n";
  for(int k=0;k<N;k++)
   {
    fout << t << " " << Data.pos[2*k] << " " << Data.pos[2*k+1];
    //for(int l=2; l<=n-1;l++){fout << " " <<  Data.pos[l*N+k];} //iterate different domains of vector of data start directly after x,y positions
    if(Cols[0]==1){fout << " " <<  Data.pos[3*N+k];}
    if(Cols[1]==1){fout << " " <<  Data.pos[4*N+k];}
    if(Cols[2]==1){fout << " " <<  Data.pos[5*N+k];}
    if(Cols[3]==1){fout << " " << pars.intensity[k];}
    if(Cols[4]==1){fout << " " << pars.VorticityA[k];}
    if(Cols[5]==1){fout << " " << Data.vel[2*k];}
    if(Cols[6]==1){fout << " " << Data.vel[2*k+1];}
    if(Cols[7]==1){fout << " " << pars.UXA[k];}
    if(Cols[8]==1){fout << " " << pars.UYA[k];}
    if(Cols[9]==1){fout << " " << pars.TrgradXA[k];}
    if(Cols[10]==1){fout << " " << pars.TrgradYA[k];}
    if(Cols[11]==1){fout << " " << pars.Vorticity_gradXA[k];}
    if(Cols[12]==1){fout << " " << pars.Vorticity_gradYA[k];}
    if(Cols[13]==1){fout << " " << Data.pos2[2*k];}
    if(Cols[14]==1){fout << " " << Data.pos2[2*k+1];}
    if(Cols[15]==1){fout << " " << Data.vel2[2*k];}
    if(Cols[16]==1){fout << " " << Data.vel2[2*k+1];}
    if(Cols[17]==1){fout << " " << pars.UXA2[k];}
    if(Cols[18]==1){fout << " " << pars.UYA2[k];}
    if(Cols[19]==1){fout << " " << Data.pos[2*N+k];}
    if(Cols[20]==1){fout << " " << pars.intensity2[k];}
    if(Cols[21]==1){fout << " " << pars.VorticityA2[k];}
    if(Cols[22]==1){fout << " " << pars.Tr_Sig_gradXA[k];}
    if(Cols[23]==1){fout << " " << pars.Tr_Sig_gradYA[k];}
    if(Cols[24]==1){fout << " " << pars.LEVA[k];}
    if(Cols[25]==1){fout << " " << pars.LEV_gradXA[k];}
    if(Cols[26]==1){fout << " " << pars.LEV_gradYA[k];}
    if(Cols[27]==1){fout << " " << pars.agent_name[k];}
    if(Cols[28]==1){fout << " " << pars.intensity_sig[k];}
    if(Cols[29]==1){fout << " " << pars.is_boundary[k][0];}
    if(Cols[30]==1){fout << " " << pars.is_boundary[k][1];}
    if(Cols[31]==1){fout << " " << pars.is_boundary[k][2];}
    if(Cols[32]==1){fout << " " << pars.is_boundary[k][3];}
    fout << "\n";
    //for(int l=3; l<=n-1;l++){fout << " " <<  Data.pos[l*N+k];} //iterate different domains of vector of data start after x,y and Ornstein-Uhlenbeck noise -> memory, direction, phi
    //fout << " " << pars.intensity[k] << " " << pars.VorticityA[k] << " " << Data.vel[2*k] << " " << Data.vel[2*k+1] << " " << pars.UXA[k] << " " << pars.UYA[k] << " " << pars.TrgradXA[k] << " " << pars.TrgradYA[k] << " " << pars.Vorticity_gradXA[k] << " " << pars.Vorticity_gradYA[k] << " " << Data.pos2[2*k] << " " << Data.pos2[2*k+1] << " " << Data.vel2[2*k] << " " << Data.vel2[2*k+1] << " " << pars.UXA2[k] << " " << pars.UYA2[k] << " " << Data.pos[2*N+k] << " " << pars.intensity2[k] << " " << pars.VorticityA2[k] << " " << pars.Tr_Sig_gradXA[k] << " " << pars.Tr_Sig_gradYA[k] << " " << pars.LEVA[k] << " " << pars.LEV_gradXA[k] << " " << pars.LEV_gradYA[k] << " " << pars.agent_name[k] << " " << pars.intensity_sig[k] << " " << pars.is_boundary[k][0] << " " << pars.is_boundary[k][1] << " " << pars.is_boundary[k][2] << " " << pars.is_boundary[k][3] << "\n";
   }
  fout << "\n";
  //fout3 << t << " " << Data.pos[0] << " " << Data.pos[1] << "\n";
 }

void Data_print_FDF(DOUB &t, FDFS &FDF, parsf &parsFDF, int Nfx, int Nfy, ofstream &fout4, int Nfxo, int Nfyo, int Xo, ofstream &fout5, vector<int> &Cols)
 {
  //NOTE: This function prints the CFD fields in the domain to two separate files. One for the outlet and one for the main domain.
  //cout << "print data FDF \n";
  for(int k=0; k<Nfx; k++)
   {
    for(int l=0; l<Nfy; l++)
     {
      fout4 << t << " " << FDF.gridX[k] << " " << FDF.gridY[l];
      if(Cols[0]==1){fout4 << " " << k;}
      if(Cols[1]==1){fout4 << " " << l;}
      if(Cols[2]==1){fout4 << " " << FDF.intensity_coupler[k*Nfy+l];}
      if(Cols[3]==1){fout4 << " " << FDF.UX_coupler[k*Nfy+l];}
      if(Cols[4]==1){fout4 << " " << FDF.UY_coupler[k*Nfy+l];}
      if(Cols[5]==1){fout4 << " " << FDF.Pr_coupler[k*Nfy+l];}
      if(Cols[6]==1){fout4 << " " << FDF.vorticity_coupler[k*Nfy+l];}
      if(Cols[7]==1){fout4 << " " << FDF.divergence_coupler[k*Nfy+l];}
      if(Cols[8]==1){fout4 << " " << FDF.TrgradX_coupler[k*Nfy+l];}
      if(Cols[9]==1){fout4 << " " << FDF.TrgradY_coupler[k*Nfy+l];}
      if(Cols[10]==1){fout4 << " " << FDF.vorticitygradX_coupler[k*Nfy+l];}
      if(Cols[11]==1){fout4 << " " << FDF.vorticitygradY_coupler[k*Nfy+l];}
      if(Cols[12]==1){fout4 << " " << FDF.intensity_sig_coupler[k*Nfy+l];}
      if(Cols[13]==1){fout4 << " " << FDF.intensity_sig_back_coupler2[k*Nfy+l];}
      if(Cols[14]==1){fout4 << " " << FDF.Tr_Sig_gradX_coupler[k*Nfy+l];}
      if(Cols[15]==1){fout4 << " " << FDF.Tr_Sig_gradY_coupler[k*Nfy+l];}
      if(Cols[16]==1){fout4 << " " << FDF.intensity_back_coupler2[k*Nfy+l];}
      if(Cols[17]==1){fout4 << " " << parsFDF.boundary[(k+1)*(parsFDF.Nfy+2)+(l+1)];}
      if(Cols[18]==1){fout4 << " " << FDF.LEV_coupler[k*Nfy+l];}
      if(Cols[19]==1){fout4 << " " << FDF.LEVgradX_coupler[k*Nfy+l];}
      if(Cols[20]==1){fout4 << " " << FDF.LEVgradY_coupler[k*Nfy+l];}
      if(Cols[21]==1){fout4 << " " << parsFDF.porous[(k+1)*(parsFDF.Nfy+2)+(l+1)];}
      fout4 << "\n";
     //<< " " << FDF.gridX[k] << " " << FDF.gridY[l] << " " << k << " " << l << " " << FDF.intensity_coupler[k*Nfy+l] << " " << FDF.UX_coupler[k*Nfy+l] << " " << FDF.UY_coupler[k*Nfy+l] << " " << FDF.Pr_coupler[k*Nfy+l] << " " << FDF.vorticity_coupler[k*Nfy+l] << " " << FDF.divergence_coupler[k*Nfy+l] << " " << FDF.TrgradX_coupler[k*Nfy+l] << " " << FDF.TrgradY_coupler[k*Nfy+l] << " " << FDF.vorticitygradX_coupler[k*Nfy+l] << " " << FDF.vorticitygradY_coupler[k*Nfy+l] << " " << FDF.intensity_sig_coupler[k*Nfy+l] << " " << FDF.intensity_sig_back_coupler2[k*Nfy+l] << " " << FDF.Tr_Sig_gradX_coupler[k*Nfy+l] << " " << FDF.Tr_Sig_gradY_coupler[k*Nfy+l] << " " << FDF.intensity_back_coupler2[k*Nfy+l] << " " << parsFDF.boundary[(k+1)*(parsFDF.Nfy+2)+(l+1)] << " " << FDF.LEV_coupler[k*Nfy+l] << " " << FDF.LEVgradX_coupler[k*Nfy+l] << " " << FDF.LEVgradY_coupler[k*Nfy+l]  << "\n";
      //fout4 << t << " " << FDF.gridX[k] << " " << FDF.gridY[l] << " " << k << " " << l << " " << FDF.intensity_coupler[k*Nfy+l] << " " << FDF.UX_coupler[k*Nfy+l] << " " << FDF.UY_coupler[k*Nfy+l] << " " << FDF.Pr_coupler[k*Nfy+l] << " " << FDF.vorticity_coupler[k*Nfy+l] << " " << FDF.divergence_coupler[k*Nfy+l] << " " << FDF.TrgradX_coupler[k*Nfy+l] << " " << FDF.TrgradY_coupler[k*Nfy+l] << " " << FDF.vorticitygradX_coupler[k*Nfy+l] << " " << FDF.vorticitygradY_coupler[k*Nfy+l] << " " << FDF.intensity_sig_coupler[k*Nfy+l] << " " << FDF.intensity_sig_back_coupler2[k*Nfy+l] << " " << FDF.Tr_Sig_gradX_coupler[k*Nfy+l] << " " << FDF.Tr_Sig_gradY_coupler[k*Nfy+l] << " " << FDF.intensity_back_coupler2[k*Nfy+l] << " " << parsFDF.boundary[(k+1)*(parsFDF.Nfy+2)+(l+1)] << " " << FDF.LEV_coupler[k*Nfy+l] << " " << FDF.LEVgradX_coupler[k*Nfy+l] << " " << FDF.LEVgradY_coupler[k*Nfy+l]  << "\n";
     } //<< " " << FDF.UX_coupler_keep[k*Nfy+l] << " " << FDF.UX_coupler_at[k*Nfy+l] << " " << FDF.UY_coupler_keep[k*Nfy+l] << " " << FDF.UY_coupler_at[k*Nfy+l] << " " << parsFDF.Calc_Pr[(k+1)*(Nfy+2)+(l+1)] 
    fout4 << "\n";
   }
  //### outlet fields ###//
  for(int k=1; k<Nfxo+1; k++)
   {
    for(int l=1; l<Nfyo; l++)
     {
      fout5 << t << " " << (FDF.gridX[1]-FDF.gridX[0])*(DOUB(Xo-1-(Nfxo-1)/2+k-Nfx/2-1)) << " " << (FDF.gridY[1]-FDF.gridY[0])*(DOUB(l-Nfyo-Nfy/2-1));
      if(Cols[0]==1){fout5 << " " << k;}
      if(Cols[1]==1){fout5 << " " << l;}
      if(Cols[2]==1){fout5 << " " << FDF.intensity[(Nfx+2)*(Nfy+2)+k*Nfyo+l];}
      if(Cols[3]==1){fout5 << " " << 0.5*(FDF.UX[(Nfx+1)*(Nfy+2)+k*Nfyo+l]+FDF.UX[(Nfx+1)*(Nfy+2)+(k-1)*Nfyo+l]);}
      if(Cols[4]==1){fout5 << " " << 0.5*(FDF.UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l]+FDF.UY[(Nfx+2)*(Nfy+1)+k*Nfyo+l-1]);}
      if(Cols[5]==1){fout5 << " " << FDF.Pr[(Nfx+2)*(Nfy+2)+k*Nfyo+l];}
      if(Cols[6]==1){fout5 << " " << FDF.vorticity[(Nfx+2)*(Nfy+2)+k*Nfyo+l];}
      if(Cols[7]==1){fout5 << " " << FDF.divergence[(Nfx+2)*(Nfy+2)+k*Nfyo+l];}
      if(Cols[12]==1){fout5 << " " << FDF.intensity_sig[(Nfx+2)*(Nfy+2)+k*Nfyo+l];}
      if(Cols[18]==1){fout5 << " " << FDF.LEV[(Nfx+2)*(Nfy+2)+k*Nfyo+l];}
      fout5 << "\n";
     }
    //fout5 << t << " " << (FDF.gridX[1]-FDF.gridX[0])*(DOUB(Xo-1-(Nfxo-1)/2+k-Nfx/2-1)) << " " << (FDF.gridY[1]-FDF.gridY[0])*(DOUB(-Nfy/2-1)) << " " << k << " " << 5 << " " << FDF.intensity[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << 0.5*(FDF.UX[(Xo-1-(Nfxo-1)/2+k-1)*(Nfy+2)]+FDF.UX[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)]) << " " << 0.5*(FDF.UY[(Xo-1-(Nfxo-1)/2+k)*(Nfy+1)]+FDF.UY[(Nfx+2)*(Nfy+1)+k*Nfyo+Nfyo-1]) << " " << FDF.Pr[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << FDF.vorticity[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << FDF.divergence[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << FDF.intensity_sig[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << " " << FDF.LEV[(Xo-1-(Nfxo-1)/2+k)*(Nfy+2)] << "\n";//highest layer of outlet in main domain
    fout5 << "\n";
   }
 }

void CFD_grid_to_agent_grid(FDFS &FDF, parsf &parsFDF)
 {
  //NOTE: This function shifts the CFD vector information to the coupling grid (also plotting grid)
  //      For velocities in the staggered grid, the average is used.
  for(int k=0; k<parsFDF.Nfx; k++)
   {
    for(int l=0; l<parsFDF.Nfy; l++)
     {
      FDF.intensity_coupler[k*parsFDF.Nfy+l] = FDF.intensity[(k+1)*(parsFDF.Nfy+2)+(l+1)];                            // copy food intenisty
      FDF.intensity_back_coupler2[k*parsFDF.Nfy+l] = FDF.intensity_back_coupler[(k+1)*(parsFDF.Nfy+2)+(l+1)];         // copy food tracer intenisty flux for printing
      FDF.intensity_sig_coupler[k*parsFDF.Nfy+l] = FDF.intensity_sig[(k+1)*(parsFDF.Nfy+2)+(l+1)];                    // copy signaling tracer intenisty
      FDF.intensity_sig_back_coupler2[k*parsFDF.Nfy+l] = FDF.intensity_sig_back_coupler[(k+1)*(parsFDF.Nfy+2)+(l+1)]; // copy signaling tracer intenisty flux for printing
      FDF.vorticity_coupler[k*parsFDF.Nfy+l] = FDF.vorticity[(k+1)*(parsFDF.Nfy+2)+(l+1)];                            // copy vorticity
      FDF.divergence_coupler[k*parsFDF.Nfy+l] = FDF.divergence[(k+1)*(parsFDF.Nfy+2)+(l+1)];                          // copy divergence
      FDF.UX_coupler[k*parsFDF.Nfy+l] = 0.5*(FDF.UX[k*(parsFDF.Nfy+2)+(l+1)]+FDF.UX[(k+1)*(parsFDF.Nfy+2)+(l+1)]);    // copy x velocity
      FDF.UY_coupler[k*parsFDF.Nfy+l] = 0.5*(FDF.UY[(k+1)*(parsFDF.Nfy+1)+l]+FDF.UY[(k+1)*(parsFDF.Nfy+1)+(l+1)]);    // copy y velocity
      FDF.Pr_coupler[k*parsFDF.Nfy+l] = FDF.Pr[(k+1)*(parsFDF.Nfy+2)+(l+1)];                                          // copy pressur
      FDF.LEV_coupler[k*parsFDF.Nfy+l] = FDF.LEV[(k+1)*(parsFDF.Nfy+2)+(l+1)];                                        // copy Large Eddy Viscosity
      FDF.TrgradX_coupler[k*parsFDF.Nfy+l] = FDF.TrgradX[(k+1)*(parsFDF.Nfy+2)+(l+1)];                                // copy X gradient of tracer
      FDF.TrgradY_coupler[k*parsFDF.Nfy+l] = FDF.TrgradY[(k+1)*(parsFDF.Nfy+2)+(l+1)];                                // copy Y gradient of tracer
      FDF.Tr_Sig_gradX_coupler[k*parsFDF.Nfy+l] = FDF.Tr_Sig_gradX[(k+1)*(parsFDF.Nfy+2)+(l+1)];                      // copy X gradient of tracer
      FDF.Tr_Sig_gradY_coupler[k*parsFDF.Nfy+l] = FDF.Tr_Sig_gradY[(k+1)*(parsFDF.Nfy+2)+(l+1)];                      // copy Y gradient of tracer
      FDF.vorticitygradX_coupler[k*parsFDF.Nfy+l] = FDF.vorticitygradX[(k+1)*(parsFDF.Nfy+2)+(l+1)];                  // copy X gradient of vorticity
      FDF.vorticitygradY_coupler[k*parsFDF.Nfy+l] = FDF.vorticitygradY[(k+1)*(parsFDF.Nfy+2)+(l+1)];                  // copy Y gradient of vorticity
      FDF.LEVgradX_coupler[k*parsFDF.Nfy+l] = FDF.LEVgradX[(k+1)*(parsFDF.Nfy+2)+(l+1)];                              // copy X gradient of vorticity
      FDF.LEVgradY_coupler[k*parsFDF.Nfy+l] = FDF.LEVgradY[(k+1)*(parsFDF.Nfy+2)+(l+1)];                              // copy Y gradient of vorticity
     }
   }
 }

void Divergence_calc(FDFS &FDF, parsf &parsFDF)
 {
  //NOTE: The Function calculates the Divergence in the whole domain. The Output is averaged and used as a control parameter for the CFD solver
  //main domain //
  for(int k=1; k<parsFDF.Nfx+1; k++)
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      FDF.divergence[k*(parsFDF.Nfy+2)+l] = (FDF.UX[k*(parsFDF.Nfy+2)+l]-FDF.UX[(k-1)*(parsFDF.Nfy+2)+l])/parsFDF.dx                                                                                                             + (FDF.UY[k*(parsFDF.Nfy+1)+l]-FDF.UY[k*(parsFDF.Nfy+1)+l-1])/parsFDF.dy;
     }
   }
  //### Outlet domain ###//
  for(int k=1; k<2*parsFDF.width_outlet+2; k++)
   {
    for(int l=3; l<parsFDF.length_outlet-1; l++)//avoid counting layers very close to the outlet itself
     {
      FDF.divergence[(parsFDF.Nfy+2)*(parsFDF.Nfx+2)+k*parsFDF.length_outlet+l] = (FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l]                                                                                                                   -FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l])/parsFDF.dx                                                                                                 + (FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l]                                                                                                                   -FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l-1])/parsFDF.dy;
     }
   }
 }

void Vorticity_calc(FDFS &FDF, parsf &parsFDF)
 {
  //NOTE: This function calculates the vorticity in the flow domain (main and outlet if needed)
  //main domain //
  vector<DOUB> Vort(FDF.vorticity.size());
  for(int k=1; k<parsFDF.Nfx+1; k++)
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      //FDF.vorticity[k*(parsFDF.Nfy+2)+l] = 0.25/(parsFDF.dx)*(FDF.UY[(k+1)*(parsFDF.Nfy+1)+l] + FDF.UY[(k+1)*(parsFDF.Nfy+1)+l-1]                                                                                                                  - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l] - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l-1])                                                                                              - 0.25/(parsFDF.dy)*(FDF.UX[k*(parsFDF.Nfy+2)+l+1] + FDF.UX[(k-1)*(parsFDF.Nfy+2)+l+1]                                                                                                                    - FDF.UX[k*(parsFDF.Nfy+2)+l-1] - FDF.UX[(k-1)*(parsFDF.Nfy+2)+l-1]);
      Vort[k*(parsFDF.Nfy+2)+l] = 0.25/(parsFDF.dx)*(FDF.UY[(k+1)*(parsFDF.Nfy+1)+l] + FDF.UY[k*(parsFDF.Nfy+1)+l]                                                                                                                        + FDF.UY[(k+1)*(parsFDF.Nfy+1)+l-1] + FDF.UY[k*(parsFDF.Nfy+1)+l-1]                                                                                                                    - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l] - FDF.UY[k*(parsFDF.Nfy+1)+l]                                                                                                                        - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l-1] - FDF.UY[k*(parsFDF.Nfy+1)+l-1])                                                                                                - 0.25/(parsFDF.dy)*(FDF.UX[(k-1)*(parsFDF.Nfy+2)+l+1] + FDF.UX[(k-1)*(parsFDF.Nfy+2)+l]                                                                                                                  + FDF.UX[k*(parsFDF.Nfy+2)+l+1] + FDF.UX[k*(parsFDF.Nfy+2)+l]                                                                                                                          - FDF.UX[(k-1)*(parsFDF.Nfy+2)+l-1] - FDF.UX[(k-1)*(parsFDF.Nfy+2)+l]                                                                                                                  - FDF.UX[k*(parsFDF.Nfy+2)+l-1] - FDF.UX[k*(parsFDF.Nfy+2)+l]);
     }
   }
  //### Outlet domain ###//
  for(int k=1; k<2*parsFDF.width_outlet+2; k++)
   {
    for(int l=1; l<parsFDF.length_outlet-1; l++)
     {
      //FDF.vorticity[k*(parsFDF.Nfy+2)+l] = 0.25/(parsFDF.dx)*(FDF.UY[(k+1)*(parsFDF.Nfy+1)+l] + FDF.UY[(k+1)*(parsFDF.Nfy+1)+l-1]                                                                                                                  - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l] - FDF.UY[(k-1)*(parsFDF.Nfy+1)+l-1])                                                                                              - 0.25/(parsFDF.dy)*(FDF.UX[k*(parsFDF.Nfy+2)+l+1] + FDF.UX[(k-1)*(parsFDF.Nfy+2)+l+1]                                                                                                                    - FDF.UX[k*(parsFDF.Nfy+2)+l-1] - FDF.UX[(k-1)*(parsFDF.Nfy+2)+l-1]);
      FDF.vorticity[(parsFDF.Nfy+2)*(parsFDF.Nfx+2)+k*parsFDF.length_outlet+l] = 0.25/(parsFDF.dx)*(FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k+1)*(parsFDF.length_outlet)+l]                                                                                                              + FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l]                                                                                                                  + FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k+1)*(parsFDF.length_outlet)+l-1]                                                                                                            + FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l-1]                                                                                                                - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k-1)*(parsFDF.length_outlet)+l]                                                                                                              - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l]                                                                                                                  - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+(k-1)*(parsFDF.length_outlet)+l-1]                                                                                                            - FDF.UY[(parsFDF.Nfx+2)*(parsFDF.Nfy+1)+k*(parsFDF.length_outlet)+l-1])                                                                                            - 0.25/(parsFDF.dy)*(FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l+1]                                                                                                            + FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l]                                                                                                              + FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l+1]                                                                                                                + FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l]                                                                                                                  - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l-1]                                                                                                            - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+(k-1)*(parsFDF.length_outlet)+l]                                                                                                              - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l-1]                                                                                                                - FDF.UX[(parsFDF.Nfx+1)*(parsFDF.Nfy+2)+k*(parsFDF.length_outlet)+l]);
     }
   }
  //### averaging ###//
  for(int k=1; k<parsFDF.Nfx+1; k++)//avoide first layer to boundary
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      FDF.vorticity[k*(parsFDF.Nfy+2)+l] = 0.2*Vort[k*(parsFDF.Nfy+2)+l] + (2.0/15.0)*(Vort[(k-1)*(parsFDF.Nfy+2)+l] + Vort[(k+1)*(parsFDF.Nfy+2)+l] + Vort[k*(parsFDF.Nfy+2)+l+1] + Vort[k*(parsFDF.Nfy+2)+l-1]) + (Vort[(k-1)*(parsFDF.Nfy+2)+l-1] + Vort[(k-1)*(parsFDF.Nfy+2)+l+1] + Vort[(k+1)*(parsFDF.Nfy+2)+l+1] + Vort[(k+1)*(parsFDF.Nfy+2)+l-1])/15.0;
     }
   }
 }

void Grads(FDFS &FDF, parsf &parsFDF)
 {
  //NOTE: This function calculates the gradients of food and signal tracers and vorticity 
  DOUB Gx(0.0), Gy(0.0);
  for(int k=1; k<parsFDF.Nfx+1; k++)
   {
    for(int l=1; l<parsFDF.Nfy+1; l++)
     {
      //food tracer
      Gx = (FDF.intensity[(k+1)*(parsFDF.Nfy+2)+l] - FDF.intensity[(k-1)*(parsFDF.Nfy+2)+l])*0.5/parsFDF.dx;
      Gy = (FDF.intensity[k*(parsFDF.Nfy+2)+l+1] - FDF.intensity[k*(parsFDF.Nfy+2)+l-1])*0.5/parsFDF.dy;
      FDF.TrgradX[k*(parsFDF.Nfy+2)+l] = Gx;//X gradient of food tracer
      FDF.TrgradY[k*(parsFDF.Nfy+2)+l] = Gy;//Y gradient of food tracer
      //signaling tracer
      Gx = (FDF.intensity_sig[(k+1)*(parsFDF.Nfy+2)+l] - FDF.intensity_sig[(k-1)*(parsFDF.Nfy+2)+l])*0.5/parsFDF.dx;
      Gy = (FDF.intensity_sig[k*(parsFDF.Nfy+2)+l+1] - FDF.intensity_sig[k*(parsFDF.Nfy+2)+l-1])*0.5/parsFDF.dy;
      FDF.Tr_Sig_gradX[k*(parsFDF.Nfy+2)+l] = Gx;//X gradient of signal tracer
      FDF.Tr_Sig_gradY[k*(parsFDF.Nfy+2)+l] = Gy;//Y gradient of signal tracer
//      Gx = (FDF.vorticity[(k+1)*(parsFDF.Nfy+2)+l] - FDF.vorticity[(k-1)*(parsFDF.Nfy+2)+l])*0.5/parsFDF.dx; //gradient of vorticity x
//      Gy = (FDF.vorticity[k*(parsFDF.Nfy+2)+l+1] - FDF.vorticity[k*(parsFDF.Nfy+2)+l-1])*0.5/parsFDF.dy; //gradient of vorticity y
      //vorticity
      Gx = (abs(FDF.vorticity[(k+1)*(parsFDF.Nfy+2)+l]) - abs(FDF.vorticity[(k-1)*(parsFDF.Nfy+2)+l]))*0.5/parsFDF.dx; // gradient of vorticity absolute value x
      Gy = (abs(FDF.vorticity[k*(parsFDF.Nfy+2)+l+1]) - abs(FDF.vorticity[k*(parsFDF.Nfy+2)+l-1]))*0.5/parsFDF.dy; // gradient of vorticity absolute value y 
      FDF.vorticitygradX[k*(parsFDF.Nfy+2)+l] = Gx;//X gradient of abs(vorticity)
      FDF.vorticitygradY[k*(parsFDF.Nfy+2)+l] = Gy;//Y gradient of abs(vorticity)
      //Large Eddy viscosity
      Gx = ((FDF.LEV[(k+1)*(parsFDF.Nfy+2)+l]) - (FDF.LEV[(k-1)*(parsFDF.Nfy+2)+l]))*0.5/parsFDF.dx; // gradient of LEV x
      Gy = ((FDF.LEV[k*(parsFDF.Nfy+2)+l+1]) - (FDF.LEV[k*(parsFDF.Nfy+2)+l-1]))*0.5/parsFDF.dy; // gradient of LEV y 
      FDF.LEVgradX[k*(parsFDF.Nfy+2)+l] = Gx;//X gradient of abs(vorticity)
      FDF.LEVgradY[k*(parsFDF.Nfy+2)+l] = Gy;//Y gradient of abs(vorticity)
     }
   }
  //TODO: outlet domain
 }

void wall_distance(parsf &parsFDF, params &pars)
 {
  //NOTE: Distance must be large enough such that agents get repelled before they enter the edge grid point of the internal pressure grid cells. The location of these cells
  //      is the same as the coupling grid cell positions (Interpolation of staggered grid velocity from the edges of cells to the center of the grid cells 
  //      i.e. pressure grid in coupling is done before)
  //      Agents are not allowed to cross the edges defined bu the center points of the outer most layer of cells in the coupling grid
  
  //set repulsion distance at walls
  //pars.wrad = 0.125; //NOTE: corresponds roughly to 81 grid points on 10 meters of domain -> there are two grid points in the repulsion region
  //pars.wrad = 0.25; //NOTE: corresponds roughly to 41 grid points on 10 meters of domain -> there are two grid points in the repulsion region
  DOUB maxDL(parsFDF.dx);
  if(parsFDF.dx>parsFDF.dy){maxDL=parsFDF.dx;}
  else{maxDL=parsFDF.dy;}
  if(maxDL > 0.5*pars.wrad){pars.wrad += maxDL;}// increases the repulsion distance if wrad is within a single grid cell
  
  //read coastal map
  //NOTE: This part reads the coastal map from a file in matrix data form to the agent coupling grid.
  //      The spacial dimensions of the read file define the grid positions (cell centers) and the edge positions
  //      The read data has to have the form x0,y0, 1 or 0
  //                                         x0,y1, 1 or 0
  //                                          .  .    .
  //                                         x0,yN, 1 or 0
  //                                         x1,y0, 1 or 0
  //                                         x1,y1, 1 or 0
  //                                          .  .    .
  //                                         x1,yN, 1 or 0
  //                                          .  .    .
  //                                         xN,yN, 1 or 0
  
  //interpolate image data onto the simulation grid as specified by parameters //
  
  string line;
  int cx(0);        //counts blocks of changing y axis
  //### EXTRACT FILE CONTENT OF FORM data <SPACE> time \n to struct members ###//
  
#ifdef complex_boundary  
  cout << "reading coastal map from file " << parsFDF.coast_name << "\n";
  ifstream coastdat (parsFDF.coast_name);
  if(coastdat.is_open())
   {
    DOUB a(0.0);
    while(getline(coastdat,line))
     {
      istringstream is(line);
      vector<DOUB> vecline;
      int jj(0);
      while(is >> a)
       {
        if(jj==0)//x axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==1)//y axis index
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==2)// coastal map
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==3)// inlet map
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==4)// outlet map
         {
          vecline.push_back(DOUB(a));
         }
        if(jj==5)// porous wall cells
         {
          vecline.push_back(DOUB(a));
         }
        jj +=1;
       }//TODO: read in the free slip boundary condition
      if(jj==0){cx +=1;}//counts the number of blocks in which y axis is changed
      //cout << "data extracted x,y,map = "; for(int k=0; k<vecline.size();k++){cout << vecline[k] << ", ";} cout << "length of elementsin line is " << jj << "\n";
      if(jj>0){parsFDF.boundaryX.push_back(vecline[0]); parsFDF.boundaryY.push_back(vecline[1]); parsFDF.boundary.push_back(vecline[2]); parsFDF.boundary_inlet.push_back(vecline[3]); parsFDF.boundary_outlet.push_back(vecline[4]); parsFDF.porous.push_back(vecline[5]);}
     }
    coastdat.close();
   } 
#else
  for(int k=0; k<parsFDF.Nfx+2; k++)
   {
    for(int l=0; l<parsFDF.Nfy+2; l++)
     {
      parsFDF.boundaryX.push_back(DOUB(k)); 
      parsFDF.boundaryY.push_back(DOUB(l)); 
      parsFDF.boundary_inlet.push_back(1.0); 
      parsFDF.boundary_outlet.push_back(1.0);
      parsFDF.porous.push_back(0.0);
      if((k>0) && (l>0) && (k<parsFDF.Nfx+1) && (l<parsFDF.Nfy+1))
       {
        //cout << "set fluid \n";
        parsFDF.boundary.push_back(0.0); 
       }
      else
       {
        //cout << "set solid \n";
        parsFDF.boundary.push_back(1.0); 
       }
     }
   }
  cout << "size of domain " << (parsFDF.Nfx+2)*(parsFDF.Nfy+2) << " number of set coastal cells= " << parsFDF.boundary.size() << "\n";
#endif     
 }
 
void gnuplot_starter()
 {
  //NOTE: This function sets the plotting environment in gnuplot. (Data is send via pipe (|))
  //cout << "gnuplot \n";
  cout << "set terminal wxt size 1200,1200 enhanced font 'Verdana,10' \n";
  cout << "set xrange[-5:5] \n";
  cout << "set yrange[-5:5] \n";
  cout << "set cbrange[0.0:2.0*pi] \n";
  cout << "unset key \n";
  cout << "mod(x,y) = x - floor(x/y)*y \n";
  cout << "set key outside \n";
 }
 
void gnuplot_plotter(FDFS &FDF, Vectordata &Data, int &N, int &n, int &Nfx, int &Nfy, DOUB &t)
 {
  //NOTE: This function plots in each plotting time step the positions of the active particles directly to gnuplot using a pipe (|)
  cout << "plot";
  //CFD field
  cout << " '-' w p pt 5 ps 5 palette title 't= " << t << "' " ;
  for(int l=1; l<Nfy; l++){cout << ", '-' w p pt 5 ps 5 palette notitle";}
  for(int k=1; k<Nfx; k++){for(int l=0; l<Nfy; l++){cout << ", '-' w p pt 5 ps 5 palette notitle";}}
  // Agents
  cout << ", '-' w p pt 7 ps 1 palette notitle";
  for(int k=1;k<N;k++){cout << ", '-' w p pt 7 ps 1 palette notitle";}
  cout << "\n";
  //CFD fields
  for(int k=0; k<Nfx; k++)
   {
    for(int l=0; l<Nfy; l++)
     {
      cout << FDF.gridX[k] << " " << FDF.gridY[l] << " " << sqrt(FDF.UX_coupler[k*Nfy+l]*FDF.UX_coupler[k*Nfy+l] +  FDF.UY_coupler[k*Nfy+l]*FDF.UY_coupler[k*Nfy+l])*2.0*pi*0.8/0.2 << " \n"; cout << "e \n"; 
//      cout << FDF.gridX[k] << " " << FDF.gridY[l] << " " << FDF.intensity_coupler[k*Nfy+l]*2.0*pi << " \n"; cout << "e \n";  
      //<< " " << FDF.Pr_coupler[k*Nfy+l] << " " << FDF.vorticity_coupler[k*Nfy+l] << " " << FDF.divergence_coupler[k*Nfy+l] << " " << FDF.TrgradX_coupler[k*Nfy+l] << " " << FDF.TrgradY_coupler[k*Nfy+l] << " " << FDF.vorticitygradX_coupler[k*Nfy+l] << " " << FDF.vorticitygradY_coupler[k*Nfy+l] << "\n";
     }
   }
  // active Agents 
  //for(int k=0;k<N;k++){cout << Data.pos[2*k] << " " << Data.pos[2*k+1] << " " << Data.pos[(n-1)*N+k] - floor(Data.pos[(n-1)*N+k]/(2.0*pi))*2.0*pi << " \n"; cout << "e \n";}
  // passive Agents 
  for(int k=0;k<N;k++){cout << Data.pos2[2*k] << " " << Data.pos2[2*k+1] << " " << 1.0 << " \n"; cout << "e \n";}
 }
 
void parameter_output(ofstream &fout2, params &pars, parsf &parsFDF, DOUB &T2, int &N, int &n)
 {
  //NOTE: This function outputs the parameters of the simulation.
  //      The first four lines are set to be used in a separate movie generation (python) and are read in as parameters there.
  fout2 << pars.xi << "\n";
  fout2 << pars.xf << "\n";
  fout2 << pars.yi << "\n";
  fout2 << pars.yf << "\n";
  fout2 << "number of agents " << N << "\n";
  fout2 << "offset velocity " << pars.vel0 << "\n";
  fout2 << "interaction strength " << pars.eps << "\n";
  fout2 << "interaction radius " << pars.irad << "\n";
  fout2 << "mean turning frequency " << pars.freq1 << "\n";
  fout2 << "stdev turning frequency " << pars.freq2 << "\n";
  fout2 << "### singel frequencies of units \n";
  for(int k=0;k<pars.freq.size();k++){fout2 << "unit " << k << " freq= " << pars.freq[k] << "\n";}
  fout2 << "thresh " << pars.thresh << "\n";
  fout2 << "minx init " << pars.sposx-0.5*pars.sposedge << "\n";
  fout2 << "maxx init " << pars.sposx+0.5*pars.sposedge << "\n";
  fout2 << "miny init " << pars.sposx-0.5*pars.sposedge << "\n";
  fout2 << "maxy init " << pars.sposx+0.5*pars.sposedge << "\n";
  fout2 << "J " << pars.J << "\n";
  fout2 << "fxi " << parsFDF.fxi << "\n";
  fout2 << "fxf " << parsFDF.fxf << "\n";
  fout2 << "fyi " << parsFDF.fyi << "\n";
  fout2 << "fyf " << parsFDF.fyf << "\n";
  fout2 << "dx " << parsFDF.dx << "\n";
  fout2 << "dy " << parsFDF.dy << "\n";
  for(int k=0;k<N;k++){fout2 << " agent" << k << "Dx,y= " << pars.D[2*k] << ", " << pars.D[2*k+1] << " Dangle= " << pars.D[(n-2)*N+k] << " Dphi= " << pars.D[(n-1)*N+k] << "\n";}
  fout2 << "if enabled: images are extracted from " << parsFDF.image_path << "\n";
  fout2 << parsFDF.kappa << " gaussian smoothing parameter to interpolate pixels onto internal grid \n";
  fout2 << pars.dir_lag << " directional phase lag parameter \n";
  fout2 << pars.J2 << " directional sensitivity of coupling \n";
  fout2 << pars.J3 << " velocity oscillation parameters \n";
  fout2 << pars.eps2 << " intercation strength of direction \n";
  fout2 << parsFDF.rho << " density of fluid \n";
  fout2 << parsFDF.mu << " viscosity of fluid \n";
  fout2 << parsFDF.beta << " relaxation prameter of pressur solver \n";
  fout2 << parsFDF.toll << " tolerance of pressur solver \n";
  fout2 << parsFDF.dtf << " dt of fluid solver (initial value) \n";
  fout2 << parsFDF.DTr1 << " Diffusion coefficients of first tracer \n";
  fout2 << pars.vel_c << " adaption velocity to counter the mean flow \n";
  fout2 << pars.eps3 << " interaction strength of direction and velocity (counter current swimming) \n";
  fout2 << parsFDF.pos_inlet << " position of inlet \n";
  fout2 << parsFDF.pos_outlet << " position of outlet \n";
  fout2 << parsFDF.width_inlet << " width of outlet \n";
  fout2 << parsFDF.width_outlet << " width of outlet \n";
  fout2 << parsFDF.vel_inlet << " velocity maximum after rampoff at the center of the inlet \n";
  fout2 << parsFDF.length_outlet << " how long is the outlet channel \n";
  fout2 << T2 << " sim time \n";
  fout2 << pars.freq_c << " frequency_activity adaption \n";
  fout2 << pars.mem_damp << " memory damping value \n";
  fout2 << pars.eps4 << " interaction strength of direction and vorticity \n";
  fout2 << pars.counter_curent << " switcher for counter-curent swimming \n";
  fout2 << pars.osc_activity << " switcher for coupling of frequency and activity \n";
  fout2 << pars.dif_activity << " switcher for coupling of diffusivity and activity \n";
  fout2 << pars.agent_atr << " switcher for attraction of agents \n";
  fout2 << pars.seasons_switch << " switcher for seasons \n";
  fout2 << pars.agent_rep << " switcher for repulsion of agents \n";
  fout2 << pars.x_tracer << " x position of tracer distribution \n";
  fout2 << pars.y_tracer << " y position of tracer distribution \n";
  fout2 << pars.amp_tracer << " amplitude of tracer density \n";
  fout2 << pars.agent_direction << " switcher for directionality of agents (Q function that determines sensitivity of sensing) \n";
  fout2 << pars.Vs << " turning point paramter of counter-current swimming \n"; 
  fout2 << pars.tr_dir3 << " switcher for tracer orientation 1 means on, 0 means off \n";
  fout2 << pars.tr_dir_steepnes << " 1/tr_dir_steepnes determines how steep the switching from 0 to 1 takes place when activity increases \n";
  fout2 << pars.eps5 << " interaction strength of direction and tracer \n";
  fout2 << pars.counter_current_act << " switcher counter curent activity enhancement 1 means on, 0 means off \n"; 
  fout2 << pars.tr_steepnes << " turning point paramter of counter-current swimming direction adaption (switches off cc swimming and swimming towards prey) \n";
  fout2 << pars.act_steepnes_2 << " turning point parameter for switch off of gradient swimming \n"; 
  fout2 << pars.tr_dir2 << " switcher for counter-current swimming reduction when tracer is present 1 means loss of orientation, 0 means no loss of orientation \n";
  fout2 << pars.tr_dir1 << " switcher for vorticity avoidance 1 means avoidance changes with activity, 0 means avoidance is not affected by vorticity \n";
  fout2 << pars.mu2 << " viscosity mu of the jellyfish couplig (basically a smaller scale turbulence viscosity) \n";
  fout2 << pars.omegmin << " minimal intensity above which the wake vorticity is considered for far field coupling of active agents to the ghost agents \n";
  fout2 << pars.phase_lag << " phase lag of phase interaction function (bell oscillation), near field \n";
  fout2 << pars.phase_lag_far << " phase lag of phase interaction function (bell oscillation), far field \n";
  fout2 << pars.dir_lag_far << " angular lag of direction, far field \n";
  fout2 << pars.sig_cfd_couple << " signal tracer density rate that is transfered to the CFD grid \n";
  fout2 << pars.eps6 << " interaction strength of signal tracer gradient direction and orientation \n";
  fout2 << pars.tr_dir4 << " switcher for signal tracer swimming \n";
  fout2 << pars.tr_dir_steepnes2 << " steepness of signal tracer direction interaction to tracer gradient switches \n";
  fout2 << pars.spread_width << " spreading parameter that gives at defines width of spreading window when signaling tracer is coupled into the CFD grid \n";
  fout2 << pars.Tmax << " maximal life time of a wake (ghost) according to minimal intensity and mu2 \n"; 
  fout2 << 2.0/pars.vel0 << " time a vortex needs to travel 2 meters \n"; 
  fout2 << parsFDF.DTr_sig << " diffusion constant of second tracer \n"; 
  fout2 << parsFDF.spread_tracer << " width parameter of tracer distribution for prey \n";
  fout2 << pars.food_cfd_couple << " food tracer density rate that is transfered to the CFD grid \n";
  fout2 << parsFDF.initial_dat << " path to file that holds initial conditions for all CFD fields \n";
  fout2 << pars.vel_dir << " turning point parameter of counter current swimming orientation for dependence on velocity magnitude \n";
  fout2 << pars.vel_dir2 << " switcher for counter current swimming orientation (dependence of orientation strength on velocity magnitude) \n";
  fout2 << pars.sig_dir << " turning point parameter of directed swimming towards signaling tracer gradient (directional coupling parameter), depending on the magnitude of the signaling tracer gradient \n";
  fout2 << pars.sig_dir2 << " switcher for signaling tracer swimming \n";
  fout2 << pars.vort_dir << " turning point parameter of vorticity avoidance orientation for dependence on vorticity \n";
  fout2 << pars.vort_dir2 << " switcher for vorticity avoidance (dependence of orientation strength on vorticity magnitude) \n";
  fout2 << parsFDF.coast_name << " path to file that holds zero-one array to specify the coast line \n";
  fout2 << parsFDF.dtmax << " maximal CFD time step \n";
  fout2 << parsFDF.dtmin << " minimal CFD time step \n"; 
  fout2 << parsFDF.CFL << "CFL critical number \n";
  fout2 << parsFDF.CSL << " Smagorinsky constant \n";
  fout2 << parsFDF.DIVMAX << " maximum divergence allowed before maximum pressure steps are increased \n";
  fout2 << parsFDF.damp << " damping factor to enforce parallel flow in the outlet \n";
  fout2 << parsFDF.Vwind << " wind speed \n";
  fout2 << parsFDF.thwind << " wind direction \n";
  fout2 << parsFDF.NPRMAX << " maximum allowed pressure steps \n";
 }
 
void phase_tracker(vector<DOUB> &oldphases, int &N, int &n, vector<DOUB> &pos)
 {
  //NOTE: This function updates the old phases of the active agents (needed to determine if a ghost is generated)
  #pragma omp parallel for
  for(int k=0;k<N;k++)
   {
    oldphases[k] = pos[N*(n-1)+k];//N*(n-1)+k selects the phases of the bells
   }
  #pragma omp barrier 
 }
 
void set_ghosts(params &pars, vector<DOUB> &pos3, vector<DOUB> &vel3, int &N, int &n, vector<DOUB> &pos, vector<DOUB> &vel, DOUB &t, parsf &parsFDF, random_device &generator)
 {
  //NOTE: The function initializes new ghost particles when the active agents have completed a full bell cycle (phi=2pi)
  //      - Iterates through all active agents
  //      - Adds new ghost at the end of the vectors, using fluid flow, velocity magnitude, orientation - pi, position, index positions, and orientation as given below.
  //      - Due to the data ordering in th etime integration, the orientation is added separately in the end. 
  DOUB oldphi(0.0), newphi(0.0), V(0.0);
  int Nres(0);
  vector<DOUB> theta_init;
  uniform_real_distribution<DOUB> distributionU(0.0,1.0);
  
  //store all angular values for later
  for(int k=0;k<pars.N3;k++)
   {
    theta_init.push_back(pos3[2*pars.N3+k]);
   }
  pos3.resize(2*pars.N3); //delete all angles
  
  for(int k=0;k<N;k++)
   {
    oldphi= pars.oldphases[k] - floor(pars.oldphases[k]/(2.0*pi))*2.0*pi;      //phi(t-dt)
    newphi= pos[N*(n-1)+k] - floor(pos[N*(n-1)+k]/(2.0*pi))*2.0*pi;            //phi(t)
    if((newphi-oldphi)<-pi)//1. check phases
     {
      V=sqrt(vel[2*k]*vel[2*k] + vel[2*k+1]*vel[2*k+1]);
      
//      cout << " generated ghost for agent " << k << "\t oldphi= " << oldphi << " newphi= " << newphi << " t= " << t << " Tmax_Wake= " << 1.0/(4.0*pi*pars.mu2*(pars.omegmin+pars.wake_life_rand*sqrt(-pars.dirdamp/pars.D[N*(n-4)+k]*pos[N*(n-4)+k]*pos[N*(n-4)+k]))) << " " << pars.wake_life_rand*sqrt(-pars.dirdamp/pars.D[N*(n-4)+k]*pos[N*(n-4)+k]*pos[N*(n-4)+k]) << " " << sqrt(-pars.dirdamp/pars.D[N*(n-4)+k]*pos[N*(n-4)+k]*pos[N*(n-4)+k]) << " " << -pars.dirdamp/pars.D[N*(n-4)+k]*pos[N*(n-4)+k]*pos[N*(n-4)+k] << " " << pos[N*(n-4)+k]*pos[N*(n-4)+k] << "\n";
      //2.1 take all necessary information from the agent and add it to the list of ghosts (create a new ghost)
      pars.UXA3.push_back(pars.UXA[k]);                                        // x fluid velocity at ghost particles 
      pars.UYA3.push_back(pars.UYA[k]);                                        // y fluid velocity at ghost particles 
      if(V<3.0*pars.vel0){pars.absvel3.push_back(V);} // magnitude of velocity of ghost particles at generation = abs(active agent velocity)
      else{pars.absvel3.push_back(3.0*pars.vel0);} // magnitude of velocity of ghost particles at generation = abs(active agent velocity)
      pos3.push_back(pos[2*k]);                                                // x start position of ghost particles at generation = active agent X
      pos3.push_back(pos[2*k+1]);                                              // y start position of ghost particles at generation = active agent Y
      theta_init.push_back(pos[N*(n-2)+k]+pi);                                 // orientation of ghost particles at generation = theta +pi 
      vel3.push_back(0.0);                                                     // x start velocity of ghost particle at generation = active agent velocity X
      vel3.push_back(0.0);                                                     // y start velocity of ghost particle at generation = active agent velocity Y
      vel3.push_back(0.0);                                                     // theta start velocity of ghost particle at generation 
      pars.FDF_indexX3.push_back(pars.FDF_indexX[k]);                          // index of closest grid points X direction, ghost agents
      pars.FDF_indexY3.push_back(pars.FDF_indexY[k]);                          // index of closest grid points Y direction, ghost agents 
      pars.Tgstart.push_back(t);                                               // starting times (t0) of ghost agents when they are generated. 
      //pars.Tmax_Wake.push_back(1.0/(4.0*pi*pars.mu2*(pars.omegmin+pars.wake_life_rand*sqrt(-pars.dirdamp/pars.D[N*(n-4)+k]*pos[N*(n-4)+k]*pos[N*(n-4)+k])))); //Additional random increment will cause wakes to be delted at different moments in time
      //pars.Tmax_Wake.push_back(1.0/(4.0*pi*pars.mu2*(pars.omegmin*(1.0-pars.wake_life_rand+pars.wake_life_rand*(1.0+log(exp(-1.0) + sqrt(-pars.dirdamp/pars.D[N*(n-4)+k]*pos[N*(n-4)+k]*pos[N*(n-4)+k]))))))); //Additional random increment will cause wakes to be delted at different moments in time
//      pars.Tmax_Wake.push_back(1.0/(4.0*pi*pars.mu2*(pars.omegmin*(1.0-pars.wake_life_rand+2.0*pars.wake_life_rand*distributionU(generator))))); //Additional random increment will cause wakes to be delted at different moments in time
      pars.Tmax_Wake.push_back(pars.Tmax*(1.0-pars.wake_life_rand*0.886+pars.wake_life_rand*(sqrt(-log(1.0-distributionU(generator)))))); //Additional random increment will cause wakes to be delted at different moments in time 0.886=Gamma(3/2) to ensure that average is still Tmax and the mean of the multiplicator is 1.
      //cout << "life time= " << pars.Tmax*(1.0-pars.wake_life_rand+pars.wake_life_rand*(sqrt(-log(1.0-distributionU(generator))))) << "\n";
      pars.VorticityA3.push_back(pars.VorticityA[k]);                          // vorticity of ghost point at start is vorticity of active agents 
      pars.start_agent.push_back(pars.agent_name[k]);                          // index name of agent from which the ghost emerges (to avoid self interactions)
      pars.x3t.push_back(pos[2*k]);                                            // x position of ghost particles at time t -> to couple vortex wake to active agents X
      pars.y3t.push_back(pos[2*k+1]);                                          // y position of ghost particles at time t -> to couple vortex wake to active agents Y   
      pars.theta3t.push_back(0.0);                                             // orientation of ghost particle at time t -> to couple vortex wake to active agents (adjustment of vortex dipole axis) 
      pars.N3 += 1;                                                            // update number of ghosts
     }  
   }
  //reappend all old and all new angles (appending back the angles, the new positions have been added in between the old positions and the angles)
  for(int k=0; k<theta_init.size(); k++)
   {
    pos3.push_back(theta_init[k]);
   }
  //set coastal tracker
  //cout << "ghost boundary keep: \n";
  for(int k=0; k<pars.N3;k++)
   {
    pars.is_boundary3.push_back({parsFDF.boundary[(pars.FDF_indexX3[k][0]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY3[k][0]+1)],parsFDF.boundary[(pars.FDF_indexX3[k][1]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY3[k][1]+1)],parsFDF.boundary[(pars.FDF_indexX3[k][2]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY3[k][2]+1)],parsFDF.boundary[(pars.FDF_indexX3[k][3]+1)*(parsFDF.Nfy+2)+(pars.FDF_indexY3[k][3]+1)]});
    //cout << pars.N3 << ", " << k << "\n";
   }
 }

void update_ghosts_coupler(params &pars, vector<DOUB> &pos3)  
 { 
  //NOTE: Updates the position of wakes as seen by the active agents
  #pragma omp parallel for
  for(int k=0; k<pars.N3;k++)
   {
    pars.x3t[k] = pos3[2*k];                          // x position of ghost particles at time t -> to couple vortex wake to active agents X
    pars.y3t[k] = pos3[2*k+1];                        // y position of ghost particles at time t -> to couple vortex wake to active agents Y
    pars.theta3t[k] = pos3[2*pars.N3+k];              // orientation of the vortex (perpendicular to the vortex quadrupol axis)
   }
  #pragma omp barrier 
 }

void check_ghosts(vector<DOUB> &pos3, vector<DOUB> &gridX, vector<DOUB> &gridY, params &pars)
 {
  //NOTE: checks if a ghost has hit a wall. According to the wall, the vorticity is set to zero such that the ghost does not return to the domain
  //      At the same time, the interpolation of new vorticity is stoped as the ghost has left the domain. 
  #pragma omp parallel for
  for(int k=0; k<pars.N3; k++)
   {
    if((pos3[2*k]<gridX[0])||(pos3[2*k]>gridX[gridX.size()-1])||(pos3[2*k+1]<gridY[0])||(pos3[2*k+1]>gridY[gridY.size()-1])){pars.VorticityA3[k]=0.0;}
   }
  #pragma omp barrier
 }

void delete_ghosts(params &pars, vector<DOUB> &pos3, vector<DOUB> &vel3, DOUB t, vector<DOUB> &gridX, vector<DOUB> &gridY)
 {
  //NOTE: This function deletes ghosts/wakes.
  //      - Checks if a ghost outside the domain walls has only sub-threshold influence on the active agents or
  //        it checks if a ghost has exceeded its maximal diffusion time.
  //        If a wake is still detectable:
  //        - Keep all informations of this wake.
  //      - Resize all vectors to new size
  //      - Reinitialize all resized vectors with remaining data
  //        In this step, it is important to put orientation and velocity of x,y, theta3 in the end because the data ordering used in the time integration.
  // vectors to keep data
  vector<DOUB> UXA3_keep;                // x fluid velocity at ghost particles 
  vector<DOUB> UYA3_keep;                // y fluid velocity at ghost particles 
  vector<DOUB> absvel3_keep;             // magnitude of velocity of ghost particles at generation = abs(active agent velocity)
  vector<DOUB> pos3_keep;                // x start position of ghost particles at generation = active agent X
  vector<DOUB> vel3_keep;                // x start velocity of ghost particle at generation = active agent velocity X
  vector<DOUB> vel_theta_keep;           // to keep theta velocity of ghost
  vector<vector<int>> FDF_indexX3_keep;  // index of closest grid points X direction, ghost agents
  vector<vector<int>> FDF_indexY3_keep;  // index of closest grid points Y direction, ghost agents 
  vector<vector<DOUB>> is_boundary3_keep;// boundry map at ghost cell corners
  vector<DOUB> Tgstart_keep;             // starting times (t0) of ghost agents when they are generated
  vector<int> start_agent_keep;          // index of agent from which the which the ghost emerged
  vector<DOUB> VorticityA3_keep;         // rotation at position of ghosts
  vector<DOUB> x3t_keep;                 // x position of ghost particles at time t -> to couple vortex wake to active agents X
  vector<DOUB> y3t_keep;                 // y position of ghost particles at time t -> to couple vortex wake to active agents Y   
  vector<DOUB> theta3t_keep;             // orientation of ghost particle at time t -> to couple vortex wake to active agents (adjustment of vortex dipole axis) 

  
  DOUB rmax(0.0), delT(0.0), BC(0.0); // maximal radius of wake intensity above pars.omegmin
  int N3_new(0);             // new number of ghosts in domain
   
  for(int k=0;k<pars.N3;k++) // check which ghosts to keep
   {
    delT=t-pars.Tgstart[k]; 
    if(delT<pars.Tmax_Wake[k])// check if a vortex is still detectable (if time after generation is below maximal time at which intensity drops below threshold)
     {
      rmax = sqrt(-log(delT/pars.Tmax_Wake[k])*4.0*pars.mu2*delT);   // calculate maximum diffusion radius at which intensity of wake is at least pars.omegmin (determines pars.Tmax for all ghost agents)
      BC = pars.is_boundary3[k][0] + pars.is_boundary3[k][1] + pars.is_boundary3[k][2] + pars.is_boundary3[k][3]; // check status of adjacent cells for coastal status
      if((pos3[2*k]>gridX[0]-rmax) && (pos3[2*k]<gridX[gridX.size()-1]+rmax) && (pos3[2*k+1]>gridY[0]-rmax) && (pos3[2*k+1]<gridY[gridY.size()-1]+rmax) && (BC!=4.0)) // check if wakes can still influence agents
       {      
        UXA3_keep.push_back(pars.UXA3[k]);                   
        UYA3_keep.push_back(pars.UYA3[k]);                   
        absvel3_keep.push_back(pars.absvel3[k]);         
        pos3_keep.push_back(pos3[2*k]);                
        pos3_keep.push_back(pos3[2*k+1]);                
        vel3_keep.push_back(vel3[2*k]);               
        vel3_keep.push_back(vel3[2*k+1]);                      
        vel_theta_keep.push_back(vel3[2*pars.N3+k]);  
        FDF_indexX3_keep.push_back(pars.FDF_indexX3[k]); 
        FDF_indexY3_keep.push_back(pars.FDF_indexY3[k]);
        is_boundary3_keep.push_back(pars.is_boundary3[k]);
        Tgstart_keep.push_back(pars.Tgstart[k]); 
        start_agent_keep.push_back(pars.start_agent[k]);  
        VorticityA3_keep.push_back(pars.VorticityA3[k]);       
        x3t_keep.push_back(pars.x3t[k]);                 
        y3t_keep.push_back(pars.y3t[k]);                   
        theta3t_keep.push_back(pos3[2*pars.N3+k]);       
        N3_new +=1;
       }
     }
   }
  //resize all vectors to new number of kept ghosts
  if(N3_new != pars.N3)
   {
    pars.UXA3.resize(N3_new);
    pars.UYA3.resize(N3_new);
    pars.absvel3.resize(N3_new);
    pos3.resize(3*N3_new);//x, y, theta3
    vel3.resize(3*N3_new);// vx, vy
    pars.FDF_indexX3 = vector<vector<int>>(N3_new, vector<int>(4));//index vector for interpolation of values
    pars.FDF_indexY3 = vector<vector<int>>(N3_new, vector<int>(4));
    pars.is_boundary3 = vector<vector<DOUB>>(N3_new, vector<DOUB>(4));//coastal status
    pars.Tgstart.resize(N3_new);
    pars.start_agent.resize(N3_new);
    pars.VorticityA3.resize(N3_new);
    pars.x3t.resize(N3_new);
    pars.y3t.resize(N3_new);
    pars.N3=N3_new;
   }
  //restore data
  for(int k=0; k<N3_new; k++)
   {
    pars.UXA3[k] = UXA3_keep[k];
    pars.UYA3[k] = UYA3_keep[k];
    pars.absvel3[k] = absvel3_keep[k];
    pos3[2*k] = pos3_keep[2*k];//x
    pos3[2*k+1] = pos3_keep[2*k+1];//y
    vel3[2*k] = vel3_keep[2*k];//vx
    vel3[2*k+1] = vel3_keep[2*k+1];//vy
    pars.FDF_indexX3[k] = FDF_indexX3_keep[k];
    pars.FDF_indexY3[k] = FDF_indexY3_keep[k];
    pars.is_boundary3[k] = is_boundary3_keep[k];
    pars.Tgstart[k] = Tgstart_keep[k];
    pars.start_agent[k] = start_agent_keep[k];
    pars.VorticityA3[k] = VorticityA3_keep[k];
    pars.x3t[k] = x3t_keep[k];
    pars.y3t[k] = y3t_keep[k];
   }
  //restore angles (separately because of data ordering and handling)
  for(int k=0; k<pars.N3; k++)
   {
    pos3[2*pars.N3+k] = theta3t_keep[k];
    vel3[2*pars.N3+k] = vel_theta_keep[k];//theta vel    
   }
 }
 
 void delete_jelly(params &pars, vector<DOUB> &pos, vector<DOUB> &vel, vector<DOUB> &pos2, vector<DOUB> &vel2, vector<DOUB> &noise,  DOUB t, parsf &parsFDF, vector<DOUB> &pos0, vector<DOUB> &pos20, vector<vector<int>> &FDF_indexX0, vector<vector<int>> &FDF_indexY0, vector<vector<int>> &FDF_indexX20, vector<vector<int>> &FDF_indexY20, vector<DOUB> &freq0, int &N, int &n, DOUB Dx, DOUB &Dy, DOUB &Dangle, DOUB &Dphi, DOUB &Dangle2, DOUB &T2, int &N0, int &Nadd, random_device &generator)
  {
   DOUB BC(0.0);
   int N_new(0), delN(0), nameA(0), kadd(0);                     // new number of active agents in domain
   uniform_real_distribution<DOUB> distributionU(0.0,1.0);
   
   // vectors to keep data
   //Data
   vector<DOUB> pos_keep;                  // dynamic variables of active agents
   vector<DOUB> pos2_keep;                 // dynamic variables of passive agents
   vector<DOUB> OU_keep;                   // Ornstein-Uhlenbeck noise of agents 
   vector<DOUB> Act_keep;                  // Activity of agents
   vector<DOUB> theta_keep;                // Angle of agents
   vector<DOUB> phi_keep;                  // Phase of agents
   vector<DOUB> vel_keep;                  // velocity of agents
   vector<DOUB> vel2_keep;                 // velocity of agents
   //pars
   vector<DOUB> UXA_keep;                  // x fluid velocity at active agents  
   vector<DOUB> UYA_keep;                  // y fluid velocity at active agents 
   vector<DOUB> UXA2_keep;                 // x velocity at passive agents 
   vector<DOUB> UYA2_keep;                 // y velocity at passive agents  
   vector<vector<int>> FDF_indexX_keep;    // index of closest grid points X direction, ghost agents
   vector<vector<int>> FDF_indexY_keep;    // index of closest grid points Y direction, ghost agents 
   vector<vector<int>> FDF_indexX2_keep;   // index of CFD vector position of each passive agent X
   vector<vector<int>> FDF_indexY2_keep;   // index of CFD vector position of each passive agent Y
   vector<DOUB> freq_keep;                 // natural frequencies
   vector<DOUB> intensity_keep;            // food intensity field for agents (to be copied from CFD simulation)
   vector<DOUB> intensity_sig_keep;        // tracer intensity field for agents (to be copied from CFD simulation)
   vector<DOUB> intensity2_keep;           // intensity field for agents (to be copied from CFD simulation) passive agents
   vector<DOUB> PrA_keep;                  // Pressur at agents   
   vector<DOUB> LEVA_keep;                 // Large Eddy Viscosity 
   vector<DOUB> VorticityA_keep;           // vorticity at agents
   vector<DOUB> VorticityA2_keep;          // vorticity at passive agents
   vector<DOUB> Vorticity_gradXA_keep;     // vorticity gradient X at agents
   vector<DOUB> Vorticity_gradYA_keep;     // vorticity gradient Y at agents
   vector<DOUB> LEV_gradXA_keep;           // LEV gradient X at agents
   vector<DOUB> LEV_gradYA_keep;           // LEV gradient Y at agents
   vector<DOUB> TrgradXA_keep;             // X gradient of food intensity at agents
   vector<DOUB> TrgradYA_keep;             // Y gradient of food intensity at agents
   vector<DOUB> Tr_Sig_gradXA_keep;        // X gradient of signal intensity at agents
   vector<DOUB> Tr_Sig_gradYA_keep;        // Y gradient of signal intensity at agents
   vector<DOUB> D_pos_keep;                // component wise Diffusion coefficients dynamic variables of agents
   vector<DOUB> D_OU_keep;                 // Ornstein-Uhlenbeck noise of agents 
   vector<DOUB> D_Act_keep;                // Activity of agents
   vector<DOUB> D_theta_keep;              // Angle of agents
   vector<DOUB> D_phi_keep;                // Phase of agents
   vector<DOUB> oldphases_keep;            // vector of old phases from bell oscillations of active particles
   vector<vector<DOUB>> is_boundary_keep;  // directly adjacent boundary cells of agents indicator
   vector<vector<DOUB>> boundaryX_keep;    // directly adjacent boundary cells of agents x position
   vector<vector<DOUB>> boundaryY_keep;    // directly adjacent boundary cells of agents y position
   vector<vector<DOUB>> is_boundary2_keep; // directly adjacent boundary cells of agents indicator passive
   vector<vector<DOUB>> boundaryX2_keep;   // directly adjacent boundary cells of agents x position passive
   vector<vector<DOUB>> boundaryY2_keep;   // directly adjacent boundary cells of agents y position passive
   vector<int> agent_name_keep;            // name of kept agents 
   
   //### Beaching check ### //
   // active agents
   for(int k=0;k<N;k++) 
    {
     BC = pars.is_boundary[k][0] + pars.is_boundary[k][1] + pars.is_boundary[k][2] + pars.is_boundary[k][3]; // check status of adjacent cells for coastal status
     if(BC==0.0)//agent not near coastal cell
      {
       pos_keep.push_back(pos[2*k]);                
       pos_keep.push_back(pos[2*k+1]);                
       OU_keep.push_back(pos[(n-4)*N+k]);                
       Act_keep.push_back(pos[(n-3)*N+k]);                
       theta_keep.push_back(pos[(n-2)*N+k]);                
       phi_keep.push_back(pos[(n-1)*N+k]);              
       vel_keep.push_back(vel[2*k]);               
       vel_keep.push_back(vel[2*k+1]);                      
       UXA_keep.push_back(pars.UXA[k]);                   
       UYA_keep.push_back(pars.UYA[k]);                   
       FDF_indexX_keep.push_back(pars.FDF_indexX[k]);
       FDF_indexY_keep.push_back(pars.FDF_indexY[k]);
       freq_keep.push_back(pars.freq[k]);                  
       intensity_keep.push_back(pars.intensity[k]);
       intensity_sig_keep.push_back(pars.intensity_sig[k]);
       PrA_keep.push_back(pars.PrA[k]);
       LEVA_keep.push_back(pars.LEVA[k]);
       VorticityA_keep.push_back(pars.VorticityA[k]);
       Vorticity_gradXA_keep.push_back(pars.Vorticity_gradXA[k]);
       Vorticity_gradYA_keep.push_back(pars.Vorticity_gradYA[k]);
       LEV_gradXA_keep.push_back(pars.LEV_gradXA[k]);
       LEV_gradYA_keep.push_back(pars.LEV_gradYA[k]);
       TrgradXA_keep.push_back(pars.TrgradXA[k]);
       TrgradYA_keep.push_back(pars.TrgradYA[k]);
       Tr_Sig_gradXA_keep.push_back(pars.Tr_Sig_gradXA[k]);
       Tr_Sig_gradYA_keep.push_back(pars.Tr_Sig_gradYA[k]);
       D_pos_keep.push_back(pars.D[2*k]);                     // component wise Diffusion coefficients dynamic variables of agents
       D_pos_keep.push_back(pars.D[2*k+1]);                   // component wise Diffusion coefficients dynamic variables of agents
       D_OU_keep.push_back(pars.D[(n-4)*N+k]);                // Ornstein-Uhlenbeck noise of agents 
       D_Act_keep.push_back(pars.D[(n-3)*N+k]);               // Activity of agents
       D_theta_keep.push_back(pars.D[(n-2)*N+k]);             // Angle of agents
       D_phi_keep.push_back(pars.D[(n-1)*N+k]);               // Phase of agents
       oldphases_keep.push_back(pars.oldphases[k]);
       is_boundary_keep.push_back(pars.is_boundary[k]);
       boundaryX_keep.push_back(pars.boundaryX[k]);
       boundaryY_keep.push_back(pars.boundaryY[k]);
       agent_name_keep.push_back(pars.agent_name[k]);
       N_new +=1;
      }
     else
      {
       cout << "deleting agent " << pars.agent_name[k] << "\n";
       for(int l=0; l<pars.N3; l++)
        {
         if(pars.start_agent[l] == pars.agent_name[k]){pars.start_agent[l] = -1;}
        }
      }
    }
   
   // passive agents 
   cout << "checking passive agents \n";
   if(N_new != 0)
    {
     for(int k=0;k<N;k++) // check which active agents to keep
      {
       BC = pars.is_boundary2[k][0] + pars.is_boundary2[k][1] + pars.is_boundary2[k][2] + pars.is_boundary2[k][3]; // check status of adjacent cells for coastal status always as many passive as active agents, by construction
       if(BC==0.0)//agent not near coastal cell
        {
         pos2_keep.push_back(pos2[2*k]);                
         pos2_keep.push_back(pos2[2*k+1]);                
         vel2_keep.push_back(vel2[2*k]);               
         vel2_keep.push_back(vel2[2*k+1]);
         UXA2_keep.push_back(pars.UXA2[k]);                   
         UYA2_keep.push_back(pars.UYA2[k]);                   
         FDF_indexX2_keep.push_back(pars.FDF_indexX2[k]);
         FDF_indexY2_keep.push_back(pars.FDF_indexY2[k]);
         intensity2_keep.push_back(pars.intensity2[k]);
         VorticityA2_keep.push_back(pars.VorticityA2[k]);
         is_boundary2_keep.push_back(pars.is_boundary2[k]);
         boundaryX2_keep.push_back(pars.boundaryX2[k]);
         boundaryY2_keep.push_back(pars.boundaryY2[k]);
        }
       else
        {
         kadd = int(DOUB(N_new-1)*distributionU(generator));//choose randomly from inital agents
         pos2_keep.push_back(pos_keep[2*kadd]);                
         pos2_keep.push_back(pos_keep[2*kadd+1]);                
         vel2_keep.push_back(vel_keep[2*kadd]);               
         vel2_keep.push_back(vel_keep[2*kadd+1]);
         UXA2_keep.push_back(UXA_keep[kadd]);                   
         UYA2_keep.push_back(UYA_keep[kadd]);                   
         FDF_indexX2_keep.push_back(FDF_indexX_keep[kadd]);
         FDF_indexY2_keep.push_back(FDF_indexY_keep[kadd]);
         intensity2_keep.push_back(intensity_keep[kadd]);
         VorticityA2_keep.push_back(VorticityA_keep[kadd]);
         is_boundary2_keep.push_back(is_boundary_keep[kadd]);
         boundaryX2_keep.push_back(boundaryX_keep[kadd]);
         boundaryY2_keep.push_back(boundaryY_keep[kadd]);
         cout << "resetting passive agent " << pars.agent_name[k] << "\n";
        }
      }

     //### determine what the largest agent name is ###//
     cout << "determine largest agent name \n";

     for(int k=0; k<agent_name_keep.size();k++){if(agent_name_keep[k]>nameA){nameA=agent_name_keep[k];}}
     nameA +=1;
    }
     
   delN=Nadd-N_new;  
   if(delN>int(0.9*DOUB(N0))){delN=int(0.9*DOUB(N0));}
   
   //### add additional agents ###//
   if(delN != 0)
    {
//     delN=Nadd-N_new;
     cout << " N_new= " << N_new << " Nadd= " << Nadd << " adding " << delN << " new agents, largest agent name is " << nameA << "\n";
     for(int k=0;k<delN;k++)
      {
       kadd = k;//int(DOUB(N0-1)*distributionU(generator));//choose randomly from inital agents
       cout << "choosing new agent with index " << kadd << "\n";
       //active agents
       pos_keep.push_back(pos0[2*kadd]);                
       pos_keep.push_back(pos0[2*kadd+1]);                
       OU_keep.push_back(pos0[(n-4)*N0+kadd]);                
       Act_keep.push_back(pos0[(n-3)*N0+kadd]);                
       theta_keep.push_back(pos0[(n-2)*N0+kadd]);                
       phi_keep.push_back(pos0[(n-1)*N0+kadd]);            
       cout << "added variables \n";  
       vel_keep.push_back(0.0);               
       vel_keep.push_back(0.0);                      
       UXA_keep.push_back(0.0);                   
       UYA_keep.push_back(0.0);                   
       FDF_indexX_keep.push_back(FDF_indexX0[kadd]);
       FDF_indexY_keep.push_back(FDF_indexY0[kadd]);            
       cout << "added index and vel \n";
       freq_keep.push_back(freq0[kadd]);                  
       intensity_keep.push_back(0.0);
       intensity_sig_keep.push_back(0.0);
       PrA_keep.push_back(0.0);
       LEVA_keep.push_back(0.0);
       VorticityA_keep.push_back(0.0);
       Vorticity_gradXA_keep.push_back(0.0);
       Vorticity_gradYA_keep.push_back(0.0);
       LEV_gradXA_keep.push_back(0.0);
       LEV_gradYA_keep.push_back(0.0);
       TrgradXA_keep.push_back(0.0);
       TrgradYA_keep.push_back(0.0);
       Tr_Sig_gradXA_keep.push_back(0.0);
       Tr_Sig_gradYA_keep.push_back(0.0);
       D_pos_keep.push_back(Dx);                     // component wise Diffusion coefficients dynamic variables of agents
       D_pos_keep.push_back(Dy);                     // component wise Diffusion coefficients dynamic variables of agents
       D_OU_keep.push_back(Dangle);                  // Ornstein-Uhlenbeck noise of agents 
       D_Act_keep.push_back(0.0);                    // Activity of agents
       D_theta_keep.push_back(Dangle2);              // Angle of agents
       D_phi_keep.push_back(Dphi);                   // Phase of agents
       oldphases_keep.push_back(pos0[(n-1)*N0+kadd]-0.01);
       cout << "added old phase \n";
       is_boundary_keep.push_back({0,0,0,0});
       boundaryX_keep.push_back({0.0,0.0,0.0,0.0});
       boundaryY_keep.push_back({0.0,0.0,0.0,0.0});
       agent_name_keep.push_back(nameA);
       
       //passive agents
       pos2_keep.push_back(pos20[2*kadd]);                
       pos2_keep.push_back(pos20[2*kadd+1]);      
       cout << "added variables passive \n";          
       vel2_keep.push_back(0.0);               
       vel2_keep.push_back(0.0);
       UXA2_keep.push_back(0.0);                   
       UYA2_keep.push_back(0.0);                   
       FDF_indexX2_keep.push_back(pars.FDF_indexX20[kadd]);
       FDF_indexY2_keep.push_back(pars.FDF_indexY20[kadd]);            
       cout << "added index passive \n";
       intensity2_keep.push_back(0.0);
       VorticityA2_keep.push_back(0.0);
       is_boundary2_keep.push_back({0,0,0,0});
       boundaryX2_keep.push_back({0.0,0.0,0.0,0.0});
       boundaryY2_keep.push_back({0.0,0.0,0.0,0.0});

       N_new +=1;//overall number of agents
       nameA +=1;//agent name labeling index
      }
    }
    
   //### Update number of active agents ###//
   N=N_new;
   cout << " N_new= " << N_new << " num_active= " << pars.UXA.size() << " num passive= " << pars.UXA2.size() << "\n";
   
   //### Resizing of vectors ###//
   // active agents 
   noise.resize(n*N);
   pos.resize(n*N);
   vel.resize(n*N);
   pars.UXA.resize(N);
   pars.UYA.resize(N);                    
   pars.FDF_indexX = vector<vector<int>>(N, vector<int>(4));
   pars.FDF_indexY = vector<vector<int>>(N, vector<int>(4));
   pars.freq.resize(N);
   pars.intensity.resize(N);
   pars.intensity_sig.resize(N);
   pars.PrA.resize(N);
   pars.LEVA.resize(N);
   pars.VorticityA.resize(N);
   pars.Vorticity_gradXA.resize(N);
   pars.Vorticity_gradYA.resize(N);
   pars.LEV_gradXA.resize(N);
   pars.LEV_gradYA.resize(N);
   pars.TrgradXA.resize(N);
   pars.TrgradYA.resize(N);
   pars.Tr_Sig_gradXA.resize(N);
   pars.Tr_Sig_gradYA.resize(N);
   pars.D.resize(n*N);
   pars.oldphases.resize(N);
   pars.is_boundary = vector<vector<DOUB>>(N, vector<DOUB>(4));
   pars.boundaryX = vector<vector<DOUB>>(N, vector<DOUB>(4));
   pars.boundaryY = vector<vector<DOUB>>(N, vector<DOUB>(4));
   pars.agent_name.resize(N);
   
   // passive agents
   pos2.resize(2*N);
   vel2.resize(2*N);           
   pars.UXA2.resize(N);                   
   pars.UYA2.resize(N);       
   pars.FDF_indexX2 = vector<vector<int>>(N, vector<int>(4));
   pars.FDF_indexY2 = vector<vector<int>>(N, vector<int>(4));
   pars.intensity2.resize(N);
   pars.VorticityA2.resize(N);
   pars.is_boundary2 = vector<vector<DOUB>>(N, vector<DOUB>(4));
   pars.boundaryX2 = vector<vector<DOUB>>(N, vector<DOUB>(4));
   pars.boundaryY2 = vector<vector<DOUB>>(N, vector<DOUB>(4));
   
   //### Refilling of vectors ###//
   // active agents
   for(int k=0;k<N;k++) 
    {
     pos[2*k] = pos_keep[2*k];                
     pos[2*k+1] = pos_keep[2*k+1];                
     vel[2*k] = vel_keep[2*k];               
     vel[2*k+1] = vel_keep[2*k+1];                      
     pars.UXA[k] = UXA_keep[k];                   
     pars.UYA[k] = UYA_keep[k];                   
     pars.FDF_indexX[k] = FDF_indexX_keep[k];
     pars.FDF_indexY[k] = FDF_indexY_keep[k];
     pars.freq[k] = freq_keep[k];                  
     pars.intensity[k] = intensity_keep[k];
     pars.intensity_sig[k] = intensity_sig_keep[k];
     pars.PrA[k] = PrA_keep[k];
     pars.LEVA[k] = LEVA_keep[k];
     pars.VorticityA[k] = VorticityA_keep[k];
     pars.Vorticity_gradXA[k] = Vorticity_gradXA_keep[k];
     pars.Vorticity_gradYA[k] = Vorticity_gradYA_keep[k];
     pars.LEV_gradXA[k] = LEV_gradXA_keep[k];
     pars.LEV_gradYA[k] = LEV_gradYA_keep[k];
     
     pars.TrgradXA[k] = TrgradXA_keep[k];
     pars.TrgradYA[k] = TrgradYA_keep[k];
     pars.Tr_Sig_gradXA[k] = Tr_Sig_gradXA_keep[k];
     pars.Tr_Sig_gradYA[k] = Tr_Sig_gradYA_keep[k];
     pars.D[2*k] = D_pos_keep[2*k];
     pars.D[2*k+1] = D_pos_keep[2*k+1];
     pars.oldphases[k] = oldphases_keep[k];
     pars.is_boundary[k] = is_boundary_keep[k];
     pars.boundaryX[k] = boundaryX_keep[k];
     pars.boundaryY[k] = boundaryY_keep[k];
     pars.agent_name[k] = agent_name_keep[k];
    }

   for(int k=0;k<N;k++)
    {
     pos[(n-4)*N+k] = OU_keep[k];        // Ornstein-Uhlenbeck                
     pos[(n-3)*N+k] = Act_keep[k];       // Activity            
     pos[(n-2)*N+k] = theta_keep[k];     // angle             
     pos[(n-1)*N+k] = phi_keep[k];       // phase
     pars.D[(n-4)*N+k] = D_OU_keep[k];   // diffusion coefficients of Ornstein Uhlenbeck noise
     pars.D[(n-3)*N+k] = D_Act_keep[k];  // Activity
     pars.D[(n-2)*N+k] = D_theta_keep[k];// angle 
     pars.D[(n-1)*N+k] = D_phi_keep[k];  // phase
    }
    
   //passive agents
   for(int k=0;k<N;k++) 
    {
     pos2[2*k] = pos2_keep[2*k];                
     pos2[2*k+1] = pos2_keep[2*k+1];                     
     vel2[2*k] = vel2_keep[2*k];               
     vel2[2*k+1] = vel2_keep[2*k+1];
     pars.UXA2[k] = UXA2_keep[k];                   
     pars.UYA2[k] = UYA2_keep[k];                  
     pars.FDF_indexX2[k] = FDF_indexX2_keep[k];
     pars.FDF_indexY2[k] = FDF_indexY2_keep[k];
     pars.VorticityA2[k] = VorticityA2_keep[k];
     pars.intensity2[k] = intensity2_keep[k];
     pars.is_boundary2[k] = is_boundary2_keep[k];
     pars.boundaryX2[k] = boundaryX2_keep[k];
     pars.boundaryY2[k] = boundaryY2_keep[k];
    }
   cout << " finished beaching \n";
 }
  
 void Agent_to_CFD_coupling(params &pars, vector<DOUB> &pos3, vector<DOUB> &vel3, DOUB t, vector<DOUB> &pos, vector<DOUB> &vel, vector<DOUB> &gridX, vector<DOUB> &gridY, vector<DOUB> &intensity_sig_back_coupler, vector<vector<int>> &FDF_indexX3, vector<vector<int>> &FDF_indexY3, int &Nfx, int &Nfy, parsf &parsFDF, DOUB &dt, int &N, int &n, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY, vector<DOUB> &intensity, vector<DOUB> &intensity_back_coupler, vector<DOUB> &intensity_sig)
  {//TODO: check if coupling destroys simulation stability
   //NOTE: This function shifts the presence of wakes to a back-exchange grid
   //      - Iterates through all wakes and checks if they will be deleted (next step in main integration loop)
   //      - Iterates through all active agents and checks the activity, spreads food consumption accordingly
   //      - Uses lower left corner of the enclosing cell of an agent (FDF_indexX/Y3[.][0]) and defines square window of coupling.
   //        Here it is necessary to make sure that the coupling kernel has decayed within the coupling window!
   //      - Iterates through the window and shifts positional information to the back-exchange grid (Here several exchange grids can be implemented)
   DOUB delT(0.0), R2(0.0);//, P(0.0), S1(0.0), xp1(0.0), xp2(0.0), xm1(0.0), xm2(0.0), yp1(0.0), yp2(0.0), ym1(0.0), ym2(0.0);

   //release of signaling tracer due to wakes/ghosts
   int nxi(0), nxf(0), nyi(0), nyf(0), delx(2*int(pars.spread_width/parsFDF.dx)), dely(2*int(pars.spread_width/parsFDF.dy));//2 meters of spreading (bandwidth)
  // {   
//  #pragma omp parallel for private (delT, R2, nxi, nxf, nyi, nyf, delx, dely)
   for(int k=0;k<pars.N3;k++) // check which ghosts are passed to the CFD grid
    {
     //Determine how many agents
     delT=t-pars.Tgstart[k];
     if(delT>=pars.Tmax_Wake[k])// check if a vortex will be deleted from resolved advection (if time after generation is above maximal time at which vorticity drops below threshold)
      {
       //set index boundaries for spreading (+1 in final index to make interpolation grid-symmetric as always lower left corner is used)
       nxi = FDF_indexX3[k][0]-delx;
       nxf = FDF_indexX3[k][0]+delx+1;
       nyi = FDF_indexY3[k][0]-dely;
       nyf = FDF_indexY3[k][0]+dely+1;
       //if(nxi<1){nxi=1;}
       //if(nxf>Nfx){nxf=Nfx;}
       //if(nyi<1){nyi=1;}
       //if(nyf>Nfy){nyf=Nfy;}
       if(nxi<0){nxi=0;}
       if(nxf>Nfx-1){nxf=Nfx-1;}
       if(nyi<0){nyi=0;}
       if(nyf>Nfy-1){nyf=Nfy-1;}
       //iterate through window
       for(int l=nxi;l<nxf+1;l++)
        {
         for(int m=nyi;m<nyf+1;m++)
          {
           //R2= (gridX[l-1]-pos3[2*k])*(gridX[l-1]-pos3[2*k]) + (gridY[m-1]-pos3[2*k+1])*(gridY[m-1]-pos3[2*k+1]);// calclate distance, -1 in grid index because grid is smaller array and starts with index 0
           R2= (gridX[l]-pos3[2*k])*(gridX[l]-pos3[2*k]) + (gridY[m]-pos3[2*k+1])*(gridY[m]-pos3[2*k+1]);// calclate distance, -1 in grid index because grid is smaller array and starts with index 0
           //P = 0.5*pars.irad;
           //S1 = 4.0*pars.mu2*(t-pars.Tgstart[k]+0.01);
           //xp1 = pos3[2*k]  -P*sin(pars.theta3t[k]);
           //yp1 = pos3[2*k+1]+P*cos(pars.theta3t[k]);
           //xm1 = pos3[2*k]  +P*sin(pars.theta3t[k]);
           //ym1 = pos3[2*k+1]-P*cos(pars.theta3t[k]);
           //xp2 = pos3[2*k]  -P*sin(pars.theta3t[k]) + 0.05*cos(pars.theta3t[k]);
           //yp2 = pos3[2*k+1]+P*cos(pars.theta3t[k]) + 0.05*sin(pars.theta3t[k]);
           //xm2 = pos3[2*k]  +P*sin(pars.theta3t[k]) + 0.05*cos(pars.theta3t[k]);
           //ym2 = pos3[2*k+1]-P*cos(pars.theta3t[k]) + 0.05*sin(pars.theta3t[k]);
           //intensity_sig_back_coupler[l*(Nfy+2)+m] += pars.sig_cfd_couple*(abs((-exp((-(gridX[l-1]-xp1)*(gridX[l-1]-xp1)-(gridY[m-1]-yp1)*(gridY[m-1]-yp1))/S1) + exp((-(gridX[l-1]-xm1)*(gridX[l-1]-xm1)-(gridY[m-1]-ym1)*(gridY[m-1]-ym1))/S1) + exp((-(gridX[l-1]-xp2)*(gridX[l-1]-xp2)-(gridY[m-1]-yp2)*(gridY[m-1]-yp2))/S1) - exp((-(gridX[l-1]-xm2)*(gridX[l-1]-xm2)-(gridY[m-1]-ym2)*(gridY[m-1]-ym2))/S1))));
           //intensity_sig_back_coupler[l*(Nfy+2)+m] += (pars.sig_cfd_couple/(1.0*pi*pars.spread_width*pars.spread_width))*exp(-R2/(1.0*pars.spread_width*pars.spread_width)); //*(1.0-intensity_sig_back_coupler[l*(Nfy+2)+m]); // spread coupling to grid point
           intensity_sig_back_coupler[(l+1)*(Nfy+2)+m+1] += (pars.sig_cfd_couple/(1.0*pi*pars.spread_width*pars.spread_width))*exp(-R2/(1.0*pars.spread_width*pars.spread_width)); //*(1.0-intensity_sig_back_coupler[l*(Nfy+2)+m]); // spread coupling to grid point
           //cout << "weight= " << pars.sig_cfd_couple*exp(-R2) << "\n";
           //if(intensity_sig_back_coupler[l*(Nfy+2)+m] + intensity_sig[l*(Nfy+2)+m]>1.0)
           // {
           //  intensity_sig_back_coupler[l*(Nfy+2)+m] = 1.0 - intensity_sig[l*(Nfy+2)+m];
           // }
           if(intensity_sig_back_coupler[(l+1)*(Nfy+2)+m+1] + intensity_sig[(l+1)*(Nfy+2)+m+1]>1.0)
            {
             intensity_sig_back_coupler[(l+1)*(Nfy+2)+m+1] = 1.0 - intensity_sig[(l+1)*(Nfy+2)+m+1];
            }
          }
        }
      }
    }//off for int k
 //  #pragma omp barrier
  // }
  
   //consumption of food tracer when jellyfish is present
   //{
//   #pragma omp parallel for private (delT, R2, nxi, nxf, nyi, nyf, delx, dely) 
   for(int k=0; k<N; k++)
    {
     nxi = FDF_indexX[k][0]-delx;
     nxf = FDF_indexX[k][0]+delx+1;
     nyi = FDF_indexY[k][0]-dely;
     nyf = FDF_indexY[k][0]+dely+1;
     //if(nxi<1){nxi=1;}
     //if(nxf>Nfx){nxf=Nfx;}
     //if(nyi<1){nyi=1;}
     //if(nyf>Nfy){nyf=Nfy;}
     if(nxi<0){nxi=1;}
     if(nxf>Nfx-1){nxf=Nfx-1;}
     if(nyi<0){nyi=1;}
     if(nyf>Nfy-1){nyf=Nfy-1;}
     //iterate through window
     for(int l=nxi;l<nxf+1;l++)
      {
       for(int m=nyi;m<nyf+1;m++)
        {//intensity_back_coupler[l*(Nfy+2)+m]*
         //R2= (gridX[l-1]-pos[2*k])*(gridX[l-1]-pos[2*k]) + (gridY[m-1]-pos[2*k+1])*(gridY[m-1]-pos[2*k+1]);// calclate distance
         R2= (gridX[l]-pos[2*k])*(gridX[l]-pos[2*k]) + (gridY[m]-pos[2*k+1])*(gridY[m]-pos[2*k+1]);// calclate distance
         //intensity_back_coupler[l*(Nfy+2)+m] -= intensity[l*(Nfy+2)+m]*(pars.food_cfd_couple/(1.0*pi*pars.spread_width*pars.spread_width))*exp(-R2/(1.0*pars.spread_width*pars.spread_width));//*intensity[l*(Nfy+2)+m];// spread coupling to grid point TODO: implement dependence on activity according to papers
         intensity_back_coupler[(l+1)*(Nfy+2)+m+1] -= intensity[(l+1)*(Nfy+2)+m+1]*(pars.food_cfd_couple/(1.0*pi*pars.spread_width*pars.spread_width))*exp(-R2/(1.0*pars.spread_width*pars.spread_width));//*intensity[l*(Nfy+2)+m];// spread coupling to grid point TODO: implement dependence on activity according to papers
         //cout << " shifting array: t= " << t << " index " << l*(Nfy+2)+m << " " << R2 << " " << intensity_back_coupler[l*(Nfy+2)+m] << "\n";
         //if(intensity_back_coupler[l*(Nfy+2)+m] + intensity[l*(Nfy+2)+m]<0.0)
         // {
         //  intensity_back_coupler[l*(Nfy+2)+m ] = 0.0;
         // }
        }
      }
    }
//   #pragma omp barrier
   //}
  }

 void Agent_to_CFD_coupling_reset(params &pars, vector<DOUB> &pos3, vector<DOUB> &vel3, DOUB t, vector<DOUB> &pos, vector<DOUB> &vel, vector<DOUB> &gridX, vector<DOUB> &gridY, vector<DOUB> &intensity_sig_back_coupler, vector<vector<int>> &FDF_indexX3, vector<vector<int>> &FDF_indexY3, int &Nfx, int &Nfy, parsf &parsFDF, DOUB &dt, vector<DOUB> &intensity_back_coupler)
  {
   //NOTE: Resets fluxes of tracer from intermediate advection (ghosts) to CFD level and for food consumtion from tracer advection
   //      -+1, +2 and +1 because FDF index maps only in the particle domain while vector also contains boundary cells, thus shifting is needed
   #pragma omp parallel for
   for(int l=0;l<Nfx+1;l++)
    {
     for(int m=0;m<Nfy+1;m++)
      {
       intensity_back_coupler[l*(Nfy+2)+m] = 0.0;     //food exchange field reset
       intensity_sig_back_coupler[l*(Nfy+2)+m] = 0.0; //signaling tracer
      }
    }
   #pragma omp barrier
  }

void NS_FORCE_TRACER(vector<DOUB> &intensity, vector<DOUB> &intensity_sig, vector<DOUB> &intensity_sig_back_coupler, vector<DOUB> &intensity_back_coupler, int &Nfx, int &Nfy)
 {
  //NOTE: Coupling of CFD tracers to presence of agents
  for(int k=1;k<Nfx+1;k++)
   {
    for(int l=1;l<Nfy+1;l++)
     {
      intensity[k*(Nfy+2)+l] += intensity_back_coupler[k*(Nfy+2)+l];         // sink of food tracer due to jellyfish presence.
      intensity_sig[k*(Nfy+2)+l] += intensity_sig_back_coupler[k*(Nfy+2)+l]; // shifting of signaling tracer to cfd grid      
     }
   }
 }
  
void Data_Print_Ghosts(DOUB &t, Vectordata &Data, params &pars, ofstream &fout10, vector<int> &Cols)  
 {
  //NOTE: This function prints the position and other informations regarding the wakes (ghosts). If no wakes are present, the code returns one position with all entries zero
  if(pars.N3==0)
   {
    fout10 << 0.0;
    for(int l=0;l<Cols.size();l++){if(Cols[l]==1){fout10 << " " << 0.0;}} 
    fout10 << "\n";
    fout10 << "\n";
   }
  for(int k=0; k<pars.N3; k++)
   {
    DOUB P(0.5*pars.irad); 
    DOUB xp1(pars.x3t[k]-P*sin(pars.theta3t[k])), yp1(pars.y3t[k]+P*cos(pars.theta3t[k]));
    DOUB xm1(pars.x3t[k]+P*sin(pars.theta3t[k])), ym1(pars.y3t[k]-P*cos(pars.theta3t[k]));
    DOUB xp2(pars.x3t[k]-P*sin(pars.theta3t[k]) + 0.05*cos(pars.theta3t[k])), yp2(pars.y3t[k]+P*cos(pars.theta3t[k]) + 0.05*sin(pars.theta3t[k]));
    DOUB xm2(pars.x3t[k]+P*sin(pars.theta3t[k]) + 0.05*cos(pars.theta3t[k])), ym2(pars.y3t[k]-P*cos(pars.theta3t[k]) + 0.05*sin(pars.theta3t[k])); 
    fout10 << t;
    if(Cols[0]==1){fout10 << " " << Data.pos3[2*k];}
    if(Cols[1]==1){fout10 << " " << Data.pos3[2*k+1];} 
    if(Cols[2]==1){fout10 << " " << Data.pos3[2*pars.N3+k];}
    if(Cols[3]==1){fout10 << " " << Data.vel3[2*k];}
    if(Cols[4]==1){fout10 << " " << Data.vel3[2*k+1];} 
    if(Cols[5]==1){fout10 << " " << Data.vel3[2*pars.N3+k];}
    if(Cols[6]==1){fout10 << " " << pars.Tgstart[k];}
    if(Cols[7]==1){fout10 << " " << pars.VorticityA3[k];}
    if(Cols[8]==1){fout10 << " " << pars.UXA3[k];}
    if(Cols[9]==1){fout10 << " " << pars.UYA3[k];}
    if(Cols[10]==1){fout10 << " " << xp1;}
    if(Cols[11]==1){fout10 << " " << yp1;}
    if(Cols[12]==1){fout10 << " " << xm1;}
    if(Cols[13]==1){fout10 << " " << ym1;}
    if(Cols[14]==1){fout10 << " " << xp2;}
    if(Cols[15]==1){fout10 << " " << yp2;}
    if(Cols[16]==1){fout10 << " " << xm2;}
    if(Cols[17]==1){fout10 << " " << ym2;}
    if(Cols[18]==1){fout10 << " " << pars.start_agent[k];}
    fout10 << "\n";
    //<< "\n";//ouptut x,y, and angle of self drift velocity of vortex (theta3(t))
   // fout10 << t << " " << Data.pos3[2*k] << " " << Data.pos3[2*k+1] << " " << Data.pos3[2*pars.N3+k] << " " << Data.vel3[2*k] << " " << Data.vel3[2*k+1] << " " << Data.vel3[2*pars.N3+k] << " " << pars.Tgstart[k] << " " << pars.VorticityA3[k] << " " << pars.UXA3[k] << " " << pars.UYA3[k] << " " << xp1 << " " << yp1 << " " << xm1 << " " << ym1 << " " << xp2 << " " << yp2 << " " << xm2 << " " << ym2 << " " << pars.start_agent[k] << "\n";//ouptut x,y, and angle of self drift velocity of vortex (theta3(t))
   }     //<< " " << pars.FDF_indexX3[k][0] << " " << pars.FDF_indexY3[k][0] << " " << pars.FDF_indexX3[k][1] << " " << pars.FDF_indexY3[k][1] << " " << pars.FDF_indexX3[k][2] << " " << pars.FDF_indexY3[k][2] << " " << pars.FDF_indexX3[k][3] << " " << pars.FDF_indexY3[k][3] 
  fout10 << "\n";  
 } 
  
void Save_Init(ofstream &fout11, FDFS &FDF, parsf &parsFDF)
 {
  //NOTE: This function saves the initialization data.
  //      - First entries are the sizes of the respective vectors
  //      - All following entries are the initial conditions in same order
  //### print out dimensions of each single array ###//
  fout11 << FDF.gridX.size() << "\n"; //0
  fout11 << FDF.gridY.size() << "\n"; //1
  fout11 << FDF.PrgridX.size() << "\n"; //2
  fout11 << FDF.PrgridY.size() << "\n"; //3
  fout11 << FDF.UX.size() << "\n"; //4
  fout11 << FDF.UY.size() << "\n"; //5
  fout11 << FDF.intensity.size() << "\n"; //6
  fout11 << FDF.intensity_sig.size() << "\n"; //7
  fout11 << FDF.intensity_sig_back_coupler.size() << "\n"; //8
  fout11 << FDF.Pr.size() << "\n"; //9
  fout11 << FDF.Pri.size() << "\n"; //10
  fout11 << FDF.Pri2.size() << "\n"; //11
  fout11 << FDF.vorticity.size() << "\n"; //12
  fout11 << FDF.vorticitygradX.size() << "\n"; //13
  fout11 << FDF.vorticitygradY.size() << "\n"; //14
  fout11 << FDF.divergence.size() << "\n"; //15
  fout11 << FDF.TrgradX.size() << "\n"; //16
  fout11 << FDF.TrgradY.size() << "\n"; //17
  fout11 << FDF.Tr_Sig_gradX.size() << "\n"; //18
  fout11 << FDF.Tr_Sig_gradY.size() << "\n"; //19
  fout11 << parsFDF.UU.size() << "\n"; //20
  fout11 << parsFDF.UL.size() << "\n"; //21
  fout11 << parsFDF.ULE.size() << "\n"; //22
  fout11 << parsFDF.UR.size() << "\n"; //23
  fout11 << parsFDF.VU.size() << "\n"; //24
  fout11 << parsFDF.VL.size() << "\n"; //25
  fout11 << parsFDF.VLE.size() << "\n"; //26
  fout11 << parsFDF.VR.size() << "\n"; //27
  
  //### saving of all vectors in a large file ###/
  for(int k=0;k<FDF.gridX.size();k++){fout11 << FDF.gridX[k] << "\n";}                                           // position of grid points X in Agent domain (grid without halo cells)
  for(int k=0;k<FDF.gridY.size();k++){fout11 << FDF.gridY[k] << "\n";}                                           // position of grid points Y in Agent domain (grid without halo cells)
  for(int k=0;k<FDF.PrgridX.size();k++){fout11 << FDF.PrgridX[k] << "\n";}                                       // position of grid points X in CFD simulation for pressur (including halo cells for boundaries)
  for(int k=0;k<FDF.PrgridY.size();k++){fout11 << FDF.PrgridY[k] << "\n";}                                       // position of grid points Y in CFD simulation for pressur (including halo cells for boundaries)
  for(int k=0;k<FDF.UX.size();k++){fout11 << FDF.UX[k] << "\n";}                                                 // fluid velocity in x direction
  for(int k=0;k<FDF.UY.size();k++){fout11 << FDF.UY[k] << "\n";}                                                 // fluid velocity in y direction
  for(int k=0;k<FDF.intensity.size();k++){fout11 << FDF.intensity[k] << "\n";}                                   // trancer intensity
  for(int k=0;k<FDF.intensity_sig.size();k++){fout11 << FDF.intensity_sig[k] << "\n";}                           // signaling trancer intensity
  for(int k=0;k<FDF.intensity_sig_back_coupler.size();k++){fout11 << FDF.intensity_sig_back_coupler[k] << "\n";} // array holding values from the agent-dynamics level (forcing of advection diffusion system)
  for(int k=0;k<FDF.Pr.size();k++){fout11 << FDF.Pr[k] << "\n";}                                                 // pressur of fluid
  for(int k=0;k<FDF.Pri.size();k++){fout11 << FDF.Pri[k] << "\n";}                                               // intermediate pressur of fluid (for Heuns method in time stepping)
  for(int k=0;k<FDF.Pri2.size();k++){fout11 << FDF.Pri2[k] << "\n";}                                             // intermediate pressur of fluid
  for(int k=0;k<FDF.vorticity.size();k++){fout11 << FDF.vorticity[k] << "\n";}                                   // vorticity of fluid 
  for(int k=0;k<FDF.vorticitygradX.size();k++){fout11 << FDF.vorticitygradX[k] << "\n";}                         // vorticity gradX of fluid 
  for(int k=0;k<FDF.vorticitygradY.size();k++){fout11 << FDF.vorticitygradY[k] << "\n";}                         // vorticity gradY of fluid 
  for(int k=0;k<FDF.divergence.size();k++){fout11 << FDF.divergence[k] << "\n";}                                 // divergence of fluid 
  for(int k=0;k<FDF.TrgradX.size();k++){fout11 << FDF.TrgradX[k] << "\n";}                                       // X gradient of tracer on CFD grid
  for(int k=0;k<FDF.TrgradY.size();k++){fout11 << FDF.TrgradY[k] << "\n";}                                       // Y gradient of tracer on CFD grid
  for(int k=0;k<FDF.Tr_Sig_gradX.size();k++){fout11 << FDF.Tr_Sig_gradX[k] << "\n";}                             // X gradient of signal tracer on CFD grid
  for(int k=0;k<FDF.Tr_Sig_gradY.size();k++){fout11 << FDF.Tr_Sig_gradY[k] << "\n";}                             // Y gradient of signal tracer on CFD grid
  for(int k=0;k<parsFDF.UU.size();k++){fout11 << parsFDF.UU[k] << "\n";}                                           // boundary X velocity of upper boundary
  for(int k=0;k<parsFDF.UL.size();k++){fout11 << parsFDF.UL[k] << "\n";}                                           // boundary X velocity of lower boundary
  for(int k=0;k<parsFDF.ULE.size();k++){fout11 << parsFDF.ULE[k] << "\n";}                                         // boundary X velocity of left boundary
  for(int k=0;k<parsFDF.UR.size();k++){fout11 << parsFDF.UR[k] << "\n";}                                           // boundary X velocity of right boundary
  for(int k=0;k<parsFDF.VU.size();k++){fout11 << parsFDF.VU[k] << "\n";}                                           // boundary V velocity of upper boundary
  for(int k=0;k<parsFDF.VL.size();k++){fout11 << parsFDF.VL[k] << "\n";}                                           // boundary V velocity of lower boundary
  for(int k=0;k<parsFDF.VLE.size();k++){fout11 << parsFDF.VLE[k] << "\n";}                                         // boundary V velocity of left boundary
  for(int k=0;k<parsFDF.VR.size()-1;k++){fout11 << parsFDF.VR[k] << "\n";}                                         // boundary V velocity of right boundary
  fout11 << parsFDF.VR[parsFDF.VR.size()-1];
 }

 void Read_Init_2(string dat, FDFS &FDF, parsf &parsFDF)
 {
  //NOTE: This function does the following:
  //      - Reading of initial CFD fields from transient file.
  //        The data ordering in th einit file is hard coded as seen below!
  //        The dimensions of initial data and in the running instance need to be the same!
  //        The first lines of the init file contain the length of the respective CFD vectors as used in the initialization run
  //      - The code generates a mapping to the adresses of the respective CFD vectors currently initialized
  //      - Stores the initialization data to a vector
  //      - iterates through the memory mapping and accesses the respective (making use of contiguous memory, pointer arythmetics)
  //        In this step, iteration through the memory and iteration through the read data are synchronized.
  //        Iteration goes through all CFD fields specified and all boundary fields specified.
  string line;
  ifstream initdat (dat);
  vector<int> vecind;    //stores length of each data domain in file
  vector<DOUB*> mapd;    //adress array that contains the addresses of all first vector entries of the FDF data structure in the initialized version of the simulation  
  vector<DOUB> vecline;  //stores all data values temporarily
  int ks(0), kf(0);
  
  //### get adresses from all necessary data structures (28 vectors) ###//
  mapd.push_back(&(FDF.gridX[0]));//k=0
  mapd.push_back(&(FDF.gridY[0]));
  mapd.push_back(&(FDF.PrgridX[0]));
  mapd.push_back(&(FDF.PrgridY[0]));
  mapd.push_back(&(FDF.UX[0]));
  mapd.push_back(&(FDF.UY[0]));
  mapd.push_back(&(FDF.intensity[0]));
  mapd.push_back(&(FDF.intensity_sig[0]));
  mapd.push_back(&(FDF.intensity_sig_back_coupler[0]));
  mapd.push_back(&(FDF.Pr[0]));
  mapd.push_back(&(FDF.Pri[0]));
  mapd.push_back(&(FDF.Pri2[0]));
  mapd.push_back(&(FDF.vorticity[0]));
  mapd.push_back(&(FDF.vorticitygradX[0]));
  mapd.push_back(&(FDF.vorticitygradY[0]));
  mapd.push_back(&(FDF.divergence[0]));
  mapd.push_back(&(FDF.TrgradX[0]));
  mapd.push_back(&(FDF.TrgradY[0]));
  mapd.push_back(&(FDF.Tr_Sig_gradX[0]));
  mapd.push_back(&(FDF.Tr_Sig_gradY[0]));//k=19
  mapd.push_back(&(parsFDF.UU[0]));
  mapd.push_back(&(parsFDF.UL[0]));
  mapd.push_back(&(parsFDF.ULE[0]));
  mapd.push_back(&(parsFDF.UR[0]));
  mapd.push_back(&(parsFDF.VU[0]));
  mapd.push_back(&(parsFDF.VL[0]));
  mapd.push_back(&(parsFDF.VLE[0]));
  mapd.push_back(&(parsFDF.VR[0]));//k=27
    
  if(initdat.is_open())  //open file
   {
    DOUB a(0.0);
    int j(0);
    while(getline(initdat,line))//read in line
     {
      istringstream is(line);
      if(j<28){while(is >> a){vecind.push_back(int(a)); cout << int(a) << " ";} cout << "\n";}//read in dimensions of arrays NOTE: first 28 entries of initialization file contain the length of the vectors
      else{    while(is >> a){vecline.push_back(DOUB(a));}}//read in element
      j += 1;
     }
    cout << " Found " << vecind.size() << " data fields to read in, overall number of found cells is " << vecline.size() << " \n";
   // dat.close();
   }
   
  int vecsum(0);
  for(int k=0; k<vecind.size();k++){vecsum += vecind[k];}
  cout << " available cells of CFD fields: " << vecsum << "\n";
  
  //### initialize data structure from vecline using pointer to data structure ###//
  kf=vecind[0];
  for(int l=0;l<vecind.size()-8;l++)//iterate vectors up to last FDF vector at l=19
   {
    cout << " intialize array" << (1+l) <<" \n";
    for(int k=ks; k<kf; k++){*(mapd[l]+(k-ks)) = vecline[k];}//iterate through vector NOTE pointer arythmetics used here, adress of first vector entry + counter accesses all other vector entries 
    ks += vecind[l];
    kf += vecind[1+l];   
   }
  for(int l=vecind.size()-8;l<vecind.size()-1;l++)//iterate vectors for boundary conditions l=20 to 26
   {
    cout << " intialize array" << (1+l) <<" \n";
    for(int k=ks; k<kf; k++){*(mapd[l]+(k-ks)) = vecline[k];}//iterate through vector NOTE pointer arythmetics used here, adress of first vector entry + counter accesses all other vector entries 
    ks += vecind[l];
    kf += vecind[1+l];   
   }
  cout << " intialize array" << vecind.size() <<" \n";
  for(int k=ks; k<kf; k++){*(mapd[vecind.size()-1]+(k-ks)) = vecline[k];}//iterate through last vector of boundary l=27
 } 
  
void Set_vector_size(FDFS &FDF, parsf &parsFDF, params &pars, Vectordata &Data, int &N, DOUB &Dx, DOUB &Dy, DOUB &Dangle, DOUB &Dphi, DOUB &Dangle2, int &n)
 {
  //NOTE: This function sets the following
  //      - size of all CFD field vectors 
  //      - size of all exchange vectors for CFD-> agents coupling 
  //      - diffusion time scale of the wakes of agents 
  //      - initial number of wakes (ghosts) to 0 (no wakes at t=0)
  //      - The diffusion constants of x,y positions and for angular and phase noise for the active agents 
  
  FDF.intensity.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //intensity (CFD grid with halo cells)
  FDF.intensity_coupler.resize(parsFDF.Nfx*parsFDF.Nfy); //intensity at positions of agents
  FDF.intensity_back_coupler.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //array holding values from the agent-dynamics level (forcing of advection diffusion system, food)
  FDF.intensity_back_coupler2.resize(parsFDF.Nfx*parsFDF.Nfy); //printing of coupling values to CFD grid
  FDF.intensity_sig.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //signaling tracer intensity (CFD grid with halo cells)
  FDF.intensity_sig_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);//signaling tracer intensity at positions of agents
  FDF.intensity_sig_back_coupler.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //array holding values from the agent-dynamics level (forcing of advection diffusion system, signaling tracer)
  FDF.intensity_sig_back_coupler2.resize(parsFDF.Nfx*parsFDF.Nfy); //printing of coupling values to CFD grid
  FDF.UX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);        //x-velocity at position of agents
  FDF.UY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);        //y-velocity at positions of agents
  FDF.Pr_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);        //pressur at position of agents
  FDF.vorticity_coupler.resize(parsFDF.Nfx*parsFDF.Nfy); //vorticity at position of agents
  FDF.vorticitygradX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy); //vorticity gradient X at position of agents
  FDF.vorticitygradY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy); //vorticity gradient Y at position of agents
  FDF.LEVgradX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy); //Large Eddy Viscosity gradient X at position of agents
  FDF.LEVgradY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy); //Large Eddy Viscosity gradient X at position of agents
  FDF.divergence_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);//divergence at position of agents
  FDF.TrgradX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);   //tracer gradient at position of agents X
  FDF.TrgradY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);   //tracer gradient at position of agents Y
  FDF.Tr_Sig_gradX_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);   //tracer gradient at position of agents X
  FDF.Tr_Sig_gradY_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);   //tracer gradient at position of agents Y
  FDF.gridX.resize(parsFDF.Nfx);                         //x positions in CFD grid (grid excluding boundary cells)
  FDF.gridY.resize(parsFDF.Nfy);                         //y positions in CFD grid (grid excluding boundary cells)
  FDF.PrgridX.resize(parsFDF.Nfx+2);                     //x positions of Pressur in CFD grid (includes boundary cells which are outside of the computational domain)
  FDF.PrgridY.resize(parsFDF.Nfy+2);                     //y positions of Pressur in CFD grid
  FDF.UX.resize((parsFDF.Nfx+1)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+2)*parsFDF.length_outlet);        //x velocity of fluid in CFD grid with boundary cells
  FDF.UY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+1) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);        //x velocity of fluid TODO: 3D simulation
  FDF.Pr.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);        //pressur of fluid
  FDF.Pri.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);       //intermediate pressur of fluid
  FDF.Pri2.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);      //intermediate pressur no 2 of fluid
  FDF.WindX.resize((parsFDF.Nfx+1)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+2)*parsFDF.length_outlet);     //wind X
  FDF.WindY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+1) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);     //wind Y
  FDF.LEV.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);       //Large Eddy Viscosity
  
//#ifndef complex_boundary
//  parsFDF.UU.resize(parsFDF.Nfx+1);                      //boundary X velocity of upper boundary
//  parsFDF.UL.resize(parsFDF.Nfx+1);                      //boundary X velocity of lower boundary
//  parsFDF.ULE.resize(parsFDF.Nfy+2);                     //boundary X velocity of left boundary
//  parsFDF.UR.resize(parsFDF.Nfy+2);                      //boundary X velocity of right boundary
//  parsFDF.VU.resize(parsFDF.Nfx+2);                      //boundary V velocity of upper boundary
//  parsFDF.VL.resize(parsFDF.Nfx+2);                      //boundary V velocity of lower boundary
//  parsFDF.VLE.resize(parsFDF.Nfy+1);                     //boundary V velocity of left boundary
//  parsFDF.VR.resize(parsFDF.Nfy+1);                      //boundary V velocity of right boundary
//#endif
  FDF.vorticity.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);     // vorticity (CFD grid with halo cells)
  FDF.divergence.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);    // divergence (CFD grid with halo cells)
  FDF.TrgradX.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);       // tracer gradient X (CFD grid with halo cells)
  FDF.TrgradY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);       // tracer gradient Y (CFD grid with halo cells)
  FDF.Tr_Sig_gradX.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);  // signaling tracer gradient X (CFD grid with halo cells)
  FDF.Tr_Sig_gradY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);  // signaling tracer gradient Y (CFD grid with halo cells)
  FDF.vorticitygradX.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);// gradient of vorticity X (CFD grid with halo cells)
  FDF.vorticitygradY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet);// gradient of vorticity Y (CFD grid with halo cells)
  FDF.LEVgradX.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //Large Eddy Viscosity gradient X at position of agents
  FDF.LEVgradY.resize((parsFDF.Nfx+2)*(parsFDF.Nfy+2) + (2*parsFDF.width_outlet+3)*parsFDF.length_outlet); //Large Eddy Viscosity gradient X at position of agents
  FDF.intensity_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);     // food tracer for time interpolation
  FDF.intensity_sig_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy); // food tracer for time interpolation
  FDF.UX_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);            // UX for time interpolation
  FDF.UY_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);            // UY for time interpolation
  FDF.vorticity_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);     // vorticity for time interpolation
  FDF.Pr_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);            // pressure for time interpolation
  FDF.vorticitygradX_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);// X vorticity gradient for time interpolation
  FDF.vorticitygradY_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);// Y
  FDF.LEVgradX_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy); //Large Eddy Viscosity gradient X at position of agents
  FDF.LEVgradY_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy); //Large Eddy Viscosity gradient X at position of agents
  FDF.TrgradX_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);       // X food tracer gradient for time interpolation
  FDF.TrgradY_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);       // Y
  FDF.Tr_Sig_gradX_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);  // X signal tracer interpolation for time interpolation
  FDF.Tr_Sig_gradY_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);  // Y
  FDF.intensity_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);     // food tracer at actual time
  FDF.intensity_sig_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy); // food tracer at actual time
  FDF.UX_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);            // UX 
  FDF.UY_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);            // UY 
  FDF.vorticity_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);     // vorticity 
  FDF.Pr_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);            // pressure 
  FDF.vorticitygradX_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);// X vorticity gradient 
  FDF.vorticitygradY_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);// Y
  FDF.LEVgradX_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy); //Large Eddy Viscosity gradient X at position of agents
  FDF.LEVgradY_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy); //Large Eddy Viscosity gradient X at position of agents
  FDF.TrgradX_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);       // X food tracer gradient 
  FDF.TrgradY_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);       // Y
  FDF.Tr_Sig_gradX_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);  // X signal tracer 
  FDF.Tr_Sig_gradY_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);  // Y
  FDF.LEV_coupler.resize(parsFDF.Nfx*parsFDF.Nfy);              // Large Eddy Viscosity 
  FDF.LEV_coupler_at.resize(parsFDF.Nfx*parsFDF.Nfy);           // Large Eddy Viscosity 
  FDF.LEV_coupler_keep.resize(parsFDF.Nfx*parsFDF.Nfy);         // Large Eddy Viscosity 
  
  //### initialize ensemble vectors ###//
  pars.freq.resize(N);                           //internal state frequency
  pars.agent_name.resize(N);                     //name of agents (given by invariant integer)
  vector<int> I4(4);                             //holds four indexes of X or Y position
  vector<DOUB> J4(4);                            //holds four positions of either X or Y or an indicator for the coast cells
  for(int k=0;k<N;k++)
   {
    pars.agent_name[k] = k;                      // set agent name
    pars.FDF_indexX.push_back(I4);               // index of closest grid point X direction active agents
    pars.FDF_indexY.push_back(I4);               // index of closest grid point Y direction active agents
    pars.FDF_indexX2.push_back(I4);              // index of closest grid point X direction passive agents
    pars.FDF_indexY2.push_back(I4);              // index of closest grid point Y direction passive agents
    pars.is_boundary.push_back(J4);              // indicator of coast line cells active agents
    pars.boundaryX.push_back(J4);                // X position of coast line cells active agents
    pars.boundaryY.push_back(J4);                // Y position of coast line cells active agents
    pars.is_boundary2.push_back(J4);             // indicator of coast line cells passive agents
    pars.boundaryX2.push_back(J4);               // X position of coast line cells passive agents
    pars.boundaryY2.push_back(J4);               // Y position of coast line cells passive agents
   }
 
  pars.N3=0;                                     //initialize ghost vectors with some number of entities in the beginning (0 for now)
  //pars.Tmax= 1.0/(4.0*pi*pars.mu2*pars.omegmin); //maximal diffusion time of wake vorticity before its maximum intensity fades below the minimal value of wake intensity (for single vortex )
  
  DOUB A(16.0*pars.irad*pars.irad*0.25/(3.0*pi*pi*pars.omegmin*pars.omegmin)), B(1.0/3.0);
  pars.Tmax= ApowB(A, B)*1.0/(4.0*pars.mu2)*0.8146; //maximal diffusion time of wake vorticity before its maximum intensity fades below the minimal value of wake intensity (approximate for dipole vortex ) the nummerical value of 0.8146 is an empiric correction factor that makes the fading time equal to the deletion time. Otherwise the wake exists further after fading. The factor arizes because of the algebraic approximation of the wake life time. It is independent of irad!
  
  for(int k=0;k<pars.N3;k++)
   {
    pars.FDF_indexX3.push_back(I4);              //index of closest grid point X direction ghost agents
    pars.FDF_indexY3.push_back(I4);              //index of closest grid point Y direction ghost agents 
   }
   
  pars.intensity.resize(N);                      //interpolated food intensity field for detection at positions of agents
  pars.intensity_sig.resize(N);                  //interpolated signal intensity field for detection at positions of agents
  pars.intensity2.resize(N);                     //interpolated food intensity field for detection at positions of passive agents
  pars.UXA.resize(N);                            //interpolated x velocity at position of active agents
  pars.UYA.resize(N);                            //interpolated y velocity at position of active agents 
  pars.UXA2.resize(N);                           //interpolated x velocity at position of passive agents
  pars.UYA2.resize(N);                           //interpolated y velocity at position of passive agents 
  
  pars.UXA3.resize(pars.N3);                     //interpolated x velocity at position of ghost agents
  pars.UYA3.resize(pars.N3);                     //interpolated y velocity at position of ghost agents 
  pars.absvel3.resize(pars.N3);                  //magnitude of self velocity at position of ghost agents (set at generation then unchanged)
  pars.x3t.resize(pars.N3);                      //x position of ghost agents (seen by active agents)
  pars.y3t.resize(pars.N3);                      //y position of ghost agents (seen by active agents) 
  pars.Tgstart.resize(pars.N3);                  //array of starting times of a ghost particle
  pars.start_agent.resize(pars.N3);              //index of active agent from which a ghost emerges
  pars.VorticityA3.resize(pars.N3);              //vorticity at the position of the ghost particle
  
  pars.PrA.resize(N);                            //interpolated pressur at positions of agents
  pars.LEVA.resize(N);                           //interpolated large eddy viscosity at positions of agents
  pars.VorticityA.resize(N);                     //interpolated vorticity at positions of agents
  pars.VorticityA2.resize(N);                    //interpolated vorticity at positions of passive agents
  pars.Vorticity_gradXA.resize(N);               //interpolated vorticity at positions of agents
  pars.Vorticity_gradYA.resize(N);               //interpolated vorticity at positions of agents
  pars.LEV_gradXA.resize(N);                     //interpolated LEV at positions of agents
  pars.LEV_gradYA.resize(N);                     //interpolated LEV at positions of agents
  
  pars.TrgradXA.resize(N);                       //interpolated food tracer X gradient at positions of agents
  pars.TrgradYA.resize(N);                       //interpolated food tracer Y gradient at positions of agents
  pars.Tr_Sig_gradXA.resize(N);                  //interpolated signal tracer X gradient at positions of agents
  pars.Tr_Sig_gradYA.resize(N);                  //interpolated signal tracer Y gradient at positions of agents
  pars.oldphases.resize(N);                      //vector holding phases phi(t-dt) of active agents at t-dt (old time step values)
  
  Data.pos.resize(n*N);                          //positions x,y, O-U noise, memory, direction angle, internal state
  Data.pos2.resize(2*N);                         //positions x,y of passive tracers
  Data.pos3.resize(3*pars.N3);                   //positions x,y and angle of orientation of drift velocity of ghost tracers
  Data.vel.resize(n*N);                          //velocity of active agents
  Data.vel2.resize(2*N);                         //velocity of passive agents
  Data.vel3.resize(3*pars.N3);                   //velocity of ghost agents
  pars.D.resize(n*N);                            //diffusion coefficients of noise
  Data.noise.resize(n*N);                        //noise components of dynamics 

  //### initialize constants ###//
  parsFDF.dx=(parsFDF.fxf-parsFDF.fxi)/(parsFDF.Nfx);//x step
  parsFDF.dy=(parsFDF.fyf-parsFDF.fyi)/(parsFDF.Nfy);//y step
  cout << " set diffusion parameters \n";
  for(int k=0; k<N;k++)//diffusion coefficients
   {
    pars.D[2*k] = Dx;
    pars.D[2*k+1] = Dy;
    pars.D[(n-4)*N+k] = Dangle;  //for Ornstein-Uhlenbeck noise in direction
    pars.D[(n-3)*N+k] = 0.0;     //Activity has no noise
    pars.D[(n-2)*N+k] = Dangle2; //angle is driven by OU noise, otherwise direct diffusion (Gaussian)
    pars.D[(n-1)*N+k] = Dphi;
   }
 }

void CFD_Set1(FDFS &FDF, parsf &parsFDF) 
 { 
  //NOTE: This function sets the following
  //      - pressure grid coordinates (the actual center cell coordinates of the CFD grid, including boundary layer of the overall domain of simulation)
  //      - the agent grid (like pressure grid but without the boundary layer) -> used for exchange of CFD fields towards agent level
 
  //x direction
  cout << "init CFD grid \n";
  FDF.PrgridX[0] = parsFDF.fxi-0.5*parsFDF.dx;
  for(int k=0;k<parsFDF.Nfx;k++)//x grid
   {
    FDF.gridX[k] = parsFDF.fxi + (DOUB(k)+0.5)*parsFDF.dx;
    FDF.PrgridX[k+1] = parsFDF.fxi + (DOUB(k)+0.5)*parsFDF.dx;
   }
  FDF.PrgridX[parsFDF.Nfx+1] = parsFDF.fxi + (DOUB(parsFDF.Nfx)+0.5)*parsFDF.dx;  

  //y direction
  FDF.PrgridY[0] = parsFDF.fyi-0.5*parsFDF.dy;
  for(int k=0;k<parsFDF.Nfy;k++)//y grid
   {
    FDF.gridY[k] = parsFDF.fyi + (DOUB(k)+0.5)*parsFDF.dy;
    FDF.PrgridY[k+1] = parsFDF.fyi + (DOUB(k)+0.5)*parsFDF.dy;
   }
  FDF.PrgridY[parsFDF.Nfy+1] = parsFDF.fyi + (DOUB(parsFDF.Nfy)+0.5)*parsFDF.dy; 
 }
  
void CFD_Set2(FDFS &FDF, parsf &parsFDF, params &pars, DOUB &tf, DOUB &t, DOUB &T, DOUB &T2)
 { 
  //NOTE: This function sets the following
  //      - When the initial conditions of the CFD fields are aquired from an integrator:
  //        - initialization of a tracer distribution (if enabled)
  //        - initialization of the pressure field as zero
  //        - initialization of the fluid velocity as zero
  //        - setting the SOR solver (pressure) to a starting value (here 3000)
  //        - integration of the fluid equations from -T to tf=-0.1T (generation of transient) Euler method
  //        - When CFD field is generated else where and stored into a text file:
  //          - read in the initial condition and, skip Euler method
  //        - integration of the fluid equations further to tf=0 Heun method (2nd order time and space) 
  //          If the Euler step is used (internally generated transient) the transient time (T) will be longer
  //          If the initialconditions are read in, the transient is integrated further using Heuns method, 
  //          to "couple in" the fluid flow. In this case the Euns method solves the equations from tf=-T to tf=0 w
  //          with much shorter T needed
  //        - Output calculate vorticity, divergence, and gradients of CFD fields
  //        - Feed forward the CFD field values to the exchange grid
  //      - When the CFD field is aquired from png visual data (converted to matrix formate):
  //        In this case there is no integration needed
  //        - Interpolations of data values onto the exchange grid

  //set CFD initial conditions ###//
#ifdef CFD_field_from_integrator
  cout << "intensity from CFD field \n";
  DOUB distx(0.0), disty(0.0);
  //set tracers and pressur
  for(int k=0;k<parsFDF.Nfx+2;k++)
   {
    for(int l=0;l<parsFDF.Nfy+2;l++)
     {
      distx=(FDF.PrgridX[k]);
      disty=(FDF.PrgridY[l]);
      //### initial conditions of velocity and tracer ###//
#ifdef tracer_distribution
      FDF.intensity[k*(parsFDF.Nfy+2)+l] = exp(-(distx*distx+disty*disty)*0.2);
      //cout << "intensity at t=0: (k,l)= (" << k << ", " << l << "), I= " << FDF.intensity[k*(parsFDF.Nfy+2)+l] << "\n";
#endif
      FDF.Pr[k*(parsFDF.Nfy+2)+l] = 0.0;
     }
   }
  //set x-velocities
  for(int k=0;k<parsFDF.Nfx+1;k++)
   {
    for(int l=0;l<parsFDF.Nfy+2;l++)
     {
      FDF.UX[k*(parsFDF.Nfy+2)+l] = 0.0;
     }
   }
  //set y-velocities
  for(int k=0;k<parsFDF.Nfx+2;k++)
   {
    for(int l=0;l<parsFDF.Nfy+1;l++)
     {
      FDF.UY[k*(parsFDF.Nfy+1)+l] = 0.0;
     }
   }

  //### Set maximum number of SOR steps ###//
  // parsFDF.Npr =3000.0;

#ifndef init_read
  //### Fluid transient ###//
  cout << "start simulation: transient \n";
  tf=-T;//start T time units before simulation
  while(tf<-0.1*T)
   {
    cout << "transient: tf= " << tf << " toll= " << parsFDF.toll << " dtf= " << parsFDF.dtf << "\n";
    Integrator_FDFS_EULER(FDF, tf, parsFDF, T, T2);
    //### Calculate divergence field ###//
    Divergence_calc(FDF, parsFDF);
    //### Adjust tolerance of pressure convergence ###//
    Toll_set(parsFDF.toll, parsFDF.Npr, parsFDF.Nsor, FDF.divergence, parsFDF.DIVMAX, parsFDF.NPRMAX);     
   }
#else
  //### reading of transient from a file (not image) ###//
  cout << "reading initial conditions from " << parsFDF.initial_dat << "\n";
  //Read_Init(parsFDF.initial_dat, FDF, parsFDF);
  Read_Init_2(parsFDF.initial_dat, FDF, parsFDF);
  parsFDF.Npr = 100;
#endif

  //### Set maximum number of SOR steps ###//
//#ifndef Euler
  //parsFDF.Npr = 250000;
//#endif 
 
  //### calculation of transient to fully developed flow ###//
  while(tf<t)
   {
#ifdef Euler
    Integrator_FDFS_EULER(FDF, tf, parsFDF, T, T2);
#else
    Integrator_FDFS_HEUN(FDF, tf, parsFDF, T, T2);
#endif
 //   cout << " FDF: (Ux, Uy) = " << FDF.UX[10] << " " << FDF.UY[10] << "\n";
 //   //tf += parsFDF.dtf;
   // cout << "tf= " << tf << " mu= " << parsFDF.mu << "\n";
    cout << "transient: tf= " << tf << " toll= " << parsFDF.toll << " dtf= " << parsFDF.dtf << "\n";
    //### Calculate divergence field ###//
    Divergence_calc(FDF, parsFDF);
    //### Adjust tolerance of pressure convergence ###//
    Toll_set(parsFDF.toll, parsFDF.Npr, parsFDF.Nsor, FDF.divergence, parsFDF.DIVMAX, parsFDF.NPRMAX);    
         
   }

  //### Reset tracer distribution ###//
#ifdef tracer_distribution
  for(int k=0;k<parsFDF.Nfx+2;k++)
   {
    for(int l=0;l<parsFDF.Nfy+2;l++)
     {
      distx=(FDF.PrgridX[k]-pars.x_tracer);//-0.5*FDF.PrgridX[0]);
      disty=(FDF.PrgridY[l]-pars.y_tracer);
      //### initial conditions of velocity and tracer ###//
      FDF.intensity[k*(parsFDF.Nfy+2)+l] = pars.amp_tracer*exp(-(distx*distx+disty*disty)*parsFDF.spread_tracer);
     }
   }
#endif

  //### Calculate divergence field ###//
  Divergence_calc(FDF, parsFDF);
  cout << "prepare agent simulation \n";

  //### Calculate vorticity field ###//
  Vorticity_calc(FDF, parsFDF);
  //### Calculate divergence field ###//
  Divergence_calc(FDF, parsFDF);
  //### Calculate gradient of tracer and vorticity ###//
  Grads(FDF, parsFDF);

  //### Transfer CFD values to Agent grid values ###//
  CFD_grid_to_agent_grid(FDF, parsFDF);//the function copies the staggered grid values all to the nodes of pressur and omits the boundary cells. If necessary the average is used
#endif//off ifdef integrator

#ifdef CFD_field_from_image
  cout << "intensity from images \n";
  stringstream image_name; image_name << parsFDF.image_path << "_Tr" << ".0.dat"; // prepare full image name tracer
  stringstream image_name; image_name << parsFDF.image_path << "_UX" << ".0.dat"; // prepare full image name UX
  stringstream image_name; image_name << parsFDF.image_path << "_UY" << ".0.dat"; // prepare full image name UY
  
  string img1 = image_name1.str();                                       // set image name
  string img2 = image_name2.str();                                       // set image name
  string img3 = image_name3.str();                                       // set image name
  Interpolate_Image_to_Grid(img1, img2, img3, FDF, parsFDF);             // interpolate image data onto the grid as specified by parameters
#endif
 }
  
void Ensemble_init_cond(Vectordata &Data, params &pars, int &N, int &n, random_device &generator)  
 { 
  //NOTE: This function sets the following
  //      - initial positions of active agents (uniformely independent distributed, uid)
  //        The code ensures that agents are not too close to each other initially
  //      - initial positions of the passive tracers onto the positions of the active agents
  //      - initialization of wakes at the positions of active agents (if any present)
  //        Usually, there are no wakes present at the start of the simulation 
  //      - Setting of the orientations and internal phases randomly (uid)
 
  cout << "seed initial positions \n";
  uniform_real_distribution<DOUB> distributionU(0.0,1.0); 

  //### Set initial conditions of position and phase ###//
  //cout << pars.xi << " " << pars.xf << " " << pars.yi << " " << pars.yf << "\n";
  DOUB x0(0.0), y0(0.0), d0(0.0), rep0(0.0);
  //for(int k=0;k<N;k++){x0 = pars.xi + (pars.xf-pars.xi)*distributionU(generator); Data.pos[2*k] = x0;}//x0
  //for(int k=0;k<N;k++){y0 = pars.yi + (pars.yf-pars.yi)*distributionU(generator); Data.pos[2*k+1] = y0;}//y0
  for(int k=0;k<N;k++)
   {
    rep0=0.0;
    while(rep0==0.0)//make sure that agents are not to close initially
     {
      x0 = pars.sposx-0.5*pars.sposedge + pars.sposedge*distributionU(generator); 
      y0 = pars.sposy-0.5*pars.sposedge + pars.sposedge*distributionU(generator);
      //active agents 
      Data.pos[2*k+1] = y0; 
      Data.pos[2*k] = x0;
      for(int l=0;l<N;l++)
       {
        d0= sqrt((Data.pos[2*k]-Data.pos[2*l])*(Data.pos[2*k]-Data.pos[2*l]) + (Data.pos[2*k+1]-Data.pos[2*l+1])*(Data.pos[2*k+1]-Data.pos[2*l+1]));
        if(d0<=0.1){rep0=1.0;}
       }
     } 
    //passive agents 
    Data.pos2[2*k] = x0;
    Data.pos2[2*k+1] = y0;
   }//x0,y0
   
  for(int k=0;k<pars.N3;k++)
   {
    //ghost agents 
    Data.pos3[2*k] = Data.pos[0];
    Data.pos3[2*k+1] = Data.pos[1];
    cout << " ghost k= " << k << "(x,y)= " << Data.pos3[2*k] << " " << Data.pos3[2*k+1] << "\n";
   } 
   
  for(int k=0;k<N;k++){Data.pos[(n-2)*N+k] = 2.0*pi*distributionU(generator);}//angle of direction
  for(int k=0;k<N;k++){Data.pos[(n-1)*N+k] = 2.0*pi*distributionU(generator);}//internal state (phase)
  //for(int k=0;k<N;k++){cout << "posx[" << k <<"]= " << "posy[" << k <<"]= " << Data.pos[2*k]  << Data.pos[2*k+1] << " internal phase= " << Data.pos[(n-1)*N+k] << "\n";}
 }
 
void CFD_to_agents_interpolation(Vectordata &Data, params &pars, FDFS &FDF, parsf &parsFDF)   
 {
  //### interpolate field values onto agent positions
  //### NOTE: Interpolates field values on pressure grid points inside the domain. these points are 0.5dx and 0.5dy shifted from the actual domain walls. Thus, the grid must be finer 
  //          than the distance of wall interaction, which keeps the agents inside the lattice of inner pressure points. This is mandatory! If the domain is too sparse, the index finder 
  //          is unable to locate agents in the grid. Thus, there can be no interpolation of field values such that the simulation breaks down.
  //          partly this problem is resolved by adapting the wall interaction distance automatically
  Interpolator(Data.pos, pars.intensity, pars.FDF_indexX, pars.FDF_indexY, FDF.intensity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate intensity
  Interpolator(Data.pos2, pars.intensity2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.intensity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate intensity passive agents
  Interpolator(Data.pos, pars.UXA, pars.FDF_indexX, pars.FDF_indexY, FDF.UX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate velocity in x direction (active agents)
  Interpolator(Data.pos, pars.UYA, pars.FDF_indexX, pars.FDF_indexY, FDF.UY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate velocity in y direction (active agents)
  Interpolator(Data.pos2, pars.UXA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate velocity in x direction (passive agents)
  Interpolator(Data.pos2, pars.UYA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate velocity in y direction (passive agents)
  Interpolator_ghost(Data.pos3, pars.UXA3, pars.FDF_indexX3, pars.FDF_indexY3, FDF.UX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate velocity in x direction (ghost agents)
  Interpolator_ghost(Data.pos3, pars.UYA3, pars.FDF_indexX3, pars.FDF_indexY3, FDF.UY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate velocity in y direction (ghost agents)
  Interpolator_ghost(Data.pos3, pars.VorticityA3, pars.FDF_indexX3, pars.FDF_indexY3, FDF.vorticity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity ghost particles
  Interpolator(Data.pos, pars.PrA, pars.FDF_indexX, pars.FDF_indexY, FDF.Pr_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate pressure
  Interpolator(Data.pos, pars.VorticityA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity
  Interpolator(Data.pos2, pars.VorticityA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.vorticity_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity passive agents
  Interpolator(Data.pos, pars.Vorticity_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity gradX
  Interpolator(Data.pos, pars.Vorticity_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate vorticity gradY
  Interpolator(Data.pos, pars.LEV_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.LEVgradX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate Large Eddy Viscosity gradX
  Interpolator(Data.pos, pars.LEV_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.LEVgradY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate LEV gradY
  Interpolator(Data.pos, pars.TrgradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate food tracer gradient X
  Interpolator(Data.pos, pars.TrgradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate food tracer gradient Y
  Interpolator(Data.pos, pars.Tr_Sig_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.Tr_Sig_gradX_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate signal tracer gradient X
  Interpolator(Data.pos, pars.Tr_Sig_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.Tr_Sig_gradY_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate signal tracer gradient Y
  Interpolator(Data.pos, pars.LEVA, pars.FDF_indexX, pars.FDF_indexY, FDF.LEV_coupler, FDF.gridX, FDF.gridY, parsFDF);//interpolate Large Eddy Viscosity
 }

void CFD_to_agents_interpolation_2(Vectordata &Data, params &pars, FDFS &FDF, parsf &parsFDF, DOUB &tf, DOUB &t)   
 {
  //### interpolate field values onto agent positions
  //### NOTE: Interpolates field values on pressure grid points inside the domain. These points are 0.5dx and 0.5dy shifted from the actual domain walls. Thus, the grid must be finer 
  //          than the distance of wall interaction, which keeps the agents inside the lattice of inner pressure points. This is mandatory! If the domain is too sparse, the index finder 
  //          is unable to locate agents in the grid. Thus, there can be no interpolation of field values such that the simulation breaks down.
  //          partly this problem is resolved by adapting the wall interaction distance automatically
  //          Here the function also interpolates the fields in time (linear interpolation) when the time step of CFD values is larger than the time step of the agents values
  //          First, the initial values are kept then, the time loop of agents makes a step, causing tf<t thus, the CFD loop makes a step such that td>>t. In the next step of the  
  //          agent dynamics, the CFD fields are interpolated in time

  //### Time interpolation ###//
  #pragma omp parallel for
  for(int k=0; k<FDF.Pr_coupler.size(); k++)
   {
    FDF.intensity_coupler_at[k] = ((tf-t)*FDF.intensity_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.intensity_coupler[k])/parsFDF.dtf;                 // food tracer at actual time
    FDF.intensity_sig_coupler_at[k] = ((tf-t)*FDF.intensity_sig_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.intensity_sig_coupler[k])/parsFDF.dtf;     // signaling tracer at actual time
    FDF.UX_coupler_at[k] = ((tf-t)*FDF.UX_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.UX_coupler[k])/parsFDF.dtf;                                      // UX
    FDF.UY_coupler_at[k] = ((tf-t)*FDF.UY_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.UY_coupler[k])/parsFDF.dtf;                                      // UY
    FDF.vorticity_coupler_at[k] = ((tf-t)*FDF.vorticity_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.vorticity_coupler[k])/parsFDF.dtf;                 // vorticity
    FDF.Pr_coupler_at[k] = ((tf-t)*FDF.Pr_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.Pr_coupler[k])/parsFDF.dtf;                                      // pressure
    FDF.vorticitygradX_coupler_at[k] = ((tf-t)*FDF.vorticitygradX_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.vorticitygradX_coupler[k])/parsFDF.dtf;  // X vorticity gradient
    FDF.vorticitygradY_coupler_at[k] = ((tf-t)*FDF.vorticitygradY_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.vorticitygradY_coupler[k])/parsFDF.dtf;  // Y
    FDF.LEVgradX_coupler_at[k] = ((tf-t)*FDF.LEVgradX_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.LEVgradX_coupler[k])/parsFDF.dtf;  // X Large Eddy Viscosity gradient
    FDF.LEVgradY_coupler_at[k] = ((tf-t)*FDF.LEVgradY_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.LEVgradY_coupler[k])/parsFDF.dtf;  // Y
    FDF.TrgradX_coupler_at[k] = ((tf-t)*FDF.TrgradX_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.TrgradX_coupler[k])/parsFDF.dtf;                       // X food tracer gradient
    FDF.TrgradY_coupler_at[k] = ((tf-t)*FDF.TrgradY_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.TrgradY_coupler[k])/parsFDF.dtf;                       // Y
    FDF.Tr_Sig_gradX_coupler_at[k] = ((tf-t)*FDF.Tr_Sig_gradX_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.Tr_Sig_gradX_coupler[k])/parsFDF.dtf;        // X signal tracer interpolation
    FDF.Tr_Sig_gradY_coupler_at[k] = ((tf-t)*FDF.Tr_Sig_gradY_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.Tr_Sig_gradY_coupler[k])/parsFDF.dtf;        // Y
    FDF.LEV_coupler_at[k] = ((tf-t)*FDF.LEV_coupler_keep[k] + (t-tf+parsFDF.dtf)*FDF.LEV_coupler[k])/parsFDF.dtf;                                   // Large Eddy Viscosity
   }
  #pragma omp barrier
  //### Space interpolation ###//
  Interpolator(Data.pos, pars.intensity, pars.FDF_indexX, pars.FDF_indexY, FDF.intensity_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                // interpolate food intensity
  Interpolator(Data.pos2, pars.intensity2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.intensity_coupler_at, FDF.gridX, FDF.gridY, parsFDF);            // interpolate food intensity passive agents
  Interpolator(Data.pos, pars.intensity_sig, pars.FDF_indexX, pars.FDF_indexY, FDF.intensity_sig_coupler_at, FDF.gridX, FDF.gridY, parsFDF);        // interpolate signaling intensity
  Interpolator(Data.pos, pars.UXA, pars.FDF_indexX, pars.FDF_indexY, FDF.UX_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                             // interpolate velocity in x direction (active agents)
  Interpolator(Data.pos, pars.UYA, pars.FDF_indexX, pars.FDF_indexY, FDF.UY_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                             // interpolate velocity in y direction (active agents)
  Interpolator(Data.pos2, pars.UXA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UX_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                         // interpolate velocity in x direction (passive agents)
  Interpolator(Data.pos2, pars.UYA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.UY_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                         // interpolate velocity in y direction (passive agents)
  Interpolator_ghost(Data.pos3, pars.UXA3, pars.FDF_indexX3, pars.FDF_indexY3, FDF.UX_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                   // interpolate velocity in x direction (ghost agents)
  Interpolator_ghost(Data.pos3, pars.UYA3, pars.FDF_indexX3, pars.FDF_indexY3, FDF.UY_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                   // interpolate velocity in y direction (ghost agents)
  Interpolator_ghost(Data.pos3, pars.VorticityA3, pars.FDF_indexX3, pars.FDF_indexY3, FDF.vorticity_coupler_at, FDF.gridX, FDF.gridY, parsFDF);     // interpolate vorticity ghost particles
  Interpolator(Data.pos, pars.PrA, pars.FDF_indexX, pars.FDF_indexY, FDF.Pr_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                             // interpolate pressure
  Interpolator(Data.pos, pars.VorticityA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticity_coupler_at, FDF.gridX, FDF.gridY, parsFDF);               // interpolate vorticity
  Interpolator(Data.pos2, pars.VorticityA2, pars.FDF_indexX2, pars.FDF_indexY2, FDF.vorticity_coupler_at, FDF.gridX, FDF.gridY, parsFDF);           // interpolate vorticity passive agents
  Interpolator(Data.pos, pars.Vorticity_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradX_coupler_at, FDF.gridX, FDF.gridY, parsFDF);    // interpolate vorticity gradX
  Interpolator(Data.pos, pars.Vorticity_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.vorticitygradY_coupler_at, FDF.gridX, FDF.gridY, parsFDF);    // interpolate vorticity gradY
  Interpolator(Data.pos, pars.LEV_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.LEVgradX_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                // interpolate LEV gradX
  Interpolator(Data.pos, pars.LEV_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.LEVgradY_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                // interpolate LEV gradY
  Interpolator(Data.pos, pars.TrgradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradX_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                   // interpolate food tracer gradient X
  Interpolator(Data.pos, pars.TrgradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.TrgradY_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                   // interpolate food tracer gradient Y
  Interpolator(Data.pos, pars.Tr_Sig_gradXA, pars.FDF_indexX, pars.FDF_indexY, FDF.Tr_Sig_gradX_coupler_at, FDF.gridX, FDF.gridY, parsFDF);         // interpolate signal tracer gradient X
  Interpolator(Data.pos, pars.Tr_Sig_gradYA, pars.FDF_indexX, pars.FDF_indexY, FDF.Tr_Sig_gradY_coupler_at, FDF.gridX, FDF.gridY, parsFDF);         // interpolate signal tracer gradient Y
  Interpolator(Data.pos, pars.LEVA, pars.FDF_indexX, pars.FDF_indexY, FDF.LEV_coupler_at, FDF.gridX, FDF.gridY, parsFDF);                           // interpolate Large Eddy Viscosity
 }

void CFD_INTEGRATE(FDFS &FDF, parsf& parsFDF, DOUB &T, DOUB &T2, DOUB &t, DOUB &tf, clock_t &pt, clock_t &t_cfd, int &step_img, ofstream &fout6, ofstream &fout7, ofstream &fout8, ofstream &fout9)
 {
  //NOTE: This function does the following:
  //      - When CFD fields are updated from solver:
  //        - Integrate fields as long as tf<t (t is the agent time, tf is the fluid time)
  //          It is possible to choose Euler or Heun method for this.
  //        - Calculation of divergence, vorticity and gradients
  //        - Feed forward CFD field values to the exchange grid
  //      - When CFD fields are read in from immage (png converted to data matrix):
  //        No integration needed, immediately interpolation of data to exchange grid 
#ifdef CFD_field_from_integrator
  //### update fluid fields ###//
  // cout << " call CFD integrator at t= " << t << " tf= " << tf << "\n";
  pt= clock();
  while(tf<t)
   {
   // cout << " call CFD integrator at t= " << t << " tf= " << tf << "\n";
#ifdef Euler
    Integrator_FDFS_EULER(FDF, tf, parsFDF, T, T2);
#else
    Integrator_FDFS_HEUN(FDF, tf, parsFDF, T, T2);
    // tf += parsFDF.dtf;//NOTE: comment out
#endif
    //### Calculate divergence field ###//
    Divergence_calc(FDF,parsFDF);
    //### Check volume flow ###//
    In_flow_outflow(parsFDF, FDF, tf, T, fout6, fout7, fout8, fout9);
    //### Adjust tolerance of pressure convergence ###//
    Toll_set(parsFDF.toll, parsFDF.Npr, parsFDF.Nsor, FDF.divergence, parsFDF.DIVMAX, parsFDF.NPRMAX);
   }
  //### Calculate divergence field ###//
  Divergence_calc(FDF,parsFDF);
  //### Calculate vorticity field ###//
  Vorticity_calc(FDF,parsFDF);
  //### Calculate gradient of tracer and vorticity ###//
  Grads(FDF, parsFDF);
  t_cfd += clock() - pt;//measure time of CFD solver
  //cout << "t_cfd= " << t_cfd << "\n";

  //### copy field values to agent grid (CFD grid positions of pressur without boundary cells ###//
  CFD_grid_to_agent_grid(FDF, parsFDF);
#endif
  pt=clock();//measure time of agents coupling to CFD

#ifdef CFD_field_from_image_dynamic
  //### Update field from png image ###//
  step_img +=1;

  stringstream image_name; image_name << parsFDF.image_path << "_Tr" << "." << step_img << ".dat"; // prepare full image name tracer
  stringstream image_name; image_name << parsFDF.image_path << "_UX" << "." << step_img << ".dat"; // prepare full image name UX
  stringstream image_name; image_name << parsFDF.image_path << "_UY" << "." << step_img << ".dat"; // prepare full image name UY

  string img1 = image_name1.str();                                       // set image name
  string img2 = image_name2.str();                                       // set image name
  string img3 = image_name3.str();                                       // set image name
  Interpolate_Image_to_Grid(img1, img2, img3, FDF, parsFDF);             // interpolate image data onto the grid as specified by parameters
#endif
 }
   
void Keep_init(FDFS &FDF)
 {
  //NOTE: Keeps the recently simulated CFD fields to interpolate in time
  cout << "keep old CFD \n";
  for(int k=0;k<FDF.Pr_coupler.size(); k++)
   {
    FDF.intensity_coupler_keep[k] = FDF.intensity_coupler[k];           //food tracer
    FDF.intensity_sig_coupler_keep[k] = FDF.intensity_sig_coupler[k];   //signaling tracer
    FDF.UX_coupler_keep[k] = FDF.UX_coupler[k];                         //Ux
    FDF.UY_coupler_keep[k] = FDF.UY_coupler[k];                         //Uy
    FDF.vorticity_coupler_keep[k] = FDF.vorticity_coupler[k];           //vorticity
    FDF.Pr_coupler_keep[k] = FDF.Pr_coupler[k];                         //Pressur
    FDF.vorticitygradX_coupler_keep[k] = FDF.vorticitygradX_coupler[k]; //gradient of vorticity in X
    FDF.vorticitygradY_coupler_keep[k] = FDF.vorticitygradY_coupler[k]; //gradient of vorticity in Y
    FDF.LEVgradX_coupler_keep[k] = FDF.LEVgradX_coupler[k]; //gradient of Large Eddy Viscosity  in X
    FDF.LEVgradY_coupler_keep[k] = FDF.LEVgradY_coupler[k]; //gradient of LEV in Y
    FDF.TrgradX_coupler_keep[k] = FDF.TrgradX_coupler[k];               //gradient of food tracer in X 
    FDF.TrgradY_coupler_keep[k] = FDF.TrgradY_coupler[k];               //gradient of food tracer in Y  
    FDF.Tr_Sig_gradX_coupler_keep[k] = FDF.Tr_Sig_gradX_coupler[k];     //gradient of signaling tracer in X
    FDF.Tr_Sig_gradY_coupler_keep[k] = FDF.Tr_Sig_gradY_coupler[k];     //gradient of signaling tracer in Y
    FDF.LEV_coupler_keep[k] = FDF.LEV_coupler[k];                       //Large Eddy Viscosity
   }
 }

DOUB Sum_time(vector<DOUB> &Texe, int k)
 {
  //NOTE: This function sums up previous execution time intervals
  DOUB S(0.0);
  for(int l=0; l<k;l++){S+=Texe[l];}
  return S;
 }

void Keep_initial_positions(vector<DOUB> &pos, vector<DOUB> &pos0, vector<DOUB> &pos2, vector<DOUB> &pos20, int &N, int &n, vector<vector<int>> &FDF_indexX, vector<vector<int>> &FDF_indexY, vector<vector<int>> &FDF_indexX0, vector<vector<int>> &FDF_indexY0, vector<vector<int>> &FDF_indexX2, vector<vector<int>> &FDF_indexY2, vector<vector<int>> &FDF_indexX20, vector<vector<int>> &FDF_indexY20, vector<DOUB> &freq, vector<DOUB> &freq0)
 {
  //NOTE: This function keeps the initial positions of the agents in the beginning of the simulation
  //      These initial conditions are used to generate new agents at runtime, if enabled
  //      This procedure avoids additional index searching at runtime.
  for(int k=0; k<n*N; k++)
   {
    pos0.push_back(pos[k]);
   }
  for(int k=0; k<2*N; k++)
   { 
    pos20.push_back(pos2[k]);
   }
  for(int k=0; k<N; k++)
   {    
    FDF_indexX0.push_back(FDF_indexX[k]);
    FDF_indexY0.push_back(FDF_indexY[k]);
    FDF_indexX20.push_back(FDF_indexX2[k]);
    FDF_indexY20.push_back(FDF_indexY2[k]);
    freq0.push_back(freq[k]);
   }
  cout << "len Data= " << pos.size() << " Data0= " << pos0.size() << " Data2= " << pos2.size() << " Data20= " << pos20.size() << " index " << FDF_indexX.size() << " index 0 " << FDF_indexX0.size() << " frequency " << freq.size() << " frequency0 " << freq0.size() << "\n";
 }

void Check_diffusion_coeff(DOUB &D1, DOUB &D2, DOUB &dx, DOUB &dy, DOUB &U)
 {
  //NOTE: This function sets the diffusion constant according to the largest expected velocity in the flow and the spacial step
  cout << "reset D1= " << D1 << "< " << sqrt(dx*dy)*3.0*U << '\n';
  if(D1<sqrt(dx*dy)*3.0*U){D1 += sqrt(dx*dy)*3.0*U; }//Food tracer
  cout << "reset D2= " << D2 << "< " << sqrt(dx*dy)*3.0*U << '\n';
  if(D2<sqrt(dx*dy)*3.0*U){D2 += sqrt(dx*dy)*3.0*U; }//Signaling tracer
 }

int Njelly_set(DOUB &t, DOUB &T, DOUB &T2)
 {
  return int((1.0 + 4.5*tanh(1.0*t/(0.3*T2)))*(1.0+tanh(1.0*((0.3*T2)-t)/(0.3*T2))));
 }

void RK_STEP_Control(vector<DOUB> &pos, DOUB &t, int &n, int &N, DOUB &dt, params &pars, vector<DOUB> &noise, vector<DOUB> &vel, DOUB &dt0, DOUB &tsim)
 {
  cout << "RK dt check \n";
  vector<DOUB> pos_tmp1(pos.size());
  vector<DOUB> pos_tmp2(pos.size());
  vector<DOUB> vel_tmp1(pos.size());
  vector<DOUB> vel_tmp2(pos.size());
  pos_tmp1=pos;
  pos_tmp2=pos;
  DOUB Q(1.0);
  while(Q==1.0)
   {
    //cout << "while \n";
    rk2(dgl, pos_tmp1, 1, t, n*N, n, dt, pars, vel_tmp1);
    t -= dt;
    rk4(dgl, pos_tmp2, 1, t, n*N, n, dt, pars, vel_tmp2);
    t -= dt;
    Q=0.0;
    for(int k=0; k<pos.size(); k++)
     {
      if(abs(pos_tmp1[k]-pos_tmp2[k])>dt0){Q=1.0;} //(isnan(pos_tmp1[k])){Q=1.0;}
     }
    if(Q==1.0){dt *= 0.5; pos_tmp1=pos; pos_tmp2=pos ; cout << "repeat RK dt check: dt= " << dt << " \n";}
    else{dt = dt0; cout << "no RK dt needed: dt,t,tsim= " << dt << " " << t << " " << tsim << " \n";}
   }
 }

void output_setter(vector<int> &Cols, vector<int> &Cols_FDFS, vector<int> &Cols_Ghosts, string &columns_name)
 {
  string line;
  ifstream cols (columns_name);
  if(cols.is_open())  //open file
   {
    DOUB a(0.0);
    int j(0);
    while(getline(cols,line))//read in line
     {
      istringstream is(line);
      if(j==0)//first line of file: agents
       {
        while(is >> a)
         {
          Cols.push_back(int(a));
          cout << int(a) << " ";
         } 
       }
      if(j==1)//CFD output
       {
        while(is >> a)
         {
          Cols_FDFS.push_back(int(a));
          cout << int(a) << " ";
         } 
       }
      if(j==2)//Ghosts
       {
        while(is >> a)
         {
          Cols_Ghosts.push_back(int(a));
          cout << int(a) << " ";
         } 
       }
      j += 1;
     }
    cols.close();
   }
 }

 vector<DOUB> Box_Muller(DOUB &r1, DOUB &r2)
 {
  vector<DOUB> V{sqrt(-2.0*log(r1))*cos(2.0*pi*r2), sqrt(-2.0*log(r1))*sin(2.0*pi*r2)};
  return V;
 }

void Update_stochastic_components(vector<DOUB> &noise, int &N, int &n, DOUB &t, DOUB &dt, random_device &generator)
 {
  //NOTE: This function performs a bootstrapping update of the noise compnents
  //      every N/4 time steps, the normal random numbers are updated
  //      in other steps, the random numbers a drawn from the sampled numbers by random shifting
  //      for this, a number between 1 and n*N/2 is used
  // cout << "### Update random components ### \n";
  int shift1(0), shift2(0), M(n*N/2), nN(n*N);
  DOUB nNd(n*N), r1(0.0), r2(0.0);
  vector<DOUB> noise2(noise.size()), SR(2);
  uniform_real_distribution<DOUB> distributionU(0.0,1.0);
  if(((int(t/dt)*4)%N)==0)      //update of sampled normal distribution every N/4 time steps
   {
    for(int k=0;k<M;k+=2)
     {
      r1=distributionU(generator);
      r2=distributionU(generator);
      SR = Box_Muller(r1, r2);
      noise[k]=SR[0];
      noise[k+1]=SR[1];
     }
   }
  else                       //shufling of normal random numbers every time step other than multiples of N/4
   {
    shift1 = int((distributionU(generator))*nNd);
    shift2 = int((distributionU(generator))*nNd);
    for(int k=0;k<nN;k++)
     {
      noise2[(k+shift1)%nN] = noise[(k+shift2)%nN]; 
     }
    noise=noise2;
   }
 }

int main(int argc, char* argv[])
{
 //### measuring time ###//
 clock_t t_cfd(clock()/CLOCKS_PER_SEC), pt(clock()/CLOCKS_PER_SEC);
#ifdef sequential_time
 vector<clock_t> Texe(33); 
 Texe[0]=clock()/CLOCKS_PER_SEC;
 cout << "t_start= " << Texe[0] << "\n";
#else 
 vector<DOUB> Texe(33);//NOTE: The code execution is divided into 31 steps
 Texe[0] = omp_get_wtime();
#endif
  
 //### initialize data arays ###//
 FDFS FDF;        //fluid dynamics field structures
 parsf parsFDF;   //fluid dynamics parameters
 params pars;     //parameters of agents
 Vectordata Data; //vector data of agents
 
 cout << "read parameters \n";
 // argv[1];                                      // distinguish file parameter , argv[0] own name
 int N=int(strtod(argv[2],NULL));                 // number of units
 pars.freq1=DOUB(strtod(argv[3],NULL));           // mean frequency
 pars.freq2=DOUB(strtod(argv[4],NULL));           // standart deviation of freq distribution
 pars.vel0 =DOUB(strtod(argv[5],NULL));           // offset velocity
 pars.eps  =DOUB(strtod(argv[6],NULL));           // intercation strength of internal states
 pars.irad =DOUB(strtod(argv[7],NULL));           // interaction radius
 DOUB T=DOUB(strtod(argv[8],NULL));               // transient time
 DOUB dt0=DOUB(strtod(argv[9],NULL));              // dt 
 pars.xi=DOUB(strtod(argv[10],NULL));             // left x bound
 pars.xf=DOUB(strtod(argv[11],NULL));             // right x bound
 pars.yi=DOUB(strtod(argv[12],NULL));             // lower y bound
 pars.yf=DOUB(strtod(argv[13],NULL));             // upper y bound
 pars.thresh=DOUB(strtod(argv[14],NULL));         // threshold at which the seasons start
 pars.sposx=DOUB(strtod(argv[15],NULL));          // center x position of rectangle
 pars.sposy=DOUB(strtod(argv[16],NULL));          // center x position of rectangle
 pars.sposedge=DOUB(strtod(argv[17],NULL));       // edge of starting positions rectangle
 pars.dirdamp=DOUB(strtod(argv[18],NULL));        // directional damping
 pars.J=DOUB(strtod(argv[19],NULL));              // coupling of internal state and attraction of agents
 int modu=int(strtod(argv[20],NULL));             // which time step to write out
 parsFDF.Nfx=int(strtod(argv[21],NULL));          // number of grid points in CFD simulation
 parsFDF.Nfy=int(strtod(argv[22],NULL));          // number of grid points in CFD simulation
 parsFDF.fxi=DOUB(strtod(argv[23],NULL));         // fluid fields left x bound
 parsFDF.fxf=DOUB(strtod(argv[24],NULL));         // right x bound
 parsFDF.fyi=DOUB(strtod(argv[25],NULL));         // left y bound
 parsFDF.fyf=DOUB(strtod(argv[26],NULL));         // right y bound
 DOUB Dx=DOUB(strtod(argv[27],NULL));             // Diffusion constant x
 DOUB Dy=DOUB(strtod(argv[28],NULL));             // y
 DOUB Dangle=DOUB(strtod(argv[29],NULL));         // angle of direction 
 DOUB Dphi=DOUB(strtod(argv[30],NULL));           // internal state
 parsFDF.image_path = argv[31];                   // file if read
 parsFDF.kappa=DOUB(strtod(argv[32],NULL));       // gaussian smoothing parameter to interpolate pixels onto internal grid
 pars.dir_lag=DOUB(strtod(argv[33],NULL));        // directional phase lag parameter
 pars.J2=DOUB(strtod(argv[34],NULL));             // directional sensitivity of coupling
 pars.J3=DOUB(strtod(argv[35],NULL));             // velocity oscillation parameters
 pars.eps2=DOUB(strtod(argv[36],NULL));           // intercation strength of direction 

 parsFDF.rho=DOUB(strtod(argv[37],NULL));         // density of fluid 
 parsFDF.mu=DOUB(strtod(argv[38],NULL));          // viscosity of fluid
 parsFDF.beta=DOUB(strtod(argv[39],NULL));        // relaxation prameter of pressur solver
 parsFDF.toll=DOUB(strtod(argv[40],NULL));        // tolerance of pressur solver 
 parsFDF.dtf=DOUB(strtod(argv[41],NULL));         // dt of fluid solver (initial value)
 parsFDF.DTr1=DOUB(strtod(argv[42],NULL));        // Diffusion coefficients of first tracer
 
 pars.vel_c=DOUB(strtod(argv[43],NULL));          // adaption velocity to counter the mean flow 
 pars.eps3=DOUB(strtod(argv[44],NULL));           // interaction strength of direction and velocity (counter current swimming)
 parsFDF.pos_inlet=int(strtod(argv[45],NULL));    // position of inlet
 parsFDF.pos_outlet=int(strtod(argv[46],NULL));   // position of outlet
 parsFDF.width_inlet=int(strtod(argv[47],NULL));  // width of outlet
 parsFDF.width_outlet=int(strtod(argv[48],NULL)); // width of outlet
 parsFDF.vel_inlet=DOUB(strtod(argv[49],NULL));   // velocity maximum after rampoff at the center of the inlet; or velocity of walls
 parsFDF.length_outlet=int(strtod(argv[50],NULL));// how long is the outlet channel
 DOUB T2=DOUB(strtod(argv[51],NULL));             // sim time
 pars.freq_c=DOUB(strtod(argv[52],NULL));         // frequency_activity adaption
 pars.mem_damp=DOUB(strtod(argv[53],NULL));       // memory damping value
 pars.eps4=DOUB(strtod(argv[54],NULL));           // interaction strength of direction and vorticity 
 pars.counter_curent=DOUB(strtod(argv[55],NULL)); // switcher for counter-curent swimming
 pars.osc_activity=DOUB(strtod(argv[56],NULL));   // switcher for coupling of frequency and activity
 pars.dif_activity=DOUB(strtod(argv[57],NULL));   // switcher for coupling of diffusivity and activity
 pars.agent_atr=DOUB(strtod(argv[58],NULL));      // switcher for attraction of agents
 pars.seasons_switch=DOUB(strtod(argv[59],NULL));  // switcher for seasons
 pars.agent_rep=DOUB(strtod(argv[60],NULL));      // switcher for repulsion of agents
 pars.x_tracer=DOUB(strtod(argv[61],NULL));       // x position of tracer distribution
 pars.y_tracer=DOUB(strtod(argv[62],NULL));       // y position of tracer distribution
 pars.amp_tracer=DOUB(strtod(argv[63],NULL));     // amplitude of tracer density
 pars.agent_direction=DOUB(strtod(argv[64],NULL));// switcher for directionality of agents (Q function that determines sensitivity of sensing)
 pars.Vs=DOUB(strtod(argv[65],NULL));             // turning point paramter of counter-current swimming 
 pars.tr_dir3=DOUB(strtod(argv[66],NULL));         // switcher for tracer orientation 1 means on, 0 means off 
 pars.tr_dir_steepnes=DOUB(strtod(argv[67],NULL));// 1/tr_dir_steepnes determines how steep the switching from 0 to 1 takes place when activity increases (vorticity and food tracer)
 pars.eps5=DOUB(strtod(argv[68],NULL));           // interaction strength of direction and tracer
 pars.counter_current_act=DOUB(strtod(argv[69],NULL)); // switcher counter curent activity enhancement 1 means on, 0 means off 
 pars.tr_steepnes=DOUB(strtod(argv[70],NULL));     // turning point paramter of counter-current swimming direction adaption (switches off cc swimming and swimming towards prey)
 pars.act_steepnes_2=DOUB(strtod(argv[71],NULL));// turning point parameter for switch off of gradient swimming towards prey
 pars.tr_dir2=DOUB(strtod(argv[72],NULL));         // switcher for counter-current swimming reduction when tracer is present 1 means loss of orientation, 0 means no loss of orientation 
 pars.tr_dir1=DOUB(strtod(argv[73],NULL));         // switcher for vorticity avoidance 1 means avoidance changes with activity, 0 means avoidance is not affected by activity 
 pars.mu2 =DOUB(strtod(argv[74],NULL));            // viscosity mu of the jellyfish couplig (basically a smaller scale turbulence viscosity)
 pars.omegmin =DOUB(strtod(argv[75],NULL));        // minimal intensity above which the wake vorticity is considered for far field coupling of active agents to the ghost agents
 int N_threats = int(strtod(argv[76],NULL));       // number of omp threats (used to check if wakes should be calculated in parallel)
 pars.phase_lag = DOUB(strtod(argv[77],NULL));     // phase lag of phase interaction function (bell oscillation), near field
 pars.phase_lag_far = DOUB(strtod(argv[78],NULL)); // phase lag of phase interaction function (bell oscillation), far field
 pars.dir_lag_far = DOUB(strtod(argv[79],NULL));   // angular lag of direction, far field
 pars.sig_cfd_couple = DOUB(strtod(argv[80],NULL));// signal tracer density rate that is transfered to the CFD grid
 pars.eps6 = DOUB(strtod(argv[81],NULL));          // interaction strength of signal tracer gradient direction and orientation
 pars.tr_dir4 = DOUB(strtod(argv[82],NULL));       // switcher for signal tracer swimming 
 pars.tr_dir_steepnes2 = DOUB(strtod(argv[83],NULL));// steepness of signal tracer direction interaction to signal tracer intensity switches
 pars.spread_width = DOUB(strtod(argv[84],NULL));  // spreading parameter that defines width of spreading window when signaling tracer is coupled into the CFD grid (also used for the food tracer)
 parsFDF.DTr_sig = DOUB(strtod(argv[85],NULL));    // Diffusion coefficients of second tracer (signaling)
 parsFDF.spread_tracer = DOUB(strtod(argv[86],NULL));// width parameter of tracer distribution for prey that sets the initial width of the distribution (inverse of length)
 pars.food_cfd_couple = DOUB(strtod(argv[87],NULL));// food tracer density rate that is transfered to the CFD grid
 parsFDF.initial_dat = argv[88];                   // path to file that holds initial conditions for all CFD fields
 pars.vel_dir = DOUB(strtod(argv[89],NULL));       // turning point parameter of counter current swimming orientation for dependence on velocity magnitude
 pars.vel_dir2 = DOUB(strtod(argv[90],NULL));      // switcher for counter current swimming orientation (dependence of orientation strength on velocity magnitude)
 pars.sig_dir = DOUB(strtod(argv[91],NULL));       // turning point parameter for orientation towards signaling tracer, dependence on magnitude of signaling tracer gradient
 pars.sig_dir2 = DOUB(strtod(argv[92],NULL));      // switcher for swimming towards signaling tracer
 pars.vort_dir = DOUB(strtod(argv[93],NULL));      // turning point parameter of vorticity avoidance orientation for dependence on vorticity
 pars.vort_dir2 = DOUB(strtod(argv[94],NULL));     // switcher for vorticity avoidance (dependence of orientation strength on vorticity magnitude)
 parsFDF.coast_name = argv[95];                    // path to file that holds zero-one array to specify the coast line
 parsFDF.dtmax = DOUB(strtod(argv[96],NULL));      // maximal CFD time step 
 parsFDF.dtmin = DOUB(strtod(argv[97],NULL));      // minimal CFD time step 
 parsFDF.CFL = DOUB(strtod(argv[98],NULL));        // CFL critical number
 parsFDF.CSL = DOUB(strtod(argv[99],NULL));        // Smagorinsky constant
 parsFDF.DIVMAX = DOUB(strtod(argv[100],NULL));    // maximum divergence allowed before maximum pressure steps are increased
 parsFDF.damp = DOUB(strtod(argv[101],NULL));      // damping factor to enforce parallel flow in the outlet
 parsFDF.Vwind = DOUB(strtod(argv[102],NULL));     // wind speed
 parsFDF.thwind = DOUB(strtod(argv[103],NULL));    // wind direction
 DOUB Dangle2=DOUB(strtod(argv[104],NULL));        // diffusion constant for direct angular white noise (not O-U) 
 parsFDF.NPRMAX = DOUB(strtod(argv[105],NULL));    // maximum allowed pressure steps 
 parsFDF.Corriolis = DOUB(strtod(argv[106],NULL)); // Corriolis parameter f=2*sin(th)*f0
 parsFDF.Npr = DOUB(strtod(argv[107],NULL));       // initial value of allowed pressure steps  
 pars.wake_life_rand  = DOUB(strtod(argv[108],NULL)); // number that causes randomized life times of wake angets. if zero, no randomization, if one, randomization according to OU-noise*T_wake_max
 pars.G = DOUB(strtod(argv[109],NULL));            // radial exponent of suction term in near field interaction
 parsFDF.columns_name = argv[110];                 // path to file that specifies the output columns of data agents, CFD and ghosts
 parsFDF.reaction_tracer =DOUB(strtod(argv[111],NULL)); // reaction rate food tracer
 parsFDF.reaction_tracer_sig =DOUB(strtod(argv[112],NULL)); // reaction rate signaling tracer
 pars.wrad = DOUB(strtod(argv[113],NULL));         // repulsion distance at outer walls of the swarming domain
 parsFDF.GravX = DOUB(strtod(argv[114],NULL));     // Gravitational force X
 parsFDF.GravY = DOUB(strtod(argv[115],NULL));     // Gravitational force Y
 parsFDF.Cpor1X = DOUB(strtod(argv[116],NULL));    // linear porous zone coefficient X
 parsFDF.Cpor1Y = DOUB(strtod(argv[117],NULL));    // linear porous zone coefficient Y
 parsFDF.Cpor2X = DOUB(strtod(argv[118],NULL));    // quadratic porous zone coefficient X
 parsFDF.Cpor2Y = DOUB(strtod(argv[119],NULL));    // quadratic porous zone coefficient Y
 pars.rho_jelly = DOUB(strtod(argv[120],NULL));    // jellyfish density
 
 vector<int> Cols;
 vector<int> Cols_FDFS;
 vector<int> Cols_Ghosts;
 output_setter(Cols, Cols_FDFS, Cols_Ghosts, parsFDF.columns_name);

 //### DEFINE VECTOR SIZES ###//
 cout << "Set_vector_size \n"; 
 int n(6), N0(N), Nadd(0);                                      //how many sets of variables x,y, Ornstein-Uhlenbeck noise, memory, direction angle, internal state (phi) =6*N
 Set_vector_size(FDF, parsFDF, pars, Data, N, Dx, Dy, Dangle, Dphi, Dangle2, n);
 Texe[1] = omp_get_wtime() - Sum_time(Texe, 1);
 
 //### SET FREQUENCY DISTRIBUTION ###//
 cout << "random frequencies \n"; 
 random_device generator;

 //default_random_engine generator;
 normal_distribution<DOUB> distribution(pars.freq1,pars.freq2);
 for(int k=0;k<pars.freq.size();k++)
  {
   pars.freq[k] = distribution(generator);
  }
 Texe[2] = omp_get_wtime() - Sum_time(Texe, 2);

 //### SET REPULSION DISTANCE AT WALLS AND COASTAL MAP ###//
 wall_distance(parsFDF, pars); 
 pars.rho_jelly = parsFDF.rho - pars.rho_jelly;// set density difference for buoyancy of jellyfish
 DOUB V_jelly(0.0), a(pars.irad/(pars.J3+1.0)), b(1.0/(2.0+pars.G));
 DOUB c(ApowB(a,b));
 cout << "Volume of jellyfish= " << 4*pi*c*c*c/3.0 << "\n";
 pars.rho_jelly *= parsFDF.GravY*4*pi*c*c*c/3.0;

//#ifdef complex_boundary
 cout << "pressure mapper \n";
 Pressure_staggered_grid_mapper(parsFDF); //introduces a mapping between the staggered grid cells of velocity and the boundary vector entries to allow for complex boundaries  
//#endif
 Texe[3] = omp_get_wtime() - Sum_time(Texe, 3);

 //### SET INITIAL POSITIONS OF SWARM ###// 
 Ensemble_init_cond(Data, pars, N, n, generator);
 
 //### Set initial noise components of agents dynamics ###//
 normal_distribution<DOUB> Snormal(0.0,1.0);
 for(int k=0;k<n*N;k++){Data.noise[k]=Snormal(generator);}//noise
 Texe[4] = omp_get_wtime() - Sum_time(Texe, 4);

 //### SET CFD GRID ###//
 CFD_Set1(FDF, parsFDF);
 Texe[5] = omp_get_wtime() - Sum_time(Texe, 5);

 //### PERFORM CFD TRANSIENT ###//
 DOUB t(0.0), tf(0.0), tsim(DOUB(modu)*dt0), dt(dt0);//time of agents simulation (main time) and time of fluid simulation (according to time-step control much shorter steps)
 int step(1), step_img(0);
 CFD_Set2(FDF, parsFDF, pars, tf, t, T, T2);
 Texe[6] = omp_get_wtime() - Sum_time(Texe, 6);

 //### Check diffusion constant ###//
#ifdef adjust_diffusion_coeff
 Check_diffusion_coeff(parsFDF.DTr1, parsFDF.DTr_sig, parsFDF.dx, parsFDF.dy, parsFDF.vel_inlet);
#endif  
  
 //### DEFINE FILE FOR PARAMETERS ###//
 cout << " output all model parameters \n"; 
 std::stringstream fouts2; fouts2 << "parameters" << argv[1];
 std::string name2 = fouts2.str(); ofstream fout2(name2,ios::out);
 fout2.setf(ios::scientific); fout2.precision(6);
 //output all model parameters
 parameter_output(fout2, pars, parsFDF, T2, N, n);
 fout2.close();
 
 //### DEFINE FILES FOR OUTPUT ###//
 //agents
 std::stringstream fouts; fouts << argv[1];
 std::string name = fouts.str(); ofstream fout(name,ios::out);
 fout.setf(ios::scientific); fout.precision(6);

 //example agent
 std::stringstream fouts3; fouts3 << "Traject_" << argv[1];
 std::string name3 = fouts3.str(); ofstream fout3(name3,ios::out);
 fout3.setf(ios::scientific); fout3.precision(6);

 //fluid field (at t=0) in the main domain
 std::stringstream fouts4; fouts4 << "FDFS_" << argv[1];
 std::string name4 = fouts4.str(); ofstream fout4(name4,ios::out);
 fout4.setf(ios::scientific); fout4.precision(6);

 //fluid field (at t=0) in the outlet
 std::stringstream fouts5; fouts5 << "FDFS_Outlet_" << argv[1];
 std::string name5 = fouts5.str(); ofstream fout5(name5,ios::out);
 fout5.setf(ios::scientific); fout5.precision(6);
 
 //Inflow vector field at boundary
 std::stringstream fouts6; fouts6 << "Volume_flow_" << argv[1];
 std::string name6 = fouts6.str(); ofstream fout6(name6,ios::out);
 fout6.setf(ios::scientific); fout6.precision(6);
 
 //Inflow and outflow rate at inlet and outlet average 
 std::stringstream fouts7; fouts7 << "Volume_flow_out_" << argv[1];
 std::string name7 = fouts7.str(); ofstream fout7(name7,ios::out);
 fout7.setf(ios::scientific); fout7.precision(6);

 //averaged y velocity as a function of x
 std::stringstream fouts8; fouts8 << "V_average_X_" << argv[1];
 std::string name8 = fouts8.str(); ofstream fout8(name8,ios::out);
 fout8.setf(ios::scientific); fout8.precision(6);
 
 //averaged x velocity as a function of y
 std::stringstream fouts9; fouts9 << "U_average_Y_" << argv[1];
 std::string name9 = fouts9.str(); ofstream fout9(name9,ios::out);
 fout9.setf(ios::scientific); fout9.precision(6);

 //output ghosts positions
 std::stringstream fouts10; fouts10 << "Ghosts_" << argv[1];
 std::string name10 = fouts10.str(); ofstream fout10(name10,ios::out);
 fout10.setf(ios::scientific); fout10.precision(6);

 //output ghosts positions
 std::stringstream foutsDist; foutsDist << "Dist_" << argv[1];
 std::string nameDist = foutsDist.str(); ofstream foutDist(nameDist,ios::out);
 foutDist.setf(ios::scientific); foutDist.precision(6);

 //output ghosts positions
 std::stringstream foutsCorr; foutsCorr << "Corr_" << argv[1];
 std::string nameCorr = foutsCorr.str(); ofstream foutCorr(nameCorr,ios::out);
 foutCorr.setf(ios::scientific); foutCorr.precision(6);
 
#ifdef save_init_cond
 std::stringstream fouts11; fouts11 << "Init_cond_" << argv[1];
 std::string name11 = fouts11.str(); ofstream fout11(name11,ios::out);
 fout11.setf(ios::scientific); fout11.precision(6);
#endif

 //### FIND INITIAL INDEX OF PARTICLES IN CFD GRID AND FIELD VALUES ###//
 cout << " find intial CFD indicees \n";
 FDF_index(Data.pos, N, FDF.gridX, FDF.gridY, pars.FDF_indexX, pars.FDF_indexY);//finds closest grid points of active agents
 FDF_index(Data.pos2, N, FDF.gridX, FDF.gridY, pars.FDF_indexX2, pars.FDF_indexY2);//finds closest grid points of passive agents
 FDF_index(Data.pos3, pars.N3, FDF.gridX, FDF.gridY, pars.FDF_indexX3, pars.FDF_indexY3);//finds closest grid points of ghost agents
 Keep_initial_positions(Data.pos, Data.pos0, Data.pos2, Data.pos20, N, n, pars.FDF_indexX, pars.FDF_indexY, pars.FDF_indexX0, pars.FDF_indexY0, pars.FDF_indexX2, pars.FDF_indexY2, pars.FDF_indexX20, pars.FDF_indexY20, pars.freq, pars.freq0);
 
 Texe[7] = omp_get_wtime() - Sum_time(Texe, 7);

 //### INTERPOLATE CFD VALUES TO AGENT POSITIONS ###//
 CFD_to_agents_interpolation(Data, pars, FDF, parsFDF);
 Texe[8] = omp_get_wtime() - Sum_time(Texe, 8);
 
 //### Set coastal cells of agents ###//
 Coast_update(parsFDF, pars, N);
 Texe[9] = omp_get_wtime() - Sum_time(Texe, 9);
 
 //print out t=0 agents, passive agents
 cout << " print initial state of agents to file \n";
 Data_Print(t, Data, N, n, pars, fout, fout3, Cols); //write out t=0 for agents

 //print out t=0 fluid fields
#ifdef FDF_out
 Data_print_FDF(t,FDF, parsFDF, parsFDF.Nfx, parsFDF.Nfy, fout4, 2*parsFDF.width_outlet+1,parsFDF.length_outlet,parsFDF.pos_outlet,fout5,Cols_FDFS);//write out t=0 for fluid fields 
#endif

 //print out t=0 example agent
 fout3 << t << " " << Data.pos[0] << " " << Data.pos[1] << " " << Data.pos[(n-3)*N] << " " << Data.pos[(n-2)*N] << " " << Data.pos[(n-1)*N]; for(int k=0;k<4;k++){fout3 << " " << FDF.gridX[pars.FDF_indexX[0][k]] << " " << FDF.gridY[pars.FDF_indexY[0][k]];} fout3 << " " << pars.intensity[0] << " " << Data.vel[0] << " " << Data.vel[1] << " " << pars.UXA[0] << " " << pars.UYA[0] << " " << Data.pos[(n-3)*N] << " " << pars.TrgradXA[0] << " " << pars.TrgradYA[0] << " " << pars.VorticityA[0] << " " << pars.Vorticity_gradXA[0] << " " << pars.Vorticity_gradYA[0] << " " << Data.pos2[0] << " " << Data.pos2[1] << " " << Data.vel2[0] << " " << Data.vel2[1] << " " << Data.pos[(n-4)*N] << "\n";

 //print to file one starting position of a ghost
 fout10 << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << "\n";//outut x,y, and angle of self drift velocity of vortex (theta3(t))
 fout10 << "\n";
 
 //step wise plotting of data with gnuplot (preparation)
#ifdef gnuplot
 gnuplot_starter();
#endif
 Texe[10] = omp_get_wtime() - Sum_time(Texe, 10);

 //keep initial conditions and reset time
 if(parsFDF.dtf>dt){cout << "keeping the CFD dynamics at t=0 \n"; Keep_init(FDF);}//keeping initial
 Texe[11] = omp_get_wtime() - Sum_time(Texe, 11);
 t=0.0;
 tf=0.0;

 //### MAIN LOOP ###//
 cout << "start simulation \n";
 DOUB T_old(0.0);
 //### test statistics ###//
 vector<DOUB> dist1(101), dist2(101), dist3(101), dist4(101), dist5(101), dist6(101), Noise1, Noise2, AutoCorr1, AutoCorr2, CrossCorr;
 DOUB Norm1(0.0), Norm2(0.0), Norm3(0.0), Norm4(0.0), Norm5(0.0), Norm6(0.0);
 while(t<T2)
  { 
   //### Keep track of old phases ###//
   phase_tracker(pars.oldphases,N,n,Data.pos);
   T_old=Sum_time(Texe, 32); Texe[12] += omp_get_wtime() - T_old;//NOTE: subtract all previously in the loop accumulated time periods TODO: not 29 but 30?
   //cout << 12 << "\n";
  
   //### convergence check of RK2 method ###//
#ifdef RK_dt
   RK_STEP_Control(Data.pos, t, n, N, dt, pars, Data.noise, Data.vel, dt0, tsim);//TODO: adapt exchange rates for tracers
#endif   
   
   //cout << " START\n";
   //DOUB TCoast(omp_get_wtime());
   //### simulate new positions active particles ###//
   rk2_stoch(dgl, Data.pos, 1, t, n*N, n, dt, pars, Data.noise, Data.vel);//2nd order stochastic Heun method (active agents)
   //rk2(dgl, Data.pos, 1, t, n*N, n, dt, pars, Data.vel);//2nd order stochastic Heun method (active agents)
   t -= dt;//avoid double stepping of t
   T_old=Sum_time(Texe, 32); Texe[13] += omp_get_wtime() - T_old;
   //cout << 13 << "\n";
   //### simulate new positions passive particles ###//
   rk2(dgl2, Data.pos2, 1, t, 2*N, 2, dt, pars, Data.vel2);//2nd order Heun method (passive agents)
   //cout << " END: " << omp_get_wtime() - TCoast << " \n";
   t -= dt;//avoid double stepping of t
   T_old=Sum_time(Texe, 32); Texe[14] += omp_get_wtime() - T_old;
   //cout << 14 << "\n";
 //!!!! t += dt;
   //### measure phases of active agents ###//
   //NOTE: This function does the following:
   //      1. checking if an agent has completed a bell cycle (phi=2pi?)
   //      2. resizing the vector of ghost positions (Data.pos3), velocity (Data.vel3), CFD grid indices (FDF_indexX3, FDF_indexY3), coupler positions (x3t,y3t) and intial velocities (velx3,vely3)  
   //         -> if a period is completed, a ghost is added
   set_ghosts(pars, Data.pos3, Data.vel3, N, n, Data.pos, Data.vel, t, parsFDF, generator);
   T_old=Sum_time(Texe, 32); Texe[15] += omp_get_wtime() - T_old;
   //cout << 15 << "\n";

   //### simulate new positions ghost particles ###// 
   if(pars.N3>=N_threats)//parallelizm for large ensembles
    {
     rk2(dgl3_omp, Data.pos3, 1, t, 3*pars.N3, 3, dt, pars, Data.vel3);//2nd order Heun method (ghost agents)
    }
   else
    {
     rk2(dgl3, Data.pos3, 1, t, 3*pars.N3, 3, dt, pars, Data.vel3);//2nd order Heun method (ghost agents)
    }
   T_old=Sum_time(Texe, 32); Texe[16] += omp_get_wtime() - T_old;
   //cout << 16 << "\n";
  
   //### update ghost positions for coupling to the active agents ###//
   //NOTE: This function copies the values of the wake dynamics to the exchange structure (pars)
   update_ghosts_coupler(pars, Data.pos3);
   T_old=Sum_time(Texe, 32); Texe[17] += omp_get_wtime() - T_old;
   //cout << 17 << "\n";

#ifdef Jelly_add    
   //### Delete active agents when they hit a boundary cell ###//
   //NOTE: Function deletes jellyfish if they are in a boundary cell (beaching)
   //      -> jellyfish are deleted here
   cout << "###### Check jellies ####### \n";
   cout << "\n";
   Nadd = 2 + N0*Njelly_set(t,T,T2);
   cout<< "Nadd= " << 2 + N0*Njelly_set(t,T,T2) << "\n";
   delete_jelly(pars, Data.pos, Data.vel, Data.pos2, Data.vel2, Data.noise, t, parsFDF, Data.pos0, Data.pos20, pars.FDF_indexX0, pars.FDF_indexY0, pars.FDF_indexX20, pars.FDF_indexY20, pars.freq0, N, n, Dx, Dy, Dangle, Dphi, Dangle2, T2, N0, Nadd, generator);
//    delete_jelly(pars, Data.pos, Data.vel, Data.pos2, Data.vel2, Data.noise, t, parsFDF, N, n);
    //delete_jelly(pars, Data.pos, Data.vel, t, FDF.gridX, FDF.gridY, parsFDF); 
   Coast_update(parsFDF, pars, N); 
   cout << "########################## \n";
   cout << "\n";
#endif
    T_old=Sum_time(Texe, 32); Texe[18] += omp_get_wtime() - T_old;
   
   //### Couple Fluid to agent presence ###//
   //NOTE: Function shifts necessary quantities to the level of the CFD simulation by averaging
   //      -> When ghosts get deleted they are used as forcing for the advection diffusion dynamics of a signaling tracer
   //      - can be augmented to more tracers if needed
   Agent_to_CFD_coupling(pars, Data.pos3, Data.vel3, t, Data.pos, Data.vel, FDF.gridX, FDF.gridY, FDF.intensity_sig_back_coupler, pars.FDF_indexX3, pars.FDF_indexY3, parsFDF.Nfx, parsFDF.Nfy, parsFDF, dt, N, n, pars.FDF_indexX, pars.FDF_indexY, FDF.intensity, FDF.intensity_back_coupler, FDF.intensity_sig);
   //NOTE: this function adds to the existing tracer field(s) the agent tracer 
   NS_FORCE_TRACER(FDF.intensity, FDF.intensity_sig, FDF.intensity_sig_back_coupler, FDF.intensity_back_coupler, parsFDF.Nfx, parsFDF.Nfy); //TODO: shift values to CFD grid only in each CFD time step! now the values are shifted in between cfd steps at each agent time step causing the interpolation to behave spiky because the end values are changed in between such that the former interpolation gets abandoned the more the simulation comes to the new cfd time. This effect is only visible in the absolute values of the food tracer and mostly signaling tracer as they are altered by the agents while other quantities, even the prognostic gradients are not suffering because they are calculated based on the interpolated fields but only weakly depend on their values. Also, th edirectional interaction does not suffer because the angles are amplitude free.
   T_old=Sum_time(Texe, 32); Texe[19] += omp_get_wtime() - T_old;
   //cout << 18 << "\n";
   
   //### delete ghosts when they leave the domain ###//
   //NOTE: Function deletes wake points when these points have left the flow domain or/and if they have decayed too much (given by pars.omegmin)
   //      -> ghosts are deleted here
   delete_ghosts(pars, Data.pos3, Data.vel3, t, FDF.gridX, FDF.gridY);
   T_old=Sum_time(Texe, 32); Texe[20] += omp_get_wtime() - T_old;
   //cout << 19 << "\n";
   
   //### check if ghosts have hit a wall ###//
   //NOTE: when ghost have hit a wall, the ambient vorticity is set to zero such that the direction of movement does not change any more
   //      -> ghosts can not return once they have left the domain 
   check_ghosts(Data.pos3, FDF.gridX, FDF.gridY, pars);
   T_old=Sum_time(Texe, 32); Texe[21] += omp_get_wtime() - T_old;
   //cout << 20 << "\n";
   
   //### KEEP OLD CFD VALUES ###//
   //NOTE: If dtf is larger than dt, the code is designed with the following workflow:
   //      1. at t=0, the agent solver updates t such that t>tf
   //         - The if below is active now: The inital conditions are kept
   //      3. The CFD solver gets active and performs one step ahead of t to tf+dtf>t
   //         - The CFD fields are updated to F(tf+dtf) but the keeping is inactive since t<tf
   //      4. Once t=tf no CFD update but still an agent update is possible with the new values F(tf+dtf)
   //         - The agent solver steps to t+dt>tf+dtf The keeping gets active and the process starts anew
   //      This procedure is needed to interpolate CFD values in time when dtf>dt
   //      Important: avoide dtf=dt it causes the interpolation to not work properly as Field keeping is disabled but time interpolation is enabled.
   //  t=0, tf=0 
   //  agents make one step                      interpolation at t=tf+dtf and update to t+dt>tf+dtf
   //  |                                         ||
   //  |.........................................|.........................................|
   //   keep old CFD, CFD solver makes a step    no CFD step 
   //   |                                        |keep old CFD as tf+dtf<t+dt, CFD solver makes a step to t+2dtf
   //   |                                        ||
   //   |                                      tf+dtf
   //  |-----------------------------------------|-----------------------------------------|
   if((parsFDF.dtf>dt)&(t>tf)){cout << "t>tf: " << t << ">" << tf << " "; Keep_init(FDF);}//keeping old CFD fields
   T_old=Sum_time(Texe, 32); Texe[22] += omp_get_wtime() - T_old;
   
   //### UPDATE CFD FIELDS ###//
   CFD_INTEGRATE(FDF, parsFDF, T, T2, t, tf, pt, t_cfd, step_img, fout6, fout7, fout8, fout9);
   T_old=Sum_time(Texe, 32); Texe[23] += omp_get_wtime() - T_old;
   
   //### INTERPOLATE FIELDS ONTO NEW POSITIONS ###//
   Update_FDF_index(Data.pos, N, FDF.gridX, FDF.gridY, pars.FDF_indexX, pars.FDF_indexY);                                        //active agents
   Update_FDF_index(Data.pos2, N, FDF.gridX, FDF.gridY, pars.FDF_indexX2, pars.FDF_indexY2);                                     //passive agents
   Update_FDF_index_ghost(Data.pos3, pars.N3, FDF.gridX, FDF.gridY, pars.FDF_indexX3, pars.FDF_indexY3, parsFDF.dx, parsFDF.dy); //ghost agents
   T_old=Sum_time(Texe, 32); Texe[24] += omp_get_wtime() - T_old;
  
#ifdef complex_boundary
   //### hard boundary reset positions ###//
   Coast_update(parsFDF, pars, N); 
   reflect_Boundary(Data, pars, parsFDF, dt);
   Update_FDF_index(Data.pos, N, FDF.gridX, FDF.gridY, pars.FDF_indexX, pars.FDF_indexY);                                        //active agents
#endif     
  
   //CFD_to_agents_interpolation(Data, pars, FDF, parsFDF);
   if(parsFDF.dtf<dt){CFD_to_agents_interpolation(Data, pars, FDF, parsFDF);}// for high CFD resolution in time
   else{CFD_to_agents_interpolation_2(Data, pars, FDF, parsFDF, tf, t);}// for low CFD resolution in time
   T_old=Sum_time(Texe, 32); Texe[25] += omp_get_wtime() - T_old;

   //### update diffusion constants for angle ###//
   for(int k=0;k<N;k++){pars.D[(n-4)*N+k] = Dangle*dir_diff(Data.pos[(n-3)*N+k], pars.dif_activity, pars.freq_c);}   
   T_old=Sum_time(Texe, 32); Texe[26] += omp_get_wtime() - T_old;
   //### update stochastic components ###//
   Update_stochastic_components(Data.noise, N, n, t, dt, generator);
   Noise1.push_back(Data.noise[1]);
   Noise2.push_back(Data.noise[N+1]);
   for(int k=0;k<dist1.size();k++)
    {
     for(int l=0;l<N;l++)
      {
       if((-5.0+(DOUB(k)-0.5)*10.0/100.0<Data.noise[l]) & (-5.0+(DOUB(k)+0.5)*10.0/100.0>=Data.noise[l])){dist1[k] +=1.0; Norm1+=0.1;}
       if((-5.0+(DOUB(k)-0.5)*10.0/100.0<Data.noise[l+N]) & (-5.0+(DOUB(k)+0.5)*10.0/100.0>=Data.noise[l+N])){dist2[k] +=1.0; Norm2+=0.1;}
       if((-5.0+(DOUB(k)-0.5)*10.0/100.0<Data.noise[l+2*N]) & (-5.0+(DOUB(k)+0.5)*10.0/100.0>=Data.noise[l+2*N])){dist3[k] +=1.0; Norm3+=0.1;}
       if((-5.0+(DOUB(k)-0.5)*10.0/100.0<Data.noise[l+3*N]) & (-5.0+(DOUB(k)+0.5)*10.0/100.0>=Data.noise[l+3*N])){dist4[k] +=1.0; Norm4+=0.1;}
       if((-5.0+(DOUB(k)-0.5)*10.0/100.0<Data.noise[l+4*N]) & (-5.0+(DOUB(k)+0.5)*10.0/100.0>=Data.noise[l+4*N])){dist5[k] +=1.0; Norm5+=0.1;}
       if((-5.0+(DOUB(k)-0.5)*10.0/100.0<Data.noise[l+5*N]) & (-5.0+(DOUB(k)+0.5)*10.0/100.0>=Data.noise[l+5*N])){dist6[k] +=1.0; Norm6+=0.1;}
      }
    }
 //  for(int k=0;k<n*N/2;k+=2)
 //    {
 //     r1=distributionU(generator); 
 //     r2=distributionU(generator); 
 //     vector<DOUB> SR(Box_Muller(r1, r2));
 //     Data.noise[k]=SR[0];
 //     Data.noise[k+1]=SR[1];
 //    } //Snormal(generator);}//noise 
   T_old=Sum_time(Texe, 32); Texe[27] += omp_get_wtime() - T_old;
   
   //### Update Coastal cells of agents ###//
   Coast_update(parsFDF, pars, N);
   T_old=Sum_time(Texe, 32); Texe[28] += omp_get_wtime() - T_old;
   
   //### Print data to files ###//
   //step+=1;
   //if(step%modu==0)
   if(t>=tsim)
    {
     cout << "Data print at t,tsim,N3= " << t << " " << tsim << " " << pars.N3 << "\n"; 
     //data output active agents
     Data_Print(tsim, Data, N, n, pars, fout, fout3, Cols); //cout << "### write out at t= " << t << "\n";
#ifdef CFD_field_from_integrator
#ifdef FDF_out
     //data output fluid fields
     Data_print_FDF(tsim,FDF, parsFDF, parsFDF.Nfx, parsFDF.Nfy,fout4, 2*parsFDF.width_outlet+1,parsFDF.length_outlet,parsFDF.pos_outlet,fout5,Cols_FDFS); //write out fluid fields for closest moment in time
#endif
#endif
#ifdef CFD_field_from_image_dynamic
#ifdef FDF_out
     //data output fluid fields
     Data_print_FDF(tsim,FDF,parsFDF.Nfx, parsFDF.Nfy,fout4, 2*parsFDF.width_outlet+1,parsFDF.length_outlet,parsFDF.pos_outlet,fout5,Cols_FDFS); //write out fluid fields for closest moment in time
#endif
#endif
     
     //data output example agent
     fout3 << tsim << " " << Data.pos[0] << " " << Data.pos[1] << " " << Data.pos[(n-3)*N] << " " << Data.pos[(n-2)*N] << " " << Data.pos[(n-1)*N]; for(int k=0;k<4;k++){fout3 << " " << FDF.gridX[pars.FDF_indexX[0][k]] << " " << FDF.gridY[pars.FDF_indexY[0][k]];} fout3 << " " << pars.intensity[0] << " " << Data.vel[0] << " " << Data.vel[1] << " " << pars.UXA[0] << " " << pars.UYA[0] << " " << Data.pos[(n-3)*N] << " " << pars.TrgradXA[0] << " " << pars.TrgradYA[0] << " " << pars.VorticityA[0] << " " << pars.Vorticity_gradXA[0] << " " << pars.Vorticity_gradYA[0] << " " << Data.pos2[0] << " " << Data.pos2[1] << " " << Data.vel2[0] << " " << Data.vel2[1] << " " << Data.pos[(n-4)*N] << "\n";
   
     //output ghosts positions
#ifdef output_ghosts
     Data_Print_Ghosts(tsim, Data, pars, fout10,Cols_Ghosts);
#endif

     cout << " N_jelly= " << N << " N_ghosts= " << pars.N3 << " " << modu << "\n"; 
     tsim += DOUB(modu)*dt0;
    }//off if step
   T_old=Sum_time(Texe, 32); Texe[29] += omp_get_wtime() - T_old;
    //### reset coupling fluxes ###//
    //NOTE: Function resets fluxes for each cell to zero as wakes can just be shifted once to the CFD level
    Agent_to_CFD_coupling_reset(pars, Data.pos3, Data.vel3, t, Data.pos, Data.vel, FDF.gridX, FDF.gridY, FDF.intensity_sig_back_coupler, pars.FDF_indexX3, pars.FDF_indexY3, parsFDF.Nfx, parsFDF.Nfy, parsFDF, dt, FDF.intensity_back_coupler);
   T_old=Sum_time(Texe, 32); Texe[30] += omp_get_wtime() - T_old;
 
    //### Check for errors in data ###//
#ifdef error_check
    ERROR_OUT(Data, FDF, N, pars, t, T2);
#endif    
    T_old=Sum_time(Texe, 32); Texe[31] += omp_get_wtime() - T_old;
 
    //stepwise plotting of data with gnuplot (plotting)
#ifdef gnuplot
    gnuplot_plotter(FDF, Data, N, n, parsFDF.Nfx, parsFDF.Nfy, t);
#endif
  }//off while
 //### Output noise distributions ###//
 for(int k=0;k<dist1.size();k++)
  {
   foutDist << (-5.0+(DOUB(k))*10.0/100.0) << " " << dist1[k]/Norm1 << " " << dist2[k]/Norm2 << " " << dist3[k]/Norm3 << " " << dist4[k]/Norm4 << " " << dist5[k]/Norm5 << " " << dist6[k]/Norm6 << "\n";
  }
 //### Determine Auto and Cross correlations ###//
 vector<DOUB> Noise1_2(Noise1);
 vector<DOUB> Noise2_2(Noise2);
 DOUB AC1(0.0), AC2(0.0), CC(0.0);
 for(int l=0;l<int(T2/dt)/2;l++)
  {
   AC1=0.0;
   AC2=0.0;
   CC=0.0;
   for(int k=0;k<Noise1.size()-int(T2/dt)/2;k++)
    {
     AC1 +=Noise1[k]*Noise1_2[k+l];
     AC2 +=Noise2[k]*Noise2_2[k+l];
     CC +=Noise1[k]*Noise2[k+l];
    }
  foutCorr << dt*DOUB(l) << " " << AC1/(DOUB(Noise1.size()-int(T2/dt)/2)) << " " << AC2/(DOUB(Noise1.size()-int(T2/dt)/2)) << " " << CC/(DOUB(Noise1.size()-int(T2/dt)/2)) << "\n";
  }
 
#ifdef save_init_cond 
 //### save initial conditions for start of other simulation ###//
 Save_Init(fout11, FDF, parsFDF);
 fout11.close();
#endif
 
 fout.close();
 fout3.close();
 fout4.close();//comment out if several files are used
 fout5.close();
 fout6.close();
 fout7.close();
 fout8.close();
 fout9.close();
 fout10.close();
 foutDist.close();
 foutCorr.close();
 Texe[32] = omp_get_wtime() - Sum_time(Texe, 32);
 
 cout << "t_start= " << Texe[0] << "\n";  
 cout << "t_end= " << Sum_time(Texe, 33) << "\n";
 cout << "Process time= " << Sum_time(Texe, 33)-Texe[0] << "\n";
 for(int k=1;k<Texe.size();k++){cout << "process task "<< k << ": "<< Texe[k] << " >> " << Texe[k]/(Sum_time(Texe, 33)-Texe[0])*100.0 << " % \n";}
 
 return 0;  
}
