// ###       STRUCTS FOR JELLFISH SIMULATION          ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Tel Aviv    ### //
// ###                                                ### //

#include "baseInclude.h"

//### Dtype for precission controlle ###//
typedef long double DOUB ;

const DOUB pi(atan(1.0)*4.0); //pi

//### DEFINITION OF MAIN DATA STRUCTURE ###
typedef struct Vectordata
{
  vector<DOUB> pos;   //positions of agents
  vector<DOUB> pos2;  //positions of passive agents
  vector<DOUB> pos0;  //positions of agents
  vector<DOUB> pos20; //positions of passive agents
  vector<DOUB> pos3;  //positions of ghost particles
  vector<DOUB> vel;   //velocity of agents
  vector<DOUB> vel2;  //velocity of passive agents
  vector<DOUB> vel3;  //velocity of ghost particles
  vector<DOUB> noise; //component wise noise
}Vectordata;

typedef struct params
 {
  DOUB freq1;        //mean of internal natural frequencies
  DOUB freq2;        //standard deviation
  DOUB vel0;         //positional velocity
  vector<DOUB> freq; //natural frequencies
  vector<DOUB> freq0;//natural frequencies at t=0
  DOUB eps;          //strength of phase interaction between agents (internal state)
  DOUB eps2;         //strength of directional interaction between agents
  DOUB irad;         //radius of interaction
  DOUB xi;           //domain left
  DOUB xf;           //domain right
  DOUB yi;           //domain bottom
  DOUB yf;           //domain top
  DOUB thresh;       //detection threshold
  vector<DOUB> intensity; //food intensity field for agents (to be copied from CFD simulation)
  vector<DOUB> intensity_sig; //tracer intensity field for agents (to be copied from CFD simulation)
  vector<DOUB> intensity2;//intensity field for agents (to be copied from CFD simulation) passive agents
  vector<DOUB> UXA;       // x velocity at agents
  vector<DOUB> UYA;       // y velocity at agents
  vector<DOUB> UXA2;      // x velocity at passive agents
  vector<DOUB> UYA2;      // y velocity at passive agents
  vector<DOUB> PrA;       // Pressur at agents
  vector<DOUB> LEVA;      // Large Eddy Viscosity
  vector<DOUB> VorticityA;         // vorticity at agents
  vector<DOUB> VorticityA2;        // vorticity at passive agents
  vector<DOUB> Vorticity_gradXA;   // vorticity gradient X at agents
  vector<DOUB> Vorticity_gradYA;   // vorticity gradient Y at agents
  vector<DOUB> LEV_gradXA;         // vorticity gradient X at agents
  vector<DOUB> LEV_gradYA;         // vorticity gradient Y at agents
  vector<DOUB> TrgradXA;           // X gradient of food intensity at agents
  vector<DOUB> TrgradYA;           // Y gradient of food intensity at agents
  vector<DOUB> Tr_Sig_gradXA;      // X gradient of signal intensity at agents
  vector<DOUB> Tr_Sig_gradYA;      // Y gradient of signal intensity at agents
  vector<vector<int>> FDF_indexX;  //index of CFD vector position of each agent X
  vector<vector<int>> FDF_indexY;  //index of CFD vector position of each agent Y
  vector<vector<int>> FDF_indexX2; //index of CFD vector position of each passive agent X
  vector<vector<int>> FDF_indexY2; //index of CFD vector position of each passive agent Y
  vector<vector<int>> FDF_indexX0; //index of CFD vector position of each agent X at t=0
  vector<vector<int>> FDF_indexY0; //index of CFD vector position of each agent Y at t=0
  vector<vector<int>> FDF_indexX20;//index of CFD vector position of each passive agent X at t=0
  vector<vector<int>> FDF_indexY20;//index of CFD vector position of each passive agent Y at t=0
  DOUB sposx;        //->X center of initial square of starting positions
  DOUB sposy;        //->Y
  DOUB sposedge;     //length of edge of starting square
  DOUB dirdamp;      //damping of moving direction
  DOUB wake_life_rand; //number in (0,1) that causes randomized deletion of wake angents. If 1: maximal randomization. If zero: no randomization
  DOUB dir_lag;      //phase lag parameter of moving direction, near field
  DOUB J;            //coupling parameter between internal state and attractive coupling
  DOUB G;            //exponent of suction term in near field interaction
  DOUB J2;           //sensitivity parameter -> Determines how sensitive the agent is in a given direction
  DOUB J3;           //velocity oscillation parameter due to oscillation of bell
  vector<DOUB> D;    //component wise Diffusion coefficients
  DOUB vel_c;        //adaption velocity of agents to counter the mean background flow
  DOUB eps3;         //influence of background flow on direction of swimming
  DOUB eps4;         //influence of vorticity gradient on direction of swimming
  DOUB freq_c;       //parameter of oscillatory activity increase according to tracer value
  DOUB mem_damp;     //parameter of memory value decay
  DOUB counter_curent; //switcher for countercurent swimming
  DOUB osc_activity; //switcher for coupling of frequency and activity
  DOUB dif_activity; //switcher for coupling of diffusivity and activity
  DOUB agent_atr;    //switcher for attraction of agents
  DOUB seasons_switch;//switcher for seasons
  DOUB agent_rep;    // switcher for repulsion of agents
  DOUB agent_direction; // switcher for repulsion of agents
  DOUB x_tracer;     // x position of tracer distribution
  DOUB y_tracer;     // y position of tracer distribution
  DOUB amp_tracer;   // amplitude of tracer density
  DOUB cut;          // constant of potential to be set in code such that at irad for given agent_rep there is a second equilibrium force cut*0.5*r^2 is the aditional potential term
  DOUB offset;       // fixed offset value of force at a given distance irad from r=0. Needed in dgl to calculate the actual interaction radius and cutoff potential
  DOUB Vs;           // turning point parameter of counter-current swimming
  DOUB tr_dir3;       // switcher of parametric changes for orientation towards prey (dependence on activity)
  DOUB tr_dir_steepnes; // parameter that determines how fast happens the switching response when activity increases
  DOUB eps5;         //influence of food tracer on direction of swimming
  DOUB counter_current_act;// switcher counter-current swimming activity
  DOUB tr_steepnes;  // switching parameter for loss of counter-current swimming orrientation
  DOUB act_steepnes_2; //stepp turning point parameter or steepness of switch off of gradient swimming
  DOUB tr_dir2;      // switcher of parametric changes from counter-current swimming to pure advection/random orientation (dependence on prey concentration)
  DOUB tr_dir1;      // switcher of parametric changes for vorticity avoidance (dependence on activity)
  DOUB wrad;         // repulsion distance at walls
  DOUB vel_dir;      // turning point paramter for velocity magnitude velocity coupling of direction
  DOUB vel_dir2;     // switcher for velocity velocity coupling (1 on 0, off, constant coupling)
  DOUB sig_dir;      // turning point paramter signaing tracer swimming, dependence on magnitude of signaling tracer gradient
  DOUB sig_dir2;     // switcher for signaling tracer swimming (1 on 0, off, constant coupling)
  DOUB vort_dir;     // turning point paramter for vorticity magnitude vorticity avoidance
  DOUB vort_dir2;    // switcher for vorticity vorticity coupling (1 on 0, off, constant coupling)
  DOUB rho_jelly;    // density of jellyfish
 
  //# for interaction of active agents #//
  vector<DOUB> UXA3;        // x velocity at ghost particles
  vector<DOUB> UYA3;        // y velocity at ghost particles
  vector<DOUB> absvel3;     // magnitude of velocity of ghost particles at generation
  vector<DOUB> Tgstart;     // start time of ghost particles at generation
  vector<DOUB> x3t;         // x position of ghost particles at time t
  vector<DOUB> y3t;         // y position of ghost particles at time t
  vector<DOUB> theta3t;     // orientation of the vortex dipole at time t (perpendicular to the dipole axis)
  vector<DOUB> VorticityA3; // vorticity at the center point of the vortex pair (position of the ghost particle)
  vector<int> start_agent;  // index of active agent from which the ghost agent emerged
  vector<int> agent_name; // index of active agent from which the ghost agent emerged (integer name not changed by reordering)
 
  vector<vector<int>> FDF_indexX3; // index of CFD vector position of each ghost particle X
  vector<vector<int>> FDF_indexY3; // index of CFD vector position of each ghost particle Y
  int N3;                 // number of ghost particles in the domain
  vector<DOUB> oldphases; // vector of old phases from bell oscillations of active particles
  DOUB mu2;               // viscosity of jellyfish vortex dynamics
  DOUB omegmin;           // minimal intensity above which the wake vorticity is considered for far field coupling of active agents to the ghost agents
  DOUB Tmax;              // maximal time of a wake vortex to have an intensity of at least omegmin
  vector<DOUB> Tmax_Wake; // individual wake life times (can be Tmax or distributed arround this value due to individual turbulence of each agent)
 
  DOUB phase_lag;         // phase lag of phase interaction function (bell oscillation), near field
  DOUB phase_lag_far;     // phase lag of phase interaction function (bell oscillation), far field
  DOUB dir_lag_far;       // angular lag of direction, far field
 
  //# Coupling of CFd fields to presence of agents in a cell #//
  DOUB sig_cfd_couple;    // signal tracer density exchange rate (when a ghost tracer is deleted)
  DOUB food_cfd_couple;   // food tracer density exchange rate
  DOUB eps6;              // influence of signal tracer on direction of swimming
  DOUB tr_dir_steepnes2;  // parameter that determines how fast happens the switching response when activity increases (for signaling tracer)
  DOUB tr_dir4;           // switcher of parametric changes from some swimming to signal tracer oriented swimming (dependence of orientation on signaling tracer)
  DOUB spread_width;      // spread width when tracer is given to the CFD domain (determines how large spreading window is in units of cell steps)
 
  //# for immersed boundary conditions #//
  vector<vector<DOUB>> is_boundary;  // directly adjacent boundary cells of agents indicator
  vector<vector<DOUB>> boundaryX;    // directly adjacent boundary cells of agents x position
  vector<vector<DOUB>> boundaryY;    // directly adjacent boundary cells of agents y position
  vector<vector<DOUB>> is_boundary2; // directly adjacent boundary cells of agents indicator passive
  vector<vector<DOUB>> boundaryX2;   // directly adjacent boundary cells of agents x position passive
  vector<vector<DOUB>> boundaryY2;   // directly adjacent boundary cells of agents y position passive
  vector<vector<DOUB>> is_boundary3; // directly adjacent boundary cells of agents indicator ghosts
  vector<vector<DOUB>> boundaryX3;   // directly adjacent boundary cells of agents x position ghosts
  vector<vector<DOUB>> boundaryY3;   // directly adjacent boundary cells of agents y position ghosts
 }params;

typedef struct FDFS
{
 vector<DOUB> intensity;                  // tracer intensity
 vector<DOUB> intensity_coupler;          // tracer intensity on agent grid (position of pressur nodes)
 vector<DOUB> intensity_back_coupler;     // array holding values from the agent-dynamics level (forcing of advection diffusion system, food)
 vector<DOUB> intensity_back_coupler2;    // array holding values from the agent-dynamics level (forcing of advection diffusion system, food)
 vector<DOUB> intensity_sig;              // signaling trancer intensity
 vector<DOUB> intensity_sig_coupler;      // signaling trancer intensity on agent grid (position of pressur nodes)
 vector<DOUB> intensity_sig_back_coupler; // array holding values from the agent-dynamics level (forcing of advection diffusion system, signaling tracer)
 vector<DOUB> intensity_sig_back_coupler2;// array holding values from the agent-dynamics level only printing
 vector<DOUB> gridX;                      // position of grid points X in Agent domain (grid without halo cells)
 vector<DOUB> gridY;                      // position of grid points Y in Agent domain (grid without halo cells)
 vector<DOUB> PrgridX;                    // position of grid points X in CFD simulation for pressur (including halo cells for boundaries)
 vector<DOUB> PrgridY;                    // position of grid points Y in CFD simulation for pressur (including halo cells for boundaries)
 vector<DOUB> UX;                         // fluid velocity in x direction
 vector<DOUB> UY;                         // fluid velocity in y direction
 vector<DOUB> UX_coupler;                 // fluid velocity in x direction on agent grid (positions of pressur nodes)
 vector<DOUB> UY_coupler;                 // fluid velocity in y direction on agent grid (positions of pressur nodes)
 vector<DOUB> UZ;                         // fluid velocity in z direction TODO: implement for 3D
 vector<DOUB> Pr;                         // pressur of fluid
 vector<DOUB> Pri;                        // intermediate pressur of fluid (for Heuns method in time stepping)
 vector<DOUB> Pri2;                       // intermediate pressur of fluid
 vector<DOUB> Pr_coupler;                 // pressur of fluid on agent grid on agent grid (position of pressur nodes)
 vector<DOUB> vorticity;                  // vorticity of fluid
 vector<DOUB> vorticitygradX;             // vorticity gradX of fluid
 vector<DOUB> vorticitygradY;             // vorticity gradY of fluid
 vector<DOUB> LEVgradX;                   // X large eddy viscosity gradient
 vector<DOUB> LEVgradY;                   // Y
 vector<DOUB> vorticity_coupler;          // vorticity of fluid on agent grid
 vector<DOUB> vorticitygradX_coupler;     // vorticity gradX of fluid on agent grid
 vector<DOUB> vorticitygradY_coupler;     // vorticity gradY of fluid on agent grid
 vector<DOUB> LEVgradX_coupler;           // X large eddy viscosity gradient
 vector<DOUB> LEVgradY_coupler;           // Y
 vector<DOUB> divergence;                 // divergence of fluid
 vector<DOUB> divergence_coupler;         // divergence of fluid on agent grid
 vector<DOUB> TrgradX;                    // X gradient of tracer on CFD grid
 vector<DOUB> TrgradY;                    // Y gradient of tracer on CFD grid
 vector<DOUB> TrgradX_coupler;            // X gradient of tracer on agent grid
 vector<DOUB> TrgradY_coupler;            // Y gradient of tracer on agent grid
 vector<DOUB> Tr_Sig_gradX;               // X gradient of signal tracer on CFD grid
 vector<DOUB> Tr_Sig_gradY;               // Y gradient of signal tracer on CFD grid
 vector<DOUB> Tr_Sig_gradX_coupler;       // X gradient of signal tracer on agent grid
 vector<DOUB> Tr_Sig_gradY_coupler;       // Y gradient of signal tracer on agent grid
 vector<DOUB> intensity_coupler_keep;     // food tracer for time interpolation at old time
 vector<DOUB> intensity_sig_coupler_keep; // signaling trancer for time interpolation at old time
 vector<DOUB> UX_coupler_keep;            // UX for time interpolation
 vector<DOUB> UY_coupler_keep;            // UY for time interpolation
 vector<DOUB> vorticity_coupler_keep;     // vorticity for time interpolation
 vector<DOUB> Pr_coupler_keep;            // pressure for time interpolation
 vector<DOUB> LEV_coupler_keep;           // Large Eddy Viscosity
 vector<DOUB> vorticitygradX_coupler_keep;// X vorticity gradient for time interpolation
 vector<DOUB> vorticitygradY_coupler_keep;// Y
 vector<DOUB> LEVgradX_coupler_keep;  // X large eddy viscosity gradient
 vector<DOUB> LEVgradY_coupler_keep;  // Y
 vector<DOUB> TrgradX_coupler_keep;       // X food tracer gradient for time interpolation
 vector<DOUB> TrgradY_coupler_keep;       // Y
 vector<DOUB> Tr_Sig_gradX_coupler_keep;  // X signal tracer interpolation for time interpolation
 vector<DOUB> Tr_Sig_gradY_coupler_keep;  // Y
 vector<DOUB> intensity_coupler_at;       // food tracer at actual time
 vector<DOUB> intensity_sig_coupler_at;   // signaling tracer at actual time
 vector<DOUB> UX_coupler_at;              // UX
 vector<DOUB> UY_coupler_at;              // UY
 vector<DOUB> vorticity_coupler_at;       // vorticity
 vector<DOUB> Pr_coupler_at;              // pressure
 vector<DOUB> LEV_coupler_at;             // Large Eddy Viscosity
 vector<DOUB> vorticitygradX_coupler_at;  // X vorticity gradient
 vector<DOUB> vorticitygradY_coupler_at;  // Y
 vector<DOUB> LEVgradX_coupler_at;  // X large eddy viscosity gradient
 vector<DOUB> LEVgradY_coupler_at;  // Y
 vector<DOUB> TrgradX_coupler_at;         // X food tracer gradient
 vector<DOUB> TrgradY_coupler_at;         // Y
 vector<DOUB> Tr_Sig_gradX_coupler_at;    // X signal tracer interpolation
 vector<DOUB> Tr_Sig_gradY_coupler_at;    // Y
 vector<DOUB> WindX;                      // Wind field X (on pressure nodes)
 vector<DOUB> WindY;                      // Wind field X (on pressure nodes)
 vector<DOUB> LEV;                        // Large Eddy Viscosity units m^2/s
 vector<DOUB> LEV_coupler;                // Large Eddy Viscosity units m^2/s
}FDFS;

typedef struct parsf
 {
  int Nfx;               // number of grid points in x direction
  int Nfy;               // in y direction
  DOUB fxi;              // domain left
  DOUB fxf;              // domain right
  DOUB fyi;              // domain bottom
  DOUB fyf;              // domain top
  DOUB dx;               // x step
  DOUB dy;               // y step
  DOUB dz;               // z step TODO: implement for 3D
  DOUB dtf;              // time step of fluid
  DOUB dtmax;            // maximal CFD time step
  DOUB dtmin;            // minimal CFD time step
  DOUB CFL;              // CFL criterion
  DOUB CSL;              // Smagorinsky constant
  string image_path;     // path name to image data files
  DOUB kappa;            // smoothing parameter for interpolation of pixels to internal grid
  DOUB rho;              // fluid density (constant)
  DOUB mu;               // viscosity (constant)
  DOUB beta;             // relaxation parameter for pressur solver
  DOUB toll;             // tolerance for pressur solver
  DOUB DTr1;             // diffusion coefficient of Tracer 1
  vector<DOUB> UU;       // U velocity at upper boundary
  vector<DOUB> VU;       // V velocity at upper boundary
  vector<DOUB> UL;       // U velocity at lower boundary
  vector<DOUB> VL;       // V velocity at lower boundary
  vector<DOUB> ULE;      // U velocity at left boundary
  vector<DOUB> VLE;      // V velocity at left boundary
  vector<DOUB> UR;       // U velocity at left boundary
  vector<DOUB> VR;       // V velocity at left boundary
  int width_inlet;       // width of inlet valve
  int width_outlet;      // width of outlet valve
  int pos_inlet;         // center of inlet valve
  int pos_outlet;        // position of outlet valve
  int length_outlet;     // position of outlet valve
  DOUB vel_inlet;        // maximum velocity of inlet flow
  DOUB Npr;              // maximal number of SOR steps adjusted if needed
  DOUB Nsor;             // keeping track of the actual SOR steps
  DOUB DTr_sig;          // Diffusion constant
  string initial_dat;    // path to file that contains initial conditions for CFD fields
  DOUB spread_tracer;    // width parameter of tracer distribution for t=0
  vector<DOUB> boundary; // array that marks coastal cells (all cells of fluid get a 1 all other cells get a 0)
  vector<int> boundary_outlet;     // indicator array that specifies all outlet cells in the main boundary
  vector<int> boundary_inlet;      // indicator array that specifies all inlet cells in the main boundary
  vector<int> boundary_free_slip;  // indicator array that specifies all free-slip cells in the main boundary
  vector<DOUB> boundaryX;// array that marks coastal cells X position
  vector<DOUB> boundaryY;// array that marks coastal cells Y position
  string coast_name;     // path name to file that contains the coastal map
  vector<int> boundary_mapper_UU;   // maps the index of the boundary vector entry for UU to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_mapper_VU;   // maps the index of the boundary vector entry for VU to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_mapper_UL;   // maps the index of the boundary vector entry for UL to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_mapper_VL;   // maps the index of the boundary vector entry for VL to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_mapper_ULE;  // maps the index of the boundary vector entry for ULE to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_mapper_VLE;  // maps the index of the boundary vector entry for VLE to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_mapper_UR;   // maps the index of the boundary vector entry for UR to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_mapper_VR;   // maps the index of the boundary vector entry for VR to the position in the index positions in the grids of the pressure and UX,UY.
  vector<int> boundary_outlet_UU;   // indicates if UU belongs to an outlet cell
  vector<int> boundary_outlet_VU;   // indicates if VU belongs to an outlet cell
  vector<int> boundary_outlet_UL;   // indicates if UL belongs to an outlet cell
  vector<int> boundary_outlet_VL;   // indicates if VL belongs to an outlet cell
  vector<int> boundary_outlet_ULE;  // indicates if ULE belongs to an outlet cell
  vector<int> boundary_outlet_VLE;  // indicates if VLE belongs to an outlet cell
  vector<int> boundary_outlet_UR;   // indicates if UR belongs to an outlet cell
  vector<int> boundary_outlet_VR;   // indicates if VR belongs to an outlet cell
  vector<int> boundary_inlet_UU;    // indicates if UU belongs to an inlet cell
  vector<int> boundary_inlet_VU;    // indicates if VU belongs to an inlet cell
  vector<int> boundary_inlet_UL;    // indicates if UL belongs to an inlet cell
  vector<int> boundary_inlet_VL;    // indicates if VL belongs to an inlet cell
  vector<int> boundary_inlet_ULE;   // indicates if ULE belongs to an inlet cell
  vector<int> boundary_inlet_VLE;   // indicates if VLE belongs to an inlet cell
  vector<int> boundary_inlet_UR;    // indicates if UR belongs to an inlet cell
  vector<int> boundary_inlet_VR;    // indicates if VR belongs to an inlet cell
  vector<DOUB> Calc_UX;             // indactes if UX staggered grid position is in boundary or not (switching on and off of calculation process)
  vector<DOUB> Calc_UY;             // indactes if UY staggered grid position is in boundary or not (switching on and off of calculation process)
  vector<DOUB> Calc_Pr;             // indactes if Pressure position is in boundary or not (switching on and off of calculation process) This vector holds the lapalcian multipliers and is set to zero if a boundary cell is present and specific if corners are encountered
  vector<DOUB> porous;              // indactes porous walls (indicated by red colour in a png)
  vector<DOUB> Calc_porousX;        // map of porous wall cells x velocity
  vector<DOUB> Calc_porousY;        // map of porous wall cells y velocity
  vector<DOUB> Calc_Tr;             // indactes if tracer position is in boundary or not (switching on and off of calculation process)
  vector<vector<int>> boundary_mapper_TrU;  // maps the index of the domain vector entry or a tracer to the index in the grid that is an adjacent boundary point
  vector<vector<int>> boundary_mapper_TrL;  // maps the index of the domain vector entry or a tracer to the index in the grid that is an adjacent boundary point
  vector<vector<int>> boundary_mapper_TrLE; // maps the index of the domain vector entry or a tracer to the index in the grid that is an adjacent boundary point
  vector<vector<int>> boundary_mapper_TrR;  // maps the index of the domain vector entry or a tracer to the index in the grid that is an adjacent boundary point
  vector<vector<int>> Pr_mapper;            // maps the index of pressure cells ann all necessary calculation indecees to the index of the pressure vector itself
  vector<vector<int>> Pr_mapper_even;       // maps the even index of pressure cells and all necessary calculation indecees to the index of the pressure vector itself
  vector<vector<int>> Pr_mapper_odd;        // maps the even index of pressure cells and all necessary calculation indecees to the index of the pressure vector itself
  DOUB DIVMAX;                              // maximum divergence allowed before maximum number of pressure steps is increased
  DOUB NPRMAX;                              // maximum number of allowed pressure steps
  DOUB damp;                                // damping constant of the outlet velocity oscillations to enforce straight outflow
  DOUB Vwind;                               // Wind speed
  DOUB thwind;                              // Wind direction
  DOUB Corriolis;                           // Corriolis parameter
  string columns_name;                      // path to file that specifies the output data to be plotted to a file for CFD, agents and ghosts
  DOUB reaction_tracer;                     // reaction rate food tracer
  DOUB reaction_tracer_sig;                 // reaction rate signaling tracer
  DOUB GravX;                               // gravitation external force X
  DOUB GravY;                               // gravitation external force Y
  DOUB Cpor1X;                              // linear porous response X
  DOUB Cpor1Y;                              // linear porous response Y
  DOUB Cpor2X;                              // quadratic porous response X
  DOUB Cpor2Y;                              // quadratic porous response Y  
 }parsf;
