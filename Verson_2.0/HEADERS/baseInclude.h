// ### BASE HEADER FOR JELLFISH MODEL                 ### //
// ###                                                ### //
// ### AUTHOR: Dr. ERIK GENGEL, Tel Aviv University   ### //
// ###                                                ### //

//#include <mpi.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <cmath>
#include <sstream>
#include <ostream>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <random>
#include <omp.h>   //for parallel computing

using namespace std;

//#define CFD_field_from_image           //enables interpolation of external field onto specified grid
//#define CFD_field_from_image_dynamic //enables interpolation of external field onto specified grid in each time step of simulation
#define CFD_field_from_integrator    //enables dynamic simulation of field in code using CFD solver
//#define Euler //if defined first order in time stepping of CFD fields, if not defined 2nd order Heun scheme for time stepping
#define tracer_distribution  //initializes a tracer distribution instead of a tracer injection (otherwise)
//#define Strogatz //sets the direction-orientation function to unity, giving essentially back the Strogatz model of swarming
//#define swarmalator // if defined, the attraction depends on the phase difference
 #define pressure_outlet //defines a setting with one/multiple velocity inlets and one pressure outlet
#define complex_boundary // enables to simulate fluid flow in a complex boundary read at initialization of the flow (influences mainly how the bounadry conditions are set)
//#define flow_damper // defines artificial bounding in the cfd solver (use only advisable if the flow shows some kind of instability to be checked)
//#define free_slip_right // enables a free slip boundary condition at the right boundary of the flow (v velocity is free to move, u velocity is zero)
//#define seasons //to enables square oscillating function mimicing seasons in which the swarm groups together
 #define buoyancy_jelly // adds a buoyancy term to the y component of jellyfish dynamics
//#define cavity_upper_wall //defines a single moving wall cavity if not defined the left and right walls are moving in the same direction
//#define averageUV_velocity //calculate the average velocities of U and V in y and x direction for each time step
//#define inflow //inlet vector field output
//#define outflow //outflow and inflow comparison output
 #define FDF_out //output of fluid fields
 //#define gnuplot // enables plotting at run time, the program needs to be called with '| gnuplot' in the end to pipe output to gnuplot
 //#define output_ghosts // output of ghost positions and array positions of the vortex quadrupol
  #define init_read // starting the simulation with pre-existing CFD data in a file
  //#define save_init_cond // saving the initial conditions for later start from file
  #define CFL_cr //enables CFL criterion
  //#define sequential_time //uses sequential time calculator for process time
  //#define Jelly_add //enables adding and deleting of jellyfish
  #define near_field_interaction_static //enables positional near-field interaction of jellyfish (non oscillatory part)
  #define near_field_interaction_oscillating //enables positional near-field interaction of jellyfish (oscillatory part)
  //#define adjust_diffusion_coeff //enables adjustion of Diffusion coefficient according to given grid spacing.
  //#define wake_interaction // enables interaction jellyfish agents with wakes (orientation and phase)
  //#define error_check //enables step-wise checking of numerical overflows.
  //#define RK_dt //enables RK time step control
