import numpy as np
import sys
import copy
from PIL import Image

namein=sys.argv[1]
nameout=sys.argv[2]

img=Image.open(namein).convert('L')
img2 =Image.open(namein).convert('RGB')
imgr, imgg, imgb = img2.split()

imgdat = np.asarray(img)
imgdatr = np.asarray(imgr)
imgdatg = np.asarray(imgg)
imgdatb = np.asarray(imgb)

f=open(nameout, "w")

Nx=75 # number of cells in domain x
Ny=71 # y
Lx=10.0
Ly=10.0
dx=(2.0*Lx)/(Nx)
dy=(2.0*Ly)/(Ny)

Coast=0
Inlet=0
Outlet=0
porous=0

#NOTE: COMMENT IN WHEN REAL COAST IS USED
maxA=-10.0
for k in range(Nx+2):
    for l in range(Ny+2):
        if imgdat[Ny+1-l][k]>maxA:
            maxA = copy.deepcopy(imgdat[Ny+1-l][k])
maxAr=-10.0
maxAb=-10.0
maxAg=-10.0
fr=open("red.dat", "w")
for k in range(Nx+2):
    for l in range(Ny+2):
        fr.write(str(k) + " " + str(l) + " " + str(imgdatr[Ny+1-l][k]) + " " + str(imgdatg[Ny+1-l][k]) + " " + str(imgdatb[Ny+1-l][k]) +"\n")
        if imgdatr[Ny+1-l][k]>maxAr:
            maxAr = copy.deepcopy(imgdatr[Ny+1-l][k])
        if imgdatg[Ny+1-l][k]>maxAg:
            maxAg = copy.deepcopy(imgdatg[Ny+1-l][k])
        if imgdatb[Ny+1-l][k]>maxAb:
            maxAb = copy.deepcopy(imgdatb[Ny+1-l][k])
print("maximum intensity= " + str(maxA) + "maximum intensity outlet " + str(maxAr))
fr.close()

#parameters to plot the "JAM" letters
a=-8
b=-25

for k in range(Nx+2):
    for l in range(Ny+2):
        if((k<1 or k>Nx or l<1 or l>Ny or imgdat[Ny+1-l][k]/maxA<0.88) and not(imgdatr[Ny+1-l][k]/maxAr-imgdatb[Ny+1-l][k]/maxAb>0.1)):      
            Coast = 1
        else:
            Coast = 0
        if (imgdatr[Ny+1-l][k]/maxAr-imgdatb[Ny+1-l][k]/maxAb>0.1):
            porous = 1 
        else:
            porous = 0
        if(abs(k-51)<=600 and l==0):#21, 9; 31, 5
            Outlet=1
        else:
            Outlet=0
        if(abs(k-51)<=600 and l==Ny+1):#21, 9; 31, 5
            Inlet=1
        else:
            Inlet=0
            
        f.write(str(-0.5*dx - Lx + (float(k))*dx) + " " + str(-0.5*dy - Ly - float(Ny+1-l)*dy) + " " + str(Coast) + " " + str(Inlet) + " " + str(Outlet) + " " + str(porous) + "\n")
f.close()
